;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v5.0.1 *
;* Date/Time created: Mon Dec 20 13:04:24 2021                 *
;***************************************************************
	.compiler_opts --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --quiet --silicon_version=28 --symdebug:coff 
FP	.set	XAR2
	.file	"C:\CCStudio_v3.3\MyProjects\Calibracion2021_3\PTC5F_main.c"
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger5+0,32
	.field  	0,16			; _logger5 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger6+0,32
	.field  	0,16			; _logger6 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger3+0,32
	.field  	0,16			; _logger3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger4+0,32
	.field  	0,16			; _logger4 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger2+0,32
	.field  	0,16			; _logger2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_clogger+0,32
	.field  	0,16			; _clogger @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_cloggaux+0,32
	.field  	0,16			; _cloggaux @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger8+0,32
	.field  	0,16			; _logger8 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_loggaux+0,32
	.field  	0,16			; _loggaux @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_timeout+0,32
	.field  	1,16			; _timeout @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_stop+0,32
	.field  	0,16			; _stop @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger1+0,32
	.field  	0,16			; _logger1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger7+0,32
	.field  	0,16			; _logger7 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_dsampled+0,32
	.field  	0,16			; _dsampled @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_logger+0,32
	.field  	0,16			; _logger @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_auxeqeptmr+0,32
	.field  	0,16			; _auxeqeptmr @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_stvel+0,32
	.field  	2,16			; _stvel @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_imcntr+0,32
	.field  	0,16			; _imcntr @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_chgflag+0,32
	.field  	0,16			; _chgflag @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_secntr+0,32
	.field  	0,16			; _secntr @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_wm_km1+0,32
	.xfloat	0.00000000000000000000e+00		; _wm_km1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_velocfactor+0,32
	.xfloat	1.88495562500000000000e+05		; _velocfactor @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_wm_kp1min+0,32
	.xfloat	0.00000000000000000000e+00		; _wm_kp1min @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_hdx+0,32
	.xfloat	0.00000000000000000000e+00		; _hdx @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_eqeptmr+0,32
	.xfloat	1.00000000000000000000e+00		; _eqeptmr @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_velocfactor2+0,32
	.xfloat	5.89048632812500000000e+03		; _velocfactor2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_metodo+0,32
	.xfloat	2.00000000000000000000e+00		; _metodo @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_hvfactor+0,32
	.xfloat	3.14159989356994628906e+00		; _hvfactor @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_wm_kp1max+0,32
	.xfloat	0.00000000000000000000e+00		; _wm_kp1max @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_KPIW+0,32
	.xfloat	6.00000000000000000000e+00		; _KPIW @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_KPW+0,32
	.xfloat	7.99999982118606567383e-02		; _KPW @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mstart2+0,32
	.xfloat	0.00000000000000000000e+00		; _mstart2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_mstart+0,32
	.xfloat	0.00000000000000000000e+00		; _mstart @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_contfallo+0,32
	.xfloat	0.00000000000000000000e+00		; _contfallo @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_ThetaI+0,32
	.xfloat	0.00000000000000000000e+00		; _ThetaI @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_Imax+0,32
	.xfloat	0.00000000000000000000e+00		; _Imax @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_wm_d3+0,32
	.xfloat	0.00000000000000000000e+00		; _wm_d3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_w2+0,32
	.xfloat	0.00000000000000000000e+00		; _w2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_w1+0,32
	.xfloat	0.00000000000000000000e+00		; _w1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_CCarga+0,32
	.xfloat	4.88000011444091796875e+00		; _CCarga @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_uvdiv+0,32
	.field  	0,16			; _uvdiv[0] @ 0
	.field  	1,16			; _uvdiv[1] @ 16
	.field  	2,16			; _uvdiv[2] @ 32
	.field  	3,16			; _uvdiv[3] @ 48
	.field  	4,16			; _uvdiv[4] @ 64
$C$IR_1:	.set	5

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_2,16
	.field  	_factorwm+0,32
	.xfloat	9.42477812500000000000e+04		; _factorwm[0] @ 0
	.xfloat	1.88495562500000000000e+05		; _factorwm[1] @ 32
	.xfloat	3.76991125000000000000e+05		; _factorwm[2] @ 64
	.xfloat	7.53982250000000000000e+05		; _factorwm[3] @ 96
	.xfloat	1.50800000000000000000e+06		; _factorwm[4] @ 128
$C$IR_2:	.set	10

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_3,16
	.field  	_vsx+0,32
	.xfloat	0.00000000000000000000e+00		; _vsx[0] @ 0
	.xfloat	-3.23610007762908935547e-01		; _vsx[1] @ 32
	.xfloat	1.23609997332096099854e-01		; _vsx[2] @ 64
	.xfloat	-2.00000002980232238770e-01		; _vsx[3] @ 96
	.xfloat	1.23609997332096099854e-01		; _vsx[4] @ 128
	.xfloat	-2.00000002980232238770e-01		; _vsx[5] @ 160
	.xfloat	2.47214004397392272949e-01		; _vsx[6] @ 192
	.xfloat	-7.63930007815361022949e-02		; _vsx[7] @ 224
	.xfloat	-3.23610007762908935547e-01		; _vsx[8] @ 256
	.xfloat	-6.47210001945495605469e-01		; _vsx[9] @ 288
	.xfloat	-2.00000002980232238770e-01		; _vsx[10] @ 320
	.xfloat	-5.23609995841979980469e-01		; _vsx[11] @ 352
	.xfloat	-2.00000002980232238770e-01		; _vsx[12] @ 384
	.xfloat	-5.23609995841979980469e-01		; _vsx[13] @ 416
	.xfloat	-7.63930007815361022949e-02		; _vsx[14] @ 448
	.xfloat	-4.00000005960464477539e-01		; _vsx[15] @ 480
	.xfloat	4.00000005960464477539e-01		; _vsx[16] @ 512
	.xfloat	7.63930007815361022949e-02		; _vsx[17] @ 544
	.xfloat	5.23609995841979980469e-01		; _vsx[18] @ 576
	.xfloat	2.00000002980232238770e-01		; _vsx[19] @ 608
	.xfloat	5.23609995841979980469e-01		; _vsx[20] @ 640
	.xfloat	2.00000002980232238770e-01		; _vsx[21] @ 672
	.xfloat	6.47213995456695556641e-01		; _vsx[22] @ 704
	.xfloat	3.23610007762908935547e-01		; _vsx[23] @ 736
	.xfloat	7.63930007815361022949e-02		; _vsx[24] @ 768
	.xfloat	-2.47214004397392272949e-01		; _vsx[25] @ 800
	.xfloat	2.00000002980232238770e-01		; _vsx[26] @ 832
	.xfloat	-1.23609997332096099854e-01		; _vsx[27] @ 864
	.xfloat	2.00000002980232238770e-01		; _vsx[28] @ 896
	.xfloat	-1.23609997332096099854e-01		; _vsx[29] @ 928
	.xfloat	3.23610007762908935547e-01		; _vsx[30] @ 960
	.xfloat	0.00000000000000000000e+00		; _vsx[31] @ 992
$C$IR_3:	.set	64

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_4,16
	.field  	_vsb+0,32
	.xfloat	0.00000000000000000000e+00		; _vsb[0] @ 0
	.xfloat	-3.80419999361038208008e-01		; _vsb[1] @ 32
	.xfloat	-2.35109999775886535645e-01		; _vsb[2] @ 64
	.xfloat	-6.15540027618408203125e-01		; _vsb[3] @ 96
	.xfloat	2.35109999775886535645e-01		; _vsb[4] @ 128
	.xfloat	-1.45309999585151672363e-01		; _vsb[5] @ 160
	.xfloat	0.00000000000000000000e+00		; _vsb[6] @ 192
	.xfloat	-3.80419999361038208008e-01		; _vsb[7] @ 224
	.xfloat	3.80419999361038208008e-01		; _vsb[8] @ 256
	.xfloat	0.00000000000000000000e+00		; _vsb[9] @ 288
	.xfloat	1.45309999585151672363e-01		; _vsb[10] @ 320
	.xfloat	-2.35113993287086486816e-01		; _vsb[11] @ 352
	.xfloat	6.15540027618408203125e-01		; _vsb[12] @ 384
	.xfloat	2.35109999775886535645e-01		; _vsb[13] @ 416
	.xfloat	3.80419999361038208008e-01		; _vsb[14] @ 448
	.xfloat	0.00000000000000000000e+00		; _vsb[15] @ 480
	.xfloat	0.00000000000000000000e+00		; _vsb[16] @ 512
	.xfloat	-3.80419999361038208008e-01		; _vsb[17] @ 544
	.xfloat	-2.35109999775886535645e-01		; _vsb[18] @ 576
	.xfloat	-6.15540027618408203125e-01		; _vsb[19] @ 608
	.xfloat	2.35113993287086486816e-01		; _vsb[20] @ 640
	.xfloat	-1.45309999585151672363e-01		; _vsb[21] @ 672
	.xfloat	0.00000000000000000000e+00		; _vsb[22] @ 704
	.xfloat	-3.80421996116638183594e-01		; _vsb[23] @ 736
	.xfloat	3.80419999361038208008e-01		; _vsb[24] @ 768
	.xfloat	0.00000000000000000000e+00		; _vsb[25] @ 800
	.xfloat	1.45309999585151672363e-01		; _vsb[26] @ 832
	.xfloat	-2.35113993287086486816e-01		; _vsb[27] @ 864
	.xfloat	6.15540027618408203125e-01		; _vsb[28] @ 896
	.xfloat	2.35113993287086486816e-01		; _vsb[29] @ 928
	.xfloat	3.80423009395599365234e-01		; _vsb[30] @ 960
	.xfloat	0.00000000000000000000e+00		; _vsb[31] @ 992
$C$IR_4:	.set	64

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_5,16
	.field  	_vsy+0,32
	.xfloat	0.00000000000000000000e+00		; _vsy[0] @ 0
	.xfloat	-2.35109999775886535645e-01		; _vsy[1] @ 32
	.xfloat	3.80419999361038208008e-01		; _vsy[2] @ 64
	.xfloat	1.45309999585151672363e-01		; _vsy[3] @ 96
	.xfloat	-3.80419999361038208008e-01		; _vsy[4] @ 128
	.xfloat	-6.15540027618408203125e-01		; _vsy[5] @ 160
	.xfloat	0.00000000000000000000e+00		; _vsy[6] @ 192
	.xfloat	-2.35113993287086486816e-01		; _vsy[7] @ 224
	.xfloat	2.35113993287086486816e-01		; _vsy[8] @ 256
	.xfloat	0.00000000000000000000e+00		; _vsy[9] @ 288
	.xfloat	6.15540027618408203125e-01		; _vsy[10] @ 320
	.xfloat	3.80419999361038208008e-01		; _vsy[11] @ 352
	.xfloat	-1.45309999585151672363e-01		; _vsy[12] @ 384
	.xfloat	-3.80419999361038208008e-01		; _vsy[13] @ 416
	.xfloat	2.35113993287086486816e-01		; _vsy[14] @ 448
	.xfloat	0.00000000000000000000e+00		; _vsy[15] @ 480
	.xfloat	0.00000000000000000000e+00		; _vsy[16] @ 512
	.xfloat	-2.35114097595214843750e-01		; _vsy[17] @ 544
	.xfloat	3.80423009395599365234e-01		; _vsy[18] @ 576
	.xfloat	1.45309999585151672363e-01		; _vsy[19] @ 608
	.xfloat	-3.80423009395599365234e-01		; _vsy[20] @ 640
	.xfloat	-6.15540027618408203125e-01		; _vsy[21] @ 672
	.xfloat	0.00000000000000000000e+00		; _vsy[22] @ 704
	.xfloat	-2.35113993287086486816e-01		; _vsy[23] @ 736
	.xfloat	2.35113993287086486816e-01		; _vsy[24] @ 768
	.xfloat	0.00000000000000000000e+00		; _vsy[25] @ 800
	.xfloat	6.15540027618408203125e-01		; _vsy[26] @ 832
	.xfloat	3.80423009395599365234e-01		; _vsy[27] @ 864
	.xfloat	-1.45309999585151672363e-01		; _vsy[28] @ 896
	.xfloat	-3.80423009395599365234e-01		; _vsy[29] @ 928
	.xfloat	2.35113993287086486816e-01		; _vsy[30] @ 960
	.xfloat	0.00000000000000000000e+00		; _vsy[31] @ 992
$C$IR_5:	.set	64

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_6,16
	.field  	_vsa+0,32
	.xfloat	0.00000000000000000000e+00		; _vsa[0] @ 0
	.xfloat	1.23609997332096099854e-01		; _vsa[1] @ 32
	.xfloat	-3.23610007762908935547e-01		; _vsa[2] @ 64
	.xfloat	-2.00000002980232238770e-01		; _vsa[3] @ 96
	.xfloat	-3.23610007762908935547e-01		; _vsa[4] @ 128
	.xfloat	-2.00000002980232238770e-01		; _vsa[5] @ 160
	.xfloat	-6.47210001945495605469e-01		; _vsa[6] @ 192
	.xfloat	-5.23609995841979980469e-01		; _vsa[7] @ 224
	.xfloat	1.23609997332096099854e-01		; _vsa[8] @ 256
	.xfloat	2.47209995985031127930e-01		; _vsa[9] @ 288
	.xfloat	-2.00000002980232238770e-01		; _vsa[10] @ 320
	.xfloat	-7.63999968767166137695e-02		; _vsa[11] @ 352
	.xfloat	-2.00000002980232238770e-01		; _vsa[12] @ 384
	.xfloat	-7.63999968767166137695e-02		; _vsa[13] @ 416
	.xfloat	-5.23609995841979980469e-01		; _vsa[14] @ 448
	.xfloat	-4.00000005960464477539e-01		; _vsa[15] @ 480
	.xfloat	4.00000005960464477539e-01		; _vsa[16] @ 512
	.xfloat	5.23609995841979980469e-01		; _vsa[17] @ 544
	.xfloat	7.63999968767166137695e-02		; _vsa[18] @ 576
	.xfloat	2.00000002980232238770e-01		; _vsa[19] @ 608
	.xfloat	7.63999968767166137695e-02		; _vsa[20] @ 640
	.xfloat	2.00000002980232238770e-01		; _vsa[21] @ 672
	.xfloat	-2.47209995985031127930e-01		; _vsa[22] @ 704
	.xfloat	-1.23609997332096099854e-01		; _vsa[23] @ 736
	.xfloat	5.23609995841979980469e-01		; _vsa[24] @ 768
	.xfloat	6.47210001945495605469e-01		; _vsa[25] @ 800
	.xfloat	2.00000002980232238770e-01		; _vsa[26] @ 832
	.xfloat	3.23610007762908935547e-01		; _vsa[27] @ 864
	.xfloat	2.00000002980232238770e-01		; _vsa[28] @ 896
	.xfloat	3.23610007762908935547e-01		; _vsa[29] @ 928
	.xfloat	-1.23609997332096099854e-01		; _vsa[30] @ 960
	.xfloat	0.00000000000000000000e+00		; _vsa[31] @ 992
$C$IR_6:	.set	64

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_7,16
	.field  	_SCMP+0,32
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][0] @ 0
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][1] @ 32
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][2] @ 64
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][3] @ 96
	.xfloat	0.00000000000000000000e+00		; _SCMP[0][4] @ 128
	.xfloat	0.00000000000000000000e+00		; _SCMP[1][0] @ 160
	.xfloat	0.00000000000000000000e+00		; _SCMP[1][1] @ 192
	.xfloat	0.00000000000000000000e+00		; _SCMP[1][2] @ 224
	.xfloat	0.00000000000000000000e+00		; _SCMP[1][3] @ 256
	.xfloat	1.00000000000000000000e+00		; _SCMP[1][4] @ 288
	.xfloat	0.00000000000000000000e+00		; _SCMP[2][0] @ 320
	.xfloat	0.00000000000000000000e+00		; _SCMP[2][1] @ 352
	.xfloat	0.00000000000000000000e+00		; _SCMP[2][2] @ 384
	.xfloat	1.00000000000000000000e+00		; _SCMP[2][3] @ 416
	.xfloat	0.00000000000000000000e+00		; _SCMP[2][4] @ 448
	.xfloat	0.00000000000000000000e+00		; _SCMP[3][0] @ 480
	.xfloat	0.00000000000000000000e+00		; _SCMP[3][1] @ 512
	.xfloat	0.00000000000000000000e+00		; _SCMP[3][2] @ 544
	.xfloat	1.00000000000000000000e+00		; _SCMP[3][3] @ 576
	.xfloat	1.00000000000000000000e+00		; _SCMP[3][4] @ 608
	.xfloat	0.00000000000000000000e+00		; _SCMP[4][0] @ 640
	.xfloat	0.00000000000000000000e+00		; _SCMP[4][1] @ 672
	.xfloat	1.00000000000000000000e+00		; _SCMP[4][2] @ 704
	.xfloat	0.00000000000000000000e+00		; _SCMP[4][3] @ 736
	.xfloat	0.00000000000000000000e+00		; _SCMP[4][4] @ 768
	.xfloat	0.00000000000000000000e+00		; _SCMP[5][0] @ 800
	.xfloat	0.00000000000000000000e+00		; _SCMP[5][1] @ 832
	.xfloat	1.00000000000000000000e+00		; _SCMP[5][2] @ 864
	.xfloat	0.00000000000000000000e+00		; _SCMP[5][3] @ 896
	.xfloat	1.00000000000000000000e+00		; _SCMP[5][4] @ 928
	.xfloat	0.00000000000000000000e+00		; _SCMP[6][0] @ 960
	.xfloat	0.00000000000000000000e+00		; _SCMP[6][1] @ 992
	.xfloat	1.00000000000000000000e+00		; _SCMP[6][2] @ 1024
	.xfloat	1.00000000000000000000e+00		; _SCMP[6][3] @ 1056
	.xfloat	0.00000000000000000000e+00		; _SCMP[6][4] @ 1088
	.xfloat	0.00000000000000000000e+00		; _SCMP[7][0] @ 1120
	.xfloat	0.00000000000000000000e+00		; _SCMP[7][1] @ 1152
	.xfloat	1.00000000000000000000e+00		; _SCMP[7][2] @ 1184
	.xfloat	1.00000000000000000000e+00		; _SCMP[7][3] @ 1216
	.xfloat	1.00000000000000000000e+00		; _SCMP[7][4] @ 1248
	.xfloat	0.00000000000000000000e+00		; _SCMP[8][0] @ 1280
	.xfloat	1.00000000000000000000e+00		; _SCMP[8][1] @ 1312
	.xfloat	0.00000000000000000000e+00		; _SCMP[8][2] @ 1344
	.xfloat	0.00000000000000000000e+00		; _SCMP[8][3] @ 1376
	.xfloat	0.00000000000000000000e+00		; _SCMP[8][4] @ 1408
	.xfloat	0.00000000000000000000e+00		; _SCMP[9][0] @ 1440
	.xfloat	1.00000000000000000000e+00		; _SCMP[9][1] @ 1472
	.xfloat	0.00000000000000000000e+00		; _SCMP[9][2] @ 1504
	.xfloat	0.00000000000000000000e+00		; _SCMP[9][3] @ 1536
	.xfloat	1.00000000000000000000e+00		; _SCMP[9][4] @ 1568
	.xfloat	0.00000000000000000000e+00		; _SCMP[10][0] @ 1600
	.xfloat	1.00000000000000000000e+00		; _SCMP[10][1] @ 1632
	.xfloat	0.00000000000000000000e+00		; _SCMP[10][2] @ 1664
	.xfloat	1.00000000000000000000e+00		; _SCMP[10][3] @ 1696
	.xfloat	0.00000000000000000000e+00		; _SCMP[10][4] @ 1728
	.xfloat	0.00000000000000000000e+00		; _SCMP[11][0] @ 1760
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][1] @ 1792
	.xfloat	0.00000000000000000000e+00		; _SCMP[11][2] @ 1824
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][3] @ 1856
	.xfloat	1.00000000000000000000e+00		; _SCMP[11][4] @ 1888
	.xfloat	0.00000000000000000000e+00		; _SCMP[12][0] @ 1920
	.xfloat	1.00000000000000000000e+00		; _SCMP[12][1] @ 1952
	.xfloat	1.00000000000000000000e+00		; _SCMP[12][2] @ 1984
	.xfloat	0.00000000000000000000e+00		; _SCMP[12][3] @ 2016
	.xfloat	0.00000000000000000000e+00		; _SCMP[12][4] @ 2048
	.xfloat	0.00000000000000000000e+00		; _SCMP[13][0] @ 2080
	.xfloat	1.00000000000000000000e+00		; _SCMP[13][1] @ 2112
	.xfloat	1.00000000000000000000e+00		; _SCMP[13][2] @ 2144
	.xfloat	0.00000000000000000000e+00		; _SCMP[13][3] @ 2176
	.xfloat	1.00000000000000000000e+00		; _SCMP[13][4] @ 2208
	.xfloat	0.00000000000000000000e+00		; _SCMP[14][0] @ 2240
	.xfloat	1.00000000000000000000e+00		; _SCMP[14][1] @ 2272
	.xfloat	1.00000000000000000000e+00		; _SCMP[14][2] @ 2304
	.xfloat	1.00000000000000000000e+00		; _SCMP[14][3] @ 2336
	.xfloat	0.00000000000000000000e+00		; _SCMP[14][4] @ 2368
	.xfloat	0.00000000000000000000e+00		; _SCMP[15][0] @ 2400
	.xfloat	1.00000000000000000000e+00		; _SCMP[15][1] @ 2432
	.xfloat	1.00000000000000000000e+00		; _SCMP[15][2] @ 2464
	.xfloat	1.00000000000000000000e+00		; _SCMP[15][3] @ 2496
	.xfloat	1.00000000000000000000e+00		; _SCMP[15][4] @ 2528
	.xfloat	1.00000000000000000000e+00		; _SCMP[16][0] @ 2560
	.xfloat	0.00000000000000000000e+00		; _SCMP[16][1] @ 2592
	.xfloat	0.00000000000000000000e+00		; _SCMP[16][2] @ 2624
	.xfloat	0.00000000000000000000e+00		; _SCMP[16][3] @ 2656
	.xfloat	0.00000000000000000000e+00		; _SCMP[16][4] @ 2688
	.xfloat	1.00000000000000000000e+00		; _SCMP[17][0] @ 2720
	.xfloat	0.00000000000000000000e+00		; _SCMP[17][1] @ 2752
	.xfloat	0.00000000000000000000e+00		; _SCMP[17][2] @ 2784
	.xfloat	0.00000000000000000000e+00		; _SCMP[17][3] @ 2816
	.xfloat	1.00000000000000000000e+00		; _SCMP[17][4] @ 2848
	.xfloat	1.00000000000000000000e+00		; _SCMP[18][0] @ 2880
	.xfloat	0.00000000000000000000e+00		; _SCMP[18][1] @ 2912
	.xfloat	0.00000000000000000000e+00		; _SCMP[18][2] @ 2944
	.xfloat	1.00000000000000000000e+00		; _SCMP[18][3] @ 2976
	.xfloat	0.00000000000000000000e+00		; _SCMP[18][4] @ 3008
	.xfloat	1.00000000000000000000e+00		; _SCMP[19][0] @ 3040
	.xfloat	0.00000000000000000000e+00		; _SCMP[19][1] @ 3072
	.xfloat	0.00000000000000000000e+00		; _SCMP[19][2] @ 3104
	.xfloat	1.00000000000000000000e+00		; _SCMP[19][3] @ 3136
	.xfloat	1.00000000000000000000e+00		; _SCMP[19][4] @ 3168
	.xfloat	1.00000000000000000000e+00		; _SCMP[20][0] @ 3200
	.xfloat	0.00000000000000000000e+00		; _SCMP[20][1] @ 3232
	.xfloat	1.00000000000000000000e+00		; _SCMP[20][2] @ 3264
	.xfloat	0.00000000000000000000e+00		; _SCMP[20][3] @ 3296
	.xfloat	0.00000000000000000000e+00		; _SCMP[20][4] @ 3328
	.xfloat	1.00000000000000000000e+00		; _SCMP[21][0] @ 3360
	.xfloat	0.00000000000000000000e+00		; _SCMP[21][1] @ 3392
	.xfloat	1.00000000000000000000e+00		; _SCMP[21][2] @ 3424
	.xfloat	0.00000000000000000000e+00		; _SCMP[21][3] @ 3456
	.xfloat	1.00000000000000000000e+00		; _SCMP[21][4] @ 3488
	.xfloat	1.00000000000000000000e+00		; _SCMP[22][0] @ 3520
	.xfloat	0.00000000000000000000e+00		; _SCMP[22][1] @ 3552
	.xfloat	1.00000000000000000000e+00		; _SCMP[22][2] @ 3584
	.xfloat	1.00000000000000000000e+00		; _SCMP[22][3] @ 3616
	.xfloat	0.00000000000000000000e+00		; _SCMP[22][4] @ 3648
	.xfloat	1.00000000000000000000e+00		; _SCMP[23][0] @ 3680
	.xfloat	0.00000000000000000000e+00		; _SCMP[23][1] @ 3712
	.xfloat	1.00000000000000000000e+00		; _SCMP[23][2] @ 3744
	.xfloat	1.00000000000000000000e+00		; _SCMP[23][3] @ 3776
	.xfloat	1.00000000000000000000e+00		; _SCMP[23][4] @ 3808
	.xfloat	1.00000000000000000000e+00		; _SCMP[24][0] @ 3840
	.xfloat	1.00000000000000000000e+00		; _SCMP[24][1] @ 3872
	.xfloat	0.00000000000000000000e+00		; _SCMP[24][2] @ 3904
	.xfloat	0.00000000000000000000e+00		; _SCMP[24][3] @ 3936
	.xfloat	0.00000000000000000000e+00		; _SCMP[24][4] @ 3968
	.xfloat	1.00000000000000000000e+00		; _SCMP[25][0] @ 4000
	.xfloat	1.00000000000000000000e+00		; _SCMP[25][1] @ 4032
	.xfloat	0.00000000000000000000e+00		; _SCMP[25][2] @ 4064
	.xfloat	0.00000000000000000000e+00		; _SCMP[25][3] @ 4096
	.xfloat	1.00000000000000000000e+00		; _SCMP[25][4] @ 4128
	.xfloat	1.00000000000000000000e+00		; _SCMP[26][0] @ 4160
	.xfloat	1.00000000000000000000e+00		; _SCMP[26][1] @ 4192
	.xfloat	0.00000000000000000000e+00		; _SCMP[26][2] @ 4224
	.xfloat	1.00000000000000000000e+00		; _SCMP[26][3] @ 4256
	.xfloat	0.00000000000000000000e+00		; _SCMP[26][4] @ 4288
	.xfloat	1.00000000000000000000e+00		; _SCMP[27][0] @ 4320
	.xfloat	1.00000000000000000000e+00		; _SCMP[27][1] @ 4352
	.xfloat	0.00000000000000000000e+00		; _SCMP[27][2] @ 4384
	.xfloat	1.00000000000000000000e+00		; _SCMP[27][3] @ 4416
	.xfloat	1.00000000000000000000e+00		; _SCMP[27][4] @ 4448
	.xfloat	1.00000000000000000000e+00		; _SCMP[28][0] @ 4480
	.xfloat	1.00000000000000000000e+00		; _SCMP[28][1] @ 4512
	.xfloat	1.00000000000000000000e+00		; _SCMP[28][2] @ 4544
	.xfloat	0.00000000000000000000e+00		; _SCMP[28][3] @ 4576
	.xfloat	0.00000000000000000000e+00		; _SCMP[28][4] @ 4608
	.xfloat	1.00000000000000000000e+00		; _SCMP[29][0] @ 4640
	.xfloat	1.00000000000000000000e+00		; _SCMP[29][1] @ 4672
	.xfloat	1.00000000000000000000e+00		; _SCMP[29][2] @ 4704
	.xfloat	0.00000000000000000000e+00		; _SCMP[29][3] @ 4736
	.xfloat	1.00000000000000000000e+00		; _SCMP[29][4] @ 4768
	.xfloat	1.00000000000000000000e+00		; _SCMP[30][0] @ 4800
	.xfloat	1.00000000000000000000e+00		; _SCMP[30][1] @ 4832
	.xfloat	1.00000000000000000000e+00		; _SCMP[30][2] @ 4864
	.xfloat	1.00000000000000000000e+00		; _SCMP[30][3] @ 4896
	.xfloat	0.00000000000000000000e+00		; _SCMP[30][4] @ 4928
	.xfloat	1.00000000000000000000e+00		; _SCMP[31][0] @ 4960
	.xfloat	1.00000000000000000000e+00		; _SCMP[31][1] @ 4992
	.xfloat	1.00000000000000000000e+00		; _SCMP[31][2] @ 5024
	.xfloat	1.00000000000000000000e+00		; _SCMP[31][3] @ 5056
	.xfloat	1.00000000000000000000e+00		; _SCMP[31][4] @ 5088
$C$IR_7:	.set	320

	.global	_logger5
_logger5:	.usect	".ebss",1,1,0
	.sym	_logger5,_logger5, 14, 2, 16
	.global	_logger6
_logger6:	.usect	".ebss",1,1,0
	.sym	_logger6,_logger6, 14, 2, 16
	.global	_logger3
_logger3:	.usect	".ebss",1,1,0
	.sym	_logger3,_logger3, 14, 2, 16
	.global	_logger4
_logger4:	.usect	".ebss",1,1,0
	.sym	_logger4,_logger4, 14, 2, 16
	.global	_logger2
_logger2:	.usect	".ebss",1,1,0
	.sym	_logger2,_logger2, 14, 2, 16
	.global	_clogger
_clogger:	.usect	".ebss",1,1,0
	.sym	_clogger,_clogger, 14, 2, 16
	.global	_cloggaux
_cloggaux:	.usect	".ebss",1,1,0
	.sym	_cloggaux,_cloggaux, 14, 2, 16
	.global	_logger8
_logger8:	.usect	".ebss",1,1,0
	.sym	_logger8,_logger8, 14, 2, 16
	.global	_loggaux
_loggaux:	.usect	".ebss",1,1,0
	.sym	_loggaux,_loggaux, 14, 2, 16
	.global	_timeout
_timeout:	.usect	".ebss",1,1,0
	.sym	_timeout,_timeout, 4, 2, 16
	.global	_pwm_period
_pwm_period:	.usect	".ebss",1,1,0
	.sym	_pwm_period,_pwm_period, 14, 2, 16
	.global	_ret_val_mon
_ret_val_mon:	.usect	".ebss",1,1,0
	.sym	_ret_val_mon,_ret_val_mon, 14, 2, 16
	.global	_stop
_stop:	.usect	".ebss",1,1,0
	.sym	_stop,_stop, 14, 2, 16
	.global	_Vdclink_medido
_Vdclink_medido:	.usect	".ebss",1,1,0
	.sym	_Vdclink_medido,_Vdclink_medido, 14, 2, 16
	.global	_logger1
_logger1:	.usect	".ebss",1,1,0
	.sym	_logger1,_logger1, 14, 2, 16
	.global	_logger7
_logger7:	.usect	".ebss",1,1,0
	.sym	_logger7,_logger7, 14, 2, 16
	.global	_dsampled
_dsampled:	.usect	".ebss",1,1,0
	.sym	_dsampled,_dsampled, 14, 2, 16
	.global	_logger
_logger:	.usect	".ebss",1,1,0
	.sym	_logger,_logger, 14, 2, 16
	.global	_auxeqeptmr
_auxeqeptmr:	.usect	".ebss",1,1,0
	.sym	_auxeqeptmr,_auxeqeptmr, 14, 2, 16
	.global	_stvel
_stvel:	.usect	".ebss",1,1,0
	.sym	_stvel,_stvel, 4, 2, 16
	.global	_imcntr
_imcntr:	.usect	".ebss",1,1,0
	.sym	_imcntr,_imcntr, 14, 2, 16
	.global	_Fm
_Fm:	.usect	".ebss",1,1,0
	.sym	_Fm,_Fm, 14, 2, 16
	.global	_chgflag
_chgflag:	.usect	".ebss",1,1,0
	.sym	_chgflag,_chgflag, 14, 2, 16
	.global	_secntr
_secntr:	.usect	".ebss",1,1,0
	.sym	_secntr,_secntr, 14, 2, 16
	.global	_TmO1_15
_TmO1_15:	.usect	".ebss",2,1,1
	.sym	_TmO1_15,_TmO1_15, 6, 2, 32
	.global	_TmO1_12
_TmO1_12:	.usect	".ebss",2,1,1
	.sym	_TmO1_12,_TmO1_12, 6, 2, 32
	.global	_TmO1_21
_TmO1_21:	.usect	".ebss",2,1,1
	.sym	_TmO1_21,_TmO1_21, 6, 2, 32
	.global	_TmB44
_TmB44:	.usect	".ebss",2,1,1
	.sym	_TmB44,_TmB44, 6, 2, 32
	.global	_TmB51
_TmB51:	.usect	".ebss",2,1,1
	.sym	_TmB51,_TmB51, 6, 2, 32
	.global	_TmB22
_TmB22:	.usect	".ebss",2,1,1
	.sym	_TmB22,_TmB22, 6, 2, 32
	.global	_TmO1_11
_TmO1_11:	.usect	".ebss",2,1,1
	.sym	_TmO1_11,_TmO1_11, 6, 2, 32
	.global	_TmO1_16
_TmO1_16:	.usect	".ebss",2,1,1
	.sym	_TmO1_16,_TmO1_16, 6, 2, 32
	.global	_TmB33
_TmB33:	.usect	".ebss",2,1,1
	.sym	_TmB33,_TmB33, 6, 2, 32
	.global	_TmB62
_TmB62:	.usect	".ebss",2,1,1
	.sym	_TmB62,_TmB62, 6, 2, 32
	.global	_TmO1_56
_TmO1_56:	.usect	".ebss",2,1,1
	.sym	_TmO1_56,_TmO1_56, 6, 2, 32
	.global	_TmO1_61
_TmO1_61:	.usect	".ebss",2,1,1
	.sym	_TmO1_61,_TmO1_61, 6, 2, 32
	.global	_TmO1_52
_TmO1_52:	.usect	".ebss",2,1,1
	.sym	_TmO1_52,_TmO1_52, 6, 2, 32
	.global	_TmO1_65
_TmO1_65:	.usect	".ebss",2,1,1
	.sym	_TmO1_65,_TmO1_65, 6, 2, 32
	.global	_TmO1_66
_TmO1_66:	.usect	".ebss",2,1,1
	.sym	_TmO1_66,_TmO1_66, 6, 2, 32
	.global	_TmO1_55
_TmO1_55:	.usect	".ebss",2,1,1
	.sym	_TmO1_55,_TmO1_55, 6, 2, 32
	.global	_TmO1_26
_TmO1_26:	.usect	".ebss",2,1,1
	.sym	_TmO1_26,_TmO1_26, 6, 2, 32
	.global	_TmO1_33
_TmO1_33:	.usect	".ebss",2,1,1
	.sym	_TmO1_33,_TmO1_33, 6, 2, 32
	.global	_TmO1_22
_TmO1_22:	.usect	".ebss",2,1,1
	.sym	_TmO1_22,_TmO1_22, 6, 2, 32
	.global	_TmO1_51
_TmO1_51:	.usect	".ebss",2,1,1
	.sym	_TmO1_51,_TmO1_51, 6, 2, 32
	.global	_TmO1_62
_TmO1_62:	.usect	".ebss",2,1,1
	.sym	_TmO1_62,_TmO1_62, 6, 2, 32
	.global	_TmO1_25
_TmO1_25:	.usect	".ebss",2,1,1
	.sym	_TmO1_25,_TmO1_25, 6, 2, 32
	.global	_TmO1_44
_TmO1_44:	.usect	".ebss",2,1,1
	.sym	_TmO1_44,_TmO1_44, 6, 2, 32
	.global	_TmA12
_TmA12:	.usect	".ebss",2,1,1
	.sym	_TmA12,_TmA12, 6, 2, 32
	.global	_TmA15
_TmA15:	.usect	".ebss",2,1,1
	.sym	_TmA15,_TmA15, 6, 2, 32
	.global	_wm_km1
_wm_km1:	.usect	".ebss",2,1,1
	.sym	_wm_km1,_wm_km1, 6, 2, 32
	.global	_TmA21
_TmA21:	.usect	".ebss",2,1,1
	.sym	_TmA21,_TmA21, 6, 2, 32
	.global	_TmA22
_TmA22:	.usect	".ebss",2,1,1
	.sym	_TmA22,_TmA22, 6, 2, 32
	.global	_TmA11
_TmA11:	.usect	".ebss",2,1,1
	.sym	_TmA11,_TmA11, 6, 2, 32
	.global	_TmA16
_TmA16:	.usect	".ebss",2,1,1
	.sym	_TmA16,_TmA16, 6, 2, 32
	.global	_e_km1
_e_km1:	.usect	".ebss",2,1,1
	.sym	_e_km1,_e_km1, 6, 2, 32
	.global	_Ke
_Ke:	.usect	".ebss",2,1,1
	.sym	_Ke,_Ke, 6, 2, 32
	.global	_wr
_wr:	.usect	".ebss",2,1,1
	.sym	_wr,_wr, 6, 2, 32
	.global	_inte_k
_inte_k:	.usect	".ebss",2,1,1
	.sym	_inte_k,_inte_k, 6, 2, 32
	.global	_inte_km1
_inte_km1:	.usect	".ebss",2,1,1
	.sym	_inte_km1,_inte_km1, 6, 2, 32
	.global	_Werror
_Werror:	.usect	".ebss",2,1,1
	.sym	_Werror,_Werror, 6, 2, 32
	.global	_TmA61
_TmA61:	.usect	".ebss",2,1,1
	.sym	_TmA61,_TmA61, 6, 2, 32
	.global	_TmA62
_TmA62:	.usect	".ebss",2,1,1
	.sym	_TmA62,_TmA62, 6, 2, 32
	.global	_TmA55
_TmA55:	.usect	".ebss",2,1,1
	.sym	_TmA55,_TmA55, 6, 2, 32
	.global	_TmA66
_TmA66:	.usect	".ebss",2,1,1
	.sym	_TmA66,_TmA66, 6, 2, 32
	.global	_TmB11
_TmB11:	.usect	".ebss",2,1,1
	.sym	_TmB11,_TmB11, 6, 2, 32
	.global	_TmA56
_TmA56:	.usect	".ebss",2,1,1
	.sym	_TmA56,_TmA56, 6, 2, 32
	.global	_TmA65
_TmA65:	.usect	".ebss",2,1,1
	.sym	_TmA65,_TmA65, 6, 2, 32
	.global	_TmA26
_TmA26:	.usect	".ebss",2,1,1
	.sym	_TmA26,_TmA26, 6, 2, 32
	.global	_TmA33
_TmA33:	.usect	".ebss",2,1,1
	.sym	_TmA33,_TmA33, 6, 2, 32
	.global	_Kinte
_Kinte:	.usect	".ebss",2,1,1
	.sym	_Kinte,_Kinte, 6, 2, 32
	.global	_TmA51
_TmA51:	.usect	".ebss",2,1,1
	.sym	_TmA51,_TmA51, 6, 2, 32
	.global	_TmA52
_TmA52:	.usect	".ebss",2,1,1
	.sym	_TmA52,_TmA52, 6, 2, 32
	.global	_TmA25
_TmA25:	.usect	".ebss",2,1,1
	.sym	_TmA25,_TmA25, 6, 2, 32
	.global	_ir_beta_p
_ir_beta_p:	.usect	".ebss",2,1,1
	.sym	_ir_beta_p,_ir_beta_p, 6, 2, 32
	.global	_z1
_z1:	.usect	".ebss",2,1,1
	.sym	_z1,_z1, 6, 2, 32
	.global	_i_y_med
_i_y_med:	.usect	".ebss",2,1,1
	.sym	_i_y_med,_i_y_med, 6, 2, 32
	.global	_z3
_z3:	.usect	".ebss",2,1,1
	.sym	_z3,_z3, 6, 2, 32
	.global	_z4
_z4:	.usect	".ebss",2,1,1
	.sym	_z4,_z4, 6, 2, 32
	.global	_ir_alpha_p
_ir_alpha_p:	.usect	".ebss",2,1,1
	.sym	_ir_alpha_p,_ir_alpha_p, 6, 2, 32
	.global	_i_y_p
_i_y_p:	.usect	".ebss",2,1,1
	.sym	_i_y_p,_i_y_p, 6, 2, 32
	.global	_i_alpha_med
_i_alpha_med:	.usect	".ebss",2,1,1
	.sym	_i_alpha_med,_i_alpha_med, 6, 2, 32
	.global	_i_beta_p
_i_beta_p:	.usect	".ebss",2,1,1
	.sym	_i_beta_p,_i_beta_p, 6, 2, 32
	.global	_i_x_med
_i_x_med:	.usect	".ebss",2,1,1
	.sym	_i_x_med,_i_x_med, 6, 2, 32
	.global	_z2
_z2:	.usect	".ebss",2,1,1
	.sym	_z2,_z2, 6, 2, 32
	.global	_i_x_p
_i_x_p:	.usect	".ebss",2,1,1
	.sym	_i_x_p,_i_x_p, 6, 2, 32
	.global	_i_beta_med
_i_beta_med:	.usect	".ebss",2,1,1
	.sym	_i_beta_med,_i_beta_med, 6, 2, 32
	.global	_velocfactor
_velocfactor:	.usect	".ebss",2,1,1
	.sym	_velocfactor,_velocfactor, 6, 2, 32
	.global	_wm_kp1min
_wm_kp1min:	.usect	".ebss",2,1,1
	.sym	_wm_kp1min,_wm_kp1min, 6, 2, 32
	.global	_veloc_radsec
_veloc_radsec:	.usect	".ebss",2,1,1
	.sym	_veloc_radsec,_veloc_radsec, 6, 2, 32
	.global	_hdx
_hdx:	.usect	".ebss",2,1,1
	.sym	_hdx,_hdx, 6, 2, 32
	.global	_eqeptmr
_eqeptmr:	.usect	".ebss",2,1,1
	.sym	_eqeptmr,_eqeptmr, 6, 2, 32
	.global	_velocfactor2
_velocfactor2:	.usect	".ebss",2,1,1
	.sym	_velocfactor2,_velocfactor2, 6, 2, 32
	.global	_metodo
_metodo:	.usect	".ebss",2,1,1
	.sym	_metodo,_metodo, 6, 2, 32
	.global	_hvfactor
_hvfactor:	.usect	".ebss",2,1,1
	.sym	_hvfactor,_hvfactor, 6, 2, 32
	.global	_z5
_z5:	.usect	".ebss",2,1,1
	.sym	_z5,_z5, 6, 2, 32
	.global	_veloc_rpm
_veloc_rpm:	.usect	".ebss",2,1,1
	.sym	_veloc_rpm,_veloc_rpm, 6, 2, 32
	.global	_wm_kp1max
_wm_kp1max:	.usect	".ebss",2,1,1
	.sym	_wm_kp1max,_wm_kp1max, 6, 2, 32
	.global	_z6
_z6:	.usect	".ebss",2,1,1
	.sym	_z6,_z6, 6, 2, 32
	.global	_TmO2_52
_TmO2_52:	.usect	".ebss",2,1,1
	.sym	_TmO2_52,_TmO2_52, 6, 2, 32
	.global	_TmO2_61
_TmO2_61:	.usect	".ebss",2,1,1
	.sym	_TmO2_61,_TmO2_61, 6, 2, 32
	.global	_TmO2_44
_TmO2_44:	.usect	".ebss",2,1,1
	.sym	_TmO2_44,_TmO2_44, 6, 2, 32
	.global	_e_alpha
_e_alpha:	.usect	".ebss",2,1,1
	.sym	_e_alpha,_e_alpha, 6, 2, 32
	.global	_e_beta
_e_beta:	.usect	".ebss",2,1,1
	.sym	_e_beta,_e_beta, 6, 2, 32
	.global	_TmO2_51
_TmO2_51:	.usect	".ebss",2,1,1
	.sym	_TmO2_51,_TmO2_51, 6, 2, 32
	.global	_TmO2_12
_TmO2_12:	.usect	".ebss",2,1,1
	.sym	_TmO2_12,_TmO2_12, 6, 2, 32
	.global	_TmO2_21
_TmO2_21:	.usect	".ebss",2,1,1
	.sym	_TmO2_21,_TmO2_21, 6, 2, 32
	.global	_TmA44
_TmA44:	.usect	".ebss",2,1,1
	.sym	_TmA44,_TmA44, 6, 2, 32
	.global	_TmO2_33
_TmO2_33:	.usect	".ebss",2,1,1
	.sym	_TmO2_33,_TmO2_33, 6, 2, 32
	.global	_TmO2_62
_TmO2_62:	.usect	".ebss",2,1,1
	.sym	_TmO2_62,_TmO2_62, 6, 2, 32
	.global	_TmO2_11
_TmO2_11:	.usect	".ebss",2,1,1
	.sym	_TmO2_11,_TmO2_11, 6, 2, 32
	.global	_TmO2_22
_TmO2_22:	.usect	".ebss",2,1,1
	.sym	_TmO2_22,_TmO2_22, 6, 2, 32
	.global	_i_x_cte
_i_x_cte:	.usect	".ebss",2,1,1
	.sym	_i_x_cte,_i_x_cte, 6, 2, 32
	.global	_i_y_cte
_i_y_cte:	.usect	".ebss",2,1,1
	.sym	_i_y_cte,_i_y_cte, 6, 2, 32
	.global	_i_alpha_cte
_i_alpha_cte:	.usect	".ebss",2,1,1
	.sym	_i_alpha_cte,_i_alpha_cte, 6, 2, 32
	.global	_i_alpha_p2
_i_alpha_p2:	.usect	".ebss",2,1,1
	.sym	_i_alpha_p2,_i_alpha_p2, 6, 2, 32
	.global	_veloc_hz
_veloc_hz:	.usect	".ebss",2,1,1
	.sym	_veloc_hz,_veloc_hz, 6, 2, 32
	.global	_i_beta_cte
_i_beta_cte:	.usect	".ebss",2,1,1
	.sym	_i_beta_cte,_i_beta_cte, 6, 2, 32
	.global	_hvel_radsec
_hvel_radsec:	.usect	".ebss",2,1,1
	.sym	_hvel_radsec,_hvel_radsec, 6, 2, 32
	.global	_e_y
_e_y:	.usect	".ebss",2,1,1
	.sym	_e_y,_e_y, 6, 2, 32
	.global	_e_x
_e_x:	.usect	".ebss",2,1,1
	.sym	_e_x,_e_x, 6, 2, 32
	.global	_Jsv
_Jsv:	.usect	".ebss",2,1,1
	.sym	_Jsv,_Jsv, 6, 2, 32
	.global	_i_alpha_p1
_i_alpha_p1:	.usect	".ebss",2,1,1
	.sym	_i_alpha_p1,_i_alpha_p1, 6, 2, 32
	.global	_hvel_hz
_hvel_hz:	.usect	".ebss",2,1,1
	.sym	_hvel_hz,_hvel_hz, 6, 2, 32
	.global	_Id_mir
_Id_mir:	.usect	".ebss",2,1,1
	.sym	_Id_mir,_Id_mir, 6, 2, 32
	.global	_Id_medido
_Id_medido:	.usect	".ebss",2,1,1
	.sym	_Id_medido,_Id_medido, 6, 2, 32
	.global	_Ie_medido
_Ie_medido:	.usect	".ebss",2,1,1
	.sym	_Ie_medido,_Ie_medido, 6, 2, 32
	.global	_i_y
_i_y:	.usect	".ebss",2,1,1
	.sym	_i_y,_i_y, 6, 2, 32
	.global	_i_beta
_i_beta:	.usect	".ebss",2,1,1
	.sym	_i_beta,_i_beta, 6, 2, 32
	.global	_Ib_mir
_Ib_mir:	.usect	".ebss",2,1,1
	.sym	_Ib_mir,_Ib_mir, 6, 2, 32
	.global	_i_x
_i_x:	.usect	".ebss",2,1,1
	.sym	_i_x,_i_x, 6, 2, 32
	.global	_Isd
_Isd:	.usect	".ebss",2,1,1
	.sym	_Isd,_Isd, 6, 2, 32
	.global	_Vdc_mir
_Vdc_mir:	.usect	".ebss",2,1,1
	.sym	_Vdc_mir,_Vdc_mir, 6, 2, 32
	.global	_Iy_ref
_Iy_ref:	.usect	".ebss",2,1,1
	.sym	_Iy_ref,_Iy_ref, 6, 2, 32
	.global	_Isq
_Isq:	.usect	".ebss",2,1,1
	.sym	_Isq,_Isq, 6, 2, 32
	.global	_Ie_mir
_Ie_mir:	.usect	".ebss",2,1,1
	.sym	_Ie_mir,_Ie_mir, 6, 2, 32
	.global	_Ic_medido
_Ic_medido:	.usect	".ebss",2,1,1
	.sym	_Ic_medido,_Ic_medido, 6, 2, 32
	.global	_Vdc_medido
_Vdc_medido:	.usect	".ebss",2,1,1
	.sym	_Vdc_medido,_Vdc_medido, 6, 2, 32
	.global	_Tm
_Tm:	.usect	".ebss",2,1,1
	.sym	_Tm,_Tm, 6, 2, 32
	.global	_KPIW
_KPIW:	.usect	".ebss",2,1,1
	.sym	_KPIW,_KPIW, 6, 2, 32
	.global	_Kb
_Kb:	.usect	".ebss",2,1,1
	.sym	_Kb,_Kb, 6, 2, 32
	.global	_Ka
_Ka:	.usect	".ebss",2,1,1
	.sym	_Ka,_Ka, 6, 2, 32
	.global	_Isq_par
_Isq_par:	.usect	".ebss",2,1,1
	.sym	_Isq_par,_Isq_par, 6, 2, 32
	.global	_Iq_med
_Iq_med:	.usect	".ebss",2,1,1
	.sym	_Iq_med,_Iq_med, 6, 2, 32
	.global	_KPW
_KPW:	.usect	".ebss",2,1,1
	.sym	_KPW,_KPW, 6, 2, 32
	.global	_i_alpha
_i_alpha:	.usect	".ebss",2,1,1
	.sym	_i_alpha,_i_alpha, 6, 2, 32
	.global	_Ia_mir
_Ia_mir:	.usect	".ebss",2,1,1
	.sym	_Ia_mir,_Ia_mir, 6, 2, 32
	.global	_If_medido
_If_medido:	.usect	".ebss",2,1,1
	.sym	_If_medido,_If_medido, 6, 2, 32
	.global	_Ib_medido
_Ib_medido:	.usect	".ebss",2,1,1
	.sym	_Ib_medido,_Ib_medido, 6, 2, 32
	.global	_Kc
_Kc:	.usect	".ebss",2,1,1
	.sym	_Kc,_Kc, 6, 2, 32
	.global	_Id_med
_Id_med:	.usect	".ebss",2,1,1
	.sym	_Id_med,_Id_med, 6, 2, 32
	.global	_Ia_medido
_Ia_medido:	.usect	".ebss",2,1,1
	.sym	_Ia_medido,_Ia_medido, 6, 2, 32
	.global	_Kd
_Kd:	.usect	".ebss",2,1,1
	.sym	_Kd,_Kd, 6, 2, 32
	.global	_mstart2
_mstart2:	.usect	".ebss",2,1,1
	.sym	_mstart2,_mstart2, 6, 2, 32
	.global	_mstart
_mstart:	.usect	".ebss",2,1,1
	.sym	_mstart,_mstart, 6, 2, 32
	.global	_contfallo
_contfallo:	.usect	".ebss",2,1,1
	.sym	_contfallo,_contfallo, 6, 2, 32
	.global	_ThetaI
_ThetaI:	.usect	".ebss",2,1,1
	.sym	_ThetaI,_ThetaI, 6, 2, 32
	.global	_Tcarga_pre
_Tcarga_pre:	.usect	".ebss",2,1,1
	.sym	_Tcarga_pre,_Tcarga_pre, 6, 2, 32
	.global	_Tcarga
_Tcarga:	.usect	".ebss",2,1,1
	.sym	_Tcarga,_Tcarga, 6, 2, 32
	.global	_Imax
_Imax:	.usect	".ebss",2,1,1
	.sym	_Imax,_Imax, 6, 2, 32
	.global	_wm_d3
_wm_d3:	.usect	".ebss",2,1,1
	.sym	_wm_d3,_wm_d3, 6, 2, 32
	.global	_wm_k
_wm_k:	.usect	".ebss",2,1,1
	.sym	_wm_k,_wm_k, 6, 2, 32
	.global	_w2
_w2:	.usect	".ebss",2,1,1
	.sym	_w2,_w2, 6, 2, 32
	.global	_w1
_w1:	.usect	".ebss",2,1,1
	.sym	_w1,_w1, 6, 2, 32
	.global	_Vdc
_Vdc:	.usect	".ebss",2,1,1
	.sym	_Vdc,_Vdc, 6, 2, 32
	.global	_CCarga
_CCarga:	.usect	".ebss",2,1,1
	.sym	_CCarga,_CCarga, 6, 2, 32
	.global	_wm_ref
_wm_ref:	.usect	".ebss",2,1,1
	.sym	_wm_ref,_wm_ref, 6, 2, 32
	.global	_Ix_ref
_Ix_ref:	.usect	".ebss",2,1,1
	.sym	_Ix_ref,_Ix_ref, 6, 2, 32
	.global	_Ibeta_ref
_Ibeta_ref:	.usect	".ebss",2,1,1
	.sym	_Ibeta_ref,_Ibeta_ref, 6, 2, 32
	.global	_costhetae_p
_costhetae_p:	.usect	".ebss",2,1,1
	.sym	_costhetae_p,_costhetae_p, 6, 2, 32
	.global	_sinthetae_p
_sinthetae_p:	.usect	".ebss",2,1,1
	.sym	_sinthetae_p,_sinthetae_p, 6, 2, 32
	.global	_Ialfa_ref_p
_Ialfa_ref_p:	.usect	".ebss",2,1,1
	.sym	_Ialfa_ref_p,_Ialfa_ref_p, 6, 2, 32
	.global	_thetae_p
_thetae_p:	.usect	".ebss",2,1,1
	.sym	_thetae_p,_thetae_p, 6, 2, 32
	.global	_Ialfa_ref
_Ialfa_ref:	.usect	".ebss",2,1,1
	.sym	_Ialfa_ref,_Ialfa_ref, 6, 2, 32
	.global	_sinthetae
_sinthetae:	.usect	".ebss",2,1,1
	.sym	_sinthetae,_sinthetae, 6, 2, 32
	.global	_thetaenm1
_thetaenm1:	.usect	".ebss",2,1,1
	.sym	_thetaenm1,_thetaenm1, 6, 2, 32
	.global	_Jopt
_Jopt:	.usect	".ebss",2,1,1
	.sym	_Jopt,_Jopt, 6, 2, 32
	.global	_costhetae
_costhetae:	.usect	".ebss",2,1,1
	.sym	_costhetae,_costhetae, 6, 2, 32
	.global	_wsl
_wsl:	.usect	".ebss",2,1,1
	.sym	_wsl,_wsl, 6, 2, 32
	.global	_Ibeta_ref_p
_Ibeta_ref_p:	.usect	".ebss",2,1,1
	.sym	_Ibeta_ref_p,_Ibeta_ref_p, 6, 2, 32
	.global	_thetae
_thetae:	.usect	".ebss",2,1,1
	.sym	_thetae,_thetae, 6, 2, 32
	.global	_we
_we:	.usect	".ebss",2,1,1
	.sym	_we,_we, 6, 2, 32
	.global	_uvdiv
_uvdiv:	.usect	".ebss",5,1,0
	.sym	_uvdiv,_uvdiv, 62, 2, 80,, 5
	.global	_CpuTimer0Regs
_CpuTimer0Regs:	.usect	"CpuTimer0RegsFile",8,1,1
	.sym	_CpuTimer0Regs,_CpuTimer0Regs, 8, 2, 128, _CPUTIMER_REGS
	.global	_FlashRegs
_FlashRegs:	.usect	"FlashRegsFile",8,1,0
	.sym	_FlashRegs,_FlashRegs, 8, 2, 128, _FLASH_REGS
	.global	_CpuTimer2Regs
_CpuTimer2Regs:	.usect	"CpuTimer2RegsFile",8,1,1
	.sym	_CpuTimer2Regs,_CpuTimer2Regs, 8, 2, 128, _CPUTIMER_REGS
	.global	_CpuTimer1Regs
_CpuTimer1Regs:	.usect	"CpuTimer1RegsFile",8,1,1
	.sym	_CpuTimer1Regs,_CpuTimer1Regs, 8, 2, 128, _CPUTIMER_REGS
	.global	_CsmPwl
_CsmPwl:	.usect	"CsmPwlFile",8,1,0
	.sym	_CsmPwl,_CsmPwl, 8, 2, 128, _CSM_PWL
	.global	_GpioIntRegs
_GpioIntRegs:	.usect	"GpioIntRegsFile",10,1,1
	.sym	_GpioIntRegs,_GpioIntRegs, 8, 2, 160, _GPIO_INT_REGS
	.global	_factorwm
_factorwm:	.usect	".ebss",10,1,1
	.sym	_factorwm,_factorwm, 54, 2, 160,, 5
	.global	_AdcMirror
_AdcMirror:	.usect	"AdcMirrorFile",16,1,0
	.sym	_AdcMirror,_AdcMirror, 8, 2, 256, _ADC_RESULT_MIRROR_REGS
	.global	_XIntruptRegs
_XIntruptRegs:	.usect	"XIntruptRegsFile",16,1,0
	.sym	_XIntruptRegs,_XIntruptRegs, 8, 2, 256, _XINTRUPT_REGS
	.global	_ScicRegs
_ScicRegs:	.usect	"ScicRegsFile",16,1,0
	.sym	_ScicRegs,_ScicRegs, 8, 2, 256, _SCI_REGS
	.global	_SpiaRegs
_SpiaRegs:	.usect	"SpiaRegsFile",16,1,0
	.sym	_SpiaRegs,_SpiaRegs, 8, 2, 256, _SPI_REGS
	.global	_CsmRegs
_CsmRegs:	.usect	"CsmRegsFile",16,1,0
	.sym	_CsmRegs,_CsmRegs, 8, 2, 256, _CSM_REGS
	.global	_ScibRegs
_ScibRegs:	.usect	"ScibRegsFile",16,1,0
	.sym	_ScibRegs,_ScibRegs, 8, 2, 256, _SCI_REGS
	.global	_SciaRegs
_SciaRegs:	.usect	"SciaRegsFile",16,1,0
	.sym	_SciaRegs,_SciaRegs, 8, 2, 256, _SCI_REGS
	.global	_PieCtrlRegs
_PieCtrlRegs:	.usect	"PieCtrlRegsFile",26,1,0
	.sym	_PieCtrlRegs,_PieCtrlRegs, 8, 2, 416, _PIE_CTRL_REGS
	.global	_AdcRegs
_AdcRegs:	.usect	"AdcRegsFile",30,1,0
	.sym	_AdcRegs,_AdcRegs, 8, 2, 480, _ADC_REGS
	.global	_XintfRegs
_XintfRegs:	.usect	"XintfRegsFile",30,1,1
	.sym	_XintfRegs,_XintfRegs, 8, 2, 480, _XINTF_REGS
	.global	_ECap2Regs
_ECap2Regs:	.usect	"ECap2RegsFile",32,1,1
	.sym	_ECap2Regs,_ECap2Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap6Regs
_ECap6Regs:	.usect	"ECap6RegsFile",32,1,1
	.sym	_ECap6Regs,_ECap6Regs, 8, 2, 512, _ECAP_REGS
	.global	_GpioDataRegs
_GpioDataRegs:	.usect	"GpioDataRegsFile",32,1,1
	.sym	_GpioDataRegs,_GpioDataRegs, 8, 2, 512, _GPIO_DATA_REGS
	.global	_ECap4Regs
_ECap4Regs:	.usect	"ECap4RegsFile",32,1,1
	.sym	_ECap4Regs,_ECap4Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap3Regs
_ECap3Regs:	.usect	"ECap3RegsFile",32,1,1
	.sym	_ECap3Regs,_ECap3Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap1Regs
_ECap1Regs:	.usect	"ECap1RegsFile",32,1,1
	.sym	_ECap1Regs,_ECap1Regs, 8, 2, 512, _ECAP_REGS
	.global	_ECap5Regs
_ECap5Regs:	.usect	"ECap5RegsFile",32,1,1
	.sym	_ECap5Regs,_ECap5Regs, 8, 2, 512, _ECAP_REGS
	.global	_SysCtrlRegs
_SysCtrlRegs:	.usect	"SysCtrlRegsFile",32,1,0
	.sym	_SysCtrlRegs,_SysCtrlRegs, 8, 2, 512, _SYS_CTRL_REGS
	.global	_EPwm2Regs
_EPwm2Regs:	.usect	"EPwm2RegsFile",34,1,1
	.sym	_EPwm2Regs,_EPwm2Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm4Regs
_EPwm4Regs:	.usect	"EPwm4RegsFile",34,1,1
	.sym	_EPwm4Regs,_EPwm4Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm1Regs
_EPwm1Regs:	.usect	"EPwm1RegsFile",34,1,1
	.sym	_EPwm1Regs,_EPwm1Regs, 8, 2, 544, _EPWM_REGS
	.global	_I2caRegs
_I2caRegs:	.usect	"I2caRegsFile",34,1,0
	.sym	_I2caRegs,_I2caRegs, 8, 2, 544, _I2C_REGS
	.global	_EPwm6Regs
_EPwm6Regs:	.usect	"EPwm6RegsFile",34,1,1
	.sym	_EPwm6Regs,_EPwm6Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm3Regs
_EPwm3Regs:	.usect	"EPwm3RegsFile",34,1,1
	.sym	_EPwm3Regs,_EPwm3Regs, 8, 2, 544, _EPWM_REGS
	.global	_EPwm5Regs
_EPwm5Regs:	.usect	"EPwm5RegsFile",34,1,1
	.sym	_EPwm5Regs,_EPwm5Regs, 8, 2, 544, _EPWM_REGS
	.global	_McbspaRegs
_McbspaRegs:	.usect	"McbspaRegsFile",37,1,0
	.sym	_McbspaRegs,_McbspaRegs, 8, 2, 592, _MCBSP_REGS
	.global	_McbspbRegs
_McbspbRegs:	.usect	"McbspbRegsFile",37,1,0
	.sym	_McbspbRegs,_McbspbRegs, 8, 2, 592, _MCBSP_REGS
	.global	_GpioCtrlRegs
_GpioCtrlRegs:	.usect	"GpioCtrlRegsFile",46,1,1
	.sym	_GpioCtrlRegs,_GpioCtrlRegs, 8, 2, 736, _GPIO_CTRL_REGS
	.global	_ECanaRegs
_ECanaRegs:	.usect	"ECanaRegsFile",52,1,1
	.sym	_ECanaRegs,_ECanaRegs, 8, 2, 832, _ECAN_REGS
	.global	_ECanbRegs
_ECanbRegs:	.usect	"ECanbRegsFile",52,1,1
	.sym	_ECanbRegs,_ECanbRegs, 8, 2, 832, _ECAN_REGS
	.global	_ECanaLAMRegs
_ECanaLAMRegs:	.usect	"ECanaLAMRegsFile",64,1,1
	.sym	_ECanaLAMRegs,_ECanaLAMRegs, 8, 2, 1024, _LAM_REGS
	.global	_vsx
_vsx:	.usect	".ebss",64,1,1
	.sym	_vsx,_vsx, 54, 2, 1024,, 32
	.global	_ECanaMOTORegs
_ECanaMOTORegs:	.usect	"ECanaMOTORegsFile",64,1,1
	.sym	_ECanaMOTORegs,_ECanaMOTORegs, 8, 2, 1024, _MOTO_REGS
	.global	_ECanbLAMRegs
_ECanbLAMRegs:	.usect	"ECanbLAMRegsFile",64,1,1
	.sym	_ECanbLAMRegs,_ECanbLAMRegs, 8, 2, 1024, _LAM_REGS
	.global	_vsb
_vsb:	.usect	".ebss",64,1,1
	.sym	_vsb,_vsb, 54, 2, 1024,, 32
	.global	_Usy
_Usy:	.usect	".ebss",64,1,1
	.sym	_Usy,_Usy, 54, 2, 1024,, 32
	.global	_Usx
_Usx:	.usect	".ebss",64,1,1
	.sym	_Usx,_Usx, 54, 2, 1024,, 32
	.global	_Urb
_Urb:	.usect	".ebss",64,1,1
	.sym	_Urb,_Urb, 54, 2, 1024,, 32
	.global	_Ura
_Ura:	.usect	".ebss",64,1,1
	.sym	_Ura,_Ura, 54, 2, 1024,, 32
	.global	_vsy
_vsy:	.usect	".ebss",64,1,1
	.sym	_vsy,_vsy, 54, 2, 1024,, 32
	.global	_ECanaMOTSRegs
_ECanaMOTSRegs:	.usect	"ECanaMOTSRegsFile",64,1,1
	.sym	_ECanaMOTSRegs,_ECanaMOTSRegs, 8, 2, 1024, _MOTS_REGS
	.global	_Usb
_Usb:	.usect	".ebss",64,1,1
	.sym	_Usb,_Usb, 54, 2, 1024,, 32
	.global	_Usa
_Usa:	.usect	".ebss",64,1,1
	.sym	_Usa,_Usa, 54, 2, 1024,, 32
	.global	_ECanbMOTORegs
_ECanbMOTORegs:	.usect	"ECanbMOTORegsFile",64,1,1
	.sym	_ECanbMOTORegs,_ECanbMOTORegs, 8, 2, 1024, _MOTO_REGS
	.global	_ECanbMOTSRegs
_ECanbMOTSRegs:	.usect	"ECanbMOTSRegsFile",64,1,1
	.sym	_ECanbMOTSRegs,_ECanbMOTSRegs, 8, 2, 1024, _MOTS_REGS
	.global	_vsa
_vsa:	.usect	".ebss",64,1,1
	.sym	_vsa,_vsa, 54, 2, 1024,, 32
	.global	_EQep1Regs
_EQep1Regs:	.usect	"EQep1RegsFile",64,1,1
	.sym	_EQep1Regs,_EQep1Regs, 8, 2, 1024, _EQEP_REGS
	.global	_EQep2Regs
_EQep2Regs:	.usect	"EQep2RegsFile",64,1,1
	.sym	_EQep2Regs,_EQep2Regs, 8, 2, 1024, _EQEP_REGS
	.global	_DevEmuRegs
_DevEmuRegs:	.usect	"DevEmuRegsFile",208,1,1
	.sym	_DevEmuRegs,_DevEmuRegs, 8, 2, 3328, _DEV_EMU_REGS
	.global	_DmaRegs
_DmaRegs:	.usect	"DmaRegsFile",224,1,1
	.sym	_DmaRegs,_DmaRegs, 8, 2, 3584, _DMA_REGS
	.global	_ECanaMboxes
_ECanaMboxes:	.usect	"ECanaMboxesFile",256,1,1
	.sym	_ECanaMboxes,_ECanaMboxes, 8, 2, 4096, _ECAN_MBOXES
	.global	_PieVectTable
_PieVectTable:	.usect	"PieVectTableFile",256,1,1
	.sym	_PieVectTable,_PieVectTable, 8, 2, 4096, _PIE_VECT_TABLE
	.global	_ECanbMboxes
_ECanbMboxes:	.usect	"ECanbMboxesFile",256,1,1
	.sym	_ECanbMboxes,_ECanbMboxes, 8, 2, 4096, _ECAN_MBOXES
	.global	_SCMP
_SCMP:	.usect	".ebss",320,1,1
	.sym	_SCMP,_SCMP, 246, 2, 5120,, 32, 5
	.global	_ids_m
_ids_m:	.usect	"LOGGER",10000,1,0
	.sym	_ids_m,_ids_m, 62, 2, 160000,, 10000
	.global	_iq_ref
_iq_ref:	.usect	"LOGGER",10000,1,0
	.sym	_iq_ref,_iq_ref, 62, 2, 160000,, 10000
	.global	_wmech1
_wmech1:	.usect	"LOGGER",10000,1,0
	.sym	_wmech1,_wmech1, 62, 2, 160000,, 10000
	.global	_iqs_m
_iqs_m:	.usect	"LOGGER",10000,1,0
	.sym	_iqs_m,_iqs_m, 62, 2, 160000,, 10000
	.global	_ibs
_ibs:	.usect	"LOGGER",10000,1,0
	.sym	_ibs,_ibs, 62, 2, 160000,, 10000
	.global	_ias
_ias:	.usect	"LOGGER",10000,1,0
	.sym	_ias,_ias, 62, 2, 160000,, 10000
	.global	_ies
_ies:	.usect	"LOGGER",10000,1,0
	.sym	_ies,_ies, 62, 2, 160000,, 10000
	.global	_ids
_ids:	.usect	"LOGGER",10000,1,0
	.sym	_ids,_ids, 62, 2, 160000,, 10000
	.global	_svopt
_svopt:	.usect	"LOGGER",10000,1,0
	.sym	_svopt,_svopt, 62, 2, 160000,, 10000
;	opt2000 C:\\DOCUME~1\\lab\\LOCALS~1\\Temp\\008643 C:\\DOCUME~1\\lab\\LOCALS~1\\Temp\\008645 
;	ac2000 -@C:\DOCUME~1\lab\LOCALS~1\Temp\0086412 
	.sect	".text"
	.global	_PTC5Feqep_isr
	.file	"C:\CCStudio_v3.3\MyProjects\Calibracion2021_3\PTC5F_eqep1.c"
	.sym	_PTC5Feqep_isr,_PTC5Feqep_isr, 32, 2, 0
	.func	93
;----------------------------------------------------------------------
;  93 | interrupt void PTC5Feqep_isr()                                         
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _PTC5Feqep_isr                FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto, 24 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Feqep_isr:
	.line	2
;----------------------------------------------------------------------
;  94 | {        // EQEP Interrupt Subrutine                                   
;  96 | // Edge Capture and Unit Time Out Direction Speed                      
;----------------------------------------------------------------------
        ASP
        PUSH      RB
        PUSH      AR1H:AR0H
        MOVL      *SP++,XAR4
        MOVL      *SP++,XAR5
        MOVL      *SP++,XAR6
        MOVL      *SP++,XAR7
        MOVL      *SP++,XT
        MOV32     *SP++,STF
        MOV32     *SP++,R0H
        MOV32     *SP++,R1H
        MOV32     *SP++,R2H
        MOV32     *SP++,R3H
        SETFLG    RNDF32=1, RNDF64=1
        SPM       0
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	6
;----------------------------------------------------------------------
;  98 | if(EQep1Regs.QEPSTS.bit.QDF)                                           
;  99 |         {
;     |                                  // Forward Diretcion QDF=1, but de pos
;     | ition of the encoder is reverse                                        
; 101 |         eqeptmr = 0 - (float) EQep1Regs.QCPRDLAT;                      
; 102 |         hdx     = 0 - (float) EQep1Regs.QPOSLAT;                       
; 105 | else                                                                   
; 106 |         {
;     |                                  // Reverse Direction QDF=0            
;     |                                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+28
        TBIT      @_EQep1Regs+28,#5     ; |98| 
        BF        $C$L1,TC              ; |98| 
        ; branchcc occurs ; |98| 
	.line	16
;----------------------------------------------------------------------
; 108 | eqeptmr = (float) EQep1Regs.QCPRDLAT;                                  
;----------------------------------------------------------------------
        UI16TOF32 R0H,@_EQep1Regs+32    ; |108| 
        MOVW      DP,#_eqeptmr
        MOV32     @_eqeptmr,R0H
	.line	17
;----------------------------------------------------------------------
; 109 | hdx     = (float) QEPCNTMAX - (float) EQep1Regs.QPOSLAT;               
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+12
        MOVIZ     R0H,#17948            ; |109| 
        UI32TOF32 R1H,@_EQep1Regs+12    ; |109| 
        MOVXI     R0H,#16384            ; |109| 
        SUBF32    R0H,R0H,R1H           ; |109| 
        MOVW      DP,#_hdx
        MOV32     @_hdx,R0H
        B         $C$L2,UNC             ; |109| 
        ; branch occurs ; |109| 
$C$L1:    
	.line	9
        UI16TOF32 R0H,@_EQep1Regs+32    ; |101| 
        NOP
        SUBF32    R0H,#0,R0H            ; |101| 
        MOVW      DP,#_eqeptmr
        MOV32     @_eqeptmr,R0H
	.line	10
        MOVW      DP,#_EQep1Regs+12
        UI32TOF32 R0H,@_EQep1Regs+12    ; |102| 
        NOP
        SUBF32    R0H,#0,R0H            ; |102| 
        MOVW      DP,#_hdx
        MOV32     @_hdx,R0H
$C$L2:    
	.line	21
;----------------------------------------------------------------------
; 113 | hvel_radsec = (float) hdx * (float) hvfactor;                          
; 115 | // Avoids zero divisor                                                 
;----------------------------------------------------------------------
        MOV32     R1H,@_hvfactor
        MPYF32    R0H,R1H,R0H           ; |113| 
        MOVW      DP,#_hvel_radsec
        MOV32     @_hvel_radsec,R0H
	.line	25
;----------------------------------------------------------------------
; 117 | if((eqeptmr != 0))                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_eqeptmr
        MOV32     R0H,@_eqeptmr
        CMPF32    R0H,#0                ; |117| 
        MOVST0    ZF, NF                ; |117| 
        BF        $C$L4,EQ              ; |117| 
        ; branchcc occurs ; |117| 
	.line	27
;----------------------------------------------------------------------
; 119 | if (metodo == 1)                                                       
; 120 |         veloc_radsec = (float)velocfactor/(float)eqeptmr;              
;----------------------------------------------------------------------
        MOV32     R0H,@_metodo
        CMPF32    R0H,#16256            ; |119| 
        MOVST0    ZF, NF                ; |119| 
        BF        $C$L3,EQ              ; |119| 
        ; branchcc occurs ; |119| 
	.line	29
;----------------------------------------------------------------------
; 121 | else if (metodo == 2)                                                  
;----------------------------------------------------------------------
        CMPF32    R0H,#16384            ; |121| 
        MOVST0    ZF, NF                ; |121| 
        BF        $C$L5,NEQ             ; |121| 
        ; branchcc occurs ; |121| 
	.line	30
;----------------------------------------------------------------------
; 122 | veloc_radsec = (float)velocfactor2/(float)eqeptmr;                     
; 124 | else                                                                   
;----------------------------------------------------------------------
        MOV32     R0H,@_velocfactor2
        MOV32     R1H,@_eqeptmr
        LCR       #FS$$DIV              ; |122| 
        ; call occurs [#FS$$DIV] ; |122| 
        MOVW      DP,#_veloc_radsec
        MOV32     @_veloc_radsec,R0H
        B         $C$L5,UNC             ; |122| 
        ; branch occurs ; |122| 
$C$L3:    
	.line	28
        MOV32     R0H,@_velocfactor
        MOV32     R1H,@_eqeptmr
        LCR       #FS$$DIV              ; |120| 
        ; call occurs [#FS$$DIV] ; |120| 
        MOVW      DP,#_veloc_radsec
        MOV32     @_veloc_radsec,R0H
        B         $C$L5,UNC             ; |120| 
        ; branch occurs ; |120| 
$C$L4:    
	.line	33
;----------------------------------------------------------------------
; 125 | veloc_radsec = (float) hvel_radsec;                                    
; 127 | // Capture Module error                                                
;----------------------------------------------------------------------
        MOVW      DP,#_hvel_radsec
        MOVL      ACC,@_hvel_radsec     ; |125| 
        MOVW      DP,#_veloc_radsec
        MOVL      @_veloc_radsec,ACC    ; |125| 
$C$L5:    
	.line	37
;----------------------------------------------------------------------
; 129 | if(EQep1Regs.QEPSTS.bit.COEF)                                          
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+28
        TBIT      @_EQep1Regs+28,#3     ; |129| 
        BF        $C$L6,NTC             ; |129| 
        ; branchcc occurs ; |129| 
	.line	40
;----------------------------------------------------------------------
; 132 | EQep1Regs.QEPSTS.bit.COEF       = 1;                                   
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+28,#0xfff7 ; |132| 
        ORB       AL,#0x08              ; |132| 
        MOV       @_EQep1Regs+28,AL     ; |132| 
	.line	41
;----------------------------------------------------------------------
; 133 | veloc_radsec                            = 0;                           
; 137 | // Filtro                                                              
;----------------------------------------------------------------------
        ZERO      R0H                   ; |133| 
        MOVW      DP,#_veloc_radsec
        MOV32     @_veloc_radsec,R0H
$C$L6:    
	.line	47
;----------------------------------------------------------------------
; 139 | wm_kp1max       = wm_km1 + VSTEPMAX;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_wm_km1
        MOVIZ     R0H,#15948            ; |139| 
        MOV32     R1H,@_wm_km1
        MOVXI     R0H,#52429            ; |139| 
        ADDF32    R0H,R0H,R1H           ; |139| 
        MOVW      DP,#_wm_kp1max
        MOV32     @_wm_kp1max,R0H
	.line	48
;----------------------------------------------------------------------
; 140 | wm_kp1min       = wm_km1 - VSTEPMAX;                                   
;----------------------------------------------------------------------
        MOVIZ     R0H,#15948            ; |140| 
        MOVXI     R0H,#52429            ; |140| 
        SUBF32    R0H,R1H,R0H           ; |140| 
        NOP
        MOV32     @_wm_kp1min,R0H
	.line	49
;----------------------------------------------------------------------
; 141 | wm_k            = (float) veloc_radsec;                                
;----------------------------------------------------------------------
        MOVL      ACC,@_veloc_radsec    ; |141| 
        MOVW      DP,#_wm_k
        MOVL      @_wm_k,ACC            ; |141| 
	.line	51
;----------------------------------------------------------------------
; 143 | if ((wm_k > wm_kp1max))                                                
;----------------------------------------------------------------------
        MOVW      DP,#_wm_kp1max
        MOV32     R0H,@_wm_kp1max
        MOVW      DP,#_wm_k
        MOV32     R1H,@_wm_k
        CMPF32    R1H,R0H               ; |143| 
        MOVST0    ZF, NF                ; |143| 
        B         $C$L7,LEQ             ; |143| 
        ; branchcc occurs ; |143| 
	.line	52
;----------------------------------------------------------------------
; 144 | wm_k = wm_kp1max;                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_wm_kp1max
        MOVL      ACC,@_wm_kp1max       ; |144| 
        MOVW      DP,#_wm_k
        MOVL      @_wm_k,ACC            ; |144| 
$C$L7:    
	.line	53
;----------------------------------------------------------------------
; 145 | if ((wm_k < wm_kp1min))                                                
;----------------------------------------------------------------------
        MOVW      DP,#_wm_kp1min
        MOV32     R0H,@_wm_kp1min
        MOVW      DP,#_wm_k
        MOV32     R1H,@_wm_k
        CMPF32    R1H,R0H               ; |145| 
        MOVST0    ZF, NF                ; |145| 
        B         $C$L8,GEQ             ; |145| 
        ; branchcc occurs ; |145| 
	.line	54
;----------------------------------------------------------------------
; 146 | wm_k = wm_kp1min;                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_wm_kp1min
        MOVL      ACC,@_wm_kp1min       ; |146| 
        MOVW      DP,#_wm_k
        MOVL      @_wm_k,ACC            ; |146| 
$C$L8:    
	.line	55
;----------------------------------------------------------------------
; 147 | if ((wm_km1 > wm_MAX))                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_wm_km1
        MOV32     R0H,@_wm_km1
        CMPF32    R0H,#17116            ; |147| 
        MOVST0    ZF, NF                ; |147| 
        B         $C$L9,LEQ             ; |147| 
        ; branchcc occurs ; |147| 
	.line	56
;----------------------------------------------------------------------
; 148 | wm_km1 = wm_MAX;                                                       
;----------------------------------------------------------------------
        MOVIZ     R0H,#17116            ; |148| 
        MOV32     @_wm_km1,R0H
$C$L9:    
	.line	57
;----------------------------------------------------------------------
; 149 | if ((wm_km1 < -wm_MAX))                                                
;----------------------------------------------------------------------
        CMPF32    R0H,#49884            ; |149| 
        MOVST0    ZF, NF                ; |149| 
        B         $C$L10,GEQ            ; |149| 
        ; branchcc occurs ; |149| 
	.line	58
;----------------------------------------------------------------------
; 150 | wm_km1 = -wm_MAX;                                                      
;----------------------------------------------------------------------
        MOVIZ     R0H,#49884            ; |150| 
        MOV32     @_wm_km1,R0H
$C$L10:    
	.line	59
;----------------------------------------------------------------------
; 151 | if ((wm_k > wm_MAX) || (wm_k < -wm_MAX))                               
;----------------------------------------------------------------------
        MOVW      DP,#_wm_k
        MOV32     R0H,@_wm_k
        CMPF32    R0H,#17116            ; |151| 
        MOVST0    ZF, NF                ; |151| 
        B         $C$L11,GT             ; |151| 
        ; branchcc occurs ; |151| 
        CMPF32    R0H,#49884            ; |151| 
        MOVST0    ZF, NF                ; |151| 
        B         $C$L12,GEQ            ; |151| 
        ; branchcc occurs ; |151| 
$C$L11:    
	.line	60
;----------------------------------------------------------------------
; 152 | wm_k = wm_km1;                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_wm_km1
        MOVL      ACC,@_wm_km1          ; |152| 
        MOVW      DP,#_wm_k
        MOVL      @_wm_k,ACC            ; |152| 
$C$L12:    
	.line	63
;----------------------------------------------------------------------
; 155 | if ((wm_k < 4.2 && metodo == 1) || (wm_k > -4.2 && metodo == 1)) //6   
;----------------------------------------------------------------------
        MOVIZ     R0H,#16518            ; |155| 
        MOV32     R1H,@_wm_k
        MOVXI     R0H,#26214            ; |155| 
        CMPF32    R1H,R0H               ; |155| 
        MOVST0    ZF, NF                ; |155| 
        B         $C$L13,GEQ            ; |155| 
        ; branchcc occurs ; |155| 
        MOVW      DP,#_metodo
        MOV32     R0H,@_metodo
        CMPF32    R0H,#16256            ; |155| 
        MOVST0    ZF, NF                ; |155| 
        BF        $C$L14,EQ             ; |155| 
        ; branchcc occurs ; |155| 
$C$L13:    
        MOVIZ     R0H,#49286            ; |155| 
        MOVXI     R0H,#26214            ; |155| 
        CMPF32    R1H,R0H               ; |155| 
        MOVST0    ZF, NF                ; |155| 
        B         $C$L15,LEQ            ; |155| 
        ; branchcc occurs ; |155| 
        MOVW      DP,#_metodo
        MOV32     R0H,@_metodo
        CMPF32    R0H,#16256            ; |155| 
        MOVST0    ZF, NF                ; |155| 
        BF        $C$L15,NEQ            ; |155| 
        ; branchcc occurs ; |155| 
$C$L14:    
	.line	65
;----------------------------------------------------------------------
; 157 | metodo = 2;                                                            
;----------------------------------------------------------------------
        MOVIZ     R0H,#16384            ; |157| 
        MOV32     @_metodo,R0H
	.line	66
;----------------------------------------------------------------------
; 158 | EQep1Regs.QCAPCTL.bit.UPPS = UPEVENTDIV2;                              
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+22
        AND       AL,@_EQep1Regs+22,#0xfff0 ; |158| 
        ORB       AL,#0x01              ; |158| 
        MOV       @_EQep1Regs+22,AL     ; |158| 
	.line	67
;----------------------------------------------------------------------
; 159 | EQep1Regs.QCAPCTL.bit.CCPS = SYSCLKDIV32;                              
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+22,#0xff8f ; |159| 
        ORB       AL,#0x50              ; |159| 
        MOV       @_EQep1Regs+22,AL     ; |159| 
$C$L15:    
	.line	70
;----------------------------------------------------------------------
; 162 | if ((wm_k > 6 && metodo == 2) || (wm_k < -8 && metodo == 2)) //7       
;----------------------------------------------------------------------
        MOVW      DP,#_wm_k
        MOV32     R0H,@_wm_k
        CMPF32    R0H,#16576            ; |162| 
        MOVST0    ZF, NF                ; |162| 
        B         $C$L16,LEQ            ; |162| 
        ; branchcc occurs ; |162| 
        MOVW      DP,#_metodo
        MOV32     R0H,@_metodo
        CMPF32    R0H,#16384            ; |162| 
        MOVST0    ZF, NF                ; |162| 
        BF        $C$L17,EQ             ; |162| 
        ; branchcc occurs ; |162| 
$C$L16:    
        MOVW      DP,#_wm_k
        MOV32     R0H,@_wm_k
        CMPF32    R0H,#49408            ; |162| 
        MOVST0    ZF, NF                ; |162| 
        B         $C$L18,GEQ            ; |162| 
        ; branchcc occurs ; |162| 
        MOVW      DP,#_metodo
        MOV32     R0H,@_metodo
        CMPF32    R0H,#16384            ; |162| 
        MOVST0    ZF, NF                ; |162| 
        BF        $C$L18,NEQ            ; |162| 
        ; branchcc occurs ; |162| 
$C$L17:    
	.line	72
;----------------------------------------------------------------------
; 164 | metodo = 1;                                                            
;----------------------------------------------------------------------
        MOVIZ     R0H,#16256            ; |164| 
        MOV32     @_metodo,R0H
	.line	73
;----------------------------------------------------------------------
; 165 | EQep1Regs.QCAPCTL.bit.UPPS = UPEVENTDIV2;                              
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+22
        AND       AL,@_EQep1Regs+22,#0xfff0 ; |165| 
        ORB       AL,#0x01              ; |165| 
        MOV       @_EQep1Regs+22,AL     ; |165| 
	.line	74
;----------------------------------------------------------------------
; 166 | EQep1Regs.QCAPCTL.bit.CCPS = SYSCLKDIV1;                               
;----------------------------------------------------------------------
        AND       @_EQep1Regs+22,#0xff8f ; |166| 
$C$L18:    
	.line	77
;----------------------------------------------------------------------
; 169 | wm_km1          = (float) wm_k;                                        
;----------------------------------------------------------------------
        MOVW      DP,#_wm_k
        MOVL      ACC,@_wm_k            ; |169| 
        MOVW      DP,#_wm_km1
        MOVL      @_wm_km1,ACC          ; |169| 
	.line	79
;----------------------------------------------------------------------
; 171 | veloc_hz        = (float) veloc_radsec * (float) RADPSTOHZ;            
;----------------------------------------------------------------------
        MOVIZ     R0H,#15906            ; |171| 
        MOVW      DP,#_veloc_radsec
        MOVXI     R0H,#63879            ; |171| 
        MOV32     R1H,@_veloc_radsec
        MPYF32    R0H,R0H,R1H           ; |171| 
        MOVW      DP,#_veloc_hz
        MOV32     @_veloc_hz,R0H
	.line	81
;----------------------------------------------------------------------
; 173 | EQep1Regs.QCLR.bit.UTO                                  = 1;
;     |                  // Clears Unit Time Out Interrupt Flag                
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+26
        AND       AL,@_EQep1Regs+26,#0xf7ff ; |173| 
        OR        AL,#0x0800            ; |173| 
        MOV       @_EQep1Regs+26,AL     ; |173| 
	.line	82
;----------------------------------------------------------------------
; 174 | EQep1Regs.QCLR.bit.INT                                  = 1;
;     |                  // Clears Global EQEP1 Interrupt Flag                 
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+26,#0xfffe ; |174| 
        ORB       AL,#0x01              ; |174| 
        MOV       @_EQep1Regs+26,AL     ; |174| 
	.line	83
;----------------------------------------------------------------------
; 175 | PieCtrlRegs.PIEACK.bit.ACK5                             = 1;
;     |                  // Clear the PIEACK of Group 5 for enables Interrupt R
;     | esquest at CPU Level                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       AL,@_PieCtrlRegs+1,#0xffef ; |175| 
        ORB       AL,#0x10              ; |175| 
        MOV       @_PieCtrlRegs+1,AL    ; |175| 
	.line	85
;----------------------------------------------------------------------
; 177 | }// interrupt void DTC5Feqep_isr()                                     
;----------------------------------------------------------------------
        MOV32     R3H,*--SP
        MOV32     R2H,*--SP
        MOV32     R1H,*--SP
        MOV32     R0H,*--SP
        MOV32     STF,*--SP
        MOVL      XT,*--SP
        MOVL      XAR7,*--SP
        MOVL      XAR6,*--SP
        MOVL      XAR5,*--SP
        MOVL      XAR4,*--SP
        POP       AR1H:AR0H
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	177,003333605h,24
	.sect	".text"
	.global	_PTC5Feqep_start
	.sym	_PTC5Feqep_start,_PTC5Feqep_start, 32, 2, 0
	.func	181
;----------------------------------------------------------------------
; 181 | void PTC5Feqep_start()                                                 
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _PTC5Feqep_start              FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Feqep_start:
	.line	2
;----------------------------------------------------------------------
; 184 | // GPIO Configure                                                      
;----------------------------------------------------------------------
	.line	5
;----------------------------------------------------------------------
; 185 | EALLOW;
;     |                                              // Enable writing to EALLO
;     | W protected registers                                                  
;----------------------------------------------------------------------
 EALLOW
	.line	6
;----------------------------------------------------------------------
; 186 | SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK             = 1;
;     |          // Enable the SYSCLKOUT to the GPIO                           
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+16
        AND       AL,@_SysCtrlRegs+16,#0xdfff ; |186| 
        OR        AL,#0x2000            ; |186| 
        MOV       @_SysCtrlRegs+16,AL   ; |186| 
	.line	7
;----------------------------------------------------------------------
; 187 | SysCtrlRegs.PCLKCR1.bit.EQEP1ENCLK              = 1;
;     |          // EQEP1 Module is Clocked by the SYSCLKOUT                   
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xbfff ; |187| 
        OR        AL,#0x4000            ; |187| 
        MOV       @_SysCtrlRegs+13,AL   ; |187| 
	.line	8
;----------------------------------------------------------------------
; 188 | GpioCtrlRegs.GPBMUX2.bit.GPIO50                 = 1;
;     |          // JP3 #13 GPIO50 as EQEP1A(Input)                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       AL,@_GpioCtrlRegs+24,#0xffcf ; |188| 
        ORB       AL,#0x10              ; |188| 
        MOV       @_GpioCtrlRegs+24,AL  ; |188| 
	.line	9
;----------------------------------------------------------------------
; 189 | GpioCtrlRegs.GPBMUX2.bit.GPIO51                     = 1;
;     |              // JP3 #14 GPIO51 as EQEP1B(Input)                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+24,#0xff3f ; |189| 
        ORB       AL,#0x40              ; |189| 
        MOV       @_GpioCtrlRegs+24,AL  ; |189| 
	.line	10
;----------------------------------------------------------------------
; 190 | GpioCtrlRegs.GPBMUX2.bit.GPIO53                     = 1;
;     |              // JP3 #14 GPIO53 as EQEP1I(Input)                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+24,#0xf3ff ; |190| 
        OR        AL,#0x0400            ; |190| 
        MOV       @_GpioCtrlRegs+24,AL  ; |190| 
	.line	11
;----------------------------------------------------------------------
; 191 | EDIS;
;     |                                          // Disable writing to EALLOW p
;     | rotected registers                                                     
; 192 | // End GPIO Configure                                                  
; 194 | // QDU Module Configuration                                            
;----------------------------------------------------------------------
 EDIS
	.line	15
;----------------------------------------------------------------------
; 195 | EQep1Regs.QDECCTL.bit.QSRC                          = 00;
;     |              // EQEP1 as Quadrature Count Mode                         
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+20
        AND       @_EQep1Regs+20,#0x3fff ; |195| 
	.line	16
;----------------------------------------------------------------------
; 196 | EQep1Regs.QDECCTL.bit.QAP                           = 0;
;     |              // EQEP1A input polarity No Efect                         
; 197 |     //EQep1Regs.QDECCTL.bit.QAP             = 1;
;     |      // EQEP1A input negate polarity                                   
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xfeff ; |196| 
	.line	18
;----------------------------------------------------------------------
; 198 | EQep1Regs.QDECCTL.bit.QBP                           = 0;
;     |              // EQEP1B input polarity No Efect                         
; 199 |     //EQep1Regs.QDECCTL.bit.QBP             = 1;
;     |      // EQEP1B input negate polarity                                   
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xff7f ; |198| 
	.line	20
;----------------------------------------------------------------------
; 200 | EQep1Regs.QDECCTL.bit.QIP                           = 0;
;     |              // EQEP1I input polarity No Efect                         
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xffbf ; |200| 
	.line	21
;----------------------------------------------------------------------
; 201 | EQep1Regs.QDECCTL.bit.QSP                           = 0;
;     |              // EQEP1S polarity No Efect                               
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xffdf ; |201| 
	.line	22
;----------------------------------------------------------------------
; 202 | EQep1Regs.QDECCTL.bit.SWAP                          = 0;
;     |              // Quadrature-clock inputs are not swaped                 
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xfbff ; |202| 
	.line	23
;----------------------------------------------------------------------
; 203 | EQep1Regs.QDECCTL.bit.IGATE                         = 0;
;     |              // Disable gating of index pulse                          
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xfdff ; |203| 
	.line	24
;----------------------------------------------------------------------
; 204 | EQep1Regs.QDECCTL.bit.XCR                           = 0;
;     |              // 2x Resolution Count                                    
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xf7ff ; |204| 
	.line	25
;----------------------------------------------------------------------
; 205 | EQep1Regs.QDECCTL.bit.SOEN                          = 0;
;     |              // Disable position-compare syn output                    
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xdfff ; |205| 
	.line	26
;----------------------------------------------------------------------
; 206 | EQep1Regs.QDECCTL.bit.SPSEL                         = 0;
;     |              // Index pin is used for sync output                      
; 207 | // End QDU Module Configuration                                        
; 209 | // PCCU Module Configuration                                           
;----------------------------------------------------------------------
        AND       @_EQep1Regs+20,#0xefff ; |206| 
	.line	30
;----------------------------------------------------------------------
; 210 | EQep1Regs.QEPCTL.bit.WDE                            = 0;
;     |              // Disable the EQEP watchdog timer                        
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xfffe ; |210| 
	.line	31
;----------------------------------------------------------------------
; 211 | EQep1Regs.QEPCTL.bit.QCLM                               = 1;
;     |                  // EQEP capture latch on Unit Time Out                
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xfffb ; |211| 
        ORB       AL,#0x04              ; |211| 
        MOV       @_EQep1Regs+21,AL     ; |211| 
	.line	32
;----------------------------------------------------------------------
; 212 | EQep1Regs.QEPCTL.bit.QPEN                           = 1;
;     |              // Enable EQEP position counter                           
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xfff7 ; |212| 
        ORB       AL,#0x08              ; |212| 
        MOV       @_EQep1Regs+21,AL     ; |212| 
	.line	33
;----------------------------------------------------------------------
; 213 | EQep1Regs.QEPCTL.bit.PCRM                           = 3;
;     |              // Position Counter Reset on Unit Time Event              
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xcfff ; |213| 
        OR        AL,#0x3000            ; |213| 
        MOV       @_EQep1Regs+21,AL     ; |213| 
	.line	34
;----------------------------------------------------------------------
; 214 | EQep1Regs.QEPCTL.bit.SEI                            = 0;
;     |              // Strobe Event actions disable                           
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xf3ff ; |214| 
	.line	35
;----------------------------------------------------------------------
; 215 | EQep1Regs.QEPCTL.bit.IEI                            = 0;
;     |              // Index Event actions disable                            
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xfcff ; |215| 
	.line	36
;----------------------------------------------------------------------
; 216 | EQep1Regs.QEPCTL.bit.SWI                            = 0;
;     |              // Software Initialization action enable                  
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xff7f ; |216| 
	.line	37
;----------------------------------------------------------------------
; 217 | EQep1Regs.QEPCTL.bit.IEL                            = 0;
;     |              // Index Event Latch Reserved                             
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xffcf ; |217| 
	.line	38
;----------------------------------------------------------------------
; 218 | EQep1Regs.QEPCTL.bit.SWI                            = 0;
;     |              // Enable Software initialization                         
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0xff7f ; |218| 
	.line	39
;----------------------------------------------------------------------
; 219 | EQep1Regs.QEPCTL.bit.FREE_SOFT                  = 0x10;
;     |          // Position Counter is Unaffected by emulation suspend        
; 220 | // End PCCU Module Configuration                                       
;----------------------------------------------------------------------
        AND       @_EQep1Regs+21,#0x3fff ; |219| 
	.line	42
;----------------------------------------------------------------------
; 222 | EQep1Regs.QPOSINIT                                          = 0;
;     |                      // EQEP Counter Initial Position                  
;----------------------------------------------------------------------
        MOVB      ACC,#0
        MOVL      @_EQep1Regs+2,ACC     ; |222| 
	.line	43
;----------------------------------------------------------------------
; 223 | EQep1Regs.QPOSMAX                                           = QEPCNTMAX
;     | ;            // EQEP Counter Max Position                              
;----------------------------------------------------------------------
        MOVL      XAR4,#10000           ; |223| 
        MOVL      @_EQep1Regs+4,XAR4    ; |223| 
	.line	44
;----------------------------------------------------------------------
; 224 | EQep1Regs.QPOSCMP                                           = QEPCNTMAX
;     | ;            // EQEP Position Compare                                  
;----------------------------------------------------------------------
        MOVL      @_EQep1Regs+6,XAR4    ; |224| 
	.line	45
;----------------------------------------------------------------------
; 225 | EQep1Regs.QCTMR                                                     = 0
;     | ;                            // EQEP Position Compare                  
; 227 | // Position-Compare Configuration                                      
;----------------------------------------------------------------------
        MOV       @_EQep1Regs+29,#0     ; |225| 
	.line	48
;----------------------------------------------------------------------
; 228 | EQep1Regs.QPOSCTL.bit.PCSHDW                        = 0;
;     |              // EQEP Position-Compare Load Inmediate                   
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0x7fff ; |228| 
	.line	49
;----------------------------------------------------------------------
; 229 | EQep1Regs.QPOSCTL.bit.PCLOAD                        = 0;
;     |              // Position Compare Loads in QPOSCNT=0                    
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0xbfff ; |229| 
	.line	50
;----------------------------------------------------------------------
; 230 | EQep1Regs.QPOSCTL.bit.PCPOL                         = 0;
;     |              // Polarity of sync output Active High pulse output       
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0xdfff ; |230| 
	.line	51
;----------------------------------------------------------------------
; 231 | EQep1Regs.QPOSCTL.bit.PCE                           = 0;
;     |              // Position Compare Disable                               
; 232 | // End Position-Compare Configuration                                  
; 234 | // Edge Capture Unit Configuration w_measure = velocfactor * X / dT    
;----------------------------------------------------------------------
        AND       @_EQep1Regs+23,#0xefff ; |231| 
	.line	56
;----------------------------------------------------------------------
; 236 | EQep1Regs.QCAPCTL.bit.UPPS                              = UPEVENTDIV1;
;     |          // EQEP Unit Event /1 (X=1)                                   
; 237 | //EQep1Regs.QCAPCTL.bit.UPPS                                = UPEVENTDI
;     | V2;          // EQEP Unit Event /2 (X=2)                               
; 238 | //EQep1Regs.QCAPCTL.bit.UPPS                            = UPEVENTDIV4;
;     |          // EQEP Unit Event /4 (X=4)                                   
; 239 | //EQep1Regs.QCAPCTL.bit.UPPS                            = UPEVENTDIV8;
;     |          // EQEP Unit Event /8 (X=8)                                   
; 240 | //EQep1Regs.QCAPCTL.bit.CCPS                                = SYSCLKDIV
;     | 1;           // EQEP Capture Timer Prescaler /1                        
; 241 | //EQep1Regs.QCAPCTL.bit.CCPS                            = SYSCLKDIV2;
;     |          // EQEP Capture Timer Prescaler /2                            
; 242 | //EQep1Regs.QCAPCTL.bit.CCPS                            = SYSCLKDIV4;
;     |          // EQEP Capture Timer Prescaler /4                            
; 243 | //EQep1Regs.QCAPCTL.bit.CCPS                            = SYSCLKDIV8;
;     |          // EQEP Capture Timer Prescaler /8                            
; 244 | //EQep1Regs.QCAPCTL.bit.CCPS                            = SYSCLKDIV16;
;     |          // EQEP Capture Timer Prescaler /16                           
;----------------------------------------------------------------------
        AND       @_EQep1Regs+22,#0xfff0 ; |236| 
	.line	65
;----------------------------------------------------------------------
; 245 | EQep1Regs.QCAPCTL.bit.CCPS                              = SYSCLKDIV32;
;     |          // EQEP Capture Timer Prescaler /32                           
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+22,#0xff8f ; |245| 
        ORB       AL,#0x50              ; |245| 
        MOV       @_EQep1Regs+22,AL     ; |245| 
	.line	66
;----------------------------------------------------------------------
; 246 | EQep1Regs.QCAPCTL.bit.CEN                           = 1;
;     |              // EQEP Capture is Enable                                 
; 247 | // End Edge Capture Unit Configuration                                 
; 249 | //UTIME Configuration                                                  
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+22,#0x7fff ; |246| 
        OR        AL,#0x8000            ; |246| 
        MOV       @_EQep1Regs+22,AL     ; |246| 
	.line	70
;----------------------------------------------------------------------
; 250 | EQep1Regs.QUPRD                                                     = Q
;     | EPUTOPRD;            // Unit Time Out Period                           
;----------------------------------------------------------------------
        MOVL      XAR4,#30000           ; |250| 
        MOVL      @_EQep1Regs+16,XAR4   ; |250| 
	.line	71
;----------------------------------------------------------------------
; 251 | EQep1Regs.QEPCTL.bit.UTE                                = 1;
;     |                  // Enable the EQEP Unit Timer                         
; 252 | //End UTIME Configuration                                              
;----------------------------------------------------------------------
        AND       AL,@_EQep1Regs+21,#0xfffd ; |251| 
        ORB       AL,#0x02              ; |251| 
        MOV       @_EQep1Regs+21,AL     ; |251| 
	.line	74
;----------------------------------------------------------------------
; 254 | EALLOW;
;     |                                              // This is needed to write
;     |  to EALLOW protected registers                                         
;----------------------------------------------------------------------
 EALLOW
	.line	75
;----------------------------------------------------------------------
; 255 | PieVectTable.EQEP1_INT                                  = &PTC5Feqep_is
;     | r;       // EQEP Interrupt Address                                     
;----------------------------------------------------------------------
        MOVL      XAR4,#_PTC5Feqep_isr  ; |255| 
        MOVW      DP,#_PieVectTable+128
        MOVL      @_PieVectTable+128,XAR4 ; |255| 
	.line	76
;----------------------------------------------------------------------
; 256 | EDIS;
;     |                                          // Disable writing to EALLOW p
;     | rotected registers                                                     
; 258 | // Unit Time Out Interrupt                                             
;----------------------------------------------------------------------
 EDIS
	.line	79
;----------------------------------------------------------------------
; 259 | IER |= M_INT5;
;     |                                  // Enable EQEP1 CPU-PIEIER5 for INT5 (
;     | Group 5)                                                               
;----------------------------------------------------------------------
        OR        IER,#0x0010           ; |259| 
	.line	80
;----------------------------------------------------------------------
; 260 | PieCtrlRegs.PIEIER5.bit.INTx1                   = 1;
;     |          // Enable the EQEP1_INT PIEIER5.1 to interrupt resquest sent t
;     | o CPU Level                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+10
        AND       AL,@_PieCtrlRegs+10,#0xfffe ; |260| 
        ORB       AL,#0x01              ; |260| 
        MOV       @_PieCtrlRegs+10,AL   ; |260| 
	.line	81
;----------------------------------------------------------------------
; 261 | PieCtrlRegs.PIEACK.bit.ACK5                             = 1;
;     |                  // Clear the PIEACK of Group 5 for enables Interrupt R
;     | esquest at CPU Level                                                   
;----------------------------------------------------------------------
        AND       AL,@_PieCtrlRegs+1,#0xffef ; |261| 
        ORB       AL,#0x10              ; |261| 
        MOV       @_PieCtrlRegs+1,AL    ; |261| 
	.line	82
;----------------------------------------------------------------------
; 262 | EQep1Regs.QEINT.bit.UTO                                 = 1;
;     |                  // Unit Time Out Interrupt Enable                     
; 263 | // End Unit Time Out Interrupt                                         
;----------------------------------------------------------------------
        MOVW      DP,#_EQep1Regs+24
        AND       AL,@_EQep1Regs+24,#0xf7ff ; |262| 
        OR        AL,#0x0800            ; |262| 
        MOV       @_EQep1Regs+24,AL     ; |262| 
	.line	85
;----------------------------------------------------------------------
; 265 | }// void DTC5Feqep_start()                                             
;----------------------------------------------------------------------
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	265,000000000h,0
	.sect	".text"
	.global	_PTC5Fepwm_isr
	.file	"C:\CCStudio_v3.3\MyProjects\Calibracion2021_3\PTC5F_epwm.c"
	.sym	_PTC5Fepwm_isr,_PTC5Fepwm_isr, 32, 2, 0
	.func	134

;***************************************************************
;* FNAME: _PTC5Fepwm_isr                FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  4 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fepwm_isr:
	.line	1
;----------------------------------------------------------------------
; 134 | interrupt void PTC5Fepwm_isr(){                    // ePWM Interrupt Se
;     | rvice Rutine                                                           
;----------------------------------------------------------------------
        ASP
        PUSH      RB
        MOV32     *SP++,STF
        SETFLG    RNDF32=1, RNDF64=1
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	3
;----------------------------------------------------------------------
; 136 | timeout                               = 0;       // Clear PWM Time Out
;     | Flag                                                                   
;----------------------------------------------------------------------
        MOVW      DP,#_timeout
        MOV       @_timeout,#0          ; |136| 
	.line	5
;----------------------------------------------------------------------
; 138 | EPwm1Regs.ETCLR.bit.INT               = 1;       // Clear EPWM1_INT Fla
;     | g                                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+28
        AND       AL,@_EPwm1Regs+28,#0xfffe ; |138| 
        ORB       AL,#0x01              ; |138| 
        MOV       @_EPwm1Regs+28,AL     ; |138| 
	.line	6
;----------------------------------------------------------------------
; 139 | PieCtrlRegs.PIEACK.bit.ACK3           = 1;       // Clear the PIEACK of
;     |  Group 3 for enables Interrupt Resquest at CPU Level                   
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       AL,@_PieCtrlRegs+1,#0xfffb ; |139| 
        ORB       AL,#0x04              ; |139| 
        MOV       @_PieCtrlRegs+1,AL    ; |139| 
	.line	8
;----------------------------------------------------------------------
; 141 | }// interrupt void IRFOC5Fepwm_isr()                                   
;----------------------------------------------------------------------
        MOV32     STF,*--SP
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	141,000000000h,4
	.sect	".text"
	.global	_PTC5Fepwm5F_start
	.sym	_PTC5Fepwm5F_start,_PTC5Fepwm5F_start, 32, 2, 0
	.func	147

;***************************************************************
;* FNAME: _PTC5Fepwm5F_start            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fepwm5F_start:
	.line	1
;----------------------------------------------------------------------
; 147 | void PTC5Fepwm5F_start(){                                              
;----------------------------------------------------------------------
	.line	3
;----------------------------------------------------------------------
; 149 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	4
;----------------------------------------------------------------------
; 150 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC     = 0;       // Stop all the TB clo
;     | cks                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       @_SysCtrlRegs+12,#0xfffb ; |150| 
	.line	5
;----------------------------------------------------------------------
; 151 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers                                             
; 153 | //  Enable the  ePWM-CLK 1-6                                           
;----------------------------------------------------------------------
 EDIS
	.line	8
;----------------------------------------------------------------------
; 154 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	9
;----------------------------------------------------------------------
; 155 | SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK    = 1;       // Enable ePWM1 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffe ; |155| 
        ORB       AL,#0x01              ; |155| 
        MOV       @_SysCtrlRegs+13,AL   ; |155| 
	.line	10
;----------------------------------------------------------------------
; 156 | SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK    = 1;       // Enable ePWM2 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffd ; |156| 
        ORB       AL,#0x02              ; |156| 
        MOV       @_SysCtrlRegs+13,AL   ; |156| 
	.line	11
;----------------------------------------------------------------------
; 157 | SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK    = 1;       // Enable ePWM3 clock*
;     | /                                                                      
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffb ; |157| 
        ORB       AL,#0x04              ; |157| 
        MOV       @_SysCtrlRegs+13,AL   ; |157| 
	.line	12
;----------------------------------------------------------------------
; 158 | SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK    = 1;       // Enable ePWM4 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfff7 ; |158| 
        ORB       AL,#0x08              ; |158| 
        MOV       @_SysCtrlRegs+13,AL   ; |158| 
	.line	13
;----------------------------------------------------------------------
; 159 | SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK    = 1;       // Enable ePWM5 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xffef ; |159| 
        ORB       AL,#0x10              ; |159| 
        MOV       @_SysCtrlRegs+13,AL   ; |159| 
	.line	14
;----------------------------------------------------------------------
; 160 | EDIS;                                                                  
; 161 | //  End Enable the      ePWM-CLK 1-6                                   
; 163 | //  Configure the ePWM1-6 shared pins                                  
;----------------------------------------------------------------------
 EDIS
	.line	18
;----------------------------------------------------------------------
; 164 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	19
;----------------------------------------------------------------------
; 165 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 1;       // Configure GPIO0 as
;     | EPWM1A                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       AL,@_GpioCtrlRegs+6,#0xfffc ; |165| 
        ORB       AL,#0x01              ; |165| 
        MOV       @_GpioCtrlRegs+6,AL   ; |165| 
	.line	20
;----------------------------------------------------------------------
; 166 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 1;       // Configure GPIO1 as
;     | EPWM1B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfff3 ; |166| 
        ORB       AL,#0x04              ; |166| 
        MOV       @_GpioCtrlRegs+6,AL   ; |166| 
	.line	21
;----------------------------------------------------------------------
; 167 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 1;       // Configure GPIO2 as
;     | EPWM2A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xffcf ; |167| 
        ORB       AL,#0x10              ; |167| 
        MOV       @_GpioCtrlRegs+6,AL   ; |167| 
	.line	22
;----------------------------------------------------------------------
; 168 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 1;       // Configure GPIO3 as
;     | EPWM2B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xff3f ; |168| 
        ORB       AL,#0x40              ; |168| 
        MOV       @_GpioCtrlRegs+6,AL   ; |168| 
	.line	23
;----------------------------------------------------------------------
; 169 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 1;       // Configure GPIO4 as
;     | EPWM3A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfcff ; |169| 
        OR        AL,#0x0100            ; |169| 
        MOV       @_GpioCtrlRegs+6,AL   ; |169| 
	.line	24
;----------------------------------------------------------------------
; 170 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 1;       // Configure GPIO5 as
;     | EPWM3B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xf3ff ; |170| 
        OR        AL,#0x0400            ; |170| 
        MOV       @_GpioCtrlRegs+6,AL   ; |170| 
	.line	25
;----------------------------------------------------------------------
; 171 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 1;       // Configure GPIO6 as
;     | EPWM4A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xcfff ; |171| 
        OR        AL,#0x1000            ; |171| 
        MOV       @_GpioCtrlRegs+6,AL   ; |171| 
	.line	26
;----------------------------------------------------------------------
; 172 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 1;       // Configure GPIO7 as
;     | EPWM4B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0x3fff ; |172| 
        OR        AL,#0x4000            ; |172| 
        MOV       @_GpioCtrlRegs+6,AL   ; |172| 
	.line	27
;----------------------------------------------------------------------
; 173 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 1;       // Configure GPIO8 as
;     | EPWM5A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfffc ; |173| 
        ORB       AL,#0x01              ; |173| 
        MOV       @_GpioCtrlRegs+7,AL   ; |173| 
	.line	28
;----------------------------------------------------------------------
; 174 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 1;       // Configure GPIO9 as
;     | EPWM5B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfff3 ; |174| 
        ORB       AL,#0x04              ; |174| 
        MOV       @_GpioCtrlRegs+7,AL   ; |174| 
	.line	29
;----------------------------------------------------------------------
; 175 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers*/                                           
; 176 | //  End Configure the ePWM1-5 shared pins                              
; 178 | // TB of ePWM 1-5 Configure                                            
;----------------------------------------------------------------------
 EDIS
	.line	33
;----------------------------------------------------------------------
; 179 | epwmmodule_config(&EPwm1Regs);                                         
;----------------------------------------------------------------------
        SPM       #0
        MOVL      XAR4,#_EPwm1Regs      ; |179| 
        LCR       #_epwmmodule_config   ; |179| 
        ; call occurs [#_epwmmodule_config] ; |179| 
	.line	34
;----------------------------------------------------------------------
; 180 | epwmmodule_config(&EPwm2Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm2Regs      ; |180| 
        LCR       #_epwmmodule_config   ; |180| 
        ; call occurs [#_epwmmodule_config] ; |180| 
	.line	35
;----------------------------------------------------------------------
; 181 | epwmmodule_config(&EPwm3Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm3Regs      ; |181| 
        LCR       #_epwmmodule_config   ; |181| 
        ; call occurs [#_epwmmodule_config] ; |181| 
	.line	36
;----------------------------------------------------------------------
; 182 | epwmmodule_config(&EPwm4Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm4Regs      ; |182| 
        LCR       #_epwmmodule_config   ; |182| 
        ; call occurs [#_epwmmodule_config] ; |182| 
	.line	37
;----------------------------------------------------------------------
; 183 | epwmmodule_config(&EPwm5Regs);                                         
; 184 | //  End TB Configure                                                   
; 187 | //  EPWM1 Module as Master Synchronization                             
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm5Regs      ; |183| 
        LCR       #_epwmmodule_config   ; |183| 
        ; call occurs [#_epwmmodule_config] ; |183| 
	.line	42
;----------------------------------------------------------------------
; 188 | EPwm1Regs.TBCTL.bit.PHSEN     = TB_DISABLE;      // Disables Phase Load
;     | ing (Master Mode ePWM1)                                                
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs
        AND       @_EPwm1Regs,#0xfffb   ; |188| 
	.line	43
;----------------------------------------------------------------------
; 189 | EPwm1Regs.TBCTL.bit.SYNCOSEL  = TB_CTR_ZERO;     // Generates EPWMxSYNC
;     |  output when CTR = Zero                                                
; 190 | //  End EPWM1 Module as Master Synchronization                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs,#0xffcf ; |189| 
        ORB       AL,#0x10              ; |189| 
        MOV       @_EPwm1Regs,AL        ; |189| 
	.line	48
;----------------------------------------------------------------------
; 194 | EALLOW;                                          // Allows CPU to write
;     |  to EALLOW protected registers                                         
;----------------------------------------------------------------------
 EALLOW
	.line	49
;----------------------------------------------------------------------
; 195 | PieVectTable.EPWM1_INT = &PTC5Fepwm_isr;         // ePWM1 Interrupt Rut
;     | ine Address                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_PieVectTable+96
        MOVL      XAR4,#_PTC5Fepwm_isr  ; |195| 
        MOVL      @_PieVectTable+96,XAR4 ; |195| 
	.line	50
;----------------------------------------------------------------------
; 196 | EDIS;
;     |                                                   // Disable CPU to wri
;     | te to EALLOW protected registers                                       
;----------------------------------------------------------------------
 EDIS
	.line	53
;----------------------------------------------------------------------
; 199 | EPwm1Regs.ETSEL.bit.INTSEL    = ET_CTR_PRD;      // Enable ePWM1 Module
;     |  Event Genaration when CTR=PRD                                         
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff8 ; |199| 
        ORB       AL,#0x02              ; |199| 
        MOV       @_EPwm1Regs+25,AL     ; |199| 
	.line	54
;----------------------------------------------------------------------
; 200 | EPwm1Regs.ETPS.bit.INTPRD     = ET_1ST;          // ePWM1 Module Event
;     | Prescaler DIV1                                                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+26,#0xfffc ; |200| 
        ORB       AL,#0x01              ; |200| 
        MOV       @_EPwm1Regs+26,AL     ; |200| 
	.line	56
;----------------------------------------------------------------------
; 202 | IER |= M_INT3;                                   // Enable EPWM1 CPU-PI
;     | EIER3 for INT5 (Group 3)                                               
;----------------------------------------------------------------------
        OR        IER,#0x0004           ; |202| 
	.line	57
;----------------------------------------------------------------------
; 203 | PieCtrlRegs.PIEIER3.bit.INTx1 = 1;               // Enable the EPWM1_IN
;     | T PIEIER3.1 to interrupt resquest sent to CPU Level                    
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+6
        AND       AL,@_PieCtrlRegs+6,#0xfffe ; |203| 
        ORB       AL,#0x01              ; |203| 
        MOV       @_PieCtrlRegs+6,AL    ; |203| 
	.line	58
;----------------------------------------------------------------------
; 204 | PieCtrlRegs.PIEACK.bit.ACK3   = 0;               // Clear the PIEACK of
;     |  Group 3 for enables Interrupt Resquest at CPU Level                   
;----------------------------------------------------------------------
        AND       @_PieCtrlRegs+1,#0xfffb ; |204| 
	.line	59
;----------------------------------------------------------------------
; 205 | EPwm1Regs.ETSEL.bit.INTEN     = ET_ETSEL_ENABLE; // Enables EPWM1_INT G
;     | eneration                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff7 ; |205| 
        ORB       AL,#0x08              ; |205| 
        MOV       @_EPwm1Regs+25,AL     ; |205| 
	.line	62
;----------------------------------------------------------------------
; 208 | }//void PTC5Fepwm5F_start()                                            
;----------------------------------------------------------------------
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	208,000000000h,0
	.sect	".text"
	.global	_PTC5Fepwm6F_start
	.sym	_PTC5Fepwm6F_start,_PTC5Fepwm6F_start, 32, 2, 0
	.func	220

;***************************************************************
;* FNAME: _PTC5Fepwm6F_start            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fepwm6F_start:
	.line	1
;----------------------------------------------------------------------
; 220 | void PTC5Fepwm6F_start(){                                              
;----------------------------------------------------------------------
	.line	3
;----------------------------------------------------------------------
; 222 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	4
;----------------------------------------------------------------------
; 223 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC     = 0;       // Stop all the TB clo
;     | cks                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       @_SysCtrlRegs+12,#0xfffb ; |223| 
	.line	5
;----------------------------------------------------------------------
; 224 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers                                             
; 226 | //  Enable the  ePWM-CLK 1-6                                           
;----------------------------------------------------------------------
 EDIS
	.line	8
;----------------------------------------------------------------------
; 227 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	9
;----------------------------------------------------------------------
; 228 | SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK    = 1;       // Enable ePWM1 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffe ; |228| 
        ORB       AL,#0x01              ; |228| 
        MOV       @_SysCtrlRegs+13,AL   ; |228| 
	.line	10
;----------------------------------------------------------------------
; 229 | SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK    = 1;       // Enable ePWM2 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffd ; |229| 
        ORB       AL,#0x02              ; |229| 
        MOV       @_SysCtrlRegs+13,AL   ; |229| 
	.line	11
;----------------------------------------------------------------------
; 230 | SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK    = 1;       // Enable ePWM3 clock*
;     | /                                                                      
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfffb ; |230| 
        ORB       AL,#0x04              ; |230| 
        MOV       @_SysCtrlRegs+13,AL   ; |230| 
	.line	12
;----------------------------------------------------------------------
; 231 | SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK    = 1;       // Enable ePWM4 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xfff7 ; |231| 
        ORB       AL,#0x08              ; |231| 
        MOV       @_SysCtrlRegs+13,AL   ; |231| 
	.line	13
;----------------------------------------------------------------------
; 232 | SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK    = 1;       // Enable ePWM5 clock 
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xffef ; |232| 
        ORB       AL,#0x10              ; |232| 
        MOV       @_SysCtrlRegs+13,AL   ; |232| 
	.line	14
;----------------------------------------------------------------------
; 233 | SysCtrlRegs.PCLKCR1.bit.EPWM6ENCLK    = 1;       // Enable ePWM6 clock*
;     | /                                                                      
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+13,#0xffdf ; |233| 
        ORB       AL,#0x20              ; |233| 
        MOV       @_SysCtrlRegs+13,AL   ; |233| 
	.line	15
;----------------------------------------------------------------------
; 234 | EDIS;                                                                  
; 235 | //  End Enable the      ePWM-CLK 1-6                                   
; 237 | //  Configure the ePWM1-6 shared pins                                  
;----------------------------------------------------------------------
 EDIS
	.line	19
;----------------------------------------------------------------------
; 238 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	20
;----------------------------------------------------------------------
; 239 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 1;       // Configure GPIO0 as
;     | EPWM1A                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       AL,@_GpioCtrlRegs+6,#0xfffc ; |239| 
        ORB       AL,#0x01              ; |239| 
        MOV       @_GpioCtrlRegs+6,AL   ; |239| 
	.line	21
;----------------------------------------------------------------------
; 240 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 1;       // Configure GPIO1 as
;     | EPWM1B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfff3 ; |240| 
        ORB       AL,#0x04              ; |240| 
        MOV       @_GpioCtrlRegs+6,AL   ; |240| 
	.line	22
;----------------------------------------------------------------------
; 241 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 1;       // Configure GPIO2 as
;     | EPWM2A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xffcf ; |241| 
        ORB       AL,#0x10              ; |241| 
        MOV       @_GpioCtrlRegs+6,AL   ; |241| 
	.line	23
;----------------------------------------------------------------------
; 242 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 1;       // Configure GPIO3 as
;     | EPWM2B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xff3f ; |242| 
        ORB       AL,#0x40              ; |242| 
        MOV       @_GpioCtrlRegs+6,AL   ; |242| 
	.line	24
;----------------------------------------------------------------------
; 243 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 1;       // Configure GPIO4 as
;     | EPWM3A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xfcff ; |243| 
        OR        AL,#0x0100            ; |243| 
        MOV       @_GpioCtrlRegs+6,AL   ; |243| 
	.line	25
;----------------------------------------------------------------------
; 244 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 1;       // Configure GPIO5 as
;     | EPWM3B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xf3ff ; |244| 
        OR        AL,#0x0400            ; |244| 
        MOV       @_GpioCtrlRegs+6,AL   ; |244| 
	.line	26
;----------------------------------------------------------------------
; 245 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 1;       // Configure GPIO6 as
;     | EPWM4A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0xcfff ; |245| 
        OR        AL,#0x1000            ; |245| 
        MOV       @_GpioCtrlRegs+6,AL   ; |245| 
	.line	27
;----------------------------------------------------------------------
; 246 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 1;       // Configure GPIO7 as
;     | EPWM4B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+6,#0x3fff ; |246| 
        OR        AL,#0x4000            ; |246| 
        MOV       @_GpioCtrlRegs+6,AL   ; |246| 
	.line	28
;----------------------------------------------------------------------
; 247 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 1;       // Configure GPIO8 as
;     | EPWM5A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfffc ; |247| 
        ORB       AL,#0x01              ; |247| 
        MOV       @_GpioCtrlRegs+7,AL   ; |247| 
	.line	29
;----------------------------------------------------------------------
; 248 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 1;       // Configure GPIO9 as
;     | EPWM5B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xfff3 ; |248| 
        ORB       AL,#0x04              ; |248| 
        MOV       @_GpioCtrlRegs+7,AL   ; |248| 
	.line	30
;----------------------------------------------------------------------
; 249 | GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 1;       // Configure GPIO8 as
;     | EPWM6A                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xffcf ; |249| 
        ORB       AL,#0x10              ; |249| 
        MOV       @_GpioCtrlRegs+7,AL   ; |249| 
	.line	31
;----------------------------------------------------------------------
; 250 | GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 1;       // Configure GPIO9 as
;     | EPWM6B                                                                 
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+7,#0xff3f ; |250| 
        ORB       AL,#0x40              ; |250| 
        MOV       @_GpioCtrlRegs+7,AL   ; |250| 
	.line	32
;----------------------------------------------------------------------
; 251 | EDIS;                                            // Disable writing to
;     | EALLOW protected registers*/                                           
; 252 | //  End Configure the ePWM1-6 shared pins                              
; 254 | // TB of ePWM 1-6 Configure                                            
;----------------------------------------------------------------------
 EDIS
	.line	36
;----------------------------------------------------------------------
; 255 | epwmmodule_config(&EPwm1Regs);                                         
;----------------------------------------------------------------------
        SPM       #0
        MOVL      XAR4,#_EPwm1Regs      ; |255| 
        LCR       #_epwmmodule_config   ; |255| 
        ; call occurs [#_epwmmodule_config] ; |255| 
	.line	37
;----------------------------------------------------------------------
; 256 | epwmmodule_config(&EPwm2Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm2Regs      ; |256| 
        LCR       #_epwmmodule_config   ; |256| 
        ; call occurs [#_epwmmodule_config] ; |256| 
	.line	38
;----------------------------------------------------------------------
; 257 | epwmmodule_config(&EPwm3Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm3Regs      ; |257| 
        LCR       #_epwmmodule_config   ; |257| 
        ; call occurs [#_epwmmodule_config] ; |257| 
	.line	39
;----------------------------------------------------------------------
; 258 | epwmmodule_config(&EPwm4Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm4Regs      ; |258| 
        LCR       #_epwmmodule_config   ; |258| 
        ; call occurs [#_epwmmodule_config] ; |258| 
	.line	40
;----------------------------------------------------------------------
; 259 | epwmmodule_config(&EPwm5Regs);                                         
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm5Regs      ; |259| 
        LCR       #_epwmmodule_config   ; |259| 
        ; call occurs [#_epwmmodule_config] ; |259| 
	.line	41
;----------------------------------------------------------------------
; 260 | epwmmodule_config(&EPwm6Regs);                                         
; 261 | //  End TB Configure                                                   
; 264 | //  EPWM1 Module as Master Synchronization                             
;----------------------------------------------------------------------
        MOVL      XAR4,#_EPwm6Regs      ; |260| 
        LCR       #_epwmmodule_config   ; |260| 
        ; call occurs [#_epwmmodule_config] ; |260| 
	.line	46
;----------------------------------------------------------------------
; 265 | EPwm1Regs.TBCTL.bit.PHSEN     = TB_DISABLE;      // Disables Phase Load
;     | ing (Master Mode ePWM1)                                                
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs
        AND       @_EPwm1Regs,#0xfffb   ; |265| 
	.line	47
;----------------------------------------------------------------------
; 266 | EPwm1Regs.TBCTL.bit.SYNCOSEL  = TB_CTR_ZERO;     // Generates EPWMxSYNC
;     |  output when CTR = Zero                                                
; 267 | //  End EPWM1 Module as Master Synchronization                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs,#0xffcf ; |266| 
        ORB       AL,#0x10              ; |266| 
        MOV       @_EPwm1Regs,AL        ; |266| 
	.line	52
;----------------------------------------------------------------------
; 271 | EALLOW;                                          // Allows CPU to write
;     |  to EALLOW protected registers                                         
;----------------------------------------------------------------------
 EALLOW
	.line	53
;----------------------------------------------------------------------
; 272 | PieVectTable.EPWM1_INT = &PTC5Fepwm_isr;       // ePWM1 Interrupt Rutin
;     | e Address                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_PieVectTable+96
        MOVL      XAR4,#_PTC5Fepwm_isr  ; |272| 
        MOVL      @_PieVectTable+96,XAR4 ; |272| 
	.line	54
;----------------------------------------------------------------------
; 273 | EDIS;
;     |                                                   // Disable CPU to wri
;     | te to EALLOW protected registers                                       
;----------------------------------------------------------------------
 EDIS
	.line	57
;----------------------------------------------------------------------
; 276 | EPwm1Regs.ETSEL.bit.INTSEL    = ET_CTR_PRD;      // Enable ePWM1 Module
;     |  Event Genaration when CTR=PRD                                         
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff8 ; |276| 
        ORB       AL,#0x02              ; |276| 
        MOV       @_EPwm1Regs+25,AL     ; |276| 
	.line	58
;----------------------------------------------------------------------
; 277 | EPwm1Regs.ETPS.bit.INTPRD     = ET_1ST;          // ePWM1 Module Event
;     | Prescaler DIV1                                                         
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+26,#0xfffc ; |277| 
        ORB       AL,#0x01              ; |277| 
        MOV       @_EPwm1Regs+26,AL     ; |277| 
	.line	60
;----------------------------------------------------------------------
; 279 | IER |= M_INT3;                                   // Enable EPWM1 CPU-PI
;     | EIER3 for INT5 (Group 3)                                               
;----------------------------------------------------------------------
        OR        IER,#0x0004           ; |279| 
	.line	61
;----------------------------------------------------------------------
; 280 | PieCtrlRegs.PIEIER3.bit.INTx1 = 1;               // Enable the EPWM1_IN
;     | T PIEIER3.1 to interrupt resquest sent to CPU Level                    
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+6
        AND       AL,@_PieCtrlRegs+6,#0xfffe ; |280| 
        ORB       AL,#0x01              ; |280| 
        MOV       @_PieCtrlRegs+6,AL    ; |280| 
	.line	62
;----------------------------------------------------------------------
; 281 | PieCtrlRegs.PIEACK.bit.ACK3   = 0;               // Clear the PIEACK of
;     |  Group 3 for enables Interrupt Resquest at CPU Level                   
;----------------------------------------------------------------------
        AND       @_PieCtrlRegs+1,#0xfffb ; |281| 
	.line	63
;----------------------------------------------------------------------
; 282 | EPwm1Regs.ETSEL.bit.INTEN     = ET_ETSEL_ENABLE; // Enables EPWM1_INT G
;     | eneration                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xfff7 ; |282| 
        ORB       AL,#0x08              ; |282| 
        MOV       @_EPwm1Regs+25,AL     ; |282| 
	.line	65
;----------------------------------------------------------------------
; 284 | }//void PTC5Fepwm6F_start()                                            
;----------------------------------------------------------------------
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	284,000000000h,0
	.sect	".text"
	.global	_epwmmodule_config
	.sym	_epwmmodule_config,_epwmmodule_config, 32, 2, 0
	.func	287

;***************************************************************
;* FNAME: _epwmmodule_config            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_epwmmodule_config:
	.line	1
;----------------------------------------------------------------------
; 287 | void epwmmodule_config(volatile struct EPWM_REGS *ePWM_Regs){          
;----------------------------------------------------------------------
;* AR4   assigned to _ePWM_Regs
	.sym	_ePWM_Regs,12, 24, 17, 22, _EPWM_REGS
;* AR4   assigned to _ePWM_Regs
	.sym	_ePWM_Regs,12, 24, 4, 22, _EPWM_REGS
	.line	3
;----------------------------------------------------------------------
; 289 | ePWM_Regs->TBPRD = pwm_period;                    // Set timer period o
;     | f EPWMx                                                                
;----------------------------------------------------------------------
        MOVW      DP,#_pwm_period
        MOV       AL,@_pwm_period       ; |289| 
        MOV       *+XAR4[5],AL          ; |289| 
	.line	5
;----------------------------------------------------------------------
; 291 | ePWM_Regs->TBPHS.half.TBPHS     = 0;                      // Set Phase
;     | register to zero                                                       
;----------------------------------------------------------------------
        MOV       *+XAR4[3],#0          ; |291| 
	.line	7
;----------------------------------------------------------------------
; 293 | ePWM_Regs->TBCTL.bit.CTRMODE   = TB_COUNT_UPDOWN; // Up-Down-Count Mode
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xfffc  ; |293| 
        ORB       AL,#0x02              ; |293| 
        MOV       *+XAR4[0],AL          ; |293| 
	.line	8
;----------------------------------------------------------------------
; 294 | ePWM_Regs->TBCTL.bit.PHSEN     = TB_ENABLE;      // Phase loading Enabl
;     | ed (Slave Mode)                                                        
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xfffb  ; |294| 
        ORB       AL,#0x04              ; |294| 
        MOV       *+XAR4[0],AL          ; |294| 
	.line	9
;----------------------------------------------------------------------
; 295 | ePWM_Regs->TBCTL.bit.PRDLD     = TB_SHADOW;       // Period load as sha
;     | dow mode                                                               
;----------------------------------------------------------------------
        AND       *+XAR4[0],#0xfff7     ; |295| 
	.line	10
;----------------------------------------------------------------------
; 296 | ePWM_Regs->TBCTL.bit.SYNCOSEL  = TB_SYNC_IN;      // Selects EPWMxSYNC
;     | as sincronization output                                               
;----------------------------------------------------------------------
        AND       *+XAR4[0],#0xffcf     ; |296| 
	.line	11
;----------------------------------------------------------------------
; 297 | ePWM_Regs->TBCTL.bit.CLKDIV    = TB_DIV4;         // division by 4     
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xe3ff  ; |297| 
        OR        AL,#0x0800            ; |297| 
        MOV       *+XAR4[0],AL          ; |297| 
	.line	12
;----------------------------------------------------------------------
; 298 | ePWM_Regs->TBCTL.bit.HSPCLKDIV = TB_DIV1;              // TBCLK = SYSCL
;     | KOUT / (HSPCLKDIV x CLKDIV) = 37.5MHz                                  
;----------------------------------------------------------------------
        AND       *+XAR4[0],#0xfc7f     ; |298| 
	.line	13
;----------------------------------------------------------------------
; 299 | ePWM_Regs->TBCTL.bit.PHSDIR    = TB_UP;                        // Phase
;     |  Direction Count-Up after the synchronization                          
;----------------------------------------------------------------------
        AND       AL,*+XAR4[0],#0xdfff  ; |299| 
        OR        AL,#0x2000            ; |299| 
        MOV       *+XAR4[0],AL          ; |299| 
	.line	16
;----------------------------------------------------------------------
; 302 | ePWM_Regs->CMPCTL.bit.SHDWAMODE = CC_SHADOW;       // Enable the CMPA t
;     | o operate in Shadow Mode                                               
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |302| 
        ADDB      XAR5,#7               ; |302| 
        AND       *+XAR5[0],#0xffef     ; |302| 
	.line	17
;----------------------------------------------------------------------
; 303 | ePWM_Regs->CMPCTL.bit.SHDWBMODE = CC_SHADOW;       // Enable the CMPB t
;     | o operate in Shadow Mode                                               
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |303| 
        ADDB      XAR5,#7               ; |303| 
        AND       *+XAR5[0],#0xffbf     ; |303| 
	.line	18
;----------------------------------------------------------------------
; 304 | ePWM_Regs->CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;     // Load the CMPA whe
;     | n TBCTR=0                                                              
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |304| 
        ADDB      XAR5,#7               ; |304| 
        AND       *+XAR5[0],#0xfffc     ; |304| 
	.line	19
;----------------------------------------------------------------------
; 305 | ePWM_Regs->CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;     // Load the CMPB whe
;     | n TBCTR=0                                                              
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |305| 
        ADDB      XAR5,#7               ; |305| 
        AND       *+XAR5[0],#0xfff3     ; |305| 
	.line	22
;----------------------------------------------------------------------
; 308 | ePWM_Regs->AQCTLA.bit.CAU      = AQ_SET;          // Set the EPWMxA whe
;     | n TBCTR=CMPA in Up Mode                                                
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |308| 
        ADDB      XAR5,#11              ; |308| 
        AND       AL,*+XAR5[0],#0xffcf  ; |308| 
        ORB       AL,#0x20              ; |308| 
        MOV       *+XAR5[0],AL          ; |308| 
	.line	23
;----------------------------------------------------------------------
; 309 | ePWM_Regs->AQCTLA.bit.CAD      = AQ_CLEAR;        // Clear the EPWMxA w
;     | hen TBCTR=CMPA in Down Mode                                            
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |309| 
        ADDB      XAR5,#11              ; |309| 
        AND       AL,*+XAR5[0],#0xff3f  ; |309| 
        ORB       AL,#0x40              ; |309| 
        MOV       *+XAR5[0],AL          ; |309| 
	.line	24
;----------------------------------------------------------------------
; 310 | ePWM_Regs->AQCTLA.bit.ZRO      = AQ_CLEAR;             // Action Counte
;     | r = Zero                                                               
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |310| 
        ADDB      XAR5,#11              ; |310| 
        AND       AL,*+XAR5[0],#0xfffc  ; |310| 
        ORB       AL,#0x01              ; |310| 
        MOV       *+XAR5[0],AL          ; |310| 
	.line	27
;----------------------------------------------------------------------
; 313 | ePWM_Regs->AQCTLA.bit.PRD      = AQ_NO_ACTION;    // Action Counter = P
;     | eriod                                                                  
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |313| 
        ADDB      XAR5,#11              ; |313| 
        AND       *+XAR5[0],#0xfff3     ; |313| 
	.line	30
;----------------------------------------------------------------------
; 316 | ePWM_Regs->DBCTL.bit.POLSEL    = DB_ACTV_HIC;     // Active High Comple
;     | mertary: EPWMxB=~EPWMxA                                                
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |316| 
        ADDB      XAR5,#15              ; |316| 
        AND       AL,*+XAR5[0],#0xfff3  ; |316| 
        ORB       AL,#0x08              ; |316| 
        MOV       *+XAR5[0],AL          ; |316| 
	.line	31
;----------------------------------------------------------------------
; 317 | ePWM_Regs->DBCTL.bit.OUT_MODE  = DB_FULL_ENABLE;  // Active Dead-Band i
;     | n the EPWMx Output                                                     
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |317| 
        ADDB      XAR5,#15              ; |317| 
        AND       AL,*+XAR5[0],#0xfffc  ; |317| 
        ORB       AL,#0x03              ; |317| 
        MOV       *+XAR5[0],AL          ; |317| 
	.line	32
;----------------------------------------------------------------------
; 318 | ePWM_Regs->DBRED               = DEADBAND_VALUE;  // Rising-edge delay
;     | time = 1 uS (assuming TBCLK = 37.5MHz)                                 
;----------------------------------------------------------------------
        MOVB      XAR0,#16              ; |318| 
        MOV       *+XAR4[AR0],#0        ; |318| 
	.line	33
;----------------------------------------------------------------------
; 319 | ePWM_Regs->DBFED               = DEADBAND_VALUE;  // Falling-edge delay
;     |  time = 1 uS (assuming TBCLK = 37.5MHz)                                
;----------------------------------------------------------------------
        MOVB      XAR0,#17              ; |319| 
        MOV       *+XAR4[AR0],#0        ; |319| 
	.line	36
;----------------------------------------------------------------------
; 322 | ePWM_Regs->PCCTL.bit.CHPEN     = 0;                               // Di
;     | sable Chopper control                                                  
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |322| 
        ADDB      XAR5,#30              ; |322| 
        AND       *+XAR5[0],#0xfffe     ; |322| 
	.line	39
;----------------------------------------------------------------------
; 325 | ePWM_Regs->TZCTL.bit.TZA       = TZ_DISABLE;      // Trip-Zone A disabl
;     | e                                                                      
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |325| 
        ADDB      XAR5,#20              ; |325| 
        AND       AL,*+XAR5[0],#0xfffc  ; |325| 
        ORB       AL,#0x03              ; |325| 
        MOV       *+XAR5[0],AL          ; |325| 
	.line	40
;----------------------------------------------------------------------
; 326 | ePWM_Regs->TZCTL.bit.TZB       = TZ_DISABLE;      // Trip-Zone B disabl
;     | e                                                                      
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |326| 
        ADDB      XAR5,#20              ; |326| 
        AND       AL,*+XAR5[0],#0xfff3  ; |326| 
        ORB       AL,#0x0c              ; |326| 
        MOV       *+XAR5[0],AL          ; |326| 
	.line	42
;----------------------------------------------------------------------
; 328 | ePWM_Regs->TZEINT.bit.OST      = 0;                               // Tr
;     | ip-Zone A disable ON SHOT                                              
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |328| 
        ADDB      XAR5,#21              ; |328| 
        AND       *+XAR5[0],#0xfffb     ; |328| 
	.line	43
;----------------------------------------------------------------------
; 329 | ePWM_Regs->TZEINT.bit.CBC      = TZ_DISABLE;      // Trip-Zone B disabl
;     | e CYCLE BY CYCLE                                                       
;----------------------------------------------------------------------
        MOVL      XAR5,XAR4             ; |329| 
        ADDB      XAR5,#21              ; |329| 
        AND       AL,*+XAR5[0],#0xfffd  ; |329| 
        ORB       AL,#0x02              ; |329| 
        MOV       *+XAR5[0],AL          ; |329| 
	.line	46
;----------------------------------------------------------------------
; 332 | ePWM_Regs->CMPA.half.CMPA      = pwm_period;      // Load de CMPA value
;     |  at PRD                                                                
;----------------------------------------------------------------------
        MOVB      XAR0,#9               ; |332| 
        MOV       AL,@_pwm_period       ; |332| 
        MOV       *+XAR4[AR0],AL        ; |332| 
	.line	50
;----------------------------------------------------------------------
; 336 | }//void epwmmodule_config(volatile struct EPWM_REGS *ePWM_Regs)        
;----------------------------------------------------------------------
        LRETR
        ; return occurs
	.endfunc	336,000000000h,0
	.sect	".text"
	.global	_PTC5Fadc_isr
	.file	"C:\CCStudio_v3.3\MyProjects\Calibracion2021_3\PTC5F_adc.c"
	.sym	_PTC5Fadc_isr,_PTC5Fadc_isr, 32, 2, 0
	.func	5

;***************************************************************
;* FNAME: _PTC5Fadc_isr                 FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto, 20 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fadc_isr:
	.line	1
;----------------------------------------------------------------------
;   5 | interrupt void PTC5Fadc_isr(){                                         
;----------------------------------------------------------------------
        ASP
        PUSH      RB
        MOVL      *SP++,XAR5
        MOVL      *SP++,XAR6
        MOVL      *SP++,XAR7
        MOVL      *SP++,XT
        MOV32     *SP++,STF
        MOV32     *SP++,R0H
        MOV32     *SP++,R1H
        MOV32     *SP++,R2H
        MOV32     *SP++,R3H
        SETFLG    RNDF32=1, RNDF64=1
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	3
;----------------------------------------------------------------------
;   7 | Ia_medido = (float) AdcMirror.ADCRESULT0;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror
        UI16TOF32 R0H,@_AdcMirror       ; |7| 
        MOVW      DP,#_Ia_medido
        MOV32     @_Ia_medido,R0H
	.line	4
;----------------------------------------------------------------------
;   8 | Ib_medido = (float) AdcMirror.ADCRESULT1;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+1
        UI16TOF32 R0H,@_AdcMirror+1     ; |8| 
        MOVW      DP,#_Ib_medido
        MOV32     @_Ib_medido,R0H
	.line	5
;----------------------------------------------------------------------
;   9 | Id_medido = (float) AdcMirror.ADCRESULT2;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+2
        UI16TOF32 R0H,@_AdcMirror+2     ; |9| 
        MOVW      DP,#_Id_medido
        MOV32     @_Id_medido,R0H
	.line	6
;----------------------------------------------------------------------
;  10 | Ie_medido = (float) AdcMirror.ADCRESULT3;                              
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+3
        UI16TOF32 R0H,@_AdcMirror+3     ; |10| 
        MOVW      DP,#_Ie_medido
        MOV32     @_Ie_medido,R0H
	.line	9
;----------------------------------------------------------------------
;  13 | Ia_medido = (Ia_medido-CONST_OFFSET_IA)*CONST_GAINFACT_IA;             
;----------------------------------------------------------------------
        MOVW      DP,#_Ia_medido
        MOVIZ     R1H,#15446            ; |13| 
        MOVIZ     R0H,#17667            ; |13| 
        MOVXI     R0H,#40168            ; |13| 
        MOV32     R2H,@_Ia_medido
        SUBF32    R0H,R2H,R0H           ; |13| 
        MOVXI     R1H,#47756            ; |13| 
        MPYF32    R0H,R1H,R0H           ; |13| 
        NOP
        MOV32     @_Ia_medido,R0H
	.line	10
;----------------------------------------------------------------------
;  14 | Ib_medido = (Ib_medido-CONST_OFFSET_IB)*CONST_GAINFACT_IB;             
;----------------------------------------------------------------------
        MOVIZ     R1H,#15449            ; |14| 
        MOVIZ     R0H,#17666            ; |14| 
        MOVXI     R0H,#15671            ; |14| 
        MOV32     R2H,@_Ib_medido
        SUBF32    R0H,R2H,R0H           ; |14| 
        MOVXI     R1H,#37979            ; |14| 
        MPYF32    R0H,R1H,R0H           ; |14| 
        NOP
        MOV32     @_Ib_medido,R0H
	.line	11
;----------------------------------------------------------------------
;  15 | Id_medido = (Id_medido-CONST_OFFSET_ID)*CONST_GAINFACT_ID;             
;----------------------------------------------------------------------
        MOVW      DP,#_Id_medido
        MOVIZ     R1H,#15447            ; |15| 
        MOVIZ     R0H,#17667            ; |15| 
        MOVXI     R0H,#13831            ; |15| 
        MOV32     R2H,@_Id_medido
        SUBF32    R0H,R2H,R0H           ; |15| 
        MOVXI     R1H,#2621             ; |15| 
        MPYF32    R0H,R1H,R0H           ; |15| 
        NOP
        MOV32     @_Id_medido,R0H
	.line	12
;----------------------------------------------------------------------
;  16 | Ie_medido = (Ie_medido-CONST_OFFSET_IE)*CONST_GAINFACT_IE;             
;  18 | // Reinitialize for next ADC sequence                                  
;----------------------------------------------------------------------
        MOVIZ     R1H,#15450            ; |16| 
        MOVIZ     R0H,#17666            ; |16| 
        MOVXI     R0H,#19297            ; |16| 
        MOV32     R2H,@_Ie_medido
        SUBF32    R0H,R2H,R0H           ; |16| 
        MOVXI     R1H,#1434             ; |16| 
        MPYF32    R0H,R1H,R0H           ; |16| 
        NOP
        MOV32     @_Ie_medido,R0H
	.line	16
;----------------------------------------------------------------------
;  20 | AdcRegs.ADCTRL2.bit.RST_SEQ1   = 1;            // Reset SEQ1
;     |                                                                        
;----------------------------------------------------------------------
        MOVW      DP,#_AdcRegs+1
        AND       AL,@_AdcRegs+1,#0xbfff ; |20| 
        OR        AL,#0x4000            ; |20| 
        MOV       @_AdcRegs+1,AL        ; |20| 
	.line	17
;----------------------------------------------------------------------
;  21 | AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;            // Clear INT SEQ1 bit   
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+25,#0xffef ; |21| 
        ORB       AL,#0x10              ; |21| 
        MOV       @_AdcRegs+25,AL       ; |21| 
	.line	18
;----------------------------------------------------------------------
;  22 | PieCtrlRegs.PIEACK.bit.ACK1    = 0;            // Clear the PIEACK of G
;     | roup 1 for enables Interrupt Resquest at CPU Level                     
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       @_PieCtrlRegs+1,#0xfffe ; |22| 
	.line	20
;----------------------------------------------------------------------
;  24 | } // interrupt void PTC5Fadc_isr()                                     
;----------------------------------------------------------------------
        MOV32     R3H,*--SP
        MOV32     R2H,*--SP
        MOV32     R1H,*--SP
        MOV32     R0H,*--SP
        MOV32     STF,*--SP
        MOVL      XT,*--SP
        MOVL      XAR7,*--SP
        MOVL      XAR6,*--SP
        MOVL      XAR5,*--SP
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	24,000000001h,20
	.sect	".text"
	.global	_PTC5Fadc_start
	.sym	_PTC5Fadc_start,_PTC5Fadc_start, 32, 2, 0
	.func	27

;***************************************************************
;* FNAME: _PTC5Fadc_start               FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fadc_start:
	.line	1
;----------------------------------------------------------------------
;  27 | void PTC5Fadc_start(){                                    // ADC Module
;     |  Configuration                                                         
;  29 | volatile Uint32 adcnt;                                                 
;----------------------------------------------------------------------
	.sym	_adcnt,-2, 15, 1, 32
        ADDB      SP,#2
	.line	5
;----------------------------------------------------------------------
;  31 | EALLOW;                                                // Enable CPU wr
;     | iting to EALLOW protected registers                                    
;----------------------------------------------------------------------
 EALLOW
	.line	6
;----------------------------------------------------------------------
;  32 | SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;                  // Enable ADC cl
;     | ock                                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       AL,@_SysCtrlRegs+12,#0xfff7 ; |32| 
        ORB       AL,#0x08              ; |32| 
        MOV       @_SysCtrlRegs+12,AL   ; |32| 
	.line	7
;----------------------------------------------------------------------
;  33 | EDIS;                                                  // Disable CPU w
;     | riting to EALLOW protected registers                                   
;----------------------------------------------------------------------
 EDIS
	.line	9
;----------------------------------------------------------------------
;  35 | AdcRegs.ADCTRL3.bit.ADCBGRFDN = 3;                     // Power up band
;     | gap/reference                                                          
;----------------------------------------------------------------------
        MOVW      DP,#_AdcRegs+24
        AND       AL,@_AdcRegs+24,#0xff3f ; |35| 
        ORB       AL,#0xc0              ; |35| 
        MOV       @_AdcRegs+24,AL       ; |35| 
	.line	10
;----------------------------------------------------------------------
;  36 | AdcRegs.ADCTRL3.bit.ADCPWDN   = 1;
;     |                 //ADC power up                                         
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+24,#0xffdf ; |36| 
        ORB       AL,#0x20              ; |36| 
        MOV       @_AdcRegs+24,AL       ; |36| 
	.line	12
;----------------------------------------------------------------------
;  38 | AdcRegs.ADCTRL1.bit.ACQ_PS = 0x1;                      // S/H width in
;     | ADC module periods = 16 ADC clocks                                     
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs,#0xf0ff  ; |38| 
        OR        AL,#0x0100            ; |38| 
        MOV       @_AdcRegs,AL          ; |38| 
	.line	13
;----------------------------------------------------------------------
;  39 | AdcRegs.ADCTRL1.bit.CONT_RUN = 0;                      // Start-Stop Mo
;     | de                                                                     
;----------------------------------------------------------------------
        AND       @_AdcRegs,#0xffbf     ; |39| 
	.line	14
;----------------------------------------------------------------------
;  40 | AdcRegs.ADCTRL1.bit.SEQ_CASC = 1;                      // Cascaded Mode
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs,#0xffef  ; |40| 
        ORB       AL,#0x10              ; |40| 
        MOV       @_AdcRegs,AL          ; |40| 
	.line	16
;----------------------------------------------------------------------
;  42 | AdcRegs.ADCTRL3.bit.SMODE_SEL = 0;
;     |                // Sampling mode --> SEQUENTIAL                         
;----------------------------------------------------------------------
        AND       @_AdcRegs+24,#0xfffe  ; |42| 
	.line	17
;----------------------------------------------------------------------
;  43 | AdcRegs.ADCTRL1.bit.CPS       = 0;                     // CPS=1; ADCLK=
;     | HSPCLK/(ADCLKPS*CPS)                                                   
;----------------------------------------------------------------------
        AND       @_AdcRegs,#0xff7f     ; |43| 
	.line	18
;----------------------------------------------------------------------
;  44 | AdcRegs.ADCTRL3.bit.ADCCLKPS  = 3;                     // ADC module cl
;     | ock = HSPCLK/(1*8)   = 150MHz/(1*8) = 18.75MHz                         
;  45 | 
;     |                                         // aprox 0.853 us por conversio
;     | n                                                                      
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+24,#0xffe1 ; |44| 
        ORB       AL,#0x06              ; |44| 
        MOV       @_AdcRegs+24,AL       ; |44| 
	.line	20
;----------------------------------------------------------------------
;  46 | AdcRegs.ADCMAXCONV.all = 4;                            // Setup the num
;     | ber of conv's on SEQ1                                                  
;  48 | //  Initialize ADC Input Channel Select Sequencing Control Register:   
;  49 |      //  Placa gris                                                    
;----------------------------------------------------------------------
        MOVB      @_AdcRegs+2,#4,UNC    ; |46| 
	.line	24
;----------------------------------------------------------------------
;  50 | AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0;                   // Setup the 1st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       @_AdcRegs+3,#0xfff0   ; |50| 
	.line	25
;----------------------------------------------------------------------
;  51 | AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 2;                   // Setup the 2st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+3,#0xff0f ; |51| 
        ORB       AL,#0x20              ; |51| 
        MOV       @_AdcRegs+3,AL        ; |51| 
	.line	26
;----------------------------------------------------------------------
;  52 | AdcRegs.ADCCHSELSEQ1.bit.CONV02 = 4;                   // Setup the 3st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+3,#0xf0ff ; |52| 
        OR        AL,#0x0400            ; |52| 
        MOV       @_AdcRegs+3,AL        ; |52| 
	.line	27
;----------------------------------------------------------------------
;  53 | AdcRegs.ADCCHSELSEQ1.bit.CONV03 = 6;                   // Setup the 4st
;     |  SEQ1 conv.                                                            
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+3,#0x0fff ; |53| 
        OR        AL,#0x6000            ; |53| 
        MOV       @_AdcRegs+3,AL        ; |53| 
	.line	28
;----------------------------------------------------------------------
;  54 | AdcRegs.ADCCHSELSEQ2.bit.CONV04 = 1;                   // Setup the 5st
;     |  SEQ2 conv. (DC-LINK)                                                  
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+4,#0xfff0 ; |54| 
        ORB       AL,#0x01              ; |54| 
        MOV       @_AdcRegs+4,AL        ; |54| 
	.line	30
;----------------------------------------------------------------------
;  56 | AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 = 1;                // Enable SOCA f
;     | rom ePWM to start SEQ1                                                 
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+1,#0xfeff ; |56| 
        OR        AL,#0x0100            ; |56| 
        MOV       @_AdcRegs+1,AL        ; |56| 
	.line	31
;----------------------------------------------------------------------
;  57 | AdcRegs.ADCTRL2.bit.INT_MOD_SEQ1   = 0;                // SEQ1 Interrup
;     | t mode at every end of SEQ1                                            
;----------------------------------------------------------------------
        AND       @_AdcRegs+1,#0xfbff   ; |57| 
	.line	32
;----------------------------------------------------------------------
;  58 | AdcRegs.ADCTRL2.bit.RST_SEQ1              = 1;                // Reset
;     | SEQ1                                                                   
;  61 | //  Start ADC with ePWM1 timer 1 event:                                
;----------------------------------------------------------------------
        AND       AL,@_AdcRegs+1,#0xbfff ; |58| 
        OR        AL,#0x4000            ; |58| 
        MOV       @_AdcRegs+1,AL        ; |58| 
	.line	36
;----------------------------------------------------------------------
;  62 | EPwm1Regs.ETSEL.bit.SOCAEN  = ET_ETSEL_ENABLE;         // Enable SOC on
;     |  A group                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_EPwm1Regs+25
        AND       AL,@_EPwm1Regs+25,#0xf7ff ; |62| 
        OR        AL,#0x0800            ; |62| 
        MOV       @_EPwm1Regs+25,AL     ; |62| 
	.line	37
;----------------------------------------------------------------------
;  63 | EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_PRD;              // Generate SOC
;     | when time-base counter equal to period.                                
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+25,#0xf8ff ; |63| 
        OR        AL,#0x0200            ; |63| 
        MOV       @_EPwm1Regs+25,AL     ; |63| 
	.line	38
;----------------------------------------------------------------------
;  64 | EPwm1Regs.ETPS.bit.SOCAPRD  = ET_1ST;                  // Generate puls
;     | e on 1st event                                                         
;  66 | //  start PWM generation                                               
;----------------------------------------------------------------------
        AND       AL,@_EPwm1Regs+26,#0xfcff ; |64| 
        OR        AL,#0x0100            ; |64| 
        MOV       @_EPwm1Regs+26,AL     ; |64| 
	.line	41
;----------------------------------------------------------------------
;  67 | EALLOW;                                                // Enable writin
;     | g to EALLOW protected registers                                        
;----------------------------------------------------------------------
 EALLOW
	.line	42
;----------------------------------------------------------------------
;  68 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;                 // Start all the
;     |  timers synced                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       AL,@_SysCtrlRegs+12,#0xfffb ; |68| 
        ORB       AL,#0x04              ; |68| 
        MOV       @_SysCtrlRegs+12,AL   ; |68| 
	.line	43
;----------------------------------------------------------------------
;  69 | EDIS;                                                  // Disable writi
;     | ng to EALLOW protected areas                                           
;  72 | //  Delay routine 5ms                                                  
;----------------------------------------------------------------------
 EDIS
	.line	47
;----------------------------------------------------------------------
;  73 | for(adcnt=0;adcnt<76000;adcnt++);                                      
;----------------------------------------------------------------------
        MOVB      XAR6,#0
        MOVL      XAR4,#76000           ; |73| 
        MOVL      *-SP[2],XAR6          ; |73| 
        MOVL      ACC,XAR4              ; |73| 
        CMPL      ACC,*-SP[2]           ; |73| 
        B         $C$L20,LOS            ; |73| 
        ; branchcc occurs ; |73| 
$C$L19:    
        MOVB      ACC,#1
        ADDL      ACC,*-SP[2]           ; |73| 
        MOVL      *-SP[2],ACC           ; |73| 
        MOVL      ACC,XAR4              ; |73| 
        CMPL      ACC,*-SP[2]           ; |73| 
        B         $C$L19,HI             ; |73| 
        ; branchcc occurs ; |73| 
$C$L20:    
	.line	49
;----------------------------------------------------------------------
;  75 | AdcRegs.ADCTRL2.bit.RST_SEQ1   = 1;            // Reset SEQ1           
;----------------------------------------------------------------------
        MOVW      DP,#_AdcRegs+1
        AND       AL,@_AdcRegs+1,#0xbfff ; |75| 
        OR        AL,#0x4000            ; |75| 
        MOV       @_AdcRegs+1,AL        ; |75| 
	.line	51
;----------------------------------------------------------------------
;  77 | AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1    = 0;                // Disable SEQ1
;     |  interrupt (every EOS)                                                 
;----------------------------------------------------------------------
        AND       @_AdcRegs+1,#0xf7ff   ; |77| 
	.line	53
;----------------------------------------------------------------------
;  79 | }//void PTC5Fadc_start()                                               
;----------------------------------------------------------------------
        SPM       #0
        SUBB      SP,#2
        LRETR
        ; return occurs
	.endfunc	79,000000000h,2
	.sect	".text"
	.global	_PTC5Fcputmr0_isr
	.file	"C:\CCStudio_v3.3\MyProjects\Calibracion2021_3\PTC5F_tmr0.c"
	.sym	_PTC5Fcputmr0_isr,_PTC5Fcputmr0_isr, 32, 2, 0
	.func	6

;***************************************************************
;* FNAME: _PTC5Fcputmr0_isr             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  4 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fcputmr0_isr:
	.line	1
;----------------------------------------------------------------------
;   6 | interrupt void PTC5Fcputmr0_isr(){                                     
;----------------------------------------------------------------------
        ASP
        PUSH      RB
        MOV32     *SP++,STF
        SETFLG    RNDF32=1, RNDF64=1
        CLRC      PAGE0,OVM
        CLRC      AMODE
	.line	3
;----------------------------------------------------------------------
;   8 | CpuTimer0Regs.TCR.bit.TIF   = 1;
;     | // Reset the Cpu-Timer0 Interrupt Flag                                 
;----------------------------------------------------------------------
        MOVW      DP,#_CpuTimer0Regs+4
        AND       AL,@_CpuTimer0Regs+4,#0x7fff ; |8| 
        OR        AL,#0x8000            ; |8| 
        MOV       @_CpuTimer0Regs+4,AL  ; |8| 
	.line	4
;----------------------------------------------------------------------
;   9 | PieCtrlRegs.PIEACK.bit.ACK1 = 1;                                   // C
;     | lear the PIEACK of Group 1 for new Interrupt Resquest                  
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+1
        AND       AL,@_PieCtrlRegs+1,#0xfffe ; |9| 
        ORB       AL,#0x01              ; |9| 
        MOV       @_PieCtrlRegs+1,AL    ; |9| 
	.line	6
;----------------------------------------------------------------------
;  11 | }// void PTC5Fcputmr0_isr()                                            
;----------------------------------------------------------------------
        MOV32     STF,*--SP
        POP       RB
        NASP
        IRET
        ; return occurs
	.endfunc	11,000000000h,4
	.sect	".text"
	.global	_PTC5Fcputmr0_start
	.sym	_PTC5Fcputmr0_start,_PTC5Fcputmr0_start, 32, 2, 0
	.func	15

;***************************************************************
;* FNAME: _PTC5Fcputmr0_start           FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_PTC5Fcputmr0_start:
	.line	1
;----------------------------------------------------------------------
;  15 | void PTC5Fcputmr0_start(){                                             
;----------------------------------------------------------------------
	.line	3
;----------------------------------------------------------------------
;  17 | CpuTimer0Regs.PRD.all = FRECUENCIA_SYSCLKOUT/FCPUTIMER0_HZ;
;     |   // Timer Period                                                      
;----------------------------------------------------------------------
        MOV       PH,#228
        MOV       PL,#57792
        MOVW      DP,#_CpuTimer0Regs+2
        MOVL      @_CpuTimer0Regs+2,P   ; |17| 
	.line	5
;----------------------------------------------------------------------
;  19 | CpuTimer0Regs.TCR.bit.FREE=1;
;     |      // CPU-TIMER0 free run                                            
;----------------------------------------------------------------------
        AND       AL,@_CpuTimer0Regs+4,#0xf7ff ; |19| 
        OR        AL,#0x0800            ; |19| 
        MOV       @_CpuTimer0Regs+4,AL  ; |19| 
	.line	7
;----------------------------------------------------------------------
;  21 | EALLOW;
;     |                                              // Enable CPU writing to p
;     | rotected registers                                                     
;----------------------------------------------------------------------
 EALLOW
	.line	8
;----------------------------------------------------------------------
;  22 | PieVectTable.TINT0=&PTC5Fcputmr0_isr;                       // Define t
;     | he CPU-TIMER0 ISR address                                              
;----------------------------------------------------------------------
        MOVL      XAR4,#_PTC5Fcputmr0_isr ; |22| 
        MOVW      DP,#_PieVectTable+76
        MOVL      @_PieVectTable+76,XAR4 ; |22| 
	.line	9
;----------------------------------------------------------------------
;  23 | EDIS;
;     |                                              // Disable CPU writing to 
;     | protected registers                                                    
;----------------------------------------------------------------------
 EDIS
	.line	11
;----------------------------------------------------------------------
;  25 | EALLOW;                                        // Enable CPU writing to
;     |  protected registers                                                   
;----------------------------------------------------------------------
 EALLOW
	.line	12
;----------------------------------------------------------------------
;  26 | SysCtrlRegs.PCLKCR3.bit.CPUTIMER0ENCLK = 1;    // Enable the SYSCLKOUT
;     | to the CPU-TIMER0                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+16
        AND       AL,@_SysCtrlRegs+16,#0xfeff ; |26| 
        OR        AL,#0x0100            ; |26| 
        MOV       @_SysCtrlRegs+16,AL   ; |26| 
	.line	13
;----------------------------------------------------------------------
;  27 | EDIS;                                          // Disable CPU writing t
;     | o protected registers                                                  
;----------------------------------------------------------------------
 EDIS
	.line	15
;----------------------------------------------------------------------
;  29 | IER |=M_INT1;
;     |                                      // Enable de CPU-TIMER0 CPU-PIEIER
;     | 1 for INT1 (Group 1)                                                   
;----------------------------------------------------------------------
        OR        IER,#0x0001           ; |29| 
	.line	16
;----------------------------------------------------------------------
;  30 | PieCtrlRegs.PIEIER1.bit.INTx7 = 1;                                  //
;     | Enable the CPU-TIMER0 PIEIER1.7 to interrupt resquest sent to CPU Level
;----------------------------------------------------------------------
        MOVW      DP,#_PieCtrlRegs+2
        AND       AL,@_PieCtrlRegs+2,#0xffbf ; |30| 
        ORB       AL,#0x40              ; |30| 
        MOV       @_PieCtrlRegs+2,AL    ; |30| 
	.line	17
;----------------------------------------------------------------------
;  31 | PieCtrlRegs.PIEACK.bit.ACK1  = 0;                                   //
;     | Clear the PIEACK of Group 1 for enables Interrupt Resquest at CPU Level
;----------------------------------------------------------------------
        AND       @_PieCtrlRegs+1,#0xfffe ; |31| 
	.line	18
;----------------------------------------------------------------------
;  32 | CpuTimer0Regs.TCR.bit.TIE   = 1;                                    //
;     | CPU-TIMER0 Interruption Enable                                         
;----------------------------------------------------------------------
        MOVW      DP,#_CpuTimer0Regs+4
        AND       AL,@_CpuTimer0Regs+4,#0xbfff ; |32| 
        OR        AL,#0x4000            ; |32| 
        MOV       @_CpuTimer0Regs+4,AL  ; |32| 
	.line	20
;----------------------------------------------------------------------
;  34 | }// PTC5Fcputmr0_start()                                               
;----------------------------------------------------------------------
        SPM       #0
        LRETR
        ; return occurs
	.endfunc	34,000000000h,0
	.sect	".text"
	.global	_main
	.file	"C:\CCStudio_v3.3\MyProjects\Calibracion2021_3\PTC5F_main.c"
	.sym	_main,_main, 32, 2, 0
	.func	21
;----------------------------------------------------------------------
;  21 | void main ()                                                           
;----------------------------------------------------------------------

;***************************************************************
;* FNAME: _main                         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  2 SOE     *
;***************************************************************


;***************************************************************
;*                                                             *
;* Using -g (debug) with optimization (-o0) may disable key op *
;*                                                             *
;***************************************************************
_main:
	.line	2
;----------------------------------------------------------------------
;  24 | int sv;                                                                
;----------------------------------------------------------------------
;* AL    assigned to _sv
	.sym	_sv,0, 4, 4, 16
;* AR1   assigned to _fds
	.sym	_fds,6, 4, 4, 16
        MOVL      *SP++,XAR1
	.line	5
;----------------------------------------------------------------------
;  25 | int fds = 1;                                                           
;  26 | int sopt, vk = 0, vk_ant = 0;                                          
;----------------------------------------------------------------------
        MOVB      XAR1,#1               ; |25| 
	.line	8
;----------------------------------------------------------------------
;  28 | EALLOW;                                      // Enable writing to EALLO
;     | W protected registers                                                  
;----------------------------------------------------------------------
 EALLOW
	.line	10
;----------------------------------------------------------------------
;  30 | SysCtrlRegs.HISPCP.bit.HSPCLK       = 0;     // HSPCLK = SYSCLKOUT / 1
;     | = 150MHz                                                               
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+10
        AND       @_SysCtrlRegs+10,#0xfff8 ; |30| 
	.line	11
;----------------------------------------------------------------------
;  31 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;     // Stop all the TB clocks 
;----------------------------------------------------------------------
        AND       @_SysCtrlRegs+12,#0xfffb ; |31| 
	.line	12
;----------------------------------------------------------------------
;  32 | SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;     // Enable the SYSCLKOUT to
;     |  the GPIO                                                              
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+16,#0xdfff ; |32| 
        OR        AL,#0x2000            ; |32| 
        MOV       @_SysCtrlRegs+16,AL   ; |32| 
	.line	14
;----------------------------------------------------------------------
;  34 | GpioCtrlRegs.GPBMUX2.bit.GPIO54           = 0;          // Configure GP
;     | IO54 (JP3-PIN 19) as digital I/O                                       
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0xcfff ; |34| 
	.line	15
;----------------------------------------------------------------------
;  35 | GpioCtrlRegs.GPBDIR.bit.GPIO54            = 1;          // Configure GP
;     | IO54 as digital Output (JP3-PIN 19)                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xffbf ; |35| 
        ORB       AL,#0x40              ; |35| 
        MOV       @_GpioCtrlRegs+27,AL  ; |35| 
	.line	16
;----------------------------------------------------------------------
;  36 | GpioDataRegs.GPBCLEAR.bit.GPIO54                = 1;            // CLEA
;     | R the TRIGGER SIGNAL (JP3-PIN 19)                                      
;  39 | // LED_1 Configuration                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xffbf ; |36| 
        ORB       AL,#0x40              ; |36| 
        MOV       @_GpioDataRegs+13,AL  ; |36| 
	.line	20
;----------------------------------------------------------------------
;  40 | GpioCtrlRegs.GPAMUX2.bit.GPIO16   = 0;       // Configure JP4#16(GPIO 1
;     | 6) as digital I/O                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+8
        AND       @_GpioCtrlRegs+8,#0xfffc ; |40| 
	.line	21
;----------------------------------------------------------------------
;  41 | GpioCtrlRegs.GPADIR.bit.GPIO16    = 1;       //Configure JP4#16(GPIO 16
;     | ) as digital Output                                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+11,#0xfffe ; |41| 
        ORB       AL,#0x01              ; |41| 
        MOV       @_GpioCtrlRegs+11,AL  ; |41| 
	.line	22
;----------------------------------------------------------------------
;  42 | GpioDataRegs.GPASET.bit.GPIO16   = 1;       // Turn-off LED_1          
;  43 | // End LED_1 Configuration                                             
;  45 | //salidas que controla rele (KSM)                                      
;  46 | // Configuracion de J3-9                                               
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+3
        AND       AL,@_GpioDataRegs+3,#0xfffe ; |42| 
        ORB       AL,#0x01              ; |42| 
        MOV       @_GpioDataRegs+3,AL   ; |42| 
	.line	27
;----------------------------------------------------------------------
;  47 | GpioCtrlRegs.GPBMUX2.bit.GPIO48 = 0;   // Configure GPIO48 as digital I
;     | /O                                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0xfffc ; |47| 
	.line	28
;----------------------------------------------------------------------
;  48 | GpioCtrlRegs.GPBDIR.bit.GPIO48  = 1;            //Configure shared pins
;     |  as digital Output (GPIO 48);                                          
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfffe ; |48| 
        ORB       AL,#0x01              ; |48| 
        MOV       @_GpioCtrlRegs+27,AL  ; |48| 
	.line	29
;----------------------------------------------------------------------
;  49 | GpioDataRegs.GPBCLEAR.bit.GPIO48        = 1;            //Enciende los
;     | ventiladores                                                           
;  51 | //salidas que controla rele (KP)                                       
;  52 | // Configuracion de J3-10                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfffe ; |49| 
        ORB       AL,#0x01              ; |49| 
        MOV       @_GpioDataRegs+13,AL  ; |49| 
	.line	33
;----------------------------------------------------------------------
;  53 | GpioCtrlRegs.GPBMUX2.bit.GPIO49 = 0;   // Configure GPIO49 as digital I
;     | /O                                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0xfff3 ; |53| 
	.line	34
;----------------------------------------------------------------------
;  54 | GpioCtrlRegs.GPBDIR.bit.GPIO49  = 1;            //Configure shared pins
;     |  as digital Output (GPIO 49);                                          
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfffd ; |54| 
        ORB       AL,#0x02              ; |54| 
        MOV       @_GpioCtrlRegs+27,AL  ; |54| 
	.line	35
;----------------------------------------------------------------------
;  55 | GpioDataRegs.GPBCLEAR.bit.GPIO49        = 1;            //habilita KP  
;  57 | // LED_2 Configuration                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfffd ; |55| 
        ORB       AL,#0x02              ; |55| 
        MOV       @_GpioDataRegs+13,AL  ; |55| 
	.line	38
;----------------------------------------------------------------------
;  58 | GpioCtrlRegs.GPAMUX2.bit.GPIO17   = 0;       // Configure JP4#18(GPIO 1
;     | 7) as digital I/O                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+8
        AND       @_GpioCtrlRegs+8,#0xfff3 ; |58| 
	.line	39
;----------------------------------------------------------------------
;  59 | GpioCtrlRegs.GPADIR.bit.GPIO17    = 1;       //Configure JP4#18(GPIO 17
;     | ) as digital Output                                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+11,#0xfffd ; |59| 
        ORB       AL,#0x02              ; |59| 
        MOV       @_GpioCtrlRegs+11,AL  ; |59| 
	.line	40
;----------------------------------------------------------------------
;  60 | GpioDataRegs.GPASET.bit.GPIO17   = 1;       // Turn-off LED_2          
;  61 | // End LED_1 Configuration                                             
;  63 | //############################### SA_UP & SA_DOWN #####################
;     | #####################                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+3
        AND       AL,@_GpioDataRegs+3,#0xfffd ; |60| 
        ORB       AL,#0x02              ; |60| 
        MOV       @_GpioDataRegs+3,AL   ; |60| 
	.line	45
;----------------------------------------------------------------------
;  65 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfffc ; |65| 
	.line	46
;----------------------------------------------------------------------
;  66 | GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffe ; |66| 
        ORB       AL,#0x01              ; |66| 
        MOV       @_GpioCtrlRegs+10,AL  ; |66| 
	.line	47
;----------------------------------------------------------------------
;  67 | GpioDataRegs.GPACLEAR.bit.GPIO0      = 1;       // Clear GPIO0         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffe ; |67| 
        ORB       AL,#0x01              ; |67| 
        MOV       @_GpioDataRegs+4,AL   ; |67| 
	.line	49
;----------------------------------------------------------------------
;  69 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfff3 ; |69| 
	.line	50
;----------------------------------------------------------------------
;  70 | GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffd ; |70| 
        ORB       AL,#0x02              ; |70| 
        MOV       @_GpioCtrlRegs+10,AL  ; |70| 
	.line	51
;----------------------------------------------------------------------
;  71 | GpioDataRegs.GPACLEAR.bit.GPIO1      = 1;       // Clear GPIO1         
;  73 | //####################################### SB_UP & SB_DOWN #############
;     | #######################                                                
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffd ; |71| 
        ORB       AL,#0x02              ; |71| 
        MOV       @_GpioDataRegs+4,AL   ; |71| 
	.line	55
;----------------------------------------------------------------------
;  75 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xffcf ; |75| 
	.line	56
;----------------------------------------------------------------------
;  76 | GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffb ; |76| 
        ORB       AL,#0x04              ; |76| 
        MOV       @_GpioCtrlRegs+10,AL  ; |76| 
	.line	57
;----------------------------------------------------------------------
;  77 | GpioDataRegs.GPACLEAR.bit.GPIO2      = 1;       // Clear GPIO2         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffb ; |77| 
        ORB       AL,#0x04              ; |77| 
        MOV       @_GpioDataRegs+4,AL   ; |77| 
	.line	59
;----------------------------------------------------------------------
;  79 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xff3f ; |79| 
	.line	60
;----------------------------------------------------------------------
;  80 | GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfff7 ; |80| 
        ORB       AL,#0x08              ; |80| 
        MOV       @_GpioCtrlRegs+10,AL  ; |80| 
	.line	61
;----------------------------------------------------------------------
;  81 | GpioDataRegs.GPACLEAR.bit.GPIO3      = 1;       // Clear GPIO3         
;  83 | //##################################### SC_UP & SC_DOWN ###############
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfff7 ; |81| 
        ORB       AL,#0x08              ; |81| 
        MOV       @_GpioDataRegs+4,AL   ; |81| 
	.line	65
;----------------------------------------------------------------------
;  85 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfcff ; |85| 
	.line	66
;----------------------------------------------------------------------
;  86 | GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffef ; |86| 
        ORB       AL,#0x10              ; |86| 
        MOV       @_GpioCtrlRegs+10,AL  ; |86| 
	.line	67
;----------------------------------------------------------------------
;  87 | GpioDataRegs.GPACLEAR.bit.GPIO4      = 1;       // Clear GPIO4         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffef ; |87| 
        ORB       AL,#0x10              ; |87| 
        MOV       @_GpioDataRegs+4,AL   ; |87| 
	.line	69
;----------------------------------------------------------------------
;  89 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xf3ff ; |89| 
	.line	70
;----------------------------------------------------------------------
;  90 | GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffdf ; |90| 
        ORB       AL,#0x20              ; |90| 
        MOV       @_GpioCtrlRegs+10,AL  ; |90| 
	.line	71
;----------------------------------------------------------------------
;  91 | GpioDataRegs.GPACLEAR.bit.GPIO5      = 1;       // Clear GPIO5         
;  93 | //################################### SD_UP & SD_DOWN #################
;     | #####################                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffdf ; |91| 
        ORB       AL,#0x20              ; |91| 
        MOV       @_GpioDataRegs+4,AL   ; |91| 
	.line	75
;----------------------------------------------------------------------
;  95 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xcfff ; |95| 
	.line	76
;----------------------------------------------------------------------
;  96 | GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffbf ; |96| 
        ORB       AL,#0x40              ; |96| 
        MOV       @_GpioCtrlRegs+10,AL  ; |96| 
	.line	77
;----------------------------------------------------------------------
;  97 | GpioDataRegs.GPACLEAR.bit.GPIO6      = 1;       // Clear GPIO6         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffbf ; |97| 
        ORB       AL,#0x40              ; |97| 
        MOV       @_GpioDataRegs+4,AL   ; |97| 
	.line	79
;----------------------------------------------------------------------
;  99 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0x3fff ; |99| 
	.line	80
;----------------------------------------------------------------------
; 100 | GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xff7f ; |100| 
        ORB       AL,#0x80              ; |100| 
        MOV       @_GpioCtrlRegs+10,AL  ; |100| 
	.line	81
;----------------------------------------------------------------------
; 101 | GpioDataRegs.GPACLEAR.bit.GPIO7      = 1;       // Clear GPIO7         
; 103 | //#################################### SE_UP & SE_DOWN ################
;     | #########################                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xff7f ; |101| 
        ORB       AL,#0x80              ; |101| 
        MOV       @_GpioDataRegs+4,AL   ; |101| 
	.line	85
;----------------------------------------------------------------------
; 105 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfffc ; |105| 
	.line	86
;----------------------------------------------------------------------
; 106 | GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfeff ; |106| 
        OR        AL,#0x0100            ; |106| 
        MOV       @_GpioCtrlRegs+10,AL  ; |106| 
	.line	87
;----------------------------------------------------------------------
; 107 | GpioDataRegs.GPACLEAR.bit.GPIO8      = 1;       // Clear GPIO8         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfeff ; |107| 
        OR        AL,#0x0100            ; |107| 
        MOV       @_GpioDataRegs+4,AL   ; |107| 
	.line	89
;----------------------------------------------------------------------
; 109 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfff3 ; |109| 
	.line	90
;----------------------------------------------------------------------
; 110 | GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfdff ; |110| 
        OR        AL,#0x0200            ; |110| 
        MOV       @_GpioCtrlRegs+10,AL  ; |110| 
	.line	91
;----------------------------------------------------------------------
; 111 | GpioDataRegs.GPACLEAR.bit.GPIO9      = 1;       // Clear GPIO9         
; 113 | //#################################### SF_UP & SF_DOWN ################
;     | #########################                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfdff ; |111| 
        OR        AL,#0x0200            ; |111| 
        MOV       @_GpioDataRegs+4,AL   ; |111| 
	.line	95
;----------------------------------------------------------------------
; 115 | GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xffcf ; |115| 
	.line	96
;----------------------------------------------------------------------
; 116 | GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfbff ; |116| 
        OR        AL,#0x0400            ; |116| 
        MOV       @_GpioCtrlRegs+10,AL  ; |116| 
	.line	97
;----------------------------------------------------------------------
; 117 | GpioDataRegs.GPACLEAR.bit.GPIO10                  = 1;       // Clear G
;     | PIO10                                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfbff ; |117| 
        OR        AL,#0x0400            ; |117| 
        MOV       @_GpioDataRegs+4,AL   ; |117| 
	.line	99
;----------------------------------------------------------------------
; 119 | GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xff3f ; |119| 
	.line	100
;----------------------------------------------------------------------
; 120 | GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xf7ff ; |120| 
        OR        AL,#0x0800            ; |120| 
        MOV       @_GpioCtrlRegs+10,AL  ; |120| 
	.line	101
;----------------------------------------------------------------------
; 121 | GpioDataRegs.GPACLEAR.bit.GPIO11                  = 1;       // Clear G
;     | PIO11                                                                  
; 122 | //#####################################################################
;     | ######################                                                 
; 124 | //#################################### TRIGGER ########################
;     | #################                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xf7ff ; |121| 
        OR        AL,#0x0800            ; |121| 
        MOV       @_GpioDataRegs+4,AL   ; |121| 
	.line	105
;----------------------------------------------------------------------
; 125 | GpioCtrlRegs.GPBMUX2.bit.GPIO55           = 0;          // Configure GP
;     | IO54 (JP3-PIN 20) as digital I/O                                       
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+24
        AND       @_GpioCtrlRegs+24,#0x3fff ; |125| 
	.line	106
;----------------------------------------------------------------------
; 126 | GpioCtrlRegs.GPBDIR.bit.GPIO55            = 1;          // Configure GP
;     | IO54 as digital Output (JP3-PIN 20)                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xff7f ; |126| 
        ORB       AL,#0x80              ; |126| 
        MOV       @_GpioCtrlRegs+27,AL  ; |126| 
	.line	107
;----------------------------------------------------------------------
; 127 | GpioDataRegs.GPBCLEAR.bit.GPIO55                = 1;            // CLEA
;     | R the TRIGGER SIGNAL (JP3-PIN 20)                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xff7f ; |127| 
        ORB       AL,#0x80              ; |127| 
        MOV       @_GpioDataRegs+13,AL  ; |127| 
	.line	109
;----------------------------------------------------------------------
; 129 | GpioCtrlRegs.GPBMUX2.bit.GPIO56           = 0;          // Configure GP
;     | IO54 (JP3-PIN 21) as digital I/O                                       
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+25
        AND       @_GpioCtrlRegs+25,#0xfffc ; |129| 
	.line	110
;----------------------------------------------------------------------
; 130 | GpioCtrlRegs.GPBDIR.bit.GPIO56    = 1;          // Configure GPIO54 as
;     | digital Output (JP3-PIN 21)                                            
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfeff ; |130| 
        OR        AL,#0x0100            ; |130| 
        MOV       @_GpioCtrlRegs+27,AL  ; |130| 
	.line	111
;----------------------------------------------------------------------
; 131 | GpioDataRegs.GPBCLEAR.bit.GPIO56                = 1;            // CLEA
;     | R the TRIGGER SIGNAL (JP3-PIN 21)                                      
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfeff ; |131| 
        OR        AL,#0x0100            ; |131| 
        MOV       @_GpioDataRegs+13,AL  ; |131| 
	.line	113
;----------------------------------------------------------------------
; 133 | GpioCtrlRegs.GPBMUX2.bit.GPIO57           = 0;          // Configure GP
;     | IO54 (JP3-PIN 22) as digital I/O                                       
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+25
        AND       @_GpioCtrlRegs+25,#0xfff3 ; |133| 
	.line	114
;----------------------------------------------------------------------
; 134 | GpioCtrlRegs.GPBDIR.bit.GPIO57            = 1;          // Configure GP
;     | IO54 as digital Output (JP3-PIN 22)                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+27,#0xfdff ; |134| 
        OR        AL,#0x0200            ; |134| 
        MOV       @_GpioCtrlRegs+27,AL  ; |134| 
	.line	115
;----------------------------------------------------------------------
; 135 | GpioDataRegs.GPBCLEAR.bit.GPIO57                = 1;            // CLEA
;     | R the TRIGGER SIGNAL (JP3-PIN 22)                                      
; 138 | //#####################################################################
;     | #########################                                              
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+13
        AND       AL,@_GpioDataRegs+13,#0xfdff ; |135| 
        OR        AL,#0x0200            ; |135| 
        MOV       @_GpioDataRegs+13,AL  ; |135| 
	.line	119
;----------------------------------------------------------------------
; 139 | EDIS;                                        // Disable writing to EALL
;     | OW protected registers                                                 
;----------------------------------------------------------------------
 EDIS
	.line	121
;----------------------------------------------------------------------
; 141 | Fm          = FRECUENCIA_MUESTREO_HZ;        // Sampling Frequency (Hz)
;----------------------------------------------------------------------
        MOVW      DP,#_Fm
        MOV       @_Fm,#10000           ; |141| 
	.line	122
;----------------------------------------------------------------------
; 142 | Tm          = 1/(float)Fm;                   // Sampling Period (s)    
;----------------------------------------------------------------------
        SPM       #0
        MOVIZ     R0H,#16256            ; |142| 
        UI16TOF32 R1H,@_Fm              ; |142| 
        LCR       #FS$$DIV              ; |142| 
        ; call occurs [#FS$$DIV] ; |142| 
        MOVW      DP,#_Tm
        MOV32     @_Tm,R0H
	.line	123
;----------------------------------------------------------------------
; 143 | pwm_period  = 37500000 / (2*Fm);             // timer de micro a 37.5 M
;     | Hz y up/down counter                                                   
; 146 |     // ################################################################
;     | #################                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_Fm
        MOV       ACC,@_Fm << #1        ; |143| 
        MOVZ      AR6,AL
        MOV       AL,#13408
        MOV       AH,#572
        MOVL      P,ACC                 ; |143| 
        MOVB      ACC,#0
        RPT       #31
||     SUBCUL    ACC,XAR6              ; |143| 
        MOV       @_pwm_period,P        ; |143| 
	.line	128
;----------------------------------------------------------------------
; 148 | i_alpha = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_i_alpha
        ZERO      R0H                   ; |148| 
        MOV32     @_i_alpha,R0H
	.line	129
;----------------------------------------------------------------------
; 149 | i_beta = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_i_beta
        MOV32     @_i_beta,R0H
	.line	130
;----------------------------------------------------------------------
; 150 | i_alpha_p1 = 0;                                                        
;----------------------------------------------------------------------
        MOV32     @_i_alpha_p1,R0H
	.line	131
;----------------------------------------------------------------------
; 151 | i_alpha_p2 = 0;                                                        
;----------------------------------------------------------------------
        MOV32     @_i_alpha_p2,R0H
	.line	132
;----------------------------------------------------------------------
; 152 | i_beta_p = 0;                                                          
;----------------------------------------------------------------------
        MOVW      DP,#_i_beta_p
        MOV32     @_i_beta_p,R0H
	.line	133
;----------------------------------------------------------------------
; 153 | ir_alpha_p = 0;                                                        
;----------------------------------------------------------------------
        MOV32     @_ir_alpha_p,R0H
	.line	134
;----------------------------------------------------------------------
; 154 | ir_beta_p = 0;                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_ir_beta_p
        MOV32     @_ir_beta_p,R0H
	.line	135
;----------------------------------------------------------------------
; 155 | i_alpha_med = 0;                                                       
;----------------------------------------------------------------------
        MOVW      DP,#_i_alpha_med
        MOV32     @_i_alpha_med,R0H
	.line	136
;----------------------------------------------------------------------
; 156 | i_beta_med = 0;                                                        
;----------------------------------------------------------------------
        MOV32     @_i_beta_med,R0H
	.line	137
;----------------------------------------------------------------------
; 157 | Werror = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_Werror
        MOV32     @_Werror,R0H
	.line	139
;----------------------------------------------------------------------
; 159 | wm_k    = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_wm_k
        MOV32     @_wm_k,R0H
	.line	140
;----------------------------------------------------------------------
; 160 | wm_km1 = 0;                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_wm_km1
        MOV32     @_wm_km1,R0H
	.line	141
;----------------------------------------------------------------------
; 161 | wr = 0;                                                                
;----------------------------------------------------------------------
        MOV32     @_wr,R0H
	.line	142
;----------------------------------------------------------------------
; 162 | inte_k = 0;                                                            
;----------------------------------------------------------------------
        MOV32     @_inte_k,R0H
	.line	143
;----------------------------------------------------------------------
; 163 | inte_km1 = 0;                                                          
;----------------------------------------------------------------------
        MOV32     @_inte_km1,R0H
	.line	145
;----------------------------------------------------------------------
; 165 | Isd = Isd_ref;                                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Isd
        MOVIZ     R0H,#16145            ; |165| 
        MOVXI     R0H,#60293            ; |165| 
        MOV32     @_Isd,R0H
	.line	146
;----------------------------------------------------------------------
; 166 | Isq = 0;                                                               
; 170 |  // ###################################### INITALITIATION #############
;     | ##############################                                         
;----------------------------------------------------------------------
        ZERO      R0H                   ; |166| 
        MOV32     @_Isq,R0H
	.line	152
;----------------------------------------------------------------------
; 172 | timeout = 0;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_timeout
        MOV       @_timeout,#0          ; |172| 
	.line	153
;----------------------------------------------------------------------
; 173 | mstart  = 0;                                                           
; 176 | //  Start Peripherics                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_mstart
        MOV32     @_mstart,R0H
	.line	158
;----------------------------------------------------------------------
; 178 | PTC5Fepwm5F_start();                                                   
;----------------------------------------------------------------------
        LCR       #_PTC5Fepwm5F_start   ; |178| 
        ; call occurs [#_PTC5Fepwm5F_start] ; |178| 
	.line	159
;----------------------------------------------------------------------
; 179 | PTC5Feqep_start();                                                     
;----------------------------------------------------------------------
        LCR       #_PTC5Feqep_start     ; |179| 
        ; call occurs [#_PTC5Feqep_start] ; |179| 
	.line	160
;----------------------------------------------------------------------
; 180 | PTC5Fadc_start();                                                      
;----------------------------------------------------------------------
        LCR       #_PTC5Fadc_start      ; |180| 
        ; call occurs [#_PTC5Fadc_start] ; |180| 
	.line	161
;----------------------------------------------------------------------
; 181 | PTC5Fcputmr0_start();                                                  
; 183 | //       End Start Peripherics                                         
; 185 | // Hist�ricos                                                          
;----------------------------------------------------------------------
        LCR       #_PTC5Fcputmr0_start  ; |181| 
        ; call occurs [#_PTC5Fcputmr0_start] ; |181| 
	.line	166
;----------------------------------------------------------------------
; 186 | for (sv = 0; sv<CANTIDAD_LOG; sv++ )                                   
;----------------------------------------------------------------------
        MOVB      AL,#0
        CMP       AL,#10000             ; |186| 
        B         $C$L22,GEQ            ; |186| 
        ; branchcc occurs ; |186| 
$C$L21:    
	.line	168
;----------------------------------------------------------------------
; 188 | ias[sv] = 0;                                                           
;----------------------------------------------------------------------
        SETC      SXM
        MOVL      XAR4,#_ias            ; |188| 
        MOV       ACC,AL                ; |188| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |188| 
	.line	169
;----------------------------------------------------------------------
; 189 | ibs[sv] = 0;                                                           
;----------------------------------------------------------------------
        MOV       ACC,AL                ; |189| 
        MOVL      XAR4,#_ibs            ; |189| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |189| 
	.line	170
;----------------------------------------------------------------------
; 190 | ids[sv] = 0;                                                           
;----------------------------------------------------------------------
        MOV       ACC,AL                ; |190| 
        MOVL      XAR4,#_ids            ; |190| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |190| 
	.line	171
;----------------------------------------------------------------------
; 191 | ies[sv] = 0;                                                           
;----------------------------------------------------------------------
        MOV       ACC,AL                ; |191| 
        MOVL      XAR4,#_ies            ; |191| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |191| 
	.line	172
;----------------------------------------------------------------------
; 192 | ids_m[sv] = 0;                                                         
;----------------------------------------------------------------------
        MOV       ACC,AL                ; |192| 
        MOVL      XAR4,#_ids_m          ; |192| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |192| 
	.line	173
;----------------------------------------------------------------------
; 193 | iqs_m[sv] = 0;                                                         
;----------------------------------------------------------------------
        MOV       ACC,AL                ; |193| 
        MOVL      XAR4,#_iqs_m          ; |193| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |193| 
	.line	174
;----------------------------------------------------------------------
; 194 | wmech1[sv] = 0;                                                        
; 195 | //we_m[sv] = 0;                                                        
;----------------------------------------------------------------------
        MOV       ACC,AL                ; |194| 
        MOVL      XAR4,#_wmech1         ; |194| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |194| 
	.line	176
;----------------------------------------------------------------------
; 196 | svopt[sv] = 0;  //A�ADIDO POR MI                                       
; 197 | //ias_p1[sv] = 0;  //A�ADIDO POR MI                                    
; 198 | //ias_p2[sv] = 0;  //A�ADIDO POR MI                                    
;----------------------------------------------------------------------
        MOV       ACC,AL                ; |196| 
        MOVL      XAR4,#_svopt          ; |196| 
        ADDL      XAR4,ACC
        MOV       *+XAR4[0],#0          ; |196| 
	.line	166
        ADDB      AL,#1                 ; |186| 
        CMP       AL,#10000             ; |186| 
        B         $C$L21,LT             ; |186| 
        ; branchcc occurs ; |186| 
$C$L22:    
	.line	181
;----------------------------------------------------------------------
; 201 | EALLOW;                                                                
;----------------------------------------------------------------------
 EALLOW
	.line	182
;----------------------------------------------------------------------
; 202 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;
;     |                  // Stop all the TB clocks                             
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+12
        AND       @_SysCtrlRegs+12,#0xfffb ; |202| 
	.line	183
;----------------------------------------------------------------------
; 203 | SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 1;
;     |                  // Start all the TB clocks                            
;----------------------------------------------------------------------
        AND       AL,@_SysCtrlRegs+12,#0xfffb ; |203| 
        ORB       AL,#0x04              ; |203| 
        MOV       @_SysCtrlRegs+12,AL   ; |203| 
	.line	184
;----------------------------------------------------------------------
; 204 | EDIS;                                                                  
;----------------------------------------------------------------------
 EDIS
	.line	186
;----------------------------------------------------------------------
; 206 | EINT;
;     |                                                                        
;     |        // Enable interrupts to CPU-Level                               
;----------------------------------------------------------------------
 clrc INTM
	.line	188
;----------------------------------------------------------------------
; 208 | CpuTimer0Regs.TCR.bit.TRB = 1;
;     |                          // Reload CPU-TIMER0 Period                   
;----------------------------------------------------------------------
        MOVW      DP,#_CpuTimer0Regs+4
        AND       AL,@_CpuTimer0Regs+4,#0xffdf ; |208| 
        ORB       AL,#0x20              ; |208| 
        MOV       @_CpuTimer0Regs+4,AL  ; |208| 
	.line	191
;----------------------------------------------------------------------
; 211 | GpioDataRegs.GPBSET.bit.GPIO49  = 1;                                   
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+11
        AND       AL,@_GpioDataRegs+11,#0xfffd ; |211| 
        ORB       AL,#0x02              ; |211| 
        MOV       @_GpioDataRegs+11,AL  ; |211| 
	.line	193
;----------------------------------------------------------------------
; 213 | wm_ref = 0.5*wnom;                                                     
; 215 | // C�lculo de matrices dependientes de wr. Se supone wr = cte          
;----------------------------------------------------------------------
        MOVIZ     R0H,#16977            ; |213| 
        MOVXI     R0H,#28797            ; |213| 
        MOVW      DP,#_wm_ref
        MOV32     @_wm_ref,R0H
	.line	196
;----------------------------------------------------------------------
; 216 | wr = P*wm_ref;                                                         
;----------------------------------------------------------------------
        MPYF32    R0H,R0H,#16448        ; |216| 
        MOVW      DP,#_wr
        MOV32     @_wr,R0H
	.line	198
;----------------------------------------------------------------------
; 218 | while(!stop)                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_stop
        MOV       AL,@_stop             ; |218| 
        BF        $C$L30,NEQ            ; |218| 
        ; branchcc occurs ; |218| 
$C$L23:    
	.line	200
;----------------------------------------------------------------------
; 220 | TRIGGER_TOGGLE_54;                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xffbf ; |220| 
        ORB       AL,#0x40              ; |220| 
        MOV       @_GpioDataRegs+15,AL  ; |220| 
	.line	202
;----------------------------------------------------------------------
; 222 | ret_val_mon = (*callmon28335)(); // Arrancar 28335. MSK Execute Control
;----------------------------------------------------------------------
        SPM       #0
        MOVL      XAR7,#3342336         ; |222| 
        LCR       *XAR7                 ; |222| 
        ; call occurs [XAR7] ; |222| 
        MOVW      DP,#_ret_val_mon
        MOV       @_ret_val_mon,AL      ; |222| 
	.line	203
;----------------------------------------------------------------------
; 223 | GpioDataRegs.GPBSET.bit.GPIO48  = 1; //ventiladores encendidos         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+11
        AND       AL,@_GpioDataRegs+11,#0xfffe ; |223| 
        ORB       AL,#0x01              ; |223| 
        MOV       @_GpioDataRegs+11,AL  ; |223| 
	.line	205
;----------------------------------------------------------------------
; 225 | TRIGGER_TOGGLE_55;                                                     
; 226 | while(timeout) // Stand by the timeout Flag cleared in the EPWM1 interr
;     | upt                                                                    
;----------------------------------------------------------------------
        AND       AL,@_GpioDataRegs+15,#0xff7f ; |225| 
        ORB       AL,#0x80              ; |225| 
        MOV       @_GpioDataRegs+15,AL  ; |225| 
$C$L24:    
	.line	208
        MOVW      DP,#_timeout
        MOV       AL,@_timeout          ; |228| 
        BF        $C$L24,NEQ            ; |228| 
        ; branchcc occurs ; |228| 
	.line	209
;----------------------------------------------------------------------
; 229 | TRIGGER_TOGGLE_55;                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+15
        AND       AL,@_GpioDataRegs+15,#0xff7f ; |229| 
        ORB       AL,#0x80              ; |229| 
        MOV       @_GpioDataRegs+15,AL  ; |229| 
	.line	211
;----------------------------------------------------------------------
; 231 | timeout = 1;
;     |                                                  // Set PWM Time Out Fl
;     | ag                                                                     
; 234 | // Deshabilitaci�n Kp de fase, en_fallo, se�al ctrl m�quina DC fallo   
; 235 | //=====================================================================
;     | ======                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_timeout
        MOVB      @_timeout,#1,UNC      ; |231| 
	.line	216
;----------------------------------------------------------------------
; 236 | if (mstart2 != 0)                                                      
;----------------------------------------------------------------------
        MOVW      DP,#_mstart2
        MOV32     R0H,@_mstart2
        CMPF32    R0H,#0                ; |236| 
        MOVST0    ZF, NF                ; |236| 
        BF        $C$L26,EQ             ; |236| 
        ; branchcc occurs ; |236| 
	.line	218
;----------------------------------------------------------------------
; 238 | contfallo++;                                                           
;----------------------------------------------------------------------
        MOV32     R0H,@_contfallo
        ADDF32    R0H,R0H,#16256        ; |238| 
        NOP
        MOV32     @_contfallo,R0H
	.line	219
;----------------------------------------------------------------------
; 239 | if (contfallo == 2.0*FRECUENCIA_MUESTREO_HZ)                           
;----------------------------------------------------------------------
        MOV32     R1H,@_contfallo
        MOVIZ     R0H,#18076            ; |239| 
        MOVXI     R0H,#16384            ; |239| 
        CMPF32    R1H,R0H               ; |239| 
        MOVST0    ZF, NF                ; |239| 
        BF        $C$L26,NEQ            ; |239| 
        ; branchcc occurs ; |239| 
	.line	221
;----------------------------------------------------------------------
; 241 | contfallo = 0;                                                         
;----------------------------------------------------------------------
        ZERO      R0H                   ; |241| 
        MOV32     @_contfallo,R0H
	.line	222
;----------------------------------------------------------------------
; 242 | mstart=1;                                                              
;----------------------------------------------------------------------
        MOVIZ     R0H,#16256            ; |242| 
        MOV32     @_mstart,R0H
	.line	223
;----------------------------------------------------------------------
; 243 | mstart2=0;                                                             
;----------------------------------------------------------------------
        ZERO      R0H                   ; |243| 
        MOV32     @_mstart2,R0H
	.line	224
;----------------------------------------------------------------------
; 244 | logger = 0;                                                            
; 249 | //----------------- Med. y  acondicionamiento corrientes de fase - Tran
;     | s. alfa-beta-x-y ------------------*/                                  
; 250 | //TRIGGER_TOGGLE_56;                                                   
; 251 | // ADC converter SEQ 1 is Busy                                         
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        MOV       @_logger,#0           ; |244| 
	.line	232
;----------------------------------------------------------------------
; 252 | while(AdcRegs.ADCST.bit.SEQ1_BSY)                                      
;----------------------------------------------------------------------
        B         $C$L26,UNC            ; |252| 
        ; branch occurs ; |252| 
$C$L25:    
	.line	234
;----------------------------------------------------------------------
; 254 | LED2_TOGGLE;                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+7
        AND       AL,@_GpioDataRegs+7,#0xfffd ; |254| 
        ORB       AL,#0x02              ; |254| 
        MOV       @_GpioDataRegs+7,AL   ; |254| 
$C$L26:    
	.line	235
        MOVW      DP,#_AdcRegs+25
        TBIT      @_AdcRegs+25,#2       ; |255| 
        BF        $C$L25,TC             ; |255| 
        ; branchcc occurs ; |255| 
	.line	237
;----------------------------------------------------------------------
; 257 | Ia_medido = AdcMirror.ADCRESULT0;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror
        UI16TOF32 R0H,@_AdcMirror       ; |257| 
        MOVW      DP,#_Ia_medido
        MOV32     @_Ia_medido,R0H
	.line	238
;----------------------------------------------------------------------
; 258 | Ib_medido = AdcMirror.ADCRESULT1;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+1
        UI16TOF32 R0H,@_AdcMirror+1     ; |258| 
        MOVW      DP,#_Ib_medido
        MOV32     @_Ib_medido,R0H
	.line	239
;----------------------------------------------------------------------
; 259 | Id_medido = AdcMirror.ADCRESULT2;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+2
        UI16TOF32 R0H,@_AdcMirror+2     ; |259| 
        MOVW      DP,#_Id_medido
        MOV32     @_Id_medido,R0H
	.line	240
;----------------------------------------------------------------------
; 260 | Ie_medido = AdcMirror.ADCRESULT3;                                      
;----------------------------------------------------------------------
        MOVW      DP,#_AdcMirror+3
        UI16TOF32 R0H,@_AdcMirror+3     ; |260| 
        MOVW      DP,#_Ie_medido
        MOV32     @_Ie_medido,R0H
	.line	241
;----------------------------------------------------------------------
; 261 | Ic_medido = -Ia_medido -Ib_medido -Id_medido -Ie_medido;               
; 262 | sopt = 15;  // para meter Vdc por fase a y 0 en resto                  
; 266 |         //TRIGGER_TOGGLE_57;                                           
; 267 |         //------------------------------- Generaci�n de disparos ------
;     | ----------------------------*/                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Ia_medido
        MOV32     R1H,@_Ib_medido
        MOV32     R0H,@_Ia_medido
        NEGF32    R0H,R0H               ; |261| 
        MOVW      DP,#_Id_medido
        SUBF32    R1H,R0H,R1H           ; |261| 
        MOV32     R0H,@_Id_medido
        SUBF32    R1H,R1H,R0H           ; |261| 
        MOV32     R0H,@_Ie_medido
        SUBF32    R0H,R1H,R0H           ; |261| 
        NOP
        MOV32     @_Ic_medido,R0H
	.line	248
;----------------------------------------------------------------------
; 268 | EPwm1Regs.CMPA.half.CMPA = (1-SCMP[sopt][0])*pwm_period;    // Load EPW
;     | M1 CMPA                                                                
;----------------------------------------------------------------------
        MOV       T,#15                 ; |268| 
        MOVL      XAR5,#_SCMP           ; |268| 
        MPYB      ACC,T,#10             ; |268| 
        ADDL      XAR5,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |268| 
        MOV32     R0H,*+XAR5[0]
        SUBF32    R0H,#16256,R0H        ; |268| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |268| 
        NOP
        F32TOUI16 R0H,R0H               ; |268| 
        NOP
        MOVW      DP,#_EPwm1Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm1Regs+9,AL      ; |268| 
	.line	249
;----------------------------------------------------------------------
; 269 | EPwm2Regs.CMPA.half.CMPA = (1-SCMP[sopt][1])*pwm_period;    // Load EPW
;     | M2 CMPA                                                                
;----------------------------------------------------------------------
        MPYB      ACC,T,#10             ; |269| 
        MOVL      XAR5,#_SCMP+2         ; |269| 
        ADDL      XAR5,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |269| 
        MOV32     R0H,*+XAR5[0]
        SUBF32    R0H,#16256,R0H        ; |269| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |269| 
        NOP
        F32TOUI16 R0H,R0H               ; |269| 
        NOP
        MOVW      DP,#_EPwm2Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm2Regs+9,AL      ; |269| 
	.line	250
;----------------------------------------------------------------------
; 270 | EPwm3Regs.CMPA.half.CMPA = (1-SCMP[sopt][2])*pwm_period;    // Load EPW
;     | M3 CMPA                                                                
;----------------------------------------------------------------------
        MPYB      ACC,T,#10             ; |270| 
        MOVL      XAR5,#_SCMP+4         ; |270| 
        ADDL      XAR5,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |270| 
        MOV32     R0H,*+XAR5[0]
        SUBF32    R0H,#16256,R0H        ; |270| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |270| 
        NOP
        F32TOUI16 R0H,R0H               ; |270| 
        NOP
        MOVW      DP,#_EPwm3Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm3Regs+9,AL      ; |270| 
	.line	251
;----------------------------------------------------------------------
; 271 | EPwm4Regs.CMPA.half.CMPA = (1-SCMP[sopt][3])*pwm_period;    // Load EPW
;     | M4 CMPA                                                                
;----------------------------------------------------------------------
        MPYB      ACC,T,#10             ; |271| 
        MOVL      XAR5,#_SCMP+6         ; |271| 
        ADDL      XAR5,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |271| 
        MOV32     R0H,*+XAR5[0]
        SUBF32    R0H,#16256,R0H        ; |271| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |271| 
        NOP
        F32TOUI16 R0H,R0H               ; |271| 
        NOP
        MOVW      DP,#_EPwm4Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm4Regs+9,AL      ; |271| 
	.line	252
;----------------------------------------------------------------------
; 272 | EPwm5Regs.CMPA.half.CMPA = (1-SCMP[sopt][4])*pwm_period;    // Load EPW
;     | M5 CMPA                                                                
; 273 | //------------------------------- End Generaci�n de disparos ----------
;     | ---------------------*/                                                
; 274 | //TRIGGER_TOGGLE_57;                                                   
;----------------------------------------------------------------------
        MOVL      XAR4,#_SCMP+8         ; |272| 
        MPYB      ACC,T,#10             ; |272| 
        ADDL      XAR4,ACC
        MOVW      DP,#_pwm_period
        UI16TOF32 R1H,@_pwm_period      ; |272| 
        MOV32     R0H,*+XAR4[0]
        SUBF32    R0H,#16256,R0H        ; |272| 
        NOP
        MPYF32    R0H,R1H,R0H           ; |272| 
        NOP
        F32TOUI16 R0H,R0H               ; |272| 
        NOP
        MOVW      DP,#_EPwm5Regs+9
        MOV32     ACC,R0H
        MOV       @_EPwm5Regs+9,AL      ; |272| 
	.line	257
;----------------------------------------------------------------------
; 277 | Tcarga = CCarga*(Isd*Isq_par);                                         
;----------------------------------------------------------------------
        MOVW      DP,#_Isq_par
        MOV32     R1H,@_Isq_par
        MOVW      DP,#_Isd
        MOV32     R0H,@_Isd
        MOVW      DP,#_CCarga
        MPYF32    R0H,R1H,R0H           ; |277| 
        MOV32     R1H,@_CCarga
        MPYF32    R0H,R1H,R0H           ; |277| 
        NOP
        MOV32     @_Tcarga,R0H
	.line	258
;----------------------------------------------------------------------
; 278 | Tcarga_pre=Tcarga;                                                     
; 280 | // Transf. de las corrientes de medida a Isd-Isq:                      
;----------------------------------------------------------------------
        MOVL      ACC,@_Tcarga          ; |278| 
        MOVL      @_Tcarga_pre,ACC      ; |278| 
	.line	261
;----------------------------------------------------------------------
; 281 | Id_med =  i_alpha_med*costhetae + i_beta_med*sinthetae;                
;----------------------------------------------------------------------
        MOVW      DP,#_costhetae
        MOV32     R1H,@_costhetae
        MOVW      DP,#_i_alpha_med
        MOV32     R0H,@_i_alpha_med
        MOVW      DP,#_sinthetae
        MOV32     R2H,@_sinthetae
        MOVW      DP,#_i_beta_med

        MOV32     R1H,@_i_beta_med
||      MPYF32    R0H,R1H,R0H           ; |281| 

        MPYF32    R1H,R2H,R1H           ; |281| 
        NOP
        ADDF32    R0H,R1H,R0H           ; |281| 
        MOVW      DP,#_Id_med
        MOV32     @_Id_med,R0H
	.line	262
;----------------------------------------------------------------------
; 282 | Iq_med = -i_alpha_med*sinthetae + i_beta_med*costhetae;                
;----------------------------------------------------------------------
        MOVW      DP,#_i_alpha_med
        MOV32     R0H,@_i_alpha_med
        MOVW      DP,#_costhetae
        NEGF32    R0H,R0H               ; |282| 
        MOV32     R2H,@_costhetae
        MOVW      DP,#_i_beta_med
        MOV32     R1H,@_i_beta_med
        MOVW      DP,#_sinthetae

        MOV32     R1H,@_sinthetae
||      MPYF32    R2H,R2H,R1H           ; |282| 

        MPYF32    R0H,R1H,R0H           ; |282| 
        NOP
        ADDF32    R0H,R2H,R0H           ; |282| 
        MOVW      DP,#_Iq_med
        MOV32     @_Iq_med,R0H
	.line	266
;----------------------------------------------------------------------
; 286 | if ((dsampled >=DOWNSAMP)&&(mstart))                                   
; 288 | fds = 1;                                                               
; 289 | dsampled=0;                                                            
; 291 | else                                                                   
;----------------------------------------------------------------------
        MOV32     R0H,@_mstart
        CMPF32    R0H,#0                ; |286| 
        MOVST0    ZF, NF                ; |286| 
        BF        $C$L27,NEQ            ; |286| 
        ; branchcc occurs ; |286| 
	.line	273
;----------------------------------------------------------------------
; 293 | dsampled++;                                                            
; 295 | //Medida decimada:                                                     
;----------------------------------------------------------------------
        MOVW      DP,#_dsampled
        INC       @_dsampled            ; |293| 
        B         $C$L28,UNC            ; |293| 
        ; branch occurs ; |293| 
$C$L27:    
	.line	268
        MOVB      XAR1,#1               ; |288| 
	.line	269
        MOVW      DP,#_dsampled
        MOV       @_dsampled,#0         ; |289| 
$C$L28:    
	.line	277
;----------------------------------------------------------------------
; 297 | if ((logger < CANTIDAD_LOG)&&(fds)&&(mstart)) // (mstart2==1)          
;----------------------------------------------------------------------
        CMP       @_logger,#10000       ; |297| 
        B         $C$L29,HIS            ; |297| 
        ; branchcc occurs ; |297| 
        MOV       AL,AR1
        BF        $C$L29,EQ             ; |297| 
        ; branchcc occurs ; |297| 
        CMPF32    R0H,#0                ; |297| 
        MOVST0    ZF, NF                ; |297| 
        BF        $C$L29,EQ             ; |297| 
        ; branchcc occurs ; |297| 
	.line	279
;----------------------------------------------------------------------
; 299 | ias[logger] = (Uint16)Ia_medido;                                       
;----------------------------------------------------------------------
        MOVZ      AR0,@_logger          ; |299| 
        MOVW      DP,#_Ia_medido
        MOV32     R0H,@_Ia_medido
        F32TOUI16 R0H,R0H               ; |299| 
        NOP
        MOVL      XAR4,#_ias            ; |299| 
        MOV32     ACC,R0H
        MOV       *+XAR4[AR0],AL        ; |299| 
	.line	280
;----------------------------------------------------------------------
; 300 | ibs[logger] = (Uint16)Ib_medido;                                       
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        MOVZ      AR0,@_logger          ; |300| 
        MOVW      DP,#_Ib_medido
        MOV32     R0H,@_Ib_medido
        F32TOUI16 R0H,R0H               ; |300| 
        NOP
        MOVL      XAR4,#_ibs            ; |300| 
        MOV32     ACC,R0H
        MOV       *+XAR4[AR0],AL        ; |300| 
	.line	281
;----------------------------------------------------------------------
; 301 | ids[logger] = (Uint16)Id_medido;                                       
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        MOVZ      AR0,@_logger          ; |301| 
        MOVW      DP,#_Id_medido
        MOV32     R0H,@_Id_medido
        F32TOUI16 R0H,R0H               ; |301| 
        NOP
        MOVL      XAR4,#_ids            ; |301| 
        MOV32     ACC,R0H
        MOV       *+XAR4[AR0],AL        ; |301| 
	.line	282
;----------------------------------------------------------------------
; 302 | ies[logger] = (Uint16)Ie_medido;                                       
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        MOVZ      AR0,@_logger          ; |302| 
        MOVW      DP,#_Ie_medido
        MOV32     R0H,@_Ie_medido
        F32TOUI16 R0H,R0H               ; |302| 
        NOP
        MOVL      XAR4,#_ies            ; |302| 
        MOV32     ACC,R0H
        MOV       *+XAR4[AR0],AL        ; |302| 
	.line	284
;----------------------------------------------------------------------
; 304 | logger++;                                                              
;----------------------------------------------------------------------
        MOVW      DP,#_logger
        INC       @_logger              ; |304| 
	.line	285
;----------------------------------------------------------------------
; 305 | fds=0;                                                                 
; 310 | //TRIGGER_TOGGLE_54;                                                   
; 312 | }// while(!stop)                                                       
;----------------------------------------------------------------------
        MOVB      XAR1,#0
$C$L29:    
	.line	198
        MOV       AL,@_stop             ; |218| 
        BF        $C$L23,EQ             ; |218| 
        ; branchcc occurs ; |218| 
$C$L30:    
	.line	294
;----------------------------------------------------------------------
; 314 | DINT;                                                                  
;----------------------------------------------------------------------
 setc INTM
	.line	296
;----------------------------------------------------------------------
; 316 | EALLOW;                                          // Enable writing to E
;     | ALLOW protected registers                                              
;----------------------------------------------------------------------
 EALLOW
	.line	298
;----------------------------------------------------------------------
; 318 | GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfffc ; |318| 
	.line	299
;----------------------------------------------------------------------
; 319 | GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffe ; |319| 
        ORB       AL,#0x01              ; |319| 
        MOV       @_GpioCtrlRegs+10,AL  ; |319| 
	.line	300
;----------------------------------------------------------------------
; 320 | GpioDataRegs.GPACLEAR.bit.GPIO0      = 1;       // Clear GPIO0         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffe ; |320| 
        ORB       AL,#0x01              ; |320| 
        MOV       @_GpioDataRegs+4,AL   ; |320| 
	.line	302
;----------------------------------------------------------------------
; 322 | GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfff3 ; |322| 
	.line	303
;----------------------------------------------------------------------
; 323 | GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffd ; |323| 
        ORB       AL,#0x02              ; |323| 
        MOV       @_GpioCtrlRegs+10,AL  ; |323| 
	.line	304
;----------------------------------------------------------------------
; 324 | GpioDataRegs.GPACLEAR.bit.GPIO1      = 1;       // Clear GPIO1         
; 325 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffd ; |324| 
        ORB       AL,#0x02              ; |324| 
        MOV       @_GpioDataRegs+4,AL   ; |324| 
	.line	307
;----------------------------------------------------------------------
; 327 | GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xffcf ; |327| 
	.line	308
;----------------------------------------------------------------------
; 328 | GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfffb ; |328| 
        ORB       AL,#0x04              ; |328| 
        MOV       @_GpioCtrlRegs+10,AL  ; |328| 
	.line	309
;----------------------------------------------------------------------
; 329 | GpioDataRegs.GPACLEAR.bit.GPIO2      = 1;       // Clear GPIO2         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfffb ; |329| 
        ORB       AL,#0x04              ; |329| 
        MOV       @_GpioDataRegs+4,AL   ; |329| 
	.line	311
;----------------------------------------------------------------------
; 331 | GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xff3f ; |331| 
	.line	312
;----------------------------------------------------------------------
; 332 | GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfff7 ; |332| 
        ORB       AL,#0x08              ; |332| 
        MOV       @_GpioCtrlRegs+10,AL  ; |332| 
	.line	313
;----------------------------------------------------------------------
; 333 | GpioDataRegs.GPACLEAR.bit.GPIO3      = 1;       // Clear GPIO3         
; 334 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfff7 ; |333| 
        ORB       AL,#0x08              ; |333| 
        MOV       @_GpioDataRegs+4,AL   ; |333| 
	.line	316
;----------------------------------------------------------------------
; 336 | GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xfcff ; |336| 
	.line	317
;----------------------------------------------------------------------
; 337 | GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffef ; |337| 
        ORB       AL,#0x10              ; |337| 
        MOV       @_GpioCtrlRegs+10,AL  ; |337| 
	.line	318
;----------------------------------------------------------------------
; 338 | GpioDataRegs.GPACLEAR.bit.GPIO4      = 1;       // Clear GPIO4         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffef ; |338| 
        ORB       AL,#0x10              ; |338| 
        MOV       @_GpioDataRegs+4,AL   ; |338| 
	.line	320
;----------------------------------------------------------------------
; 340 | GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xf3ff ; |340| 
	.line	321
;----------------------------------------------------------------------
; 341 | GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffdf ; |341| 
        ORB       AL,#0x20              ; |341| 
        MOV       @_GpioCtrlRegs+10,AL  ; |341| 
	.line	322
;----------------------------------------------------------------------
; 342 | GpioDataRegs.GPACLEAR.bit.GPIO5      = 1;       // Clear GPIO5         
; 343 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffdf ; |342| 
        ORB       AL,#0x20              ; |342| 
        MOV       @_GpioDataRegs+4,AL   ; |342| 
	.line	325
;----------------------------------------------------------------------
; 345 | GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0xcfff ; |345| 
	.line	326
;----------------------------------------------------------------------
; 346 | GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xffbf ; |346| 
        ORB       AL,#0x40              ; |346| 
        MOV       @_GpioCtrlRegs+10,AL  ; |346| 
	.line	327
;----------------------------------------------------------------------
; 347 | GpioDataRegs.GPACLEAR.bit.GPIO6      = 1;       // Clear GPIO6         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xffbf ; |347| 
        ORB       AL,#0x40              ; |347| 
        MOV       @_GpioDataRegs+4,AL   ; |347| 
	.line	329
;----------------------------------------------------------------------
; 349 | GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+6
        AND       @_GpioCtrlRegs+6,#0x3fff ; |349| 
	.line	330
;----------------------------------------------------------------------
; 350 | GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xff7f ; |350| 
        ORB       AL,#0x80              ; |350| 
        MOV       @_GpioCtrlRegs+10,AL  ; |350| 
	.line	331
;----------------------------------------------------------------------
; 351 | GpioDataRegs.GPACLEAR.bit.GPIO7      = 1;       // Clear GPIO7         
; 352 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xff7f ; |351| 
        ORB       AL,#0x80              ; |351| 
        MOV       @_GpioDataRegs+4,AL   ; |351| 
	.line	334
;----------------------------------------------------------------------
; 354 | GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfffc ; |354| 
	.line	335
;----------------------------------------------------------------------
; 355 | GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfeff ; |355| 
        OR        AL,#0x0100            ; |355| 
        MOV       @_GpioCtrlRegs+10,AL  ; |355| 
	.line	336
;----------------------------------------------------------------------
; 356 | GpioDataRegs.GPACLEAR.bit.GPIO8      = 1;       // Clear GPIO8         
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfeff ; |356| 
        OR        AL,#0x0100            ; |356| 
        MOV       @_GpioDataRegs+4,AL   ; |356| 
	.line	338
;----------------------------------------------------------------------
; 358 | GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as
;     | Digital I/O                                                            
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xfff3 ; |358| 
	.line	339
;----------------------------------------------------------------------
; 359 | GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as
;     | digital Output                                                         
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfdff ; |359| 
        OR        AL,#0x0200            ; |359| 
        MOV       @_GpioCtrlRegs+10,AL  ; |359| 
	.line	340
;----------------------------------------------------------------------
; 360 | GpioDataRegs.GPACLEAR.bit.GPIO9      = 1;       // Clear GPIO9         
; 361 | //#####################################################################
;     | ######################                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfdff ; |360| 
        OR        AL,#0x0200            ; |360| 
        MOV       @_GpioDataRegs+4,AL   ; |360| 
	.line	343
;----------------------------------------------------------------------
; 363 | GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xffcf ; |363| 
	.line	344
;----------------------------------------------------------------------
; 364 | GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xfbff ; |364| 
        OR        AL,#0x0400            ; |364| 
        MOV       @_GpioCtrlRegs+10,AL  ; |364| 
	.line	345
;----------------------------------------------------------------------
; 365 | GpioDataRegs.GPACLEAR.bit.GPIO10                  = 1;       // Clear G
;     | PIO10                                                                  
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xfbff ; |365| 
        OR        AL,#0x0400            ; |365| 
        MOV       @_GpioDataRegs+4,AL   ; |365| 
	.line	347
;----------------------------------------------------------------------
; 367 | GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as
;     |  Digital I/O                                                           
;----------------------------------------------------------------------
        MOVW      DP,#_GpioCtrlRegs+7
        AND       @_GpioCtrlRegs+7,#0xff3f ; |367| 
	.line	348
;----------------------------------------------------------------------
; 368 | GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as
;     |  digital Output                                                        
;----------------------------------------------------------------------
        AND       AL,@_GpioCtrlRegs+10,#0xf7ff ; |368| 
        OR        AL,#0x0800            ; |368| 
        MOV       @_GpioCtrlRegs+10,AL  ; |368| 
	.line	349
;----------------------------------------------------------------------
; 369 | GpioDataRegs.GPACLEAR.bit.GPIO11             = 1;       // Clear GPIO11
; 370 | //#####################################################################
;     | ######################                                                 
; 371 |                               // Enable writing to EALLOW protected reg
;     | isters                                                                 
;----------------------------------------------------------------------
        MOVW      DP,#_GpioDataRegs+4
        AND       AL,@_GpioDataRegs+4,#0xf7ff ; |369| 
        OR        AL,#0x0800            ; |369| 
        MOV       @_GpioDataRegs+4,AL   ; |369| 
	.line	352
;----------------------------------------------------------------------
; 372 | SysCtrlRegs.WDCR = 0x000F;            // Enable Watchdog and write inco
;     | rrect WD Check Bits                                                    
;----------------------------------------------------------------------
        MOVW      DP,#_SysCtrlRegs+25
        MOVB      @_SysCtrlRegs+25,#15,UNC ; |372| 
	.line	353
;----------------------------------------------------------------------
; 373 | asm(" NOP");                                                           
;----------------------------------------------------------------------
 NOP
	.line	354
;----------------------------------------------------------------------
; 374 | asm(" NOP");                                                           
;----------------------------------------------------------------------
 NOP
	.line	355
;----------------------------------------------------------------------
; 375 | asm(" NOP");                                                           
;----------------------------------------------------------------------
 NOP
	.line	356
;----------------------------------------------------------------------
; 376 | }// void main ()                                                       
;----------------------------------------------------------------------
        SPM       #0
        MOVL      XAR1,*--SP
        LRETR
        ; return occurs
	.endfunc	376,000000018h,2
;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************
	.sym	_PINT, 0, 144, 13, 22
	.sym	_int16, 0, 4, 13, 16
	.sym	_Uint16, 0, 14, 13, 16
	.sym	_Uint16, 0, 14, 13, 16
	.sym	_int32, 0, 5, 13, 32
	.sym	_Uint32, 0, 15, 13, 32
	.sym	_Uint32, 0, 15, 13, 32
	.sym	_float32, 0, 6, 13, 32
	.sym	_float64, 0, 11, 13, 64
	.stag	_TBCTL_BITS, 16
	.member	_CTRMODE, 0, 14, 18, 2
	.member	_PHSEN, 2, 14, 18, 1
	.member	_PRDLD, 3, 14, 18, 1
	.member	_SYNCOSEL, 4, 14, 18, 2
	.member	_SWFSYNC, 6, 14, 18, 1
	.member	_HSPCLKDIV, 7, 14, 18, 3
	.member	_CLKDIV, 10, 14, 18, 3
	.member	_PHSDIR, 13, 14, 18, 1
	.member	_FREE_SOFT, 14, 14, 18, 2
	.eos
	.utag	_TBCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TBCTL_BITS
	.eos
	.stag	_TBSTS_BITS, 16
	.member	_CTRDIR, 0, 14, 18, 1
	.member	_SYNCI, 1, 14, 18, 1
	.member	_CTRMAX, 2, 14, 18, 1
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_TBSTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TBSTS_BITS
	.eos
	.stag	_TBPHS_HRPWM_REG, 32
	.member	_TBPHSHR, 0, 14, 8, 16
	.member	_TBPHS, 16, 14, 8, 16
	.eos
	.utag	_TBPHS_HRPWM_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _TBPHS_HRPWM_REG
	.eos
	.stag	_CMPCTL_BITS, 16
	.member	_LOADAMODE, 0, 14, 18, 2
	.member	_LOADBMODE, 2, 14, 18, 2
	.member	_SHDWAMODE, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 1
	.member	_SHDWBMODE, 6, 14, 18, 1
	.member	_rsvd2, 7, 14, 18, 1
	.member	_SHDWAFULL, 8, 14, 18, 1
	.member	_SHDWBFULL, 9, 14, 18, 1
	.member	_rsvd3, 10, 14, 18, 6
	.eos
	.utag	_CMPCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _CMPCTL_BITS
	.eos
	.stag	_CMPA_HRPWM_REG, 32
	.member	_CMPAHR, 0, 14, 8, 16
	.member	_CMPA, 16, 14, 8, 16
	.eos
	.utag	_CMPA_HRPWM_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _CMPA_HRPWM_REG
	.eos
	.stag	_AQCTL_BITS, 16
	.member	_ZRO, 0, 14, 18, 2
	.member	_PRD, 2, 14, 18, 2
	.member	_CAU, 4, 14, 18, 2
	.member	_CAD, 6, 14, 18, 2
	.member	_CBU, 8, 14, 18, 2
	.member	_CBD, 10, 14, 18, 2
	.member	_rsvd, 12, 14, 18, 4
	.eos
	.utag	_AQCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _AQCTL_BITS
	.eos
	.stag	_AQSFRC_BITS, 16
	.member	_ACTSFA, 0, 14, 18, 2
	.member	_OTSFA, 2, 14, 18, 1
	.member	_ACTSFB, 3, 14, 18, 2
	.member	_OTSFB, 5, 14, 18, 1
	.member	_RLDCSF, 6, 14, 18, 2
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_AQSFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _AQSFRC_BITS
	.eos
	.stag	_AQCSFRC_BITS, 16
	.member	_CSFA, 0, 14, 18, 2
	.member	_CSFB, 2, 14, 18, 2
	.member	_rsvd1, 4, 14, 18, 12
	.eos
	.utag	_AQCSFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _AQCSFRC_BITS
	.eos
	.stag	_DBCTL_BITS, 16
	.member	_OUT_MODE, 0, 14, 18, 2
	.member	_POLSEL, 2, 14, 18, 2
	.member	_IN_MODE, 4, 14, 18, 2
	.member	_rsvd1, 6, 14, 18, 10
	.eos
	.utag	_DBCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DBCTL_BITS
	.eos
	.stag	_TZSEL_BITS, 16
	.member	_CBC1, 0, 14, 18, 1
	.member	_CBC2, 1, 14, 18, 1
	.member	_CBC3, 2, 14, 18, 1
	.member	_CBC4, 3, 14, 18, 1
	.member	_CBC5, 4, 14, 18, 1
	.member	_CBC6, 5, 14, 18, 1
	.member	_rsvd1, 6, 14, 18, 2
	.member	_OSHT1, 8, 14, 18, 1
	.member	_OSHT2, 9, 14, 18, 1
	.member	_OSHT3, 10, 14, 18, 1
	.member	_OSHT4, 11, 14, 18, 1
	.member	_OSHT5, 12, 14, 18, 1
	.member	_OSHT6, 13, 14, 18, 1
	.member	_rsvd2, 14, 14, 18, 2
	.eos
	.utag	_TZSEL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZSEL_BITS
	.eos
	.stag	_TZCTL_BITS, 16
	.member	_TZA, 0, 14, 18, 2
	.member	_TZB, 2, 14, 18, 2
	.member	_rsvd, 4, 14, 18, 12
	.eos
	.utag	_TZCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZCTL_BITS
	.eos
	.stag	_TZEINT_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZEINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZEINT_BITS
	.eos
	.stag	_TZFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZFLG_BITS
	.eos
	.stag	_TZCLR_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZCLR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZCLR_BITS
	.eos
	.stag	_TZFRC_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_CBC, 1, 14, 18, 1
	.member	_OST, 2, 14, 18, 1
	.member	_rsvd2, 3, 14, 18, 13
	.eos
	.utag	_TZFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TZFRC_BITS
	.eos
	.stag	_ETSEL_BITS, 16
	.member	_INTSEL, 0, 14, 18, 3
	.member	_INTEN, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 4
	.member	_SOCASEL, 8, 14, 18, 3
	.member	_SOCAEN, 11, 14, 18, 1
	.member	_SOCBSEL, 12, 14, 18, 3
	.member	_SOCBEN, 15, 14, 18, 1
	.eos
	.utag	_ETSEL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETSEL_BITS
	.eos
	.stag	_ETPS_BITS, 16
	.member	_INTPRD, 0, 14, 18, 2
	.member	_INTCNT, 2, 14, 18, 2
	.member	_rsvd1, 4, 14, 18, 4
	.member	_SOCAPRD, 8, 14, 18, 2
	.member	_SOCACNT, 10, 14, 18, 2
	.member	_SOCBPRD, 12, 14, 18, 2
	.member	_SOCBCNT, 14, 14, 18, 2
	.eos
	.utag	_ETPS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETPS_BITS
	.eos
	.stag	_ETFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_SOCA, 2, 14, 18, 1
	.member	_SOCB, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_ETFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETFLG_BITS
	.eos
	.stag	_ETCLR_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_SOCA, 2, 14, 18, 1
	.member	_SOCB, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_ETCLR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETCLR_BITS
	.eos
	.stag	_ETFRC_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_SOCA, 2, 14, 18, 1
	.member	_SOCB, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_ETFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ETFRC_BITS
	.eos
	.stag	_PCCTL_BITS, 16
	.member	_CHPEN, 0, 14, 18, 1
	.member	_OSHTWTH, 1, 14, 18, 4
	.member	_CHPFREQ, 5, 14, 18, 3
	.member	_CHPDUTY, 8, 14, 18, 3
	.member	_rsvd1, 11, 14, 18, 5
	.eos
	.utag	_PCCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCCTL_BITS
	.eos
	.stag	_HRCNFG_BITS, 16
	.member	_EDGMODE, 0, 14, 18, 2
	.member	_CTLMODE, 2, 14, 18, 1
	.member	_HRLOAD, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 12
	.eos
	.utag	_HRCNFG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _HRCNFG_BITS
	.eos
	.stag	_EPWM_REGS, 544
	.member	_TBCTL, 0, 9, 8, 16, _TBCTL_REG
	.member	_TBSTS, 16, 9, 8, 16, _TBSTS_REG
	.member	_TBPHS, 32, 9, 8, 32, _TBPHS_HRPWM_GROUP
	.member	_TBCTR, 64, 14, 8, 16
	.member	_TBPRD, 80, 14, 8, 16
	.member	_rsvd1, 96, 14, 8, 16
	.member	_CMPCTL, 112, 9, 8, 16, _CMPCTL_REG
	.member	_CMPA, 128, 9, 8, 32, _CMPA_HRPWM_GROUP
	.member	_CMPB, 160, 14, 8, 16
	.member	_AQCTLA, 176, 9, 8, 16, _AQCTL_REG
	.member	_AQCTLB, 192, 9, 8, 16, _AQCTL_REG
	.member	_AQSFRC, 208, 9, 8, 16, _AQSFRC_REG
	.member	_AQCSFRC, 224, 9, 8, 16, _AQCSFRC_REG
	.member	_DBCTL, 240, 9, 8, 16, _DBCTL_REG
	.member	_DBRED, 256, 14, 8, 16
	.member	_DBFED, 272, 14, 8, 16
	.member	_TZSEL, 288, 9, 8, 16, _TZSEL_REG
	.member	_rsvd2, 304, 14, 8, 16
	.member	_TZCTL, 320, 9, 8, 16, _TZCTL_REG
	.member	_TZEINT, 336, 9, 8, 16, _TZEINT_REG
	.member	_TZFLG, 352, 9, 8, 16, _TZFLG_REG
	.member	_TZCLR, 368, 9, 8, 16, _TZCLR_REG
	.member	_TZFRC, 384, 9, 8, 16, _TZFRC_REG
	.member	_ETSEL, 400, 9, 8, 16, _ETSEL_REG
	.member	_ETPS, 416, 9, 8, 16, _ETPS_REG
	.member	_ETFLG, 432, 9, 8, 16, _ETFLG_REG
	.member	_ETCLR, 448, 9, 8, 16, _ETCLR_REG
	.member	_ETFRC, 464, 9, 8, 16, _ETFRC_REG
	.member	_PCCTL, 480, 9, 8, 16, _PCCTL_REG
	.member	_rsvd3, 496, 14, 8, 16
	.member	_HRCNFG, 512, 9, 8, 16, _HRCNFG_REG
	.eos
	.stag	_ADCTRL1_BITS, 16
	.member	_rsvd1, 0, 14, 18, 4
	.member	_SEQ_CASC, 4, 14, 18, 1
	.member	_SEQ_OVRD, 5, 14, 18, 1
	.member	_CONT_RUN, 6, 14, 18, 1
	.member	_CPS, 7, 14, 18, 1
	.member	_ACQ_PS, 8, 14, 18, 4
	.member	_SUSMOD, 12, 14, 18, 2
	.member	_RESET, 14, 14, 18, 1
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_ADCTRL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCTRL1_BITS
	.eos
	.stag	_ADCTRL2_BITS, 16
	.member	_EPWM_SOCB_SEQ2, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_INT_MOD_SEQ2, 2, 14, 18, 1
	.member	_INT_ENA_SEQ2, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 1
	.member	_SOC_SEQ2, 5, 14, 18, 1
	.member	_RST_SEQ2, 6, 14, 18, 1
	.member	_EXT_SOC_SEQ1, 7, 14, 18, 1
	.member	_EPWM_SOCA_SEQ1, 8, 14, 18, 1
	.member	_rsvd3, 9, 14, 18, 1
	.member	_INT_MOD_SEQ1, 10, 14, 18, 1
	.member	_INT_ENA_SEQ1, 11, 14, 18, 1
	.member	_rsvd4, 12, 14, 18, 1
	.member	_SOC_SEQ1, 13, 14, 18, 1
	.member	_RST_SEQ1, 14, 14, 18, 1
	.member	_EPWM_SOCB_SEQ, 15, 14, 18, 1
	.eos
	.utag	_ADCTRL2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCTRL2_BITS
	.eos
	.stag	_ADCMAXCONV_BITS, 16
	.member	_MAX_CONV1, 0, 14, 18, 4
	.member	_MAX_CONV2, 4, 14, 18, 3
	.member	_rsvd1, 7, 14, 18, 9
	.eos
	.utag	_ADCMAXCONV_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCMAXCONV_BITS
	.eos
	.stag	_ADCCHSELSEQ1_BITS, 16
	.member	_CONV00, 0, 14, 18, 4
	.member	_CONV01, 4, 14, 18, 4
	.member	_CONV02, 8, 14, 18, 4
	.member	_CONV03, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ1_BITS
	.eos
	.stag	_ADCCHSELSEQ2_BITS, 16
	.member	_CONV04, 0, 14, 18, 4
	.member	_CONV05, 4, 14, 18, 4
	.member	_CONV06, 8, 14, 18, 4
	.member	_CONV07, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ2_BITS
	.eos
	.stag	_ADCCHSELSEQ3_BITS, 16
	.member	_CONV08, 0, 14, 18, 4
	.member	_CONV09, 4, 14, 18, 4
	.member	_CONV10, 8, 14, 18, 4
	.member	_CONV11, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ3_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ3_BITS
	.eos
	.stag	_ADCCHSELSEQ4_BITS, 16
	.member	_CONV12, 0, 14, 18, 4
	.member	_CONV13, 4, 14, 18, 4
	.member	_CONV14, 8, 14, 18, 4
	.member	_CONV15, 12, 14, 18, 4
	.eos
	.utag	_ADCCHSELSEQ4_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCCHSELSEQ4_BITS
	.eos
	.stag	_ADCASEQSR_BITS, 16
	.member	_SEQ1_STATE, 0, 14, 18, 4
	.member	_SEQ2_STATE, 4, 14, 18, 3
	.member	_rsvd1, 7, 14, 18, 1
	.member	_SEQ_CNTR, 8, 14, 18, 4
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_ADCASEQSR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCASEQSR_BITS
	.eos
	.stag	_ADCTRL3_BITS, 16
	.member	_SMODE_SEL, 0, 14, 18, 1
	.member	_ADCCLKPS, 1, 14, 18, 4
	.member	_ADCPWDN, 5, 14, 18, 1
	.member	_ADCBGRFDN, 6, 14, 18, 2
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_ADCTRL3_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCTRL3_BITS
	.eos
	.stag	_ADCST_BITS, 16
	.member	_INT_SEQ1, 0, 14, 18, 1
	.member	_INT_SEQ2, 1, 14, 18, 1
	.member	_SEQ1_BSY, 2, 14, 18, 1
	.member	_SEQ2_BSY, 3, 14, 18, 1
	.member	_INT_SEQ1_CLR, 4, 14, 18, 1
	.member	_INT_SEQ2_CLR, 5, 14, 18, 1
	.member	_EOS_BUF1, 6, 14, 18, 1
	.member	_EOS_BUF2, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_ADCST_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCST_BITS
	.eos
	.stag	_ADCREFSEL_BITS, 16
	.member	_rsvd1, 0, 14, 18, 14
	.member	_REF_SEL, 14, 14, 18, 2
	.eos
	.utag	_ADCREFSEL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCREFSEL_BITS
	.eos
	.stag	_ADCOFFTRIM_BITS, 16
	.member	_OFFSET_TRIM, 0, 4, 18, 9
	.member	_rsvd1, 9, 14, 18, 7
	.eos
	.utag	_ADCOFFTRIM_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ADCOFFTRIM_BITS
	.eos
	.stag	_ADC_REGS, 480
	.member	_ADCTRL1, 0, 9, 8, 16, _ADCTRL1_REG
	.member	_ADCTRL2, 16, 9, 8, 16, _ADCTRL2_REG
	.member	_ADCMAXCONV, 32, 9, 8, 16, _ADCMAXCONV_REG
	.member	_ADCCHSELSEQ1, 48, 9, 8, 16, _ADCCHSELSEQ1_REG
	.member	_ADCCHSELSEQ2, 64, 9, 8, 16, _ADCCHSELSEQ2_REG
	.member	_ADCCHSELSEQ3, 80, 9, 8, 16, _ADCCHSELSEQ3_REG
	.member	_ADCCHSELSEQ4, 96, 9, 8, 16, _ADCCHSELSEQ4_REG
	.member	_ADCASEQSR, 112, 9, 8, 16, _ADCASEQSR_REG
	.member	_ADCRESULT0, 128, 14, 8, 16
	.member	_ADCRESULT1, 144, 14, 8, 16
	.member	_ADCRESULT2, 160, 14, 8, 16
	.member	_ADCRESULT3, 176, 14, 8, 16
	.member	_ADCRESULT4, 192, 14, 8, 16
	.member	_ADCRESULT5, 208, 14, 8, 16
	.member	_ADCRESULT6, 224, 14, 8, 16
	.member	_ADCRESULT7, 240, 14, 8, 16
	.member	_ADCRESULT8, 256, 14, 8, 16
	.member	_ADCRESULT9, 272, 14, 8, 16
	.member	_ADCRESULT10, 288, 14, 8, 16
	.member	_ADCRESULT11, 304, 14, 8, 16
	.member	_ADCRESULT12, 320, 14, 8, 16
	.member	_ADCRESULT13, 336, 14, 8, 16
	.member	_ADCRESULT14, 352, 14, 8, 16
	.member	_ADCRESULT15, 368, 14, 8, 16
	.member	_ADCTRL3, 384, 9, 8, 16, _ADCTRL3_REG
	.member	_ADCST, 400, 9, 8, 16, _ADCST_REG
	.member	_rsvd1, 416, 14, 8, 16
	.member	_rsvd2, 432, 14, 8, 16
	.member	_ADCREFSEL, 448, 9, 8, 16, _ADCREFSEL_REG
	.member	_ADCOFFTRIM, 464, 9, 8, 16, _ADCOFFTRIM_REG
	.eos
	.stag	_ADC_RESULT_MIRROR_REGS, 256
	.member	_ADCRESULT0, 0, 14, 8, 16
	.member	_ADCRESULT1, 16, 14, 8, 16
	.member	_ADCRESULT2, 32, 14, 8, 16
	.member	_ADCRESULT3, 48, 14, 8, 16
	.member	_ADCRESULT4, 64, 14, 8, 16
	.member	_ADCRESULT5, 80, 14, 8, 16
	.member	_ADCRESULT6, 96, 14, 8, 16
	.member	_ADCRESULT7, 112, 14, 8, 16
	.member	_ADCRESULT8, 128, 14, 8, 16
	.member	_ADCRESULT9, 144, 14, 8, 16
	.member	_ADCRESULT10, 160, 14, 8, 16
	.member	_ADCRESULT11, 176, 14, 8, 16
	.member	_ADCRESULT12, 192, 14, 8, 16
	.member	_ADCRESULT13, 208, 14, 8, 16
	.member	_ADCRESULT14, 224, 14, 8, 16
	.member	_ADCRESULT15, 240, 14, 8, 16
	.eos
	.stag	_TIM_REG, 32
	.member	_LSW, 0, 14, 8, 16
	.member	_MSW, 16, 14, 8, 16
	.eos
	.utag	_TIM_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _TIM_REG
	.eos
	.stag	_PRD_REG, 32
	.member	_LSW, 0, 14, 8, 16
	.member	_MSW, 16, 14, 8, 16
	.eos
	.utag	_PRD_GROUP, 32
	.member	_all, 0, 15, 11, 32
	.member	_half, 0, 8, 11, 32, _PRD_REG
	.eos
	.stag	_TCR_BITS, 16
	.member	_rsvd1, 0, 14, 18, 4
	.member	_TSS, 4, 14, 18, 1
	.member	_TRB, 5, 14, 18, 1
	.member	_rsvd2, 6, 14, 18, 4
	.member	_SOFT, 10, 14, 18, 1
	.member	_FREE, 11, 14, 18, 1
	.member	_rsvd3, 12, 14, 18, 2
	.member	_TIE, 14, 14, 18, 1
	.member	_TIF, 15, 14, 18, 1
	.eos
	.utag	_TCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TCR_BITS
	.eos
	.stag	_TPR_BITS, 16
	.member	_TDDR, 0, 14, 18, 8
	.member	_PSC, 8, 14, 18, 8
	.eos
	.utag	_TPR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TPR_BITS
	.eos
	.stag	_TPRH_BITS, 16
	.member	_TDDRH, 0, 14, 18, 8
	.member	_PSCH, 8, 14, 18, 8
	.eos
	.utag	_TPRH_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _TPRH_BITS
	.eos
	.stag	_CPUTIMER_REGS, 128
	.member	_TIM, 0, 9, 8, 32, _TIM_GROUP
	.member	_PRD, 32, 9, 8, 32, _PRD_GROUP
	.member	_TCR, 64, 9, 8, 16, _TCR_REG
	.member	_rsvd1, 80, 14, 8, 16
	.member	_TPR, 96, 9, 8, 16, _TPR_REG
	.member	_TPRH, 112, 9, 8, 16, _TPRH_REG
	.eos
	.stag	_CSM_PWL, 128
	.member	_PSWD0, 0, 14, 8, 16
	.member	_PSWD1, 16, 14, 8, 16
	.member	_PSWD2, 32, 14, 8, 16
	.member	_PSWD3, 48, 14, 8, 16
	.member	_PSWD4, 64, 14, 8, 16
	.member	_PSWD5, 80, 14, 8, 16
	.member	_PSWD6, 96, 14, 8, 16
	.member	_PSWD7, 112, 14, 8, 16
	.eos
	.stag	_CSMSCR_BITS, 16
	.member	_SECURE, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 14
	.member	_FORCESEC, 15, 14, 18, 1
	.eos
	.utag	_CSMSCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _CSMSCR_BITS
	.eos
	.stag	_CSM_REGS, 256
	.member	_KEY0, 0, 14, 8, 16
	.member	_KEY1, 16, 14, 8, 16
	.member	_KEY2, 32, 14, 8, 16
	.member	_KEY3, 48, 14, 8, 16
	.member	_KEY4, 64, 14, 8, 16
	.member	_KEY5, 80, 14, 8, 16
	.member	_KEY6, 96, 14, 8, 16
	.member	_KEY7, 112, 14, 8, 16
	.member	_rsvd1, 128, 14, 8, 16
	.member	_rsvd2, 144, 14, 8, 16
	.member	_rsvd3, 160, 14, 8, 16
	.member	_rsvd4, 176, 14, 8, 16
	.member	_rsvd5, 192, 14, 8, 16
	.member	_rsvd6, 208, 14, 8, 16
	.member	_rsvd7, 224, 14, 8, 16
	.member	_CSMSCR, 240, 9, 8, 16, _CSMSCR_REG
	.eos
	.stag	_DEVICECNF_BITS, 32
	.member	_rsvd1, 0, 14, 18, 3
	.member	_VMAPS, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 1
	.member	_XRSn, 5, 14, 18, 1
	.member	_rsvd3, 6, 14, 18, 10
	.member	_rsvd4, 16, 14, 18, 3
	.member	_ENPROT, 19, 14, 18, 1
	.member	_MONPRIV, 20, 14, 18, 1
	.member	_rsvd5, 21, 14, 18, 1
	.member	_EMU0SEL, 22, 14, 18, 2
	.member	_EMU1SEL, 24, 14, 18, 2
	.member	_MCBSPCON, 26, 14, 18, 1
	.member	_rsvd6, 27, 14, 18, 5
	.eos
	.utag	_DEVICECNF_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _DEVICECNF_BITS
	.eos
	.stag	_PARTID_BITS, 16
	.member	_PARTNO, 0, 14, 18, 8
	.member	_PARTTYPE, 8, 14, 18, 8
	.eos
	.utag	_PARTID_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PARTID_BITS
	.eos
	.stag	_DEV_EMU_REGS, 3328
	.member	_DEVICECNF, 0, 9, 8, 32, _DEVICECNF_REG
	.member	_PARTID, 32, 9, 8, 16, _PARTID_REG
	.member	_REVID, 48, 14, 8, 16
	.member	_PROTSTART, 64, 14, 8, 16
	.member	_PROTRANGE, 80, 14, 8, 16
	.member	_rsvd2, 96, 62, 8, 3232, , 202
	.eos
	.stag	_DMACTRL_BITS, 16
	.member	_HARDRESET, 0, 14, 18, 1
	.member	_PRIORITYRESET, 1, 14, 18, 1
	.member	_rsvd1, 2, 14, 18, 14
	.eos
	.utag	_DMACTRL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DMACTRL_BITS
	.eos
	.stag	_DEBUGCTRL_BITS, 16
	.member	_rsvd1, 0, 14, 18, 15
	.member	_FREE, 15, 14, 18, 1
	.eos
	.utag	_DEBUGCTRL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DEBUGCTRL_BITS
	.eos
	.stag	_PRIORITYCTRL1_BITS, 16
	.member	_CH1PRIORITY, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 15
	.eos
	.utag	_PRIORITYCTRL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PRIORITYCTRL1_BITS
	.eos
	.stag	_PRIORITYSTAT_BITS, 16
	.member	_ACTIVESTS, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 1
	.member	_ACTIVESTS_SHADOW, 4, 14, 18, 3
	.member	_rsvd2, 7, 14, 18, 9
	.eos
	.utag	_PRIORITYSTAT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PRIORITYSTAT_BITS
	.eos
	.stag	_MODE_BITS, 16
	.member	_PERINTSEL, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 2
	.member	_OVRINTE, 7, 14, 18, 1
	.member	_PERINTE, 8, 14, 18, 1
	.member	_CHINTMODE, 9, 14, 18, 1
	.member	_ONESHOT, 10, 14, 18, 1
	.member	_CONTINUOUS, 11, 14, 18, 1
	.member	_SYNCE, 12, 14, 18, 1
	.member	_SYNCSEL, 13, 14, 18, 1
	.member	_DATASIZE, 14, 14, 18, 1
	.member	_CHINTE, 15, 14, 18, 1
	.eos
	.utag	_MODE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MODE_BITS
	.eos
	.stag	_CONTROL_BITS, 16
	.member	_RUN, 0, 14, 18, 1
	.member	_HALT, 1, 14, 18, 1
	.member	_SOFTRESET, 2, 14, 18, 1
	.member	_PERINTFRC, 3, 14, 18, 1
	.member	_PERINTCLR, 4, 14, 18, 1
	.member	_SYNCFRC, 5, 14, 18, 1
	.member	_SYNCCLR, 6, 14, 18, 1
	.member	_ERRCLR, 7, 14, 18, 1
	.member	_PERINTFLG, 8, 14, 18, 1
	.member	_SYNCFLG, 9, 14, 18, 1
	.member	_SYNCERR, 10, 14, 18, 1
	.member	_TRANSFERSTS, 11, 14, 18, 1
	.member	_BURSTSTS, 12, 14, 18, 1
	.member	_RUNSTS, 13, 14, 18, 1
	.member	_OVRFLG, 14, 14, 18, 1
	.member	_rsvd1, 15, 14, 18, 1
	.eos
	.utag	_CONTROL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _CONTROL_BITS
	.eos
	.stag	_BURST_SIZE_BITS, 16
	.member	_BURSTSIZE, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 11
	.eos
	.utag	_BURST_SIZE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _BURST_SIZE_BITS
	.eos
	.stag	_BURST_COUNT_BITS, 16
	.member	_BURSTCOUNT, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 11
	.eos
	.utag	_BURST_COUNT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _BURST_COUNT_BITS
	.eos
	.stag	_CH_REGS, 512
	.member	_MODE, 0, 9, 8, 16, _MODE_REG
	.member	_CONTROL, 16, 9, 8, 16, _CONTROL_REG
	.member	_BURST_SIZE, 32, 9, 8, 16, _BURST_SIZE_REG
	.member	_BURST_COUNT, 48, 9, 8, 16, _BURST_COUNT_REG
	.member	_SRC_BURST_STEP, 64, 4, 8, 16
	.member	_DST_BURST_STEP, 80, 4, 8, 16
	.member	_TRANSFER_SIZE, 96, 14, 8, 16
	.member	_TRANSFER_COUNT, 112, 14, 8, 16
	.member	_SRC_TRANSFER_STEP, 128, 4, 8, 16
	.member	_DST_TRANSFER_STEP, 144, 4, 8, 16
	.member	_SRC_WRAP_SIZE, 160, 14, 8, 16
	.member	_SRC_WRAP_COUNT, 176, 14, 8, 16
	.member	_SRC_WRAP_STEP, 192, 4, 8, 16
	.member	_DST_WRAP_SIZE, 208, 14, 8, 16
	.member	_DST_WRAP_COUNT, 224, 14, 8, 16
	.member	_DST_WRAP_STEP, 240, 4, 8, 16
	.member	_SRC_BEG_ADDR_SHADOW, 256, 15, 8, 32
	.member	_SRC_ADDR_SHADOW, 288, 15, 8, 32
	.member	_SRC_BEG_ADDR_ACTIVE, 320, 15, 8, 32
	.member	_SRC_ADDR_ACTIVE, 352, 15, 8, 32
	.member	_DST_BEG_ADDR_SHADOW, 384, 15, 8, 32
	.member	_DST_ADDR_SHADOW, 416, 15, 8, 32
	.member	_DST_BEG_ADDR_ACTIVE, 448, 15, 8, 32
	.member	_DST_ADDR_ACTIVE, 480, 15, 8, 32
	.eos
	.stag	_DMA_REGS, 3584
	.member	_DMACTRL, 0, 9, 8, 16, _DMACTRL_REG
	.member	_DEBUGCTRL, 16, 9, 8, 16, _DEBUGCTRL_REG
	.member	_rsvd0, 32, 14, 8, 16
	.member	_rsvd1, 48, 14, 8, 16
	.member	_PRIORITYCTRL1, 64, 9, 8, 16, _PRIORITYCTRL1_REG
	.member	_rsvd2, 80, 14, 8, 16
	.member	_PRIORITYSTAT, 96, 9, 8, 16, _PRIORITYSTAT_REG
	.member	_rsvd3, 112, 62, 8, 400, , 25
	.member	_CH1, 512, 8, 8, 512, _CH_REGS
	.member	_CH2, 1024, 8, 8, 512, _CH_REGS
	.member	_CH3, 1536, 8, 8, 512, _CH_REGS
	.member	_CH4, 2048, 8, 8, 512, _CH_REGS
	.member	_CH5, 2560, 8, 8, 512, _CH_REGS
	.member	_CH6, 3072, 8, 8, 512, _CH_REGS
	.eos
	.stag	_CANME_BITS, 32
	.member	_ME0, 0, 14, 18, 1
	.member	_ME1, 1, 14, 18, 1
	.member	_ME2, 2, 14, 18, 1
	.member	_ME3, 3, 14, 18, 1
	.member	_ME4, 4, 14, 18, 1
	.member	_ME5, 5, 14, 18, 1
	.member	_ME6, 6, 14, 18, 1
	.member	_ME7, 7, 14, 18, 1
	.member	_ME8, 8, 14, 18, 1
	.member	_ME9, 9, 14, 18, 1
	.member	_ME10, 10, 14, 18, 1
	.member	_ME11, 11, 14, 18, 1
	.member	_ME12, 12, 14, 18, 1
	.member	_ME13, 13, 14, 18, 1
	.member	_ME14, 14, 14, 18, 1
	.member	_ME15, 15, 14, 18, 1
	.member	_ME16, 16, 14, 18, 1
	.member	_ME17, 17, 14, 18, 1
	.member	_ME18, 18, 14, 18, 1
	.member	_ME19, 19, 14, 18, 1
	.member	_ME20, 20, 14, 18, 1
	.member	_ME21, 21, 14, 18, 1
	.member	_ME22, 22, 14, 18, 1
	.member	_ME23, 23, 14, 18, 1
	.member	_ME24, 24, 14, 18, 1
	.member	_ME25, 25, 14, 18, 1
	.member	_ME26, 26, 14, 18, 1
	.member	_ME27, 27, 14, 18, 1
	.member	_ME28, 28, 14, 18, 1
	.member	_ME29, 29, 14, 18, 1
	.member	_ME30, 30, 14, 18, 1
	.member	_ME31, 31, 14, 18, 1
	.eos
	.utag	_CANME_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANME_BITS
	.eos
	.stag	_CANMD_BITS, 32
	.member	_MD0, 0, 14, 18, 1
	.member	_MD1, 1, 14, 18, 1
	.member	_MD2, 2, 14, 18, 1
	.member	_MD3, 3, 14, 18, 1
	.member	_MD4, 4, 14, 18, 1
	.member	_MD5, 5, 14, 18, 1
	.member	_MD6, 6, 14, 18, 1
	.member	_MD7, 7, 14, 18, 1
	.member	_MD8, 8, 14, 18, 1
	.member	_MD9, 9, 14, 18, 1
	.member	_MD10, 10, 14, 18, 1
	.member	_MD11, 11, 14, 18, 1
	.member	_MD12, 12, 14, 18, 1
	.member	_MD13, 13, 14, 18, 1
	.member	_MD14, 14, 14, 18, 1
	.member	_MD15, 15, 14, 18, 1
	.member	_MD16, 16, 14, 18, 1
	.member	_MD17, 17, 14, 18, 1
	.member	_MD18, 18, 14, 18, 1
	.member	_MD19, 19, 14, 18, 1
	.member	_MD20, 20, 14, 18, 1
	.member	_MD21, 21, 14, 18, 1
	.member	_MD22, 22, 14, 18, 1
	.member	_MD23, 23, 14, 18, 1
	.member	_MD24, 24, 14, 18, 1
	.member	_MD25, 25, 14, 18, 1
	.member	_MD26, 26, 14, 18, 1
	.member	_MD27, 27, 14, 18, 1
	.member	_MD28, 28, 14, 18, 1
	.member	_MD29, 29, 14, 18, 1
	.member	_MD30, 30, 14, 18, 1
	.member	_MD31, 31, 14, 18, 1
	.eos
	.utag	_CANMD_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMD_BITS
	.eos
	.stag	_CANTRS_BITS, 32
	.member	_TRS0, 0, 14, 18, 1
	.member	_TRS1, 1, 14, 18, 1
	.member	_TRS2, 2, 14, 18, 1
	.member	_TRS3, 3, 14, 18, 1
	.member	_TRS4, 4, 14, 18, 1
	.member	_TRS5, 5, 14, 18, 1
	.member	_TRS6, 6, 14, 18, 1
	.member	_TRS7, 7, 14, 18, 1
	.member	_TRS8, 8, 14, 18, 1
	.member	_TRS9, 9, 14, 18, 1
	.member	_TRS10, 10, 14, 18, 1
	.member	_TRS11, 11, 14, 18, 1
	.member	_TRS12, 12, 14, 18, 1
	.member	_TRS13, 13, 14, 18, 1
	.member	_TRS14, 14, 14, 18, 1
	.member	_TRS15, 15, 14, 18, 1
	.member	_TRS16, 16, 14, 18, 1
	.member	_TRS17, 17, 14, 18, 1
	.member	_TRS18, 18, 14, 18, 1
	.member	_TRS19, 19, 14, 18, 1
	.member	_TRS20, 20, 14, 18, 1
	.member	_TRS21, 21, 14, 18, 1
	.member	_TRS22, 22, 14, 18, 1
	.member	_TRS23, 23, 14, 18, 1
	.member	_TRS24, 24, 14, 18, 1
	.member	_TRS25, 25, 14, 18, 1
	.member	_TRS26, 26, 14, 18, 1
	.member	_TRS27, 27, 14, 18, 1
	.member	_TRS28, 28, 14, 18, 1
	.member	_TRS29, 29, 14, 18, 1
	.member	_TRS30, 30, 14, 18, 1
	.member	_TRS31, 31, 14, 18, 1
	.eos
	.utag	_CANTRS_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTRS_BITS
	.eos
	.stag	_CANTRR_BITS, 32
	.member	_TRR0, 0, 14, 18, 1
	.member	_TRR1, 1, 14, 18, 1
	.member	_TRR2, 2, 14, 18, 1
	.member	_TRR3, 3, 14, 18, 1
	.member	_TRR4, 4, 14, 18, 1
	.member	_TRR5, 5, 14, 18, 1
	.member	_TRR6, 6, 14, 18, 1
	.member	_TRR7, 7, 14, 18, 1
	.member	_TRR8, 8, 14, 18, 1
	.member	_TRR9, 9, 14, 18, 1
	.member	_TRR10, 10, 14, 18, 1
	.member	_TRR11, 11, 14, 18, 1
	.member	_TRR12, 12, 14, 18, 1
	.member	_TRR13, 13, 14, 18, 1
	.member	_TRR14, 14, 14, 18, 1
	.member	_TRR15, 15, 14, 18, 1
	.member	_TRR16, 16, 14, 18, 1
	.member	_TRR17, 17, 14, 18, 1
	.member	_TRR18, 18, 14, 18, 1
	.member	_TRR19, 19, 14, 18, 1
	.member	_TRR20, 20, 14, 18, 1
	.member	_TRR21, 21, 14, 18, 1
	.member	_TRR22, 22, 14, 18, 1
	.member	_TRR23, 23, 14, 18, 1
	.member	_TRR24, 24, 14, 18, 1
	.member	_TRR25, 25, 14, 18, 1
	.member	_TRR26, 26, 14, 18, 1
	.member	_TRR27, 27, 14, 18, 1
	.member	_TRR28, 28, 14, 18, 1
	.member	_TRR29, 29, 14, 18, 1
	.member	_TRR30, 30, 14, 18, 1
	.member	_TRR31, 31, 14, 18, 1
	.eos
	.utag	_CANTRR_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTRR_BITS
	.eos
	.stag	_CANTA_BITS, 32
	.member	_TA0, 0, 14, 18, 1
	.member	_TA1, 1, 14, 18, 1
	.member	_TA2, 2, 14, 18, 1
	.member	_TA3, 3, 14, 18, 1
	.member	_TA4, 4, 14, 18, 1
	.member	_TA5, 5, 14, 18, 1
	.member	_TA6, 6, 14, 18, 1
	.member	_TA7, 7, 14, 18, 1
	.member	_TA8, 8, 14, 18, 1
	.member	_TA9, 9, 14, 18, 1
	.member	_TA10, 10, 14, 18, 1
	.member	_TA11, 11, 14, 18, 1
	.member	_TA12, 12, 14, 18, 1
	.member	_TA13, 13, 14, 18, 1
	.member	_TA14, 14, 14, 18, 1
	.member	_TA15, 15, 14, 18, 1
	.member	_TA16, 16, 14, 18, 1
	.member	_TA17, 17, 14, 18, 1
	.member	_TA18, 18, 14, 18, 1
	.member	_TA19, 19, 14, 18, 1
	.member	_TA20, 20, 14, 18, 1
	.member	_TA21, 21, 14, 18, 1
	.member	_TA22, 22, 14, 18, 1
	.member	_TA23, 23, 14, 18, 1
	.member	_TA24, 24, 14, 18, 1
	.member	_TA25, 25, 14, 18, 1
	.member	_TA26, 26, 14, 18, 1
	.member	_TA27, 27, 14, 18, 1
	.member	_TA28, 28, 14, 18, 1
	.member	_TA29, 29, 14, 18, 1
	.member	_TA30, 30, 14, 18, 1
	.member	_TA31, 31, 14, 18, 1
	.eos
	.utag	_CANTA_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTA_BITS
	.eos
	.stag	_CANAA_BITS, 32
	.member	_AA0, 0, 14, 18, 1
	.member	_AA1, 1, 14, 18, 1
	.member	_AA2, 2, 14, 18, 1
	.member	_AA3, 3, 14, 18, 1
	.member	_AA4, 4, 14, 18, 1
	.member	_AA5, 5, 14, 18, 1
	.member	_AA6, 6, 14, 18, 1
	.member	_AA7, 7, 14, 18, 1
	.member	_AA8, 8, 14, 18, 1
	.member	_AA9, 9, 14, 18, 1
	.member	_AA10, 10, 14, 18, 1
	.member	_AA11, 11, 14, 18, 1
	.member	_AA12, 12, 14, 18, 1
	.member	_AA13, 13, 14, 18, 1
	.member	_AA14, 14, 14, 18, 1
	.member	_AA15, 15, 14, 18, 1
	.member	_AA16, 16, 14, 18, 1
	.member	_AA17, 17, 14, 18, 1
	.member	_AA18, 18, 14, 18, 1
	.member	_AA19, 19, 14, 18, 1
	.member	_AA20, 20, 14, 18, 1
	.member	_AA21, 21, 14, 18, 1
	.member	_AA22, 22, 14, 18, 1
	.member	_AA23, 23, 14, 18, 1
	.member	_AA24, 24, 14, 18, 1
	.member	_AA25, 25, 14, 18, 1
	.member	_AA26, 26, 14, 18, 1
	.member	_AA27, 27, 14, 18, 1
	.member	_AA28, 28, 14, 18, 1
	.member	_AA29, 29, 14, 18, 1
	.member	_AA30, 30, 14, 18, 1
	.member	_AA31, 31, 14, 18, 1
	.eos
	.utag	_CANAA_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANAA_BITS
	.eos
	.stag	_CANRMP_BITS, 32
	.member	_RMP0, 0, 14, 18, 1
	.member	_RMP1, 1, 14, 18, 1
	.member	_RMP2, 2, 14, 18, 1
	.member	_RMP3, 3, 14, 18, 1
	.member	_RMP4, 4, 14, 18, 1
	.member	_RMP5, 5, 14, 18, 1
	.member	_RMP6, 6, 14, 18, 1
	.member	_RMP7, 7, 14, 18, 1
	.member	_RMP8, 8, 14, 18, 1
	.member	_RMP9, 9, 14, 18, 1
	.member	_RMP10, 10, 14, 18, 1
	.member	_RMP11, 11, 14, 18, 1
	.member	_RMP12, 12, 14, 18, 1
	.member	_RMP13, 13, 14, 18, 1
	.member	_RMP14, 14, 14, 18, 1
	.member	_RMP15, 15, 14, 18, 1
	.member	_RMP16, 16, 14, 18, 1
	.member	_RMP17, 17, 14, 18, 1
	.member	_RMP18, 18, 14, 18, 1
	.member	_RMP19, 19, 14, 18, 1
	.member	_RMP20, 20, 14, 18, 1
	.member	_RMP21, 21, 14, 18, 1
	.member	_RMP22, 22, 14, 18, 1
	.member	_RMP23, 23, 14, 18, 1
	.member	_RMP24, 24, 14, 18, 1
	.member	_RMP25, 25, 14, 18, 1
	.member	_RMP26, 26, 14, 18, 1
	.member	_RMP27, 27, 14, 18, 1
	.member	_RMP28, 28, 14, 18, 1
	.member	_RMP29, 29, 14, 18, 1
	.member	_RMP30, 30, 14, 18, 1
	.member	_RMP31, 31, 14, 18, 1
	.eos
	.utag	_CANRMP_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRMP_BITS
	.eos
	.stag	_CANRML_BITS, 32
	.member	_RML0, 0, 14, 18, 1
	.member	_RML1, 1, 14, 18, 1
	.member	_RML2, 2, 14, 18, 1
	.member	_RML3, 3, 14, 18, 1
	.member	_RML4, 4, 14, 18, 1
	.member	_RML5, 5, 14, 18, 1
	.member	_RML6, 6, 14, 18, 1
	.member	_RML7, 7, 14, 18, 1
	.member	_RML8, 8, 14, 18, 1
	.member	_RML9, 9, 14, 18, 1
	.member	_RML10, 10, 14, 18, 1
	.member	_RML11, 11, 14, 18, 1
	.member	_RML12, 12, 14, 18, 1
	.member	_RML13, 13, 14, 18, 1
	.member	_RML14, 14, 14, 18, 1
	.member	_RML15, 15, 14, 18, 1
	.member	_RML16, 16, 14, 18, 1
	.member	_RML17, 17, 14, 18, 1
	.member	_RML18, 18, 14, 18, 1
	.member	_RML19, 19, 14, 18, 1
	.member	_RML20, 20, 14, 18, 1
	.member	_RML21, 21, 14, 18, 1
	.member	_RML22, 22, 14, 18, 1
	.member	_RML23, 23, 14, 18, 1
	.member	_RML24, 24, 14, 18, 1
	.member	_RML25, 25, 14, 18, 1
	.member	_RML26, 26, 14, 18, 1
	.member	_RML27, 27, 14, 18, 1
	.member	_RML28, 28, 14, 18, 1
	.member	_RML29, 29, 14, 18, 1
	.member	_RML30, 30, 14, 18, 1
	.member	_RML31, 31, 14, 18, 1
	.eos
	.utag	_CANRML_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRML_BITS
	.eos
	.stag	_CANRFP_BITS, 32
	.member	_RFP0, 0, 14, 18, 1
	.member	_RFP1, 1, 14, 18, 1
	.member	_RFP2, 2, 14, 18, 1
	.member	_RFP3, 3, 14, 18, 1
	.member	_RFP4, 4, 14, 18, 1
	.member	_RFP5, 5, 14, 18, 1
	.member	_RFP6, 6, 14, 18, 1
	.member	_RFP7, 7, 14, 18, 1
	.member	_RFP8, 8, 14, 18, 1
	.member	_RFP9, 9, 14, 18, 1
	.member	_RFP10, 10, 14, 18, 1
	.member	_RFP11, 11, 14, 18, 1
	.member	_RFP12, 12, 14, 18, 1
	.member	_RFP13, 13, 14, 18, 1
	.member	_RFP14, 14, 14, 18, 1
	.member	_RFP15, 15, 14, 18, 1
	.member	_RFP16, 16, 14, 18, 1
	.member	_RFP17, 17, 14, 18, 1
	.member	_RFP18, 18, 14, 18, 1
	.member	_RFP19, 19, 14, 18, 1
	.member	_RFP20, 20, 14, 18, 1
	.member	_RFP21, 21, 14, 18, 1
	.member	_RFP22, 22, 14, 18, 1
	.member	_RFP23, 23, 14, 18, 1
	.member	_RFP24, 24, 14, 18, 1
	.member	_RFP25, 25, 14, 18, 1
	.member	_RFP26, 26, 14, 18, 1
	.member	_RFP27, 27, 14, 18, 1
	.member	_RFP28, 28, 14, 18, 1
	.member	_RFP29, 29, 14, 18, 1
	.member	_RFP30, 30, 14, 18, 1
	.member	_RFP31, 31, 14, 18, 1
	.eos
	.utag	_CANRFP_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRFP_BITS
	.eos
	.stag	_CANGAM_BITS, 32
	.member	_GAM150, 0, 14, 18, 16
	.member	_GAM2816, 16, 14, 18, 13
	.member	_rsvd, 29, 14, 18, 2
	.member	_AMI, 31, 14, 18, 1
	.eos
	.utag	_CANGAM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGAM_BITS
	.eos
	.stag	_CANMC_BITS, 32
	.member	_MBNR, 0, 14, 18, 5
	.member	_SRES, 5, 14, 18, 1
	.member	_STM, 6, 14, 18, 1
	.member	_ABO, 7, 14, 18, 1
	.member	_CDR, 8, 14, 18, 1
	.member	_WUBA, 9, 14, 18, 1
	.member	_DBO, 10, 14, 18, 1
	.member	_PDR, 11, 14, 18, 1
	.member	_CCR, 12, 14, 18, 1
	.member	_SCB, 13, 14, 18, 1
	.member	_TCC, 14, 14, 18, 1
	.member	_MBCC, 15, 14, 18, 1
	.member	_SUSP, 16, 14, 18, 1
	.member	_rsvd, 17, 14, 18, 15
	.eos
	.utag	_CANMC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMC_BITS
	.eos
	.stag	_CANBTC_BITS, 32
	.member	_TSEG2REG, 0, 14, 18, 3
	.member	_TSEG1REG, 3, 14, 18, 4
	.member	_SAM, 7, 14, 18, 1
	.member	_SJWREG, 8, 14, 18, 2
	.member	_rsvd1, 10, 14, 18, 6
	.member	_BRPREG, 16, 14, 18, 8
	.member	_rsvd2, 24, 14, 18, 8
	.eos
	.utag	_CANBTC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANBTC_BITS
	.eos
	.stag	_CANES_BITS, 32
	.member	_TM, 0, 14, 18, 1
	.member	_RM, 1, 14, 18, 1
	.member	_rsvd1, 2, 14, 18, 1
	.member	_PDA, 3, 14, 18, 1
	.member	_CCE, 4, 14, 18, 1
	.member	_SMA, 5, 14, 18, 1
	.member	_rsvd2, 6, 14, 18, 10
	.member	_EW, 16, 14, 18, 1
	.member	_EP, 17, 14, 18, 1
	.member	_BO, 18, 14, 18, 1
	.member	_ACKE, 19, 14, 18, 1
	.member	_SE, 20, 14, 18, 1
	.member	_CRCE, 21, 14, 18, 1
	.member	_SA1, 22, 14, 18, 1
	.member	_BE, 23, 14, 18, 1
	.member	_FE, 24, 14, 18, 1
	.member	_rsvd3, 25, 14, 18, 7
	.eos
	.utag	_CANES_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANES_BITS
	.eos
	.stag	_CANTEC_BITS, 32
	.member	_TEC, 0, 14, 18, 8
	.member	_rsvd1, 8, 14, 18, 8
	.member	_rsvd2, 16, 14, 18, 16
	.eos
	.utag	_CANTEC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTEC_BITS
	.eos
	.stag	_CANREC_BITS, 32
	.member	_REC, 0, 14, 18, 8
	.member	_rsvd1, 8, 14, 18, 8
	.member	_rsvd2, 16, 14, 18, 16
	.eos
	.utag	_CANREC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANREC_BITS
	.eos
	.stag	_CANGIF0_BITS, 32
	.member	_MIV0, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 3
	.member	_WLIF0, 8, 14, 18, 1
	.member	_EPIF0, 9, 14, 18, 1
	.member	_BOIF0, 10, 14, 18, 1
	.member	_RMLIF0, 11, 14, 18, 1
	.member	_WUIF0, 12, 14, 18, 1
	.member	_WDIF0, 13, 14, 18, 1
	.member	_AAIF0, 14, 14, 18, 1
	.member	_GMIF0, 15, 14, 18, 1
	.member	_TCOF0, 16, 14, 18, 1
	.member	_MTOF0, 17, 14, 18, 1
	.member	_rsvd2, 18, 14, 18, 14
	.eos
	.utag	_CANGIF0_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGIF0_BITS
	.eos
	.stag	_CANGIM_BITS, 32
	.member	_I0EN, 0, 14, 18, 1
	.member	_I1EN, 1, 14, 18, 1
	.member	_GIL, 2, 14, 18, 1
	.member	_rsvd1, 3, 14, 18, 5
	.member	_WLIM, 8, 14, 18, 1
	.member	_EPIM, 9, 14, 18, 1
	.member	_BOIM, 10, 14, 18, 1
	.member	_RMLIM, 11, 14, 18, 1
	.member	_WUIM, 12, 14, 18, 1
	.member	_WDIM, 13, 14, 18, 1
	.member	_AAIM, 14, 14, 18, 1
	.member	_rsvd2, 15, 14, 18, 1
	.member	_TCOM, 16, 14, 18, 1
	.member	_MTOM, 17, 14, 18, 1
	.member	_rsvd3, 18, 14, 18, 14
	.eos
	.utag	_CANGIM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGIM_BITS
	.eos
	.stag	_CANGIF1_BITS, 32
	.member	_MIV1, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 3
	.member	_WLIF1, 8, 14, 18, 1
	.member	_EPIF1, 9, 14, 18, 1
	.member	_BOIF1, 10, 14, 18, 1
	.member	_RMLIF1, 11, 14, 18, 1
	.member	_WUIF1, 12, 14, 18, 1
	.member	_WDIF1, 13, 14, 18, 1
	.member	_AAIF1, 14, 14, 18, 1
	.member	_GMIF1, 15, 14, 18, 1
	.member	_TCOF1, 16, 14, 18, 1
	.member	_MTOF1, 17, 14, 18, 1
	.member	_rsvd2, 18, 14, 18, 14
	.eos
	.utag	_CANGIF1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANGIF1_BITS
	.eos
	.stag	_CANMIM_BITS, 32
	.member	_MIM0, 0, 14, 18, 1
	.member	_MIM1, 1, 14, 18, 1
	.member	_MIM2, 2, 14, 18, 1
	.member	_MIM3, 3, 14, 18, 1
	.member	_MIM4, 4, 14, 18, 1
	.member	_MIM5, 5, 14, 18, 1
	.member	_MIM6, 6, 14, 18, 1
	.member	_MIM7, 7, 14, 18, 1
	.member	_MIM8, 8, 14, 18, 1
	.member	_MIM9, 9, 14, 18, 1
	.member	_MIM10, 10, 14, 18, 1
	.member	_MIM11, 11, 14, 18, 1
	.member	_MIM12, 12, 14, 18, 1
	.member	_MIM13, 13, 14, 18, 1
	.member	_MIM14, 14, 14, 18, 1
	.member	_MIM15, 15, 14, 18, 1
	.member	_MIM16, 16, 14, 18, 1
	.member	_MIM17, 17, 14, 18, 1
	.member	_MIM18, 18, 14, 18, 1
	.member	_MIM19, 19, 14, 18, 1
	.member	_MIM20, 20, 14, 18, 1
	.member	_MIM21, 21, 14, 18, 1
	.member	_MIM22, 22, 14, 18, 1
	.member	_MIM23, 23, 14, 18, 1
	.member	_MIM24, 24, 14, 18, 1
	.member	_MIM25, 25, 14, 18, 1
	.member	_MIM26, 26, 14, 18, 1
	.member	_MIM27, 27, 14, 18, 1
	.member	_MIM28, 28, 14, 18, 1
	.member	_MIM29, 29, 14, 18, 1
	.member	_MIM30, 30, 14, 18, 1
	.member	_MIM31, 31, 14, 18, 1
	.eos
	.utag	_CANMIM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMIM_BITS
	.eos
	.stag	_CANMIL_BITS, 32
	.member	_MIL0, 0, 14, 18, 1
	.member	_MIL1, 1, 14, 18, 1
	.member	_MIL2, 2, 14, 18, 1
	.member	_MIL3, 3, 14, 18, 1
	.member	_MIL4, 4, 14, 18, 1
	.member	_MIL5, 5, 14, 18, 1
	.member	_MIL6, 6, 14, 18, 1
	.member	_MIL7, 7, 14, 18, 1
	.member	_MIL8, 8, 14, 18, 1
	.member	_MIL9, 9, 14, 18, 1
	.member	_MIL10, 10, 14, 18, 1
	.member	_MIL11, 11, 14, 18, 1
	.member	_MIL12, 12, 14, 18, 1
	.member	_MIL13, 13, 14, 18, 1
	.member	_MIL14, 14, 14, 18, 1
	.member	_MIL15, 15, 14, 18, 1
	.member	_MIL16, 16, 14, 18, 1
	.member	_MIL17, 17, 14, 18, 1
	.member	_MIL18, 18, 14, 18, 1
	.member	_MIL19, 19, 14, 18, 1
	.member	_MIL20, 20, 14, 18, 1
	.member	_MIL21, 21, 14, 18, 1
	.member	_MIL22, 22, 14, 18, 1
	.member	_MIL23, 23, 14, 18, 1
	.member	_MIL24, 24, 14, 18, 1
	.member	_MIL25, 25, 14, 18, 1
	.member	_MIL26, 26, 14, 18, 1
	.member	_MIL27, 27, 14, 18, 1
	.member	_MIL28, 28, 14, 18, 1
	.member	_MIL29, 29, 14, 18, 1
	.member	_MIL30, 30, 14, 18, 1
	.member	_MIL31, 31, 14, 18, 1
	.eos
	.utag	_CANMIL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMIL_BITS
	.eos
	.stag	_CANOPC_BITS, 32
	.member	_OPC0, 0, 14, 18, 1
	.member	_OPC1, 1, 14, 18, 1
	.member	_OPC2, 2, 14, 18, 1
	.member	_OPC3, 3, 14, 18, 1
	.member	_OPC4, 4, 14, 18, 1
	.member	_OPC5, 5, 14, 18, 1
	.member	_OPC6, 6, 14, 18, 1
	.member	_OPC7, 7, 14, 18, 1
	.member	_OPC8, 8, 14, 18, 1
	.member	_OPC9, 9, 14, 18, 1
	.member	_OPC10, 10, 14, 18, 1
	.member	_OPC11, 11, 14, 18, 1
	.member	_OPC12, 12, 14, 18, 1
	.member	_OPC13, 13, 14, 18, 1
	.member	_OPC14, 14, 14, 18, 1
	.member	_OPC15, 15, 14, 18, 1
	.member	_OPC16, 16, 14, 18, 1
	.member	_OPC17, 17, 14, 18, 1
	.member	_OPC18, 18, 14, 18, 1
	.member	_OPC19, 19, 14, 18, 1
	.member	_OPC20, 20, 14, 18, 1
	.member	_OPC21, 21, 14, 18, 1
	.member	_OPC22, 22, 14, 18, 1
	.member	_OPC23, 23, 14, 18, 1
	.member	_OPC24, 24, 14, 18, 1
	.member	_OPC25, 25, 14, 18, 1
	.member	_OPC26, 26, 14, 18, 1
	.member	_OPC27, 27, 14, 18, 1
	.member	_OPC28, 28, 14, 18, 1
	.member	_OPC29, 29, 14, 18, 1
	.member	_OPC30, 30, 14, 18, 1
	.member	_OPC31, 31, 14, 18, 1
	.eos
	.utag	_CANOPC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANOPC_BITS
	.eos
	.stag	_CANTIOC_BITS, 32
	.member	_rsvd1, 0, 14, 18, 3
	.member	_TXFUNC, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.member	_rsvd3, 16, 14, 18, 16
	.eos
	.utag	_CANTIOC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTIOC_BITS
	.eos
	.stag	_CANRIOC_BITS, 32
	.member	_rsvd1, 0, 14, 18, 3
	.member	_RXFUNC, 3, 14, 18, 1
	.member	_rsvd2, 4, 14, 18, 12
	.member	_rsvd3, 16, 14, 18, 16
	.eos
	.utag	_CANRIOC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANRIOC_BITS
	.eos
	.stag	_CANTOC_BITS, 32
	.member	_TOC0, 0, 14, 18, 1
	.member	_TOC1, 1, 14, 18, 1
	.member	_TOC2, 2, 14, 18, 1
	.member	_TOC3, 3, 14, 18, 1
	.member	_TOC4, 4, 14, 18, 1
	.member	_TOC5, 5, 14, 18, 1
	.member	_TOC6, 6, 14, 18, 1
	.member	_TOC7, 7, 14, 18, 1
	.member	_TOC8, 8, 14, 18, 1
	.member	_TOC9, 9, 14, 18, 1
	.member	_TOC10, 10, 14, 18, 1
	.member	_TOC11, 11, 14, 18, 1
	.member	_TOC12, 12, 14, 18, 1
	.member	_TOC13, 13, 14, 18, 1
	.member	_TOC14, 14, 14, 18, 1
	.member	_TOC15, 15, 14, 18, 1
	.member	_TOC16, 16, 14, 18, 1
	.member	_TOC17, 17, 14, 18, 1
	.member	_TOC18, 18, 14, 18, 1
	.member	_TOC19, 19, 14, 18, 1
	.member	_TOC20, 20, 14, 18, 1
	.member	_TOC21, 21, 14, 18, 1
	.member	_TOC22, 22, 14, 18, 1
	.member	_TOC23, 23, 14, 18, 1
	.member	_TOC24, 24, 14, 18, 1
	.member	_TOC25, 25, 14, 18, 1
	.member	_TOC26, 26, 14, 18, 1
	.member	_TOC27, 27, 14, 18, 1
	.member	_TOC28, 28, 14, 18, 1
	.member	_TOC29, 29, 14, 18, 1
	.member	_TOC30, 30, 14, 18, 1
	.member	_TOC31, 31, 14, 18, 1
	.eos
	.utag	_CANTOC_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTOC_BITS
	.eos
	.stag	_CANTOS_BITS, 32
	.member	_TOS0, 0, 14, 18, 1
	.member	_TOS1, 1, 14, 18, 1
	.member	_TOS2, 2, 14, 18, 1
	.member	_TOS3, 3, 14, 18, 1
	.member	_TOS4, 4, 14, 18, 1
	.member	_TOS5, 5, 14, 18, 1
	.member	_TOS6, 6, 14, 18, 1
	.member	_TOS7, 7, 14, 18, 1
	.member	_TOS8, 8, 14, 18, 1
	.member	_TOS9, 9, 14, 18, 1
	.member	_TOS10, 10, 14, 18, 1
	.member	_TOS11, 11, 14, 18, 1
	.member	_TOS12, 12, 14, 18, 1
	.member	_TOS13, 13, 14, 18, 1
	.member	_TOS14, 14, 14, 18, 1
	.member	_TOS15, 15, 14, 18, 1
	.member	_TOS16, 16, 14, 18, 1
	.member	_TOS17, 17, 14, 18, 1
	.member	_TOS18, 18, 14, 18, 1
	.member	_TOS19, 19, 14, 18, 1
	.member	_TOS20, 20, 14, 18, 1
	.member	_TOS21, 21, 14, 18, 1
	.member	_TOS22, 22, 14, 18, 1
	.member	_TOS23, 23, 14, 18, 1
	.member	_TOS24, 24, 14, 18, 1
	.member	_TOS25, 25, 14, 18, 1
	.member	_TOS26, 26, 14, 18, 1
	.member	_TOS27, 27, 14, 18, 1
	.member	_TOS28, 28, 14, 18, 1
	.member	_TOS29, 29, 14, 18, 1
	.member	_TOS30, 30, 14, 18, 1
	.member	_TOS31, 31, 14, 18, 1
	.eos
	.utag	_CANTOS_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANTOS_BITS
	.eos
	.stag	_ECAN_REGS, 832
	.member	_CANME, 0, 9, 8, 32, _CANME_REG
	.member	_CANMD, 32, 9, 8, 32, _CANMD_REG
	.member	_CANTRS, 64, 9, 8, 32, _CANTRS_REG
	.member	_CANTRR, 96, 9, 8, 32, _CANTRR_REG
	.member	_CANTA, 128, 9, 8, 32, _CANTA_REG
	.member	_CANAA, 160, 9, 8, 32, _CANAA_REG
	.member	_CANRMP, 192, 9, 8, 32, _CANRMP_REG
	.member	_CANRML, 224, 9, 8, 32, _CANRML_REG
	.member	_CANRFP, 256, 9, 8, 32, _CANRFP_REG
	.member	_CANGAM, 288, 9, 8, 32, _CANGAM_REG
	.member	_CANMC, 320, 9, 8, 32, _CANMC_REG
	.member	_CANBTC, 352, 9, 8, 32, _CANBTC_REG
	.member	_CANES, 384, 9, 8, 32, _CANES_REG
	.member	_CANTEC, 416, 9, 8, 32, _CANTEC_REG
	.member	_CANREC, 448, 9, 8, 32, _CANREC_REG
	.member	_CANGIF0, 480, 9, 8, 32, _CANGIF0_REG
	.member	_CANGIM, 512, 9, 8, 32, _CANGIM_REG
	.member	_CANGIF1, 544, 9, 8, 32, _CANGIF1_REG
	.member	_CANMIM, 576, 9, 8, 32, _CANMIM_REG
	.member	_CANMIL, 608, 9, 8, 32, _CANMIL_REG
	.member	_CANOPC, 640, 9, 8, 32, _CANOPC_REG
	.member	_CANTIOC, 672, 9, 8, 32, _CANTIOC_REG
	.member	_CANRIOC, 704, 9, 8, 32, _CANRIOC_REG
	.member	_CANTSC, 736, 15, 8, 32
	.member	_CANTOC, 768, 9, 8, 32, _CANTOC_REG
	.member	_CANTOS, 800, 9, 8, 32, _CANTOS_REG
	.eos
	.stag	_CANMSGID_BITS, 32
	.member	_EXTMSGID_L, 0, 14, 18, 16
	.member	_EXTMSGID_H, 16, 14, 18, 2
	.member	_STDMSGID, 18, 14, 18, 11
	.member	_AAM, 29, 14, 18, 1
	.member	_AME, 30, 14, 18, 1
	.member	_IDE, 31, 14, 18, 1
	.eos
	.utag	_CANMSGID_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMSGID_BITS
	.eos
	.stag	_CANMSGCTRL_BITS, 32
	.member	_DLC, 0, 14, 18, 4
	.member	_RTR, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 3
	.member	_TPL, 8, 14, 18, 5
	.member	_rsvd2, 13, 14, 18, 3
	.member	_rsvd3, 16, 14, 18, 16
	.eos
	.utag	_CANMSGCTRL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANMSGCTRL_BITS
	.eos
	.stag	_CANMDL_WORDS, 32
	.member	_LOW_WORD, 0, 14, 18, 16
	.member	_HI_WORD, 16, 14, 18, 16
	.eos
	.stag	_CANMDL_BYTES, 32
	.member	_BYTE3, 0, 14, 18, 8
	.member	_BYTE2, 8, 14, 18, 8
	.member	_BYTE1, 16, 14, 18, 8
	.member	_BYTE0, 24, 14, 18, 8
	.eos
	.utag	_CANMDL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_word, 0, 8, 11, 32, _CANMDL_WORDS
	.member	_byte, 0, 8, 11, 32, _CANMDL_BYTES
	.eos
	.stag	_CANMDH_WORDS, 32
	.member	_LOW_WORD, 0, 14, 18, 16
	.member	_HI_WORD, 16, 14, 18, 16
	.eos
	.stag	_CANMDH_BYTES, 32
	.member	_BYTE7, 0, 14, 18, 8
	.member	_BYTE6, 8, 14, 18, 8
	.member	_BYTE5, 16, 14, 18, 8
	.member	_BYTE4, 24, 14, 18, 8
	.eos
	.utag	_CANMDH_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_word, 0, 8, 11, 32, _CANMDH_WORDS
	.member	_byte, 0, 8, 11, 32, _CANMDH_BYTES
	.eos
	.stag	_MBOX, 128
	.member	_MSGID, 0, 9, 8, 32, _CANMSGID_REG
	.member	_MSGCTRL, 32, 9, 8, 32, _CANMSGCTRL_REG
	.member	_MDL, 64, 9, 8, 32, _CANMDL_REG
	.member	_MDH, 96, 9, 8, 32, _CANMDH_REG
	.eos
	.stag	_ECAN_MBOXES, 4096
	.member	_MBOX0, 0, 8, 8, 128, _MBOX
	.member	_MBOX1, 128, 8, 8, 128, _MBOX
	.member	_MBOX2, 256, 8, 8, 128, _MBOX
	.member	_MBOX3, 384, 8, 8, 128, _MBOX
	.member	_MBOX4, 512, 8, 8, 128, _MBOX
	.member	_MBOX5, 640, 8, 8, 128, _MBOX
	.member	_MBOX6, 768, 8, 8, 128, _MBOX
	.member	_MBOX7, 896, 8, 8, 128, _MBOX
	.member	_MBOX8, 1024, 8, 8, 128, _MBOX
	.member	_MBOX9, 1152, 8, 8, 128, _MBOX
	.member	_MBOX10, 1280, 8, 8, 128, _MBOX
	.member	_MBOX11, 1408, 8, 8, 128, _MBOX
	.member	_MBOX12, 1536, 8, 8, 128, _MBOX
	.member	_MBOX13, 1664, 8, 8, 128, _MBOX
	.member	_MBOX14, 1792, 8, 8, 128, _MBOX
	.member	_MBOX15, 1920, 8, 8, 128, _MBOX
	.member	_MBOX16, 2048, 8, 8, 128, _MBOX
	.member	_MBOX17, 2176, 8, 8, 128, _MBOX
	.member	_MBOX18, 2304, 8, 8, 128, _MBOX
	.member	_MBOX19, 2432, 8, 8, 128, _MBOX
	.member	_MBOX20, 2560, 8, 8, 128, _MBOX
	.member	_MBOX21, 2688, 8, 8, 128, _MBOX
	.member	_MBOX22, 2816, 8, 8, 128, _MBOX
	.member	_MBOX23, 2944, 8, 8, 128, _MBOX
	.member	_MBOX24, 3072, 8, 8, 128, _MBOX
	.member	_MBOX25, 3200, 8, 8, 128, _MBOX
	.member	_MBOX26, 3328, 8, 8, 128, _MBOX
	.member	_MBOX27, 3456, 8, 8, 128, _MBOX
	.member	_MBOX28, 3584, 8, 8, 128, _MBOX
	.member	_MBOX29, 3712, 8, 8, 128, _MBOX
	.member	_MBOX30, 3840, 8, 8, 128, _MBOX
	.member	_MBOX31, 3968, 8, 8, 128, _MBOX
	.eos
	.stag	_CANLAM_BITS, 32
	.member	_LAM_L, 0, 14, 18, 16
	.member	_LAM_H, 16, 14, 18, 13
	.member	_rsvd1, 29, 14, 18, 2
	.member	_LAMI, 31, 14, 18, 1
	.eos
	.utag	_CANLAM_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _CANLAM_BITS
	.eos
	.stag	_LAM_REGS, 1024
	.member	_LAM0, 0, 9, 8, 32, _CANLAM_REG
	.member	_LAM1, 32, 9, 8, 32, _CANLAM_REG
	.member	_LAM2, 64, 9, 8, 32, _CANLAM_REG
	.member	_LAM3, 96, 9, 8, 32, _CANLAM_REG
	.member	_LAM4, 128, 9, 8, 32, _CANLAM_REG
	.member	_LAM5, 160, 9, 8, 32, _CANLAM_REG
	.member	_LAM6, 192, 9, 8, 32, _CANLAM_REG
	.member	_LAM7, 224, 9, 8, 32, _CANLAM_REG
	.member	_LAM8, 256, 9, 8, 32, _CANLAM_REG
	.member	_LAM9, 288, 9, 8, 32, _CANLAM_REG
	.member	_LAM10, 320, 9, 8, 32, _CANLAM_REG
	.member	_LAM11, 352, 9, 8, 32, _CANLAM_REG
	.member	_LAM12, 384, 9, 8, 32, _CANLAM_REG
	.member	_LAM13, 416, 9, 8, 32, _CANLAM_REG
	.member	_LAM14, 448, 9, 8, 32, _CANLAM_REG
	.member	_LAM15, 480, 9, 8, 32, _CANLAM_REG
	.member	_LAM16, 512, 9, 8, 32, _CANLAM_REG
	.member	_LAM17, 544, 9, 8, 32, _CANLAM_REG
	.member	_LAM18, 576, 9, 8, 32, _CANLAM_REG
	.member	_LAM19, 608, 9, 8, 32, _CANLAM_REG
	.member	_LAM20, 640, 9, 8, 32, _CANLAM_REG
	.member	_LAM21, 672, 9, 8, 32, _CANLAM_REG
	.member	_LAM22, 704, 9, 8, 32, _CANLAM_REG
	.member	_LAM23, 736, 9, 8, 32, _CANLAM_REG
	.member	_LAM24, 768, 9, 8, 32, _CANLAM_REG
	.member	_LAM25, 800, 9, 8, 32, _CANLAM_REG
	.member	_LAM26, 832, 9, 8, 32, _CANLAM_REG
	.member	_LAM27, 864, 9, 8, 32, _CANLAM_REG
	.member	_LAM28, 896, 9, 8, 32, _CANLAM_REG
	.member	_LAM29, 928, 9, 8, 32, _CANLAM_REG
	.member	_LAM30, 960, 9, 8, 32, _CANLAM_REG
	.member	_LAM31, 992, 9, 8, 32, _CANLAM_REG
	.eos
	.stag	_MOTS_REGS, 1024
	.member	_MOTS0, 0, 15, 8, 32
	.member	_MOTS1, 32, 15, 8, 32
	.member	_MOTS2, 64, 15, 8, 32
	.member	_MOTS3, 96, 15, 8, 32
	.member	_MOTS4, 128, 15, 8, 32
	.member	_MOTS5, 160, 15, 8, 32
	.member	_MOTS6, 192, 15, 8, 32
	.member	_MOTS7, 224, 15, 8, 32
	.member	_MOTS8, 256, 15, 8, 32
	.member	_MOTS9, 288, 15, 8, 32
	.member	_MOTS10, 320, 15, 8, 32
	.member	_MOTS11, 352, 15, 8, 32
	.member	_MOTS12, 384, 15, 8, 32
	.member	_MOTS13, 416, 15, 8, 32
	.member	_MOTS14, 448, 15, 8, 32
	.member	_MOTS15, 480, 15, 8, 32
	.member	_MOTS16, 512, 15, 8, 32
	.member	_MOTS17, 544, 15, 8, 32
	.member	_MOTS18, 576, 15, 8, 32
	.member	_MOTS19, 608, 15, 8, 32
	.member	_MOTS20, 640, 15, 8, 32
	.member	_MOTS21, 672, 15, 8, 32
	.member	_MOTS22, 704, 15, 8, 32
	.member	_MOTS23, 736, 15, 8, 32
	.member	_MOTS24, 768, 15, 8, 32
	.member	_MOTS25, 800, 15, 8, 32
	.member	_MOTS26, 832, 15, 8, 32
	.member	_MOTS27, 864, 15, 8, 32
	.member	_MOTS28, 896, 15, 8, 32
	.member	_MOTS29, 928, 15, 8, 32
	.member	_MOTS30, 960, 15, 8, 32
	.member	_MOTS31, 992, 15, 8, 32
	.eos
	.stag	_MOTO_REGS, 1024
	.member	_MOTO0, 0, 15, 8, 32
	.member	_MOTO1, 32, 15, 8, 32
	.member	_MOTO2, 64, 15, 8, 32
	.member	_MOTO3, 96, 15, 8, 32
	.member	_MOTO4, 128, 15, 8, 32
	.member	_MOTO5, 160, 15, 8, 32
	.member	_MOTO6, 192, 15, 8, 32
	.member	_MOTO7, 224, 15, 8, 32
	.member	_MOTO8, 256, 15, 8, 32
	.member	_MOTO9, 288, 15, 8, 32
	.member	_MOTO10, 320, 15, 8, 32
	.member	_MOTO11, 352, 15, 8, 32
	.member	_MOTO12, 384, 15, 8, 32
	.member	_MOTO13, 416, 15, 8, 32
	.member	_MOTO14, 448, 15, 8, 32
	.member	_MOTO15, 480, 15, 8, 32
	.member	_MOTO16, 512, 15, 8, 32
	.member	_MOTO17, 544, 15, 8, 32
	.member	_MOTO18, 576, 15, 8, 32
	.member	_MOTO19, 608, 15, 8, 32
	.member	_MOTO20, 640, 15, 8, 32
	.member	_MOTO21, 672, 15, 8, 32
	.member	_MOTO22, 704, 15, 8, 32
	.member	_MOTO23, 736, 15, 8, 32
	.member	_MOTO24, 768, 15, 8, 32
	.member	_MOTO25, 800, 15, 8, 32
	.member	_MOTO26, 832, 15, 8, 32
	.member	_MOTO27, 864, 15, 8, 32
	.member	_MOTO28, 896, 15, 8, 32
	.member	_MOTO29, 928, 15, 8, 32
	.member	_MOTO30, 960, 15, 8, 32
	.member	_MOTO31, 992, 15, 8, 32
	.eos
	.stag	_ECCTL1_BITS, 16
	.member	_CAP1POL, 0, 14, 18, 1
	.member	_CTRRST1, 1, 14, 18, 1
	.member	_CAP2POL, 2, 14, 18, 1
	.member	_CTRRST2, 3, 14, 18, 1
	.member	_CAP3POL, 4, 14, 18, 1
	.member	_CTRRST3, 5, 14, 18, 1
	.member	_CAP4POL, 6, 14, 18, 1
	.member	_CTRRST4, 7, 14, 18, 1
	.member	_CAPLDEN, 8, 14, 18, 1
	.member	_PRESCALE, 9, 14, 18, 5
	.member	_FREE_SOFT, 14, 14, 18, 2
	.eos
	.utag	_ECCTL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECCTL1_BITS
	.eos
	.stag	_ECCTL2_BITS, 16
	.member	_CONT_ONESHT, 0, 14, 18, 1
	.member	_STOP_WRAP, 1, 14, 18, 2
	.member	_REARM, 3, 14, 18, 1
	.member	_TSCTRSTOP, 4, 14, 18, 1
	.member	_SYNCI_EN, 5, 14, 18, 1
	.member	_SYNCO_SEL, 6, 14, 18, 2
	.member	_SWSYNC, 8, 14, 18, 1
	.member	_CAP_APWM, 9, 14, 18, 1
	.member	_APWMPOL, 10, 14, 18, 1
	.member	_rsvd1, 11, 14, 18, 5
	.eos
	.utag	_ECCTL2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECCTL2_BITS
	.eos
	.stag	_ECEINT_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_CEVT1, 1, 14, 18, 1
	.member	_CEVT2, 2, 14, 18, 1
	.member	_CEVT3, 3, 14, 18, 1
	.member	_CEVT4, 4, 14, 18, 1
	.member	_CTROVF, 5, 14, 18, 1
	.member	_CTR_EQ_PRD, 6, 14, 18, 1
	.member	_CTR_EQ_CMP, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_ECEINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECEINT_BITS
	.eos
	.stag	_ECFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_CEVT1, 1, 14, 18, 1
	.member	_CEVT2, 2, 14, 18, 1
	.member	_CEVT3, 3, 14, 18, 1
	.member	_CEVT4, 4, 14, 18, 1
	.member	_CTROVF, 5, 14, 18, 1
	.member	_CTR_EQ_PRD, 6, 14, 18, 1
	.member	_CTR_EQ_CMP, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_ECFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _ECFLG_BITS
	.eos
	.stag	_ECAP_REGS, 512
	.member	_TSCTR, 0, 15, 8, 32
	.member	_CTRPHS, 32, 15, 8, 32
	.member	_CAP1, 64, 15, 8, 32
	.member	_CAP2, 96, 15, 8, 32
	.member	_CAP3, 128, 15, 8, 32
	.member	_CAP4, 160, 15, 8, 32
	.member	_rsvd1, 192, 62, 8, 128, , 8
	.member	_ECCTL1, 320, 9, 8, 16, _ECCTL1_REG
	.member	_ECCTL2, 336, 9, 8, 16, _ECCTL2_REG
	.member	_ECEINT, 352, 9, 8, 16, _ECEINT_REG
	.member	_ECFLG, 368, 9, 8, 16, _ECFLG_REG
	.member	_ECCLR, 384, 9, 8, 16, _ECFLG_REG
	.member	_ECFRC, 400, 9, 8, 16, _ECEINT_REG
	.member	_rsvd2, 416, 62, 8, 96, , 6
	.eos
	.stag	_QDECCTL_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_QSP, 5, 14, 18, 1
	.member	_QIP, 6, 14, 18, 1
	.member	_QBP, 7, 14, 18, 1
	.member	_QAP, 8, 14, 18, 1
	.member	_IGATE, 9, 14, 18, 1
	.member	_SWAP, 10, 14, 18, 1
	.member	_XCR, 11, 14, 18, 1
	.member	_SPSEL, 12, 14, 18, 1
	.member	_SOEN, 13, 14, 18, 1
	.member	_QSRC, 14, 14, 18, 2
	.eos
	.utag	_QDECCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QDECCTL_BITS
	.eos
	.stag	_QEPCTL_BITS, 16
	.member	_WDE, 0, 14, 18, 1
	.member	_UTE, 1, 14, 18, 1
	.member	_QCLM, 2, 14, 18, 1
	.member	_QPEN, 3, 14, 18, 1
	.member	_IEL, 4, 14, 18, 2
	.member	_SEL, 6, 14, 18, 1
	.member	_SWI, 7, 14, 18, 1
	.member	_IEI, 8, 14, 18, 2
	.member	_SEI, 10, 14, 18, 2
	.member	_PCRM, 12, 14, 18, 2
	.member	_FREE_SOFT, 14, 14, 18, 2
	.eos
	.utag	_QEPCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QEPCTL_BITS
	.eos
	.stag	_QCAPCTL_BITS, 16
	.member	_UPPS, 0, 14, 18, 4
	.member	_CCPS, 4, 14, 18, 3
	.member	_rsvd1, 7, 14, 18, 8
	.member	_CEN, 15, 14, 18, 1
	.eos
	.utag	_QCAPCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QCAPCTL_BITS
	.eos
	.stag	_QPOSCTL_BITS, 16
	.member	_PCSPW, 0, 14, 18, 12
	.member	_PCE, 12, 14, 18, 1
	.member	_PCPOL, 13, 14, 18, 1
	.member	_PCLOAD, 14, 14, 18, 1
	.member	_PCSHDW, 15, 14, 18, 1
	.eos
	.utag	_QPOSCTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QPOSCTL_BITS
	.eos
	.stag	_QEINT_BITS, 16
	.member	_rsvd1, 0, 14, 18, 1
	.member	_PCE, 1, 14, 18, 1
	.member	_QPE, 2, 14, 18, 1
	.member	_QDC, 3, 14, 18, 1
	.member	_WTO, 4, 14, 18, 1
	.member	_PCU, 5, 14, 18, 1
	.member	_PCO, 6, 14, 18, 1
	.member	_PCR, 7, 14, 18, 1
	.member	_PCM, 8, 14, 18, 1
	.member	_SEL, 9, 14, 18, 1
	.member	_IEL, 10, 14, 18, 1
	.member	_UTO, 11, 14, 18, 1
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_QEINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QEINT_BITS
	.eos
	.stag	_QFLG_BITS, 16
	.member	_INT, 0, 14, 18, 1
	.member	_PCE, 1, 14, 18, 1
	.member	_PHE, 2, 14, 18, 1
	.member	_QDC, 3, 14, 18, 1
	.member	_WTO, 4, 14, 18, 1
	.member	_PCU, 5, 14, 18, 1
	.member	_PCO, 6, 14, 18, 1
	.member	_PCR, 7, 14, 18, 1
	.member	_PCM, 8, 14, 18, 1
	.member	_SEL, 9, 14, 18, 1
	.member	_IEL, 10, 14, 18, 1
	.member	_UTO, 11, 14, 18, 1
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_QFLG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QFLG_BITS
	.eos
	.stag	_QFRC_BITS, 16
	.member	_reserved, 0, 14, 18, 1
	.member	_PCE, 1, 14, 18, 1
	.member	_PHE, 2, 14, 18, 1
	.member	_QDC, 3, 14, 18, 1
	.member	_WTO, 4, 14, 18, 1
	.member	_PCU, 5, 14, 18, 1
	.member	_PCO, 6, 14, 18, 1
	.member	_PCR, 7, 14, 18, 1
	.member	_PCM, 8, 14, 18, 1
	.member	_SEL, 9, 14, 18, 1
	.member	_IEL, 10, 14, 18, 1
	.member	_UTO, 11, 14, 18, 1
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_QFRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QFRC_BITS
	.eos
	.stag	_QEPSTS_BITS, 16
	.member	_PCEF, 0, 14, 18, 1
	.member	_FIMF, 1, 14, 18, 1
	.member	_CDEF, 2, 14, 18, 1
	.member	_COEF, 3, 14, 18, 1
	.member	_QDLF, 4, 14, 18, 1
	.member	_QDF, 5, 14, 18, 1
	.member	_FIDF, 6, 14, 18, 1
	.member	_UPEVNT, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_QEPSTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _QEPSTS_BITS
	.eos
	.stag	_EQEP_REGS, 1024
	.member	_QPOSCNT, 0, 15, 8, 32
	.member	_QPOSINIT, 32, 15, 8, 32
	.member	_QPOSMAX, 64, 15, 8, 32
	.member	_QPOSCMP, 96, 15, 8, 32
	.member	_QPOSILAT, 128, 15, 8, 32
	.member	_QPOSSLAT, 160, 15, 8, 32
	.member	_QPOSLAT, 192, 15, 8, 32
	.member	_QUTMR, 224, 15, 8, 32
	.member	_QUPRD, 256, 15, 8, 32
	.member	_QWDTMR, 288, 14, 8, 16
	.member	_QWDPRD, 304, 14, 8, 16
	.member	_QDECCTL, 320, 9, 8, 16, _QDECCTL_REG
	.member	_QEPCTL, 336, 9, 8, 16, _QEPCTL_REG
	.member	_QCAPCTL, 352, 9, 8, 16, _QCAPCTL_REG
	.member	_QPOSCTL, 368, 9, 8, 16, _QPOSCTL_REG
	.member	_QEINT, 384, 9, 8, 16, _QEINT_REG
	.member	_QFLG, 400, 9, 8, 16, _QFLG_REG
	.member	_QCLR, 416, 9, 8, 16, _QFLG_REG
	.member	_QFRC, 432, 9, 8, 16, _QFRC_REG
	.member	_QEPSTS, 448, 9, 8, 16, _QEPSTS_REG
	.member	_QCTMR, 464, 14, 8, 16
	.member	_QCPRD, 480, 14, 8, 16
	.member	_QCTMRLAT, 496, 14, 8, 16
	.member	_QCPRDLAT, 512, 14, 8, 16
	.member	_rsvd1, 528, 62, 8, 480, , 30
	.eos
	.stag	_GPACTRL_BITS, 32
	.member	_QUALPRD0, 0, 14, 18, 8
	.member	_QUALPRD1, 8, 14, 18, 8
	.member	_QUALPRD2, 16, 14, 18, 8
	.member	_QUALPRD3, 24, 14, 18, 8
	.eos
	.utag	_GPACTRL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPACTRL_BITS
	.eos
	.stag	_GPA1_BITS, 32
	.member	_GPIO0, 0, 14, 18, 2
	.member	_GPIO1, 2, 14, 18, 2
	.member	_GPIO2, 4, 14, 18, 2
	.member	_GPIO3, 6, 14, 18, 2
	.member	_GPIO4, 8, 14, 18, 2
	.member	_GPIO5, 10, 14, 18, 2
	.member	_GPIO6, 12, 14, 18, 2
	.member	_GPIO7, 14, 14, 18, 2
	.member	_GPIO8, 16, 14, 18, 2
	.member	_GPIO9, 18, 14, 18, 2
	.member	_GPIO10, 20, 14, 18, 2
	.member	_GPIO11, 22, 14, 18, 2
	.member	_GPIO12, 24, 14, 18, 2
	.member	_GPIO13, 26, 14, 18, 2
	.member	_GPIO14, 28, 14, 18, 2
	.member	_GPIO15, 30, 14, 18, 2
	.eos
	.utag	_GPA1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPA1_BITS
	.eos
	.stag	_GPA2_BITS, 32
	.member	_GPIO16, 0, 14, 18, 2
	.member	_GPIO17, 2, 14, 18, 2
	.member	_GPIO18, 4, 14, 18, 2
	.member	_GPIO19, 6, 14, 18, 2
	.member	_GPIO20, 8, 14, 18, 2
	.member	_GPIO21, 10, 14, 18, 2
	.member	_GPIO22, 12, 14, 18, 2
	.member	_GPIO23, 14, 14, 18, 2
	.member	_GPIO24, 16, 14, 18, 2
	.member	_GPIO25, 18, 14, 18, 2
	.member	_GPIO26, 20, 14, 18, 2
	.member	_GPIO27, 22, 14, 18, 2
	.member	_GPIO28, 24, 14, 18, 2
	.member	_GPIO29, 26, 14, 18, 2
	.member	_GPIO30, 28, 14, 18, 2
	.member	_GPIO31, 30, 14, 18, 2
	.eos
	.utag	_GPA2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPA2_BITS
	.eos
	.stag	_GPADAT_BITS, 32
	.member	_GPIO0, 0, 14, 18, 1
	.member	_GPIO1, 1, 14, 18, 1
	.member	_GPIO2, 2, 14, 18, 1
	.member	_GPIO3, 3, 14, 18, 1
	.member	_GPIO4, 4, 14, 18, 1
	.member	_GPIO5, 5, 14, 18, 1
	.member	_GPIO6, 6, 14, 18, 1
	.member	_GPIO7, 7, 14, 18, 1
	.member	_GPIO8, 8, 14, 18, 1
	.member	_GPIO9, 9, 14, 18, 1
	.member	_GPIO10, 10, 14, 18, 1
	.member	_GPIO11, 11, 14, 18, 1
	.member	_GPIO12, 12, 14, 18, 1
	.member	_GPIO13, 13, 14, 18, 1
	.member	_GPIO14, 14, 14, 18, 1
	.member	_GPIO15, 15, 14, 18, 1
	.member	_GPIO16, 16, 14, 18, 1
	.member	_GPIO17, 17, 14, 18, 1
	.member	_GPIO18, 18, 14, 18, 1
	.member	_GPIO19, 19, 14, 18, 1
	.member	_GPIO20, 20, 14, 18, 1
	.member	_GPIO21, 21, 14, 18, 1
	.member	_GPIO22, 22, 14, 18, 1
	.member	_GPIO23, 23, 14, 18, 1
	.member	_GPIO24, 24, 14, 18, 1
	.member	_GPIO25, 25, 14, 18, 1
	.member	_GPIO26, 26, 14, 18, 1
	.member	_GPIO27, 27, 14, 18, 1
	.member	_GPIO28, 28, 14, 18, 1
	.member	_GPIO29, 29, 14, 18, 1
	.member	_GPIO30, 30, 14, 18, 1
	.member	_GPIO31, 31, 14, 18, 1
	.eos
	.utag	_GPADAT_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPADAT_BITS
	.eos
	.stag	_GPBCTRL_BITS, 32
	.member	_QUALPRD0, 0, 14, 18, 8
	.member	_QUALPRD1, 8, 14, 18, 8
	.member	_QUALPRD2, 16, 14, 18, 8
	.member	_QUALPRD3, 24, 14, 18, 8
	.eos
	.utag	_GPBCTRL_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPBCTRL_BITS
	.eos
	.stag	_GPB1_BITS, 32
	.member	_GPIO32, 0, 14, 18, 2
	.member	_GPIO33, 2, 14, 18, 2
	.member	_GPIO34, 4, 14, 18, 2
	.member	_GPIO35, 6, 14, 18, 2
	.member	_GPIO36, 8, 14, 18, 2
	.member	_GPIO37, 10, 14, 18, 2
	.member	_GPIO38, 12, 14, 18, 2
	.member	_GPIO39, 14, 14, 18, 2
	.member	_GPIO40, 16, 14, 18, 2
	.member	_GPIO41, 18, 14, 18, 2
	.member	_GPIO42, 20, 14, 18, 2
	.member	_GPIO43, 22, 14, 18, 2
	.member	_GPIO44, 24, 14, 18, 2
	.member	_GPIO45, 26, 14, 18, 2
	.member	_GPIO46, 28, 14, 18, 2
	.member	_GPIO47, 30, 14, 18, 2
	.eos
	.utag	_GPB1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPB1_BITS
	.eos
	.stag	_GPB2_BITS, 32
	.member	_GPIO48, 0, 14, 18, 2
	.member	_GPIO49, 2, 14, 18, 2
	.member	_GPIO50, 4, 14, 18, 2
	.member	_GPIO51, 6, 14, 18, 2
	.member	_GPIO52, 8, 14, 18, 2
	.member	_GPIO53, 10, 14, 18, 2
	.member	_GPIO54, 12, 14, 18, 2
	.member	_GPIO55, 14, 14, 18, 2
	.member	_GPIO56, 16, 14, 18, 2
	.member	_GPIO57, 18, 14, 18, 2
	.member	_GPIO58, 20, 14, 18, 2
	.member	_GPIO59, 22, 14, 18, 2
	.member	_GPIO60, 24, 14, 18, 2
	.member	_GPIO61, 26, 14, 18, 2
	.member	_GPIO62, 28, 14, 18, 2
	.member	_GPIO63, 30, 14, 18, 2
	.eos
	.utag	_GPB2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPB2_BITS
	.eos
	.stag	_GPBDAT_BITS, 32
	.member	_GPIO32, 0, 14, 18, 1
	.member	_GPIO33, 1, 14, 18, 1
	.member	_GPIO34, 2, 14, 18, 1
	.member	_GPIO35, 3, 14, 18, 1
	.member	_GPIO36, 4, 14, 18, 1
	.member	_GPIO37, 5, 14, 18, 1
	.member	_GPIO38, 6, 14, 18, 1
	.member	_GPIO39, 7, 14, 18, 1
	.member	_GPIO40, 8, 14, 18, 1
	.member	_GPIO41, 9, 14, 18, 1
	.member	_GPIO42, 10, 14, 18, 1
	.member	_GPIO43, 11, 14, 18, 1
	.member	_GPIO44, 12, 14, 18, 1
	.member	_GPIO45, 13, 14, 18, 1
	.member	_GPIO46, 14, 14, 18, 1
	.member	_GPIO47, 15, 14, 18, 1
	.member	_GPIO48, 16, 14, 18, 1
	.member	_GPIO49, 17, 14, 18, 1
	.member	_GPIO50, 18, 14, 18, 1
	.member	_GPIO51, 19, 14, 18, 1
	.member	_GPIO52, 20, 14, 18, 1
	.member	_GPIO53, 21, 14, 18, 1
	.member	_GPIO54, 22, 14, 18, 1
	.member	_GPIO55, 23, 14, 18, 1
	.member	_GPIO56, 24, 14, 18, 1
	.member	_GPIO57, 25, 14, 18, 1
	.member	_GPIO58, 26, 14, 18, 1
	.member	_GPIO59, 27, 14, 18, 1
	.member	_GPIO60, 28, 14, 18, 1
	.member	_GPIO61, 29, 14, 18, 1
	.member	_GPIO62, 30, 14, 18, 1
	.member	_GPIO63, 31, 14, 18, 1
	.eos
	.utag	_GPBDAT_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPBDAT_BITS
	.eos
	.stag	_GPC1_BITS, 32
	.member	_GPIO64, 0, 14, 18, 2
	.member	_GPIO65, 2, 14, 18, 2
	.member	_GPIO66, 4, 14, 18, 2
	.member	_GPIO67, 6, 14, 18, 2
	.member	_GPIO68, 8, 14, 18, 2
	.member	_GPIO69, 10, 14, 18, 2
	.member	_GPIO70, 12, 14, 18, 2
	.member	_GPIO71, 14, 14, 18, 2
	.member	_GPIO72, 16, 14, 18, 2
	.member	_GPIO73, 18, 14, 18, 2
	.member	_GPIO74, 20, 14, 18, 2
	.member	_GPIO75, 22, 14, 18, 2
	.member	_GPIO76, 24, 14, 18, 2
	.member	_GPIO77, 26, 14, 18, 2
	.member	_GPIO78, 28, 14, 18, 2
	.member	_GPIO79, 30, 14, 18, 2
	.eos
	.utag	_GPC1_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPC1_BITS
	.eos
	.stag	_GPC2_BITS, 32
	.member	_GPIO80, 0, 14, 18, 2
	.member	_GPIO81, 2, 14, 18, 2
	.member	_GPIO82, 4, 14, 18, 2
	.member	_GPIO83, 6, 14, 18, 2
	.member	_GPIO84, 8, 14, 18, 2
	.member	_GPIO85, 10, 14, 18, 2
	.member	_GPIO86, 12, 14, 18, 2
	.member	_GPIO87, 14, 14, 18, 2
	.member	_rsvd, 16, 14, 18, 16
	.eos
	.utag	_GPC2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPC2_BITS
	.eos
	.stag	_GPCDAT_BITS, 32
	.member	_GPIO64, 0, 14, 18, 1
	.member	_GPIO65, 1, 14, 18, 1
	.member	_GPIO66, 2, 14, 18, 1
	.member	_GPIO67, 3, 14, 18, 1
	.member	_GPIO68, 4, 14, 18, 1
	.member	_GPIO69, 5, 14, 18, 1
	.member	_GPIO70, 6, 14, 18, 1
	.member	_GPIO71, 7, 14, 18, 1
	.member	_GPIO72, 8, 14, 18, 1
	.member	_GPIO73, 9, 14, 18, 1
	.member	_GPIO74, 10, 14, 18, 1
	.member	_GPIO75, 11, 14, 18, 1
	.member	_GPIO76, 12, 14, 18, 1
	.member	_GPIO77, 13, 14, 18, 1
	.member	_GPIO78, 14, 14, 18, 1
	.member	_GPIO79, 15, 14, 18, 1
	.member	_GPIO80, 16, 14, 18, 1
	.member	_GPIO81, 17, 14, 18, 1
	.member	_GPIO82, 18, 14, 18, 1
	.member	_GPIO83, 19, 14, 18, 1
	.member	_GPIO84, 20, 14, 18, 1
	.member	_GPIO85, 21, 14, 18, 1
	.member	_GPIO86, 22, 14, 18, 1
	.member	_GPIO87, 23, 14, 18, 1
	.member	_rsvd1, 24, 14, 18, 8
	.eos
	.utag	_GPCDAT_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _GPCDAT_BITS
	.eos
	.stag	_GPIO_CTRL_REGS, 736
	.member	_GPACTRL, 0, 9, 8, 32, _GPACTRL_REG
	.member	_GPAQSEL1, 32, 9, 8, 32, _GPA1_REG
	.member	_GPAQSEL2, 64, 9, 8, 32, _GPA2_REG
	.member	_GPAMUX1, 96, 9, 8, 32, _GPA1_REG
	.member	_GPAMUX2, 128, 9, 8, 32, _GPA2_REG
	.member	_GPADIR, 160, 9, 8, 32, _GPADAT_REG
	.member	_GPAPUD, 192, 9, 8, 32, _GPADAT_REG
	.member	_rsvd1, 224, 15, 8, 32
	.member	_GPBCTRL, 256, 9, 8, 32, _GPBCTRL_REG
	.member	_GPBQSEL1, 288, 9, 8, 32, _GPB1_REG
	.member	_GPBQSEL2, 320, 9, 8, 32, _GPB2_REG
	.member	_GPBMUX1, 352, 9, 8, 32, _GPB1_REG
	.member	_GPBMUX2, 384, 9, 8, 32, _GPB2_REG
	.member	_GPBDIR, 416, 9, 8, 32, _GPBDAT_REG
	.member	_GPBPUD, 448, 9, 8, 32, _GPBDAT_REG
	.member	_rsvd2, 480, 62, 8, 128, , 8
	.member	_GPCMUX1, 608, 9, 8, 32, _GPC1_REG
	.member	_GPCMUX2, 640, 9, 8, 32, _GPC2_REG
	.member	_GPCDIR, 672, 9, 8, 32, _GPCDAT_REG
	.member	_GPCPUD, 704, 9, 8, 32, _GPCDAT_REG
	.eos
	.stag	_GPIO_DATA_REGS, 512
	.member	_GPADAT, 0, 9, 8, 32, _GPADAT_REG
	.member	_GPASET, 32, 9, 8, 32, _GPADAT_REG
	.member	_GPACLEAR, 64, 9, 8, 32, _GPADAT_REG
	.member	_GPATOGGLE, 96, 9, 8, 32, _GPADAT_REG
	.member	_GPBDAT, 128, 9, 8, 32, _GPBDAT_REG
	.member	_GPBSET, 160, 9, 8, 32, _GPBDAT_REG
	.member	_GPBCLEAR, 192, 9, 8, 32, _GPBDAT_REG
	.member	_GPBTOGGLE, 224, 9, 8, 32, _GPBDAT_REG
	.member	_GPCDAT, 256, 9, 8, 32, _GPCDAT_REG
	.member	_GPCSET, 288, 9, 8, 32, _GPCDAT_REG
	.member	_GPCCLEAR, 320, 9, 8, 32, _GPCDAT_REG
	.member	_GPCTOGGLE, 352, 9, 8, 32, _GPCDAT_REG
	.member	_rsvd1, 384, 62, 8, 128, , 8
	.eos
	.stag	_GPIOXINT_BITS, 16
	.member	_GPIOSEL, 0, 14, 18, 5
	.member	_rsvd1, 5, 14, 18, 11
	.eos
	.utag	_GPIOXINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _GPIOXINT_BITS
	.eos
	.stag	_GPIO_INT_REGS, 160
	.member	_GPIOXINT1SEL, 0, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT2SEL, 16, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXNMISEL, 32, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT3SEL, 48, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT4SEL, 64, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT5SEL, 80, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT6SEL, 96, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOXINT7SEL, 112, 9, 8, 16, _GPIOXINT_REG
	.member	_GPIOLPMSEL, 128, 9, 8, 32, _GPADAT_REG
	.eos
	.stag	_I2CIER_BITS, 16
	.member	_ARBL, 0, 14, 18, 1
	.member	_NACK, 1, 14, 18, 1
	.member	_ARDY, 2, 14, 18, 1
	.member	_RRDY, 3, 14, 18, 1
	.member	_XRDY, 4, 14, 18, 1
	.member	_SCD, 5, 14, 18, 1
	.member	_AAS, 6, 14, 18, 1
	.member	_rsvd, 7, 14, 18, 9
	.eos
	.utag	_I2CIER_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CIER_BITS
	.eos
	.stag	_I2CSTR_BITS, 16
	.member	_ARBL, 0, 14, 18, 1
	.member	_NACK, 1, 14, 18, 1
	.member	_ARDY, 2, 14, 18, 1
	.member	_RRDY, 3, 14, 18, 1
	.member	_XRDY, 4, 14, 18, 1
	.member	_SCD, 5, 14, 18, 1
	.member	_rsvd1, 6, 14, 18, 2
	.member	_AD0, 8, 14, 18, 1
	.member	_AAS, 9, 14, 18, 1
	.member	_XSMT, 10, 14, 18, 1
	.member	_RSFULL, 11, 14, 18, 1
	.member	_BB, 12, 14, 18, 1
	.member	_NACKSNT, 13, 14, 18, 1
	.member	_SDIR, 14, 14, 18, 1
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_I2CSTR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CSTR_BITS
	.eos
	.stag	_I2CMDR_BITS, 16
	.member	_BC, 0, 14, 18, 3
	.member	_FDF, 3, 14, 18, 1
	.member	_STB, 4, 14, 18, 1
	.member	_IRS, 5, 14, 18, 1
	.member	_DLB, 6, 14, 18, 1
	.member	_RM, 7, 14, 18, 1
	.member	_XA, 8, 14, 18, 1
	.member	_TRX, 9, 14, 18, 1
	.member	_MST, 10, 14, 18, 1
	.member	_STP, 11, 14, 18, 1
	.member	_rsvd1, 12, 14, 18, 1
	.member	_STT, 13, 14, 18, 1
	.member	_FREE, 14, 14, 18, 1
	.member	_NACKMOD, 15, 14, 18, 1
	.eos
	.utag	_I2CMDR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CMDR_BITS
	.eos
	.stag	_I2CISRC_BITS, 16
	.member	_INTCODE, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_I2CISRC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CISRC_BITS
	.eos
	.stag	_I2CPSC_BITS, 16
	.member	_IPSC, 0, 14, 18, 8
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_I2CPSC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CPSC_BITS
	.eos
	.stag	_I2CFFTX_BITS, 16
	.member	_TXFFIL, 0, 14, 18, 5
	.member	_TXFFIENA, 5, 14, 18, 1
	.member	_TXFFINTCLR, 6, 14, 18, 1
	.member	_TXFFINT, 7, 14, 18, 1
	.member	_TXFFST, 8, 14, 18, 5
	.member	_TXFFRST, 13, 14, 18, 1
	.member	_I2CFFEN, 14, 14, 18, 1
	.member	_rsvd1, 15, 14, 18, 1
	.eos
	.utag	_I2CFFTX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CFFTX_BITS
	.eos
	.stag	_I2CFFRX_BITS, 16
	.member	_RXFFIL, 0, 14, 18, 5
	.member	_RXFFIENA, 5, 14, 18, 1
	.member	_RXFFINTCLR, 6, 14, 18, 1
	.member	_RXFFINT, 7, 14, 18, 1
	.member	_RXFFST, 8, 14, 18, 5
	.member	_RXFFRST, 13, 14, 18, 1
	.member	_rsvd1, 14, 14, 18, 2
	.eos
	.utag	_I2CFFRX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _I2CFFRX_BITS
	.eos
	.stag	_I2C_REGS, 544
	.member	_I2COAR, 0, 14, 8, 16
	.member	_I2CIER, 16, 9, 8, 16, _I2CIER_REG
	.member	_I2CSTR, 32, 9, 8, 16, _I2CSTR_REG
	.member	_I2CCLKL, 48, 14, 8, 16
	.member	_I2CCLKH, 64, 14, 8, 16
	.member	_I2CCNT, 80, 14, 8, 16
	.member	_I2CDRR, 96, 14, 8, 16
	.member	_I2CSAR, 112, 14, 8, 16
	.member	_I2CDXR, 128, 14, 8, 16
	.member	_I2CMDR, 144, 9, 8, 16, _I2CMDR_REG
	.member	_I2CISRC, 160, 9, 8, 16, _I2CISRC_REG
	.member	_rsvd1, 176, 14, 8, 16
	.member	_I2CPSC, 192, 9, 8, 16, _I2CPSC_REG
	.member	_rsvd2, 208, 62, 8, 304, , 19
	.member	_I2CFFTX, 512, 9, 8, 16, _I2CFFTX_REG
	.member	_I2CFFRX, 528, 9, 8, 16, _I2CFFRX_REG
	.eos
	.stag	_DRR2_BITS, 16
	.member	_HWLB, 0, 14, 18, 8
	.member	_HWHB, 8, 14, 18, 8
	.eos
	.utag	_DRR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DRR2_BITS
	.eos
	.stag	_DRR1_BITS, 16
	.member	_LWLB, 0, 14, 18, 8
	.member	_LWHB, 8, 14, 18, 8
	.eos
	.utag	_DRR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DRR1_BITS
	.eos
	.stag	_DXR2_BITS, 16
	.member	_HWLB, 0, 14, 18, 8
	.member	_HWHB, 8, 14, 18, 8
	.eos
	.utag	_DXR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DXR2_BITS
	.eos
	.stag	_DXR1_BITS, 16
	.member	_LWLB, 0, 14, 18, 8
	.member	_LWHB, 8, 14, 18, 8
	.eos
	.utag	_DXR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _DXR1_BITS
	.eos
	.stag	_SPCR2_BITS, 16
	.member	_XRST, 0, 14, 18, 1
	.member	_XRDY, 1, 14, 18, 1
	.member	_XEMPTY, 2, 14, 18, 1
	.member	_XSYNCERR, 3, 14, 18, 1
	.member	_XINTM, 4, 14, 18, 2
	.member	_GRST, 6, 14, 18, 1
	.member	_FRST, 7, 14, 18, 1
	.member	_SOFT, 8, 14, 18, 1
	.member	_FREE, 9, 14, 18, 1
	.member	_rsvd, 10, 14, 18, 6
	.eos
	.utag	_SPCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPCR2_BITS
	.eos
	.stag	_SPCR1_BITS, 16
	.member	_RRST, 0, 14, 18, 1
	.member	_RRDY, 1, 14, 18, 1
	.member	_RFULL, 2, 14, 18, 1
	.member	_RSYNCERR, 3, 14, 18, 1
	.member	_RINTM, 4, 14, 18, 2
	.member	_ABIS, 6, 14, 18, 1
	.member	_DXENA, 7, 14, 18, 1
	.member	_rsvd, 8, 14, 18, 3
	.member	_CLKSTP, 11, 14, 18, 2
	.member	_RJUST, 13, 14, 18, 2
	.member	_DLB, 15, 14, 18, 1
	.eos
	.utag	_SPCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPCR1_BITS
	.eos
	.stag	_RCR2_BITS, 16
	.member	_RDATDLY, 0, 14, 18, 2
	.member	_RFIG, 2, 14, 18, 1
	.member	_RCOMPAND, 3, 14, 18, 2
	.member	_RWDLEN2, 5, 14, 18, 3
	.member	_RFRLEN2, 8, 14, 18, 7
	.member	_RPHASE, 15, 14, 18, 1
	.eos
	.utag	_RCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCR2_BITS
	.eos
	.stag	_RCR1_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_RWDLEN1, 5, 14, 18, 3
	.member	_RFRLEN1, 8, 14, 18, 7
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_RCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCR1_BITS
	.eos
	.stag	_XCR2_BITS, 16
	.member	_XDATDLY, 0, 14, 18, 2
	.member	_XFIG, 2, 14, 18, 1
	.member	_XCOMPAND, 3, 14, 18, 2
	.member	_XWDLEN2, 5, 14, 18, 3
	.member	_XFRLEN2, 8, 14, 18, 7
	.member	_XPHASE, 15, 14, 18, 1
	.eos
	.utag	_XCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCR2_BITS
	.eos
	.stag	_XCR1_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_XWDLEN1, 5, 14, 18, 3
	.member	_XFRLEN1, 8, 14, 18, 7
	.member	_rsvd2, 15, 14, 18, 1
	.eos
	.utag	_XCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCR1_BITS
	.eos
	.stag	_SRGR2_BITS, 16
	.member	_FPER, 0, 14, 18, 12
	.member	_FSGM, 12, 14, 18, 1
	.member	_CLKSM, 13, 14, 18, 1
	.member	_rsvd, 14, 14, 18, 1
	.member	_GSYNC, 15, 14, 18, 1
	.eos
	.utag	_SRGR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SRGR2_BITS
	.eos
	.stag	_SRGR1_BITS, 16
	.member	_CLKGDV, 0, 14, 18, 8
	.member	_FWID, 8, 14, 18, 8
	.eos
	.utag	_SRGR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SRGR1_BITS
	.eos
	.stag	_MCR2_BITS, 16
	.member	_XMCM, 0, 14, 18, 2
	.member	_XCBLK, 2, 14, 18, 3
	.member	_XPABLK, 5, 14, 18, 2
	.member	_XPBBLK, 7, 14, 18, 2
	.member	_XMCME, 9, 14, 18, 1
	.member	_rsvd, 10, 14, 18, 6
	.eos
	.utag	_MCR2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MCR2_BITS
	.eos
	.stag	_MCR1_BITS, 16
	.member	_RMCM, 0, 14, 18, 1
	.member	_rsvd, 1, 14, 18, 1
	.member	_RCBLK, 2, 14, 18, 3
	.member	_RPABLK, 5, 14, 18, 2
	.member	_RPBBLK, 7, 14, 18, 2
	.member	_RMCME, 9, 14, 18, 1
	.member	_rsvd1, 10, 14, 18, 6
	.eos
	.utag	_MCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MCR1_BITS
	.eos
	.stag	_RCERA_BITS, 16
	.member	_RCEA0, 0, 14, 18, 1
	.member	_RCEA1, 1, 14, 18, 1
	.member	_RCEA2, 2, 14, 18, 1
	.member	_RCEA3, 3, 14, 18, 1
	.member	_RCEA4, 4, 14, 18, 1
	.member	_RCEA5, 5, 14, 18, 1
	.member	_RCEA6, 6, 14, 18, 1
	.member	_RCEA7, 7, 14, 18, 1
	.member	_RCEA8, 8, 14, 18, 1
	.member	_RCEA9, 9, 14, 18, 1
	.member	_RCEA10, 10, 14, 18, 1
	.member	_RCEA11, 11, 14, 18, 1
	.member	_RCEA12, 12, 14, 18, 1
	.member	_RCEA13, 13, 14, 18, 1
	.member	_RCEA14, 14, 14, 18, 1
	.member	_RCEA15, 15, 14, 18, 1
	.eos
	.utag	_RCERA_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERA_BITS
	.eos
	.stag	_RCERB_BITS, 16
	.member	_RCEB0, 0, 14, 18, 1
	.member	_RCEB1, 1, 14, 18, 1
	.member	_RCEB2, 2, 14, 18, 1
	.member	_RCEB3, 3, 14, 18, 1
	.member	_RCEB4, 4, 14, 18, 1
	.member	_RCEB5, 5, 14, 18, 1
	.member	_RCEB6, 6, 14, 18, 1
	.member	_RCEB7, 7, 14, 18, 1
	.member	_RCEB8, 8, 14, 18, 1
	.member	_RCEB9, 9, 14, 18, 1
	.member	_RCEB10, 10, 14, 18, 1
	.member	_RCEB11, 11, 14, 18, 1
	.member	_RCEB12, 12, 14, 18, 1
	.member	_RCEB13, 13, 14, 18, 1
	.member	_RCEB14, 14, 14, 18, 1
	.member	_RCEB15, 15, 14, 18, 1
	.eos
	.utag	_RCERB_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERB_BITS
	.eos
	.stag	_XCERA_BITS, 16
	.member	_XCERA0, 0, 14, 18, 1
	.member	_XCERA1, 1, 14, 18, 1
	.member	_XCERA2, 2, 14, 18, 1
	.member	_XCERA3, 3, 14, 18, 1
	.member	_XCERA4, 4, 14, 18, 1
	.member	_XCERA5, 5, 14, 18, 1
	.member	_XCERA6, 6, 14, 18, 1
	.member	_XCERA7, 7, 14, 18, 1
	.member	_XCERA8, 8, 14, 18, 1
	.member	_XCERA9, 9, 14, 18, 1
	.member	_XCERA10, 10, 14, 18, 1
	.member	_XCERA11, 11, 14, 18, 1
	.member	_XCERA12, 12, 14, 18, 1
	.member	_XCERA13, 13, 14, 18, 1
	.member	_XCERA14, 14, 14, 18, 1
	.member	_XCERA15, 15, 14, 18, 1
	.eos
	.utag	_XCERA_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERA_BITS
	.eos
	.stag	_XCERB_BITS, 16
	.member	_XCERB0, 0, 14, 18, 1
	.member	_XCERB1, 1, 14, 18, 1
	.member	_XCERB2, 2, 14, 18, 1
	.member	_XCERB3, 3, 14, 18, 1
	.member	_XCERB4, 4, 14, 18, 1
	.member	_XCERB5, 5, 14, 18, 1
	.member	_XCERB6, 6, 14, 18, 1
	.member	_XCERB7, 7, 14, 18, 1
	.member	_XCERB8, 8, 14, 18, 1
	.member	_XCERB9, 9, 14, 18, 1
	.member	_XCERB10, 10, 14, 18, 1
	.member	_XCERB11, 11, 14, 18, 1
	.member	_XCERB12, 12, 14, 18, 1
	.member	_XCERB13, 13, 14, 18, 1
	.member	_XCERB14, 14, 14, 18, 1
	.member	_XCERB15, 15, 14, 18, 1
	.eos
	.utag	_XCERB_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERB_BITS
	.eos
	.stag	_PCR_BITS, 16
	.member	_CLKRP, 0, 14, 18, 1
	.member	_CLKXP, 1, 14, 18, 1
	.member	_FSRP, 2, 14, 18, 1
	.member	_FSXP, 3, 14, 18, 1
	.member	_DR_STAT, 4, 14, 18, 1
	.member	_DX_STAT, 5, 14, 18, 1
	.member	_CLKS_STAT, 6, 14, 18, 1
	.member	_SCLKME, 7, 14, 18, 1
	.member	_CLKRM, 8, 14, 18, 1
	.member	_CLKXM, 9, 14, 18, 1
	.member	_FSRM, 10, 14, 18, 1
	.member	_FSXM, 11, 14, 18, 1
	.member	_RIOEN, 12, 14, 18, 1
	.member	_XIOEN, 13, 14, 18, 1
	.member	_IDEL_EN, 14, 14, 18, 1
	.member	_rsvd, 15, 14, 18, 1
	.eos
	.utag	_PCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCR_BITS
	.eos
	.stag	_RCERC_BITS, 16
	.member	_RCEC0, 0, 14, 18, 1
	.member	_RCEC1, 1, 14, 18, 1
	.member	_RCEC2, 2, 14, 18, 1
	.member	_RCEC3, 3, 14, 18, 1
	.member	_RCEC4, 4, 14, 18, 1
	.member	_RCEC5, 5, 14, 18, 1
	.member	_RCEC6, 6, 14, 18, 1
	.member	_RCEC7, 7, 14, 18, 1
	.member	_RCEC8, 8, 14, 18, 1
	.member	_RCEC9, 9, 14, 18, 1
	.member	_RCEC10, 10, 14, 18, 1
	.member	_RCEC11, 11, 14, 18, 1
	.member	_RCEC12, 12, 14, 18, 1
	.member	_RCEC13, 13, 14, 18, 1
	.member	_RCEC14, 14, 14, 18, 1
	.member	_RCEC15, 15, 14, 18, 1
	.eos
	.utag	_RCERC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERC_BITS
	.eos
	.stag	_RCERD_BITS, 16
	.member	_RCED0, 0, 14, 18, 1
	.member	_RCED1, 1, 14, 18, 1
	.member	_RCED2, 2, 14, 18, 1
	.member	_RCED3, 3, 14, 18, 1
	.member	_RCED4, 4, 14, 18, 1
	.member	_RCED5, 5, 14, 18, 1
	.member	_RCED6, 6, 14, 18, 1
	.member	_RCED7, 7, 14, 18, 1
	.member	_RCED8, 8, 14, 18, 1
	.member	_RCED9, 9, 14, 18, 1
	.member	_RCED10, 10, 14, 18, 1
	.member	_RCED11, 11, 14, 18, 1
	.member	_RCED12, 12, 14, 18, 1
	.member	_RCED13, 13, 14, 18, 1
	.member	_RCED14, 14, 14, 18, 1
	.member	_RCED15, 15, 14, 18, 1
	.eos
	.utag	_RCERD_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERD_BITS
	.eos
	.stag	_XCERC_BITS, 16
	.member	_XCERC0, 0, 14, 18, 1
	.member	_XCERC1, 1, 14, 18, 1
	.member	_XCERC2, 2, 14, 18, 1
	.member	_XCERC3, 3, 14, 18, 1
	.member	_XCERC4, 4, 14, 18, 1
	.member	_XCERC5, 5, 14, 18, 1
	.member	_XCERC6, 6, 14, 18, 1
	.member	_XCERC7, 7, 14, 18, 1
	.member	_XCERC8, 8, 14, 18, 1
	.member	_XCERC9, 9, 14, 18, 1
	.member	_XCERC10, 10, 14, 18, 1
	.member	_XCERC11, 11, 14, 18, 1
	.member	_XCERC12, 12, 14, 18, 1
	.member	_XCERC13, 13, 14, 18, 1
	.member	_XCERC14, 14, 14, 18, 1
	.member	_XCERC15, 15, 14, 18, 1
	.eos
	.utag	_XCERC_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERC_BITS
	.eos
	.stag	_XCERD_BITS, 16
	.member	_XCERD0, 0, 14, 18, 1
	.member	_XCERD1, 1, 14, 18, 1
	.member	_XCERD2, 2, 14, 18, 1
	.member	_XCERD3, 3, 14, 18, 1
	.member	_XCERD4, 4, 14, 18, 1
	.member	_XCERD5, 5, 14, 18, 1
	.member	_XCERD6, 6, 14, 18, 1
	.member	_XCERD7, 7, 14, 18, 1
	.member	_XCERD8, 8, 14, 18, 1
	.member	_XCERD9, 9, 14, 18, 1
	.member	_XCERD10, 10, 14, 18, 1
	.member	_XCERD11, 11, 14, 18, 1
	.member	_XCERD12, 12, 14, 18, 1
	.member	_XCERD13, 13, 14, 18, 1
	.member	_XCERD14, 14, 14, 18, 1
	.member	_XCERD15, 15, 14, 18, 1
	.eos
	.utag	_XCERD_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERD_BITS
	.eos
	.stag	_RCERE_BITS, 16
	.member	_RCEE0, 0, 14, 18, 1
	.member	_RCEE1, 1, 14, 18, 1
	.member	_RCEE2, 2, 14, 18, 1
	.member	_RCEE3, 3, 14, 18, 1
	.member	_RCEE4, 4, 14, 18, 1
	.member	_RCEE5, 5, 14, 18, 1
	.member	_RCEE6, 6, 14, 18, 1
	.member	_RCEE7, 7, 14, 18, 1
	.member	_RCEE8, 8, 14, 18, 1
	.member	_RCEE9, 9, 14, 18, 1
	.member	_RCEE10, 10, 14, 18, 1
	.member	_RCEE11, 11, 14, 18, 1
	.member	_RCEE12, 12, 14, 18, 1
	.member	_RCEE13, 13, 14, 18, 1
	.member	_RCEE14, 14, 14, 18, 1
	.member	_RCEE15, 15, 14, 18, 1
	.eos
	.utag	_RCERE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERE_BITS
	.eos
	.stag	_RCERF_BITS, 16
	.member	_RCEF0, 0, 14, 18, 1
	.member	_RCEF1, 1, 14, 18, 1
	.member	_RCEF2, 2, 14, 18, 1
	.member	_RCEF3, 3, 14, 18, 1
	.member	_RCEF4, 4, 14, 18, 1
	.member	_RCEF5, 5, 14, 18, 1
	.member	_RCEF6, 6, 14, 18, 1
	.member	_RCEF7, 7, 14, 18, 1
	.member	_RCEF8, 8, 14, 18, 1
	.member	_RCEF9, 9, 14, 18, 1
	.member	_RCEF10, 10, 14, 18, 1
	.member	_RCEF11, 11, 14, 18, 1
	.member	_RCEF12, 12, 14, 18, 1
	.member	_RCEF13, 13, 14, 18, 1
	.member	_RCEF14, 14, 14, 18, 1
	.member	_RCEF15, 15, 14, 18, 1
	.eos
	.utag	_RCERF_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERF_BITS
	.eos
	.stag	_XCERE_BITS, 16
	.member	_XCERE0, 0, 14, 18, 1
	.member	_XCERE1, 1, 14, 18, 1
	.member	_XCERE2, 2, 14, 18, 1
	.member	_XCERE3, 3, 14, 18, 1
	.member	_XCERE4, 4, 14, 18, 1
	.member	_XCERE5, 5, 14, 18, 1
	.member	_XCERE6, 6, 14, 18, 1
	.member	_XCERE7, 7, 14, 18, 1
	.member	_XCERE8, 8, 14, 18, 1
	.member	_XCERE9, 9, 14, 18, 1
	.member	_XCERE10, 10, 14, 18, 1
	.member	_XCERE11, 11, 14, 18, 1
	.member	_XCERE12, 12, 14, 18, 1
	.member	_XCERE13, 13, 14, 18, 1
	.member	_XCERE14, 14, 14, 18, 1
	.member	_XCERE15, 15, 14, 18, 1
	.eos
	.utag	_XCERE_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERE_BITS
	.eos
	.stag	_XCERF_BITS, 16
	.member	_XCERF0, 0, 14, 18, 1
	.member	_XCERF1, 1, 14, 18, 1
	.member	_XCERF2, 2, 14, 18, 1
	.member	_XCERF3, 3, 14, 18, 1
	.member	_XCERF4, 4, 14, 18, 1
	.member	_XCERF5, 5, 14, 18, 1
	.member	_XCERF6, 6, 14, 18, 1
	.member	_XCERF7, 7, 14, 18, 1
	.member	_XCERF8, 8, 14, 18, 1
	.member	_XCERF9, 9, 14, 18, 1
	.member	_XCERF10, 10, 14, 18, 1
	.member	_XCERF11, 11, 14, 18, 1
	.member	_XCERF12, 12, 14, 18, 1
	.member	_XCERF13, 13, 14, 18, 1
	.member	_XCERF14, 14, 14, 18, 1
	.member	_XCERF15, 15, 14, 18, 1
	.eos
	.utag	_XCERF_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERF_BITS
	.eos
	.stag	_RCERG_BITS, 16
	.member	_RCEG0, 0, 14, 18, 1
	.member	_RCEG1, 1, 14, 18, 1
	.member	_RCEG2, 2, 14, 18, 1
	.member	_RCEG3, 3, 14, 18, 1
	.member	_RCEG4, 4, 14, 18, 1
	.member	_RCEG5, 5, 14, 18, 1
	.member	_RCEG6, 6, 14, 18, 1
	.member	_RCEG7, 7, 14, 18, 1
	.member	_RCEG8, 8, 14, 18, 1
	.member	_RCEG9, 9, 14, 18, 1
	.member	_RCEG10, 10, 14, 18, 1
	.member	_RCEG11, 11, 14, 18, 1
	.member	_RCEG12, 12, 14, 18, 1
	.member	_RCEG13, 13, 14, 18, 1
	.member	_RCEG14, 14, 14, 18, 1
	.member	_RCEG15, 15, 14, 18, 1
	.eos
	.utag	_RCERG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERG_BITS
	.eos
	.stag	_RCERH_BITS, 16
	.member	_RCEH0, 0, 14, 18, 1
	.member	_RCEH1, 1, 14, 18, 1
	.member	_RCEH2, 2, 14, 18, 1
	.member	_RCEH3, 3, 14, 18, 1
	.member	_RCEH4, 4, 14, 18, 1
	.member	_RCEH5, 5, 14, 18, 1
	.member	_RCEH6, 6, 14, 18, 1
	.member	_RCEH7, 7, 14, 18, 1
	.member	_RCEH8, 8, 14, 18, 1
	.member	_RCEH9, 9, 14, 18, 1
	.member	_RCEH10, 10, 14, 18, 1
	.member	_RCEH11, 11, 14, 18, 1
	.member	_RCEH12, 12, 14, 18, 1
	.member	_RCEH13, 13, 14, 18, 1
	.member	_RCEH14, 14, 14, 18, 1
	.member	_RCEH15, 15, 14, 18, 1
	.eos
	.utag	_RCERH_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _RCERH_BITS
	.eos
	.stag	_XCERG_BITS, 16
	.member	_XCERG0, 0, 14, 18, 1
	.member	_XCERG1, 1, 14, 18, 1
	.member	_XCERG2, 2, 14, 18, 1
	.member	_XCERG3, 3, 14, 18, 1
	.member	_XCERG4, 4, 14, 18, 1
	.member	_XCERG5, 5, 14, 18, 1
	.member	_XCERG6, 6, 14, 18, 1
	.member	_XCERG7, 7, 14, 18, 1
	.member	_XCERG8, 8, 14, 18, 1
	.member	_XCERG9, 9, 14, 18, 1
	.member	_XCERG10, 10, 14, 18, 1
	.member	_XCERG11, 11, 14, 18, 1
	.member	_XCERG12, 12, 14, 18, 1
	.member	_XCERG13, 13, 14, 18, 1
	.member	_XCERG14, 14, 14, 18, 1
	.member	_XCERG15, 15, 14, 18, 1
	.eos
	.utag	_XCERG_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERG_BITS
	.eos
	.stag	_XCERH_BITS, 16
	.member	_XCEH0, 0, 14, 18, 1
	.member	_XCEH1, 1, 14, 18, 1
	.member	_XCEH2, 2, 14, 18, 1
	.member	_XCEH3, 3, 14, 18, 1
	.member	_XCEH4, 4, 14, 18, 1
	.member	_XCEH5, 5, 14, 18, 1
	.member	_XCEH6, 6, 14, 18, 1
	.member	_XCEH7, 7, 14, 18, 1
	.member	_XCEH8, 8, 14, 18, 1
	.member	_XCEH9, 9, 14, 18, 1
	.member	_XCEH10, 10, 14, 18, 1
	.member	_XCEH11, 11, 14, 18, 1
	.member	_XCEH12, 12, 14, 18, 1
	.member	_XCEH13, 13, 14, 18, 1
	.member	_XCEH14, 14, 14, 18, 1
	.member	_XCEH15, 15, 14, 18, 1
	.eos
	.utag	_XCERH_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XCERH_BITS
	.eos
	.stag	_MFFINT_BITS, 16
	.member	_XINT, 0, 14, 18, 1
	.member	_XEVTA, 1, 14, 18, 1
	.member	_RINT, 2, 14, 18, 1
	.member	_REVTA, 3, 14, 18, 1
	.member	_rsvd, 4, 14, 18, 12
	.eos
	.utag	_MFFINT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MFFINT_BITS
	.eos
	.stag	_MFFST_BITS, 16
	.member	_EOBX, 0, 14, 18, 1
	.member	_FSX, 1, 14, 18, 1
	.member	_EOBR, 2, 14, 18, 1
	.member	_FSR, 3, 14, 18, 1
	.member	_rsvd, 4, 14, 18, 12
	.eos
	.utag	_MFFST_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _MFFST_BITS
	.eos
	.stag	_MCBSP_REGS, 592
	.member	_DRR2, 0, 9, 8, 16, _DRR2_REG
	.member	_DRR1, 16, 9, 8, 16, _DRR1_REG
	.member	_DXR2, 32, 9, 8, 16, _DXR2_REG
	.member	_DXR1, 48, 9, 8, 16, _DXR1_REG
	.member	_SPCR2, 64, 9, 8, 16, _SPCR2_REG
	.member	_SPCR1, 80, 9, 8, 16, _SPCR1_REG
	.member	_RCR2, 96, 9, 8, 16, _RCR2_REG
	.member	_RCR1, 112, 9, 8, 16, _RCR1_REG
	.member	_XCR2, 128, 9, 8, 16, _XCR2_REG
	.member	_XCR1, 144, 9, 8, 16, _XCR1_REG
	.member	_SRGR2, 160, 9, 8, 16, _SRGR2_REG
	.member	_SRGR1, 176, 9, 8, 16, _SRGR1_REG
	.member	_MCR2, 192, 9, 8, 16, _MCR2_REG
	.member	_MCR1, 208, 9, 8, 16, _MCR1_REG
	.member	_RCERA, 224, 9, 8, 16, _RCERA_REG
	.member	_RCERB, 240, 9, 8, 16, _RCERB_REG
	.member	_XCERA, 256, 9, 8, 16, _XCERA_REG
	.member	_XCERB, 272, 9, 8, 16, _XCERB_REG
	.member	_PCR, 288, 9, 8, 16, _PCR_REG
	.member	_RCERC, 304, 9, 8, 16, _RCERC_REG
	.member	_RCERD, 320, 9, 8, 16, _RCERD_REG
	.member	_XCERC, 336, 9, 8, 16, _XCERC_REG
	.member	_XCERD, 352, 9, 8, 16, _XCERD_REG
	.member	_RCERE, 368, 9, 8, 16, _RCERE_REG
	.member	_RCERF, 384, 9, 8, 16, _RCERF_REG
	.member	_XCERE, 400, 9, 8, 16, _XCERE_REG
	.member	_XCERF, 416, 9, 8, 16, _XCERF_REG
	.member	_RCERG, 432, 9, 8, 16, _RCERG_REG
	.member	_RCERH, 448, 9, 8, 16, _RCERH_REG
	.member	_XCERG, 464, 9, 8, 16, _XCERG_REG
	.member	_XCERH, 480, 9, 8, 16, _XCERH_REG
	.member	_rsvd1, 496, 62, 8, 64, , 4
	.member	_MFFINT, 560, 9, 8, 16, _MFFINT_REG
	.member	_MFFST, 576, 9, 8, 16, _MFFST_REG
	.eos
	.stag	_PIECTRL_BITS, 16
	.member	_ENPIE, 0, 14, 18, 1
	.member	_PIEVECT, 1, 14, 18, 15
	.eos
	.utag	_PIECTRL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIECTRL_BITS
	.eos
	.stag	_PIEACK_BITS, 16
	.member	_ACK1, 0, 14, 18, 1
	.member	_ACK2, 1, 14, 18, 1
	.member	_ACK3, 2, 14, 18, 1
	.member	_ACK4, 3, 14, 18, 1
	.member	_ACK5, 4, 14, 18, 1
	.member	_ACK6, 5, 14, 18, 1
	.member	_ACK7, 6, 14, 18, 1
	.member	_ACK8, 7, 14, 18, 1
	.member	_ACK9, 8, 14, 18, 1
	.member	_ACK10, 9, 14, 18, 1
	.member	_ACK11, 10, 14, 18, 1
	.member	_ACK12, 11, 14, 18, 1
	.member	_rsvd, 12, 14, 18, 4
	.eos
	.utag	_PIEACK_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIEACK_BITS
	.eos
	.stag	_PIEIER_BITS, 16
	.member	_INTx1, 0, 14, 18, 1
	.member	_INTx2, 1, 14, 18, 1
	.member	_INTx3, 2, 14, 18, 1
	.member	_INTx4, 3, 14, 18, 1
	.member	_INTx5, 4, 14, 18, 1
	.member	_INTx6, 5, 14, 18, 1
	.member	_INTx7, 6, 14, 18, 1
	.member	_INTx8, 7, 14, 18, 1
	.member	_rsvd, 8, 14, 18, 8
	.eos
	.utag	_PIEIER_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIEIER_BITS
	.eos
	.stag	_PIEIFR_BITS, 16
	.member	_INTx1, 0, 14, 18, 1
	.member	_INTx2, 1, 14, 18, 1
	.member	_INTx3, 2, 14, 18, 1
	.member	_INTx4, 3, 14, 18, 1
	.member	_INTx5, 4, 14, 18, 1
	.member	_INTx6, 5, 14, 18, 1
	.member	_INTx7, 6, 14, 18, 1
	.member	_INTx8, 7, 14, 18, 1
	.member	_rsvd, 8, 14, 18, 8
	.eos
	.utag	_PIEIFR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PIEIFR_BITS
	.eos
	.stag	_PIE_CTRL_REGS, 416
	.member	_PIECTRL, 0, 9, 8, 16, _PIECTRL_REG
	.member	_PIEACK, 16, 9, 8, 16, _PIEACK_REG
	.member	_PIEIER1, 32, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR1, 48, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER2, 64, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR2, 80, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER3, 96, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR3, 112, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER4, 128, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR4, 144, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER5, 160, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR5, 176, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER6, 192, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR6, 208, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER7, 224, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR7, 240, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER8, 256, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR8, 272, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER9, 288, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR9, 304, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER10, 320, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR10, 336, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER11, 352, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR11, 368, 9, 8, 16, _PIEIFR_REG
	.member	_PIEIER12, 384, 9, 8, 16, _PIEIER_REG
	.member	_PIEIFR12, 400, 9, 8, 16, _PIEIFR_REG
	.eos
	.stag	_PIE_VECT_TABLE, 4096
	.member	_PIE1_RESERVED, 0, 144, 8, 22
	.member	_PIE2_RESERVED, 32, 144, 8, 22
	.member	_PIE3_RESERVED, 64, 144, 8, 22
	.member	_PIE4_RESERVED, 96, 144, 8, 22
	.member	_PIE5_RESERVED, 128, 144, 8, 22
	.member	_PIE6_RESERVED, 160, 144, 8, 22
	.member	_PIE7_RESERVED, 192, 144, 8, 22
	.member	_PIE8_RESERVED, 224, 144, 8, 22
	.member	_PIE9_RESERVED, 256, 144, 8, 22
	.member	_PIE10_RESERVED, 288, 144, 8, 22
	.member	_PIE11_RESERVED, 320, 144, 8, 22
	.member	_PIE12_RESERVED, 352, 144, 8, 22
	.member	_PIE13_RESERVED, 384, 144, 8, 22
	.member	_XINT13, 416, 144, 8, 22
	.member	_TINT2, 448, 144, 8, 22
	.member	_DATALOG, 480, 144, 8, 22
	.member	_RTOSINT, 512, 144, 8, 22
	.member	_EMUINT, 544, 144, 8, 22
	.member	_XNMI, 576, 144, 8, 22
	.member	_ILLEGAL, 608, 144, 8, 22
	.member	_USER1, 640, 144, 8, 22
	.member	_USER2, 672, 144, 8, 22
	.member	_USER3, 704, 144, 8, 22
	.member	_USER4, 736, 144, 8, 22
	.member	_USER5, 768, 144, 8, 22
	.member	_USER6, 800, 144, 8, 22
	.member	_USER7, 832, 144, 8, 22
	.member	_USER8, 864, 144, 8, 22
	.member	_USER9, 896, 144, 8, 22
	.member	_USER10, 928, 144, 8, 22
	.member	_USER11, 960, 144, 8, 22
	.member	_USER12, 992, 144, 8, 22
	.member	_SEQ1INT, 1024, 144, 8, 22
	.member	_SEQ2INT, 1056, 144, 8, 22
	.member	_rsvd1_3, 1088, 144, 8, 22
	.member	_XINT1, 1120, 144, 8, 22
	.member	_XINT2, 1152, 144, 8, 22
	.member	_ADCINT, 1184, 144, 8, 22
	.member	_TINT0, 1216, 144, 8, 22
	.member	_WAKEINT, 1248, 144, 8, 22
	.member	_EPWM1_TZINT, 1280, 144, 8, 22
	.member	_EPWM2_TZINT, 1312, 144, 8, 22
	.member	_EPWM3_TZINT, 1344, 144, 8, 22
	.member	_EPWM4_TZINT, 1376, 144, 8, 22
	.member	_EPWM5_TZINT, 1408, 144, 8, 22
	.member	_EPWM6_TZINT, 1440, 144, 8, 22
	.member	_rsvd2_7, 1472, 144, 8, 22
	.member	_rsvd2_8, 1504, 144, 8, 22
	.member	_EPWM1_INT, 1536, 144, 8, 22
	.member	_EPWM2_INT, 1568, 144, 8, 22
	.member	_EPWM3_INT, 1600, 144, 8, 22
	.member	_EPWM4_INT, 1632, 144, 8, 22
	.member	_EPWM5_INT, 1664, 144, 8, 22
	.member	_EPWM6_INT, 1696, 144, 8, 22
	.member	_rsvd3_7, 1728, 144, 8, 22
	.member	_rsvd3_8, 1760, 144, 8, 22
	.member	_ECAP1_INT, 1792, 144, 8, 22
	.member	_ECAP2_INT, 1824, 144, 8, 22
	.member	_ECAP3_INT, 1856, 144, 8, 22
	.member	_ECAP4_INT, 1888, 144, 8, 22
	.member	_ECAP5_INT, 1920, 144, 8, 22
	.member	_ECAP6_INT, 1952, 144, 8, 22
	.member	_rsvd4_7, 1984, 144, 8, 22
	.member	_rsvd4_8, 2016, 144, 8, 22
	.member	_EQEP1_INT, 2048, 144, 8, 22
	.member	_EQEP2_INT, 2080, 144, 8, 22
	.member	_rsvd5_3, 2112, 144, 8, 22
	.member	_rsvd5_4, 2144, 144, 8, 22
	.member	_rsvd5_5, 2176, 144, 8, 22
	.member	_rsvd5_6, 2208, 144, 8, 22
	.member	_rsvd5_7, 2240, 144, 8, 22
	.member	_rsvd5_8, 2272, 144, 8, 22
	.member	_SPIRXINTA, 2304, 144, 8, 22
	.member	_SPITXINTA, 2336, 144, 8, 22
	.member	_MRINTB, 2368, 144, 8, 22
	.member	_MXINTB, 2400, 144, 8, 22
	.member	_MRINTA, 2432, 144, 8, 22
	.member	_MXINTA, 2464, 144, 8, 22
	.member	_rsvd6_7, 2496, 144, 8, 22
	.member	_rsvd6_8, 2528, 144, 8, 22
	.member	_DINTCH1, 2560, 144, 8, 22
	.member	_DINTCH2, 2592, 144, 8, 22
	.member	_DINTCH3, 2624, 144, 8, 22
	.member	_DINTCH4, 2656, 144, 8, 22
	.member	_DINTCH5, 2688, 144, 8, 22
	.member	_DINTCH6, 2720, 144, 8, 22
	.member	_rsvd7_7, 2752, 144, 8, 22
	.member	_rsvd7_8, 2784, 144, 8, 22
	.member	_I2CINT1A, 2816, 144, 8, 22
	.member	_I2CINT2A, 2848, 144, 8, 22
	.member	_rsvd8_3, 2880, 144, 8, 22
	.member	_rsvd8_4, 2912, 144, 8, 22
	.member	_SCIRXINTC, 2944, 144, 8, 22
	.member	_SCITXINTC, 2976, 144, 8, 22
	.member	_rsvd8_7, 3008, 144, 8, 22
	.member	_rsvd8_8, 3040, 144, 8, 22
	.member	_SCIRXINTA, 3072, 144, 8, 22
	.member	_SCITXINTA, 3104, 144, 8, 22
	.member	_SCIRXINTB, 3136, 144, 8, 22
	.member	_SCITXINTB, 3168, 144, 8, 22
	.member	_ECAN0INTA, 3200, 144, 8, 22
	.member	_ECAN1INTA, 3232, 144, 8, 22
	.member	_ECAN0INTB, 3264, 144, 8, 22
	.member	_ECAN1INTB, 3296, 144, 8, 22
	.member	_rsvd10_1, 3328, 144, 8, 22
	.member	_rsvd10_2, 3360, 144, 8, 22
	.member	_rsvd10_3, 3392, 144, 8, 22
	.member	_rsvd10_4, 3424, 144, 8, 22
	.member	_rsvd10_5, 3456, 144, 8, 22
	.member	_rsvd10_6, 3488, 144, 8, 22
	.member	_rsvd10_7, 3520, 144, 8, 22
	.member	_rsvd10_8, 3552, 144, 8, 22
	.member	_rsvd11_1, 3584, 144, 8, 22
	.member	_rsvd11_2, 3616, 144, 8, 22
	.member	_rsvd11_3, 3648, 144, 8, 22
	.member	_rsvd11_4, 3680, 144, 8, 22
	.member	_rsvd11_5, 3712, 144, 8, 22
	.member	_rsvd11_6, 3744, 144, 8, 22
	.member	_rsvd11_7, 3776, 144, 8, 22
	.member	_rsvd11_8, 3808, 144, 8, 22
	.member	_XINT3, 3840, 144, 8, 22
	.member	_XINT4, 3872, 144, 8, 22
	.member	_XINT5, 3904, 144, 8, 22
	.member	_XINT6, 3936, 144, 8, 22
	.member	_XINT7, 3968, 144, 8, 22
	.member	_rsvd12_6, 4000, 144, 8, 22
	.member	_LVF, 4032, 144, 8, 22
	.member	_LUF, 4064, 144, 8, 22
	.eos
	.stag	_SCICCR_BITS, 16
	.member	_SCICHAR, 0, 14, 18, 3
	.member	_ADDRIDLE_MODE, 3, 14, 18, 1
	.member	_LOOPBKENA, 4, 14, 18, 1
	.member	_PARITYENA, 5, 14, 18, 1
	.member	_PARITY, 6, 14, 18, 1
	.member	_STOPBITS, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_SCICCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCICCR_BITS
	.eos
	.stag	_SCICTL1_BITS, 16
	.member	_RXENA, 0, 14, 18, 1
	.member	_TXENA, 1, 14, 18, 1
	.member	_SLEEP, 2, 14, 18, 1
	.member	_TXWAKE, 3, 14, 18, 1
	.member	_rsvd, 4, 14, 18, 1
	.member	_SWRESET, 5, 14, 18, 1
	.member	_RXERRINTENA, 6, 14, 18, 1
	.member	_rsvd1, 7, 14, 18, 9
	.eos
	.utag	_SCICTL1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCICTL1_BITS
	.eos
	.stag	_SCICTL2_BITS, 16
	.member	_TXINTENA, 0, 14, 18, 1
	.member	_RXBKINTENA, 1, 14, 18, 1
	.member	_rsvd, 2, 14, 18, 4
	.member	_TXEMPTY, 6, 14, 18, 1
	.member	_TXRDY, 7, 14, 18, 1
	.member	_rsvd1, 8, 14, 18, 8
	.eos
	.utag	_SCICTL2_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCICTL2_BITS
	.eos
	.stag	_SCIRXST_BITS, 16
	.member	_rsvd, 0, 14, 18, 1
	.member	_RXWAKE, 1, 14, 18, 1
	.member	_PE, 2, 14, 18, 1
	.member	_OE, 3, 14, 18, 1
	.member	_FE, 4, 14, 18, 1
	.member	_BRKDT, 5, 14, 18, 1
	.member	_RXRDY, 6, 14, 18, 1
	.member	_RXERROR, 7, 14, 18, 1
	.eos
	.utag	_SCIRXST_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIRXST_BITS
	.eos
	.stag	_SCIRXBUF_BITS, 16
	.member	_RXDT, 0, 14, 18, 8
	.member	_rsvd, 8, 14, 18, 6
	.member	_SCIFFPE, 14, 14, 18, 1
	.member	_SCIFFFE, 15, 14, 18, 1
	.eos
	.utag	_SCIRXBUF_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIRXBUF_BITS
	.eos
	.stag	_SCIFFTX_BITS, 16
	.member	_TXFFIL, 0, 14, 18, 5
	.member	_TXFFIENA, 5, 14, 18, 1
	.member	_TXFFINTCLR, 6, 14, 18, 1
	.member	_TXFFINT, 7, 14, 18, 1
	.member	_TXFFST, 8, 14, 18, 5
	.member	_TXFIFOXRESET, 13, 14, 18, 1
	.member	_SCIFFENA, 14, 14, 18, 1
	.member	_SCIRST, 15, 14, 18, 1
	.eos
	.utag	_SCIFFTX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIFFTX_BITS
	.eos
	.stag	_SCIFFRX_BITS, 16
	.member	_RXFFIL, 0, 14, 18, 5
	.member	_RXFFIENA, 5, 14, 18, 1
	.member	_RXFFINTCLR, 6, 14, 18, 1
	.member	_RXFFINT, 7, 14, 18, 1
	.member	_RXFFST, 8, 14, 18, 5
	.member	_RXFIFORESET, 13, 14, 18, 1
	.member	_RXFFOVRCLR, 14, 14, 18, 1
	.member	_RXFFOVF, 15, 14, 18, 1
	.eos
	.utag	_SCIFFRX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIFFRX_BITS
	.eos
	.stag	_SCIFFCT_BITS, 16
	.member	_FFTXDLY, 0, 14, 18, 8
	.member	_rsvd, 8, 14, 18, 5
	.member	_CDC, 13, 14, 18, 1
	.member	_ABDCLR, 14, 14, 18, 1
	.member	_ABD, 15, 14, 18, 1
	.eos
	.utag	_SCIFFCT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIFFCT_BITS
	.eos
	.stag	_SCIPRI_BITS, 16
	.member	_rsvd, 0, 14, 18, 3
	.member	_FREE, 3, 14, 18, 1
	.member	_SOFT, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 3
	.eos
	.utag	_SCIPRI_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SCIPRI_BITS
	.eos
	.stag	_SCI_REGS, 256
	.member	_SCICCR, 0, 9, 8, 16, _SCICCR_REG
	.member	_SCICTL1, 16, 9, 8, 16, _SCICTL1_REG
	.member	_SCIHBAUD, 32, 14, 8, 16
	.member	_SCILBAUD, 48, 14, 8, 16
	.member	_SCICTL2, 64, 9, 8, 16, _SCICTL2_REG
	.member	_SCIRXST, 80, 9, 8, 16, _SCIRXST_REG
	.member	_SCIRXEMU, 96, 14, 8, 16
	.member	_SCIRXBUF, 112, 9, 8, 16, _SCIRXBUF_REG
	.member	_rsvd1, 128, 14, 8, 16
	.member	_SCITXBUF, 144, 14, 8, 16
	.member	_SCIFFTX, 160, 9, 8, 16, _SCIFFTX_REG
	.member	_SCIFFRX, 176, 9, 8, 16, _SCIFFRX_REG
	.member	_SCIFFCT, 192, 9, 8, 16, _SCIFFCT_REG
	.member	_rsvd2, 208, 14, 8, 16
	.member	_rsvd3, 224, 14, 8, 16
	.member	_SCIPRI, 240, 9, 8, 16, _SCIPRI_REG
	.eos
	.stag	_SPICCR_BITS, 16
	.member	_SPICHAR, 0, 14, 18, 4
	.member	_SPILBK, 4, 14, 18, 1
	.member	_rsvd1, 5, 14, 18, 1
	.member	_CLKPOLARITY, 6, 14, 18, 1
	.member	_SPISWRESET, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_SPICCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPICCR_BITS
	.eos
	.stag	_SPICTL_BITS, 16
	.member	_SPIINTENA, 0, 14, 18, 1
	.member	_TALK, 1, 14, 18, 1
	.member	_MASTER_SLAVE, 2, 14, 18, 1
	.member	_CLK_PHASE, 3, 14, 18, 1
	.member	_OVERRUNINTENA, 4, 14, 18, 1
	.member	_rsvd, 5, 14, 18, 11
	.eos
	.utag	_SPICTL_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPICTL_BITS
	.eos
	.stag	_SPISTS_BITS, 16
	.member	_rsvd1, 0, 14, 18, 5
	.member	_BUFFULL_FLAG, 5, 14, 18, 1
	.member	_INT_FLAG, 6, 14, 18, 1
	.member	_OVERRUN_FLAG, 7, 14, 18, 1
	.member	_rsvd2, 8, 14, 18, 8
	.eos
	.utag	_SPISTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPISTS_BITS
	.eos
	.stag	_SPIFFTX_BITS, 16
	.member	_TXFFIL, 0, 14, 18, 5
	.member	_TXFFIENA, 5, 14, 18, 1
	.member	_TXFFINTCLR, 6, 14, 18, 1
	.member	_TXFFINT, 7, 14, 18, 1
	.member	_TXFFST, 8, 14, 18, 5
	.member	_TXFIFO, 13, 14, 18, 1
	.member	_SPIFFENA, 14, 14, 18, 1
	.member	_SPIRST, 15, 14, 18, 1
	.eos
	.utag	_SPIFFTX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIFFTX_BITS
	.eos
	.stag	_SPIFFRX_BITS, 16
	.member	_RXFFIL, 0, 14, 18, 5
	.member	_RXFFIENA, 5, 14, 18, 1
	.member	_RXFFINTCLR, 6, 14, 18, 1
	.member	_RXFFINT, 7, 14, 18, 1
	.member	_RXFFST, 8, 14, 18, 5
	.member	_RXFIFORESET, 13, 14, 18, 1
	.member	_RXFFOVFCLR, 14, 14, 18, 1
	.member	_RXFFOVF, 15, 14, 18, 1
	.eos
	.utag	_SPIFFRX_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIFFRX_BITS
	.eos
	.stag	_SPIFFCT_BITS, 16
	.member	_TXDLY, 0, 14, 18, 8
	.member	_rsvd, 8, 14, 18, 8
	.eos
	.utag	_SPIFFCT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIFFCT_BITS
	.eos
	.stag	_SPIPRI_BITS, 16
	.member	_rsvd1, 0, 14, 18, 4
	.member	_FREE, 4, 14, 18, 1
	.member	_SOFT, 5, 14, 18, 1
	.member	_PRIORITY, 6, 14, 18, 1
	.member	_rsvd2, 7, 14, 18, 9
	.eos
	.utag	_SPIPRI_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _SPIPRI_BITS
	.eos
	.stag	_SPI_REGS, 256
	.member	_SPICCR, 0, 9, 8, 16, _SPICCR_REG
	.member	_SPICTL, 16, 9, 8, 16, _SPICTL_REG
	.member	_SPISTS, 32, 9, 8, 16, _SPISTS_REG
	.member	_rsvd1, 48, 14, 8, 16
	.member	_SPIBRR, 64, 14, 8, 16
	.member	_rsvd2, 80, 14, 8, 16
	.member	_SPIRXEMU, 96, 14, 8, 16
	.member	_SPIRXBUF, 112, 14, 8, 16
	.member	_SPITXBUF, 128, 14, 8, 16
	.member	_SPIDAT, 144, 14, 8, 16
	.member	_SPIFFTX, 160, 9, 8, 16, _SPIFFTX_REG
	.member	_SPIFFRX, 176, 9, 8, 16, _SPIFFRX_REG
	.member	_SPIFFCT, 192, 9, 8, 16, _SPIFFCT_REG
	.member	_rsvd3, 208, 62, 8, 32, , 2
	.member	_SPIPRI, 240, 9, 8, 16, _SPIPRI_REG
	.eos
	.stag	_PLLSTS_BITS, 16
	.member	_PLLLOCKS, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_PLLOFF, 2, 14, 18, 1
	.member	_MCLKSTS, 3, 14, 18, 1
	.member	_MCLKCLR, 4, 14, 18, 1
	.member	_OSCOFF, 5, 14, 18, 1
	.member	_MCLKOFF, 6, 14, 18, 1
	.member	_DIVSEL, 7, 14, 18, 2
	.member	_rsvd2, 9, 14, 18, 7
	.eos
	.utag	_PLLSTS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PLLSTS_BITS
	.eos
	.stag	_HISPCP_BITS, 16
	.member	_HSPCLK, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_HISPCP_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _HISPCP_BITS
	.eos
	.stag	_LOSPCP_BITS, 16
	.member	_LSPCLK, 0, 14, 18, 3
	.member	_rsvd1, 3, 14, 18, 13
	.eos
	.utag	_LOSPCP_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _LOSPCP_BITS
	.eos
	.stag	_PCLKCR0_BITS, 16
	.member	_rsvd1, 0, 14, 18, 2
	.member	_TBCLKSYNC, 2, 14, 18, 1
	.member	_ADCENCLK, 3, 14, 18, 1
	.member	_I2CAENCLK, 4, 14, 18, 1
	.member	_SCICENCLK, 5, 14, 18, 1
	.member	_rsvd2, 6, 14, 18, 2
	.member	_SPIAENCLK, 8, 14, 18, 1
	.member	_rsvd3, 9, 14, 18, 1
	.member	_SCIAENCLK, 10, 14, 18, 1
	.member	_SCIBENCLK, 11, 14, 18, 1
	.member	_MCBSPAENCLK, 12, 14, 18, 1
	.member	_MCBSPBENCLK, 13, 14, 18, 1
	.member	_ECANAENCLK, 14, 14, 18, 1
	.member	_ECANBENCLK, 15, 14, 18, 1
	.eos
	.utag	_PCLKCR0_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCLKCR0_BITS
	.eos
	.stag	_PCLKCR1_BITS, 16
	.member	_EPWM1ENCLK, 0, 14, 18, 1
	.member	_EPWM2ENCLK, 1, 14, 18, 1
	.member	_EPWM3ENCLK, 2, 14, 18, 1
	.member	_EPWM4ENCLK, 3, 14, 18, 1
	.member	_EPWM5ENCLK, 4, 14, 18, 1
	.member	_EPWM6ENCLK, 5, 14, 18, 1
	.member	_rsvd1, 6, 14, 18, 2
	.member	_ECAP1ENCLK, 8, 14, 18, 1
	.member	_ECAP2ENCLK, 9, 14, 18, 1
	.member	_ECAP3ENCLK, 10, 14, 18, 1
	.member	_ECAP4ENCLK, 11, 14, 18, 1
	.member	_ECAP5ENCLK, 12, 14, 18, 1
	.member	_ECAP6ENCLK, 13, 14, 18, 1
	.member	_EQEP1ENCLK, 14, 14, 18, 1
	.member	_EQEP2ENCLK, 15, 14, 18, 1
	.eos
	.utag	_PCLKCR1_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCLKCR1_BITS
	.eos
	.stag	_LPMCR0_BITS, 16
	.member	_LPM, 0, 14, 18, 2
	.member	_QUALSTDBY, 2, 14, 18, 6
	.member	_rsvd1, 8, 14, 18, 7
	.member	_WDINTE, 15, 14, 18, 1
	.eos
	.utag	_LPMCR0_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _LPMCR0_BITS
	.eos
	.stag	_PCLKCR3_BITS, 16
	.member	_rsvd1, 0, 14, 18, 8
	.member	_CPUTIMER0ENCLK, 8, 14, 18, 1
	.member	_CPUTIMER1ENCLK, 9, 14, 18, 1
	.member	_CPUTIMER2ENCLK, 10, 14, 18, 1
	.member	_DMAENCLK, 11, 14, 18, 1
	.member	_XINTFENCLK, 12, 14, 18, 1
	.member	_GPIOINENCLK, 13, 14, 18, 1
	.member	_rsvd2, 14, 14, 18, 2
	.eos
	.utag	_PCLKCR3_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PCLKCR3_BITS
	.eos
	.stag	_PLLCR_BITS, 16
	.member	_DIV, 0, 14, 18, 4
	.member	_rsvd1, 4, 14, 18, 12
	.eos
	.utag	_PLLCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _PLLCR_BITS
	.eos
	.stag	_SYS_CTRL_REGS, 512
	.member	_rsvd7, 0, 14, 8, 16
	.member	_PLLSTS, 16, 9, 8, 16, _PLLSTS_REG
	.member	_rsvd1, 32, 62, 8, 128, , 8
	.member	_HISPCP, 160, 9, 8, 16, _HISPCP_REG
	.member	_LOSPCP, 176, 9, 8, 16, _LOSPCP_REG
	.member	_PCLKCR0, 192, 9, 8, 16, _PCLKCR0_REG
	.member	_PCLKCR1, 208, 9, 8, 16, _PCLKCR1_REG
	.member	_LPMCR0, 224, 9, 8, 16, _LPMCR0_REG
	.member	_rsvd2, 240, 14, 8, 16
	.member	_PCLKCR3, 256, 9, 8, 16, _PCLKCR3_REG
	.member	_PLLCR, 272, 9, 8, 16, _PLLCR_REG
	.member	_SCSR, 288, 14, 8, 16
	.member	_WDCNTR, 304, 14, 8, 16
	.member	_rsvd4, 320, 14, 8, 16
	.member	_WDKEY, 336, 14, 8, 16
	.member	_rsvd5, 352, 62, 8, 48, , 3
	.member	_WDCR, 400, 14, 8, 16
	.member	_rsvd6, 416, 62, 8, 96, , 6
	.eos
	.stag	_FOPT_BITS, 16
	.member	_ENPIPE, 0, 14, 18, 1
	.member	_rsvd, 1, 14, 18, 15
	.eos
	.utag	_FOPT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FOPT_BITS
	.eos
	.stag	_FPWR_BITS, 16
	.member	_PWR, 0, 14, 18, 2
	.member	_rsvd, 2, 14, 18, 14
	.eos
	.utag	_FPWR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FPWR_BITS
	.eos
	.stag	_FSTATUS_BITS, 16
	.member	_PWRS, 0, 14, 18, 2
	.member	_STDBYWAITS, 2, 14, 18, 1
	.member	_ACTIVEWAITS, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 4
	.member	_V3STAT, 8, 14, 18, 1
	.member	_rsvd2, 9, 14, 18, 7
	.eos
	.utag	_FSTATUS_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FSTATUS_BITS
	.eos
	.stag	_FSTDBYWAIT_BITS, 16
	.member	_STDBYWAIT, 0, 14, 18, 9
	.member	_rsvd, 9, 14, 18, 7
	.eos
	.utag	_FSTDBYWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FSTDBYWAIT_BITS
	.eos
	.stag	_FACTIVEWAIT_BITS, 16
	.member	_ACTIVEWAIT, 0, 14, 18, 9
	.member	_rsvd, 9, 14, 18, 7
	.eos
	.utag	_FACTIVEWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FACTIVEWAIT_BITS
	.eos
	.stag	_FBANKWAIT_BITS, 16
	.member	_RANDWAIT, 0, 14, 18, 4
	.member	_rsvd1, 4, 14, 18, 4
	.member	_PAGEWAIT, 8, 14, 18, 4
	.member	_rsvd2, 12, 14, 18, 4
	.eos
	.utag	_FBANKWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FBANKWAIT_BITS
	.eos
	.stag	_FOTPWAIT_BITS, 16
	.member	_OTPWAIT, 0, 14, 18, 5
	.member	_rsvd, 5, 14, 18, 11
	.eos
	.utag	_FOTPWAIT_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _FOTPWAIT_BITS
	.eos
	.stag	_FLASH_REGS, 128
	.member	_FOPT, 0, 9, 8, 16, _FOPT_REG
	.member	_rsvd1, 16, 14, 8, 16
	.member	_FPWR, 32, 9, 8, 16, _FPWR_REG
	.member	_FSTATUS, 48, 9, 8, 16, _FSTATUS_REG
	.member	_FSTDBYWAIT, 64, 9, 8, 16, _FSTDBYWAIT_REG
	.member	_FACTIVEWAIT, 80, 9, 8, 16, _FACTIVEWAIT_REG
	.member	_FBANKWAIT, 96, 9, 8, 16, _FBANKWAIT_REG
	.member	_FOTPWAIT, 112, 9, 8, 16, _FOTPWAIT_REG
	.eos
	.stag	_XINTCR_BITS, 16
	.member	_ENABLE, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 1
	.member	_POLARITY, 2, 14, 18, 2
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_XINTCR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XINTCR_BITS
	.eos
	.stag	_XNMICR_BITS, 16
	.member	_ENABLE, 0, 14, 18, 1
	.member	_SELECT, 1, 14, 18, 1
	.member	_POLARITY, 2, 14, 18, 2
	.member	_rsvd2, 4, 14, 18, 12
	.eos
	.utag	_XNMICR_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XNMICR_BITS
	.eos
	.stag	_XINTRUPT_REGS, 256
	.member	_XINT1CR, 0, 9, 8, 16, _XINTCR_REG
	.member	_XINT2CR, 16, 9, 8, 16, _XINTCR_REG
	.member	_XINT3CR, 32, 9, 8, 16, _XINTCR_REG
	.member	_XINT4CR, 48, 9, 8, 16, _XINTCR_REG
	.member	_XINT5CR, 64, 9, 8, 16, _XINTCR_REG
	.member	_XINT6CR, 80, 9, 8, 16, _XINTCR_REG
	.member	_XINT7CR, 96, 9, 8, 16, _XINTCR_REG
	.member	_XNMICR, 112, 9, 8, 16, _XNMICR_REG
	.member	_XINT1CTR, 128, 14, 8, 16
	.member	_XINT2CTR, 144, 14, 8, 16
	.member	_rsvd, 160, 62, 8, 80, , 5
	.member	_XNMICTR, 240, 14, 8, 16
	.eos
	.stag	_XTIMING_BITS, 32
	.member	_XWRTRAIL, 0, 14, 18, 2
	.member	_XWRACTIVE, 2, 14, 18, 3
	.member	_XWRLEAD, 5, 14, 18, 2
	.member	_XRDTRAIL, 7, 14, 18, 2
	.member	_XRDACTIVE, 9, 14, 18, 3
	.member	_XRDLEAD, 12, 14, 18, 2
	.member	_USEREADY, 14, 14, 18, 1
	.member	_READYMODE, 15, 14, 18, 1
	.member	_XSIZE, 16, 14, 18, 2
	.member	_rsvd1, 18, 14, 18, 4
	.member	_X2TIMING, 22, 14, 18, 1
	.member	_rsvd3, 23, 14, 18, 9
	.eos
	.utag	_XTIMING_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _XTIMING_BITS
	.eos
	.stag	_XINTCNF2_BITS, 32
	.member	_WRBUFF, 0, 14, 18, 2
	.member	_CLKMODE, 2, 14, 18, 1
	.member	_CLKOFF, 3, 14, 18, 1
	.member	_rsvd1, 4, 14, 18, 2
	.member	_WLEVEL, 6, 14, 18, 2
	.member	_rsvd2, 8, 14, 18, 1
	.member	_HOLD, 9, 14, 18, 1
	.member	_HOLDS, 10, 14, 18, 1
	.member	_HOLDAS, 11, 14, 18, 1
	.member	_rsvd3, 12, 14, 18, 4
	.member	_XTIMCLK, 16, 14, 18, 3
	.member	_rsvd4, 19, 14, 18, 13
	.eos
	.utag	_XINTCNF2_REG, 32
	.member	_all, 0, 15, 11, 32
	.member	_bit, 0, 8, 11, 32, _XINTCNF2_BITS
	.eos
	.stag	_XBANK_BITS, 16
	.member	_BANK, 0, 14, 18, 3
	.member	_BCYC, 3, 14, 18, 3
	.member	_rsvd, 6, 14, 18, 10
	.eos
	.utag	_XBANK_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XBANK_BITS
	.eos
	.stag	_XRESET_BITS, 16
	.member	_XHARDRESET, 0, 14, 18, 1
	.member	_rsvd1, 1, 14, 18, 15
	.eos
	.utag	_XRESET_REG, 16
	.member	_all, 0, 14, 11, 16
	.member	_bit, 0, 8, 11, 16, _XRESET_BITS
	.eos
	.stag	_XINTF_REGS, 480
	.member	_XTIMING0, 0, 9, 8, 32, _XTIMING_REG
	.member	_rsvd1, 32, 63, 8, 160, , 5
	.member	_XTIMING6, 192, 9, 8, 32, _XTIMING_REG
	.member	_XTIMING7, 224, 9, 8, 32, _XTIMING_REG
	.member	_rsvd2, 256, 63, 8, 64, , 2
	.member	_XINTCNF2, 320, 9, 8, 32, _XINTCNF2_REG
	.member	_rsvd3, 352, 15, 8, 32
	.member	_XBANK, 384, 9, 8, 16, _XBANK_REG
	.member	_rsvd4, 400, 14, 8, 16
	.member	_XREVISION, 416, 14, 8, 16
	.member	_rsvd5, 432, 62, 8, 32, , 2
	.member	_XRESET, 464, 9, 8, 16, _XRESET_REG
	.eos
	.stag	_CPUTIMER_VARS, 128
	.member	_RegsAddr, 0, 24, 8, 22, _CPUTIMER_REGS
	.member	_InterruptCount, 32, 15, 8, 32
	.member	_CPUFreqInMHz, 64, 6, 8, 32
	.member	_PeriodInUSec, 96, 6, 8, 32
	.eos
