---
title: "Sesión de Laboratorio 20/12"
author: David
---

# Planificación

1. Se pondrá en marcha el sistema y se comprobará el correcto funcionamiento. Se usarán valores de 27 y 15 V DC en la batería.
1. Se tomará los datos de ** intensidad de fase por osciloscopio e intensidad de fase por sensor**, para cada una de las fases para cada uno de los valores de bateria, dos veces por valor y configuración
1. Se cerrará todo adecuadamente.

# Notas de desarrollo

0 en osc con -33mA Por la cara!!


# Resultados
|Fase|Posición  |Intensidad F|Voltaje Bat|Carpeta|Dir|
|----|:---------|:----------:|:---------:|:-----:|:-:|
|E   |00001 = 01|1.51        |27         |[x]    |00 |
|E   |00001 = 01|0.773       |15         |[x]    |01 |
|E   |11110 = 30|-0.836      |15         |[x]    |02 |
|E   |11110 = 30|-1.58       |27         |[x]    |03 |
|D   |00010 = 02|1.53        |27         |[x]    |04 |
|D   |00010 = 02|0.789       |15         |[x]    |05 |
|D   |11101 = 29|-0.843      |15         |[x]    |06 |
|D   |11101 = 29|-1.59       |27         |[x]    |07 |
|C   |00100 = 04|1.54        |27         |[x]    |08 |
|C   |00100 = 04|0.792       |15         |[x]    |09 |
|C   |11011 = 27|-0.841      |15         |[x]    |10 |
|C   |11011 = 27|-1.58       |27         |[x]    |11 |
|B   |01000 = 08|1.53        |27         |[x]    |12 |
|B   |01000 = 08|0.782       |15         |[x]    |13 |
|B   |10111 = 23|-0.835      |15         |[x]    |14 |
|B   |10111 = 23|-1.58       |27         |[x]    |15 |
|A   |10000 = 16|1.52        |27         |[x]    |16 |
|A   |10000 = 16|0.769       |15         |[x]    |17 |
|A   |01111 = 15|-0.816      |15         |[x]    |18 |
|A   |01111 = 15|-1.55       |27         |[x]    |19 |
