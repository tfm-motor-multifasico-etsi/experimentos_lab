'''Calibrado de los sensores utilizando el modelo completado'''

import json
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    np.set_printoptions(suppress=True, linewidth=800)
    with open('filled_model.json', 'r') as f:
        experiments = json.load(f)
    A = np.zeros([40, 8])
    b = np.empty([40, 1])
    line = np.zeros([32])
    col = 0
    ind = 0
    ii = 0
    for phs in experiments:
        for sgn in ['+', '-']:
            for vol in ['36', '18']:
                for idx in range(2):
                    if phs != 'c':
                        A[ind, col] = experiments[phs][sgn][vol]['sens'][idx]
                        line[ii] = experiments[phs][sgn][vol]['sens'][idx]
                        ii += 1
                        A[ind, col+4] = -1
                    else:
                        A[ind, 0:4] = -np.array(
                            experiments[phs][sgn][vol]['sens'][idx]
                        )
                        A[ind, 4:] = [1, 1, 1, 1]
                    b[ind] = experiments[phs][sgn][vol]['osc'][idx]
                    ind += 1
        if phs != 'c':
            col += 1
    result = np.linalg.lstsq(A, b, rcond=None)[0]
    print(np.linalg.cond(A.T@A))
    error = np.absolute(b - A@result)/np.absolute(b)*100
    err = np.mean(error.reshape(10, 4), 1)
    out = {
        'parameters': result.T.tolist(),
        'error': error.T.tolist(),
        'avg:': err.tolist(),
    }
    with open('cal_parameters.json', 'w') as f:
        json.dump(out, f, indent=4)

    line = line.reshape(4, 8)
    b_no_c = np.delete(b, range(16, 24), axis=0)
    titles = ['Fase A', 'Fase B', 'Fase D', 'Fase E']
    fig, axs = plt.subplots(2, 2, figsize=(10, 10))
    axs = axs.flatten()
    for row, title in zip(range(line.shape[0]), titles):
        for jj in range(line.shape[1]):
            axs[row].plot(line[row, jj], b_no_c[row * 8 + jj], 'x')
        x = np.linspace(1900, 2270)
        y = x * result[row] - result[row + 4]
        axs[row].plot(x, y)
        axs[row].set_title(title)
        axs[row].set_xlabel('Sensor')
        axs[row].set_ylabel('Interpretación')

    for ii in range(4):
        x = result[ii][0]
        y = result[ii+4][0]
        z = y / x
        print(f'${x:f}$ & ${z:f}$ & ${y:f}$ \\\\')
    plt.show()
    fig.savefig('curvas.eps', bbox_inches="tight")
    fig.savefig('curvas.svg', bbox_inches="tight")
