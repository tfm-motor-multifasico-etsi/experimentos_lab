'''Extracción de los resultados de los experimentos. Se exportan(?) a un
archivo llamado filled_model.json'''

import json
from copy import deepcopy


def process(key, value, osc, sign, vol, exp):
    value[sign][vol]['osc'][exp] = osc[key][sign][vol][exp]
    path = 'datos/' + osc[key]['path'] % (
        osc[key][sign]['pos'],
        vol,
        str(exp+1)
    )
    if key != 'c':
        with open(path, 'r') as f:
            run_tot = 0
            for x in f:
                run_tot += int(x)
            val = run_tot/10000.0
            value[sign][vol]['sens'][exp] = val
    else:
        files = [
            'ias.txt',
            'ibs.txt',
            'ids.txt',
            'ies.txt'
        ]
        for idx, each in enumerate(files):
            path = 'datos/' + osc[key]['path'] % (
                osc[key][sign]['pos'],
                vol,
                str(exp + 1)
            ) + each
            with open(path, 'r') as f:
                run_tot = 0
                for x in f:
                    run_tot += int(x)
                val = run_tot/10000.0
                value[sign][vol]['sens'][exp][idx] = val


def preprocess_osc(osc):
    '''Eliminate systemic bias'''
    osc_copy = deepcopy(osc)  # We don't want to mess with the original
    for key in osc:
        for vol in ['18', '36']:
            avr_p = sum(osc[key]['+'][vol])/2
            avr_n = sum(osc[key]['-'][vol])/2
            sys_err = (avr_p - avr_n)/2 - avr_p
            for sign in ['+', '-']:
                for exp in range(2):
                    val = osc_copy[key][sign][vol][exp]
                    '''El menos de la flecha es por ↓ la orientación de la
                    sonda'''
                    osc_copy[key][sign][vol][exp] = - (val + sys_err)
    return osc_copy


if __name__ == "__main__":
    '''Importar el modelo y los datos del osciloscopio'''
    with open('model.json', 'r') as f:
        experiment = json.load(f)
    with open('datos/osc.json', 'r') as f:
        osc = json.load(f)

    osc_copy = preprocess_osc(osc)

    '''For each possible experiment from each phase'''
    for key, value in experiment.items():
        for sign in ['+', '-']:
            for vol in ['18', '36']:
                for exp in range(2):
                    process(key, value, osc_copy, sign, vol, exp)

    '''Print everything out'''
    with open('filled_model.json', 'w') as f:
        json.dump(experiment, f, indent=4)
    with open('osc_processed.json', 'w') as f:
        json.dump(osc_copy, f, indent=4)
