from pathlib import Path
from preprocesado import preprocess
import numpy as np
import matplotlib.pyplot as plt


def calibration_1(s, i):
    S = np.empty((s.shape[1]*4, 8))
    for idx, row in enumerate(s.T):
        S[idx * 4: (idx + 1)*4, :] = np.array([
            [row[0], 0, 0, 0, -1, 0, 0, 0],
            [0, row[1], 0, 0, 0, -1, 0, 0],
            [0, 0, row[2], 0, 0, 0, -1, 0],
            [0, 0, 0, row[3], 0, 0, 0, -1]
            ])
    I_osc = np.delete(i, 2, 0).T.flatten()
    # S_h = S.T @ S
    # print(np.linalg.cond(S_h))
    # C1 = np.linalg.inv(S_h) @ S.T @ I_osc
    C1, _, _, _ = np.linalg.lstsq(S, I_osc, None)
    c1 = np.array([
        [C1[0], 0, 0, 0, -C1[4]],
        [0, C1[1], 0, 0, -C1[5]],
        [-C1[0], -C1[1], -C1[2], -C1[3], C1[4] + C1[5] + C1[6] + C1[7]],
        [0, 0, C1[2], 0, -C1[6]],
        [0, 0, 0, C1[3], -C1[7]],
        ])
    return c1


def calibration_2(s, i):
    S = np.empty((s.shape[1]*5, 8))
    for idx, row in enumerate(s.T):
        S[idx * 5: (idx + 1)*5, :] = np.array([
            [row[0], 0, 0, 0, -1, 0, 0, 0],
            [0, row[1], 0, 0, 0, -1, 0, 0],
            [-row[0], -row[1], -row[2], -row[3], 1, 1, 1, 1],
            [0, 0, row[2], 0, 0, 0, -1, 0],
            [0, 0, 0, row[3], 0, 0, 0, -1]
            ])
    I_osc = i.T.flatten()
    C2, _, _, _ = np.linalg.lstsq(S, I_osc, None)

    # S_h = S.T @ S
    # print(np.linalg.cond(S_h))
    # C2 = np.linalg.inv(S_h) @ S.T @ I_osc
    c2 = np.array([
        [C2[0], 0, 0, 0, -C2[4]],
        [0, C2[1], 0, 0, -C2[5]],
        [-C2[0], -C2[1], -C2[2], -C2[3], C2[4] + C2[5] + C2[6] + C2[7]],
        [0, 0, C2[2], 0, -C2[6]],
        [0, 0, 0, C2[3], -C2[7]],
        ])
    return c2


def calibration_3(s, i):
    # S = s@s.T
    # print(np.linalg.cond(S))
    # return i@s.T@np.linalg.inv((S))
    c3, _, _, _ = np.linalg.lstsq(s.T, i.T, None)
    return c3.T


def calibration():
    model = preprocess(Path('datos'))
    s = model['sens']
    i = model['osc']
    c1 = calibration_1(s, i)
    c2 = calibration_2(s, i)
    c3 = calibration_3(s, i)
    print('c1:\n', c1, '\nc2:\n', c2, '\nc3:\n', c3)
    return c1, c2, c3, i, s


if __name__ == '__main__':
    c1, c2, c3, i, s = calibration()

    i_hat1 = c1 @ s
    i_hat2 = c2 @ s
    i_hat3 = c3 @ s

    e1 = i - i_hat1
    e2 = i - i_hat2
    e3 = i - i_hat3

    titles = ['Fase A', 'Fase B', 'Fase C', 'Fase D', 'Fase E']
    fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharex='col', sharey='all')
    axs = axs.flatten()
    for row, title in enumerate(titles):
        i_hat_r = i_hat1[row, :]
        i_r = i[row, :]
        index = np.argsort(i_hat_r)
        axs[row].plot(i_r[index], i_hat_r[index], 'o-')
        axs[row].set_title(title)
        axs[row].set_ylabel('Corriente estimada')
        axs[row].set_xlabel('Corriente real')
    axs[-1].set_visible(False)
    axs[4].set_position(
        [(1-0.352)/2, 0.12, 0.352, 0.23]
    )
    # fig.savefig('curvas_i_i_1.eps', bbox_inches="tight")
    fig.savefig('curvas_i_i_1.svg', bbox_inches="tight")

    titles = ['Fase A', 'Fase B', 'Fase C', 'Fase D', 'Fase E']
    fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharex='col', sharey='all')
    axs = axs.flatten()
    for row, title in enumerate(titles):
        i_hat_r = i_hat2[row, :]
        i_r = i[row, :]
        index = np.argsort(i_hat_r)
        axs[row].plot(i_r[index], i_hat_r[index], 'o-')
        axs[row].set_title(title)
        axs[row].set_ylabel('Corriente estimada')
        axs[row].set_xlabel('Corriente real')
    axs[-1].set_visible(False)
    axs[4].set_position(
        [(1-0.352)/2, 0.12, 0.352, 0.23]
    )
    # fig.savefig('curvas_i_i.eps', bbox_inches="tight")
    fig.savefig('curvas_i_i_2.svg', bbox_inches="tight")

    titles = ['Fase A', 'Fase B', 'Fase C', 'Fase D', 'Fase E']
    fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharex='col', sharey='all')
    axs = axs.flatten()
    for row, title in enumerate(titles):
        i_hat_r = i_hat3[row, :]
        i_r = i[row, :]
        index = np.argsort(i_hat_r)
        axs[row].plot(i_r[index], i_hat_r[index], 'o-')
        axs[row].set_title(title)
        axs[row].set_ylabel('Corriente estimada')
        axs[row].set_xlabel('Corriente real')
    axs[-1].set_visible(False)
    axs[4].set_position(
        [(1-0.352)/2, 0.12, 0.352, 0.23]
    )
    # fig.savefig('curvas_i_i_3.eps', bbox_inches="tight")
    fig.savefig('curvas_i_i_3.svg', bbox_inches="tight")

    fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharex='col', sharey='all')
    axs = axs.flatten()
    for row, title in enumerate(titles):
        index = np.argsort(i_hat_r)
        axs[row].plot(i[row], e1[row], 'x')
        axs[row].plot(i[row], e2[row], 'o')
        axs[row].plot(i[row], e3[row], '^')
        axs[row].legend(['c1', 'c2', 'c3'])
        axs[row].set_title(title)
        axs[row].set_ylabel('Error')
        axs[row].set_xlabel('Corriente real')
    axs[-1].set_visible(False)
    axs[4].set_position(
        [(1-0.352)/2, 0.12, 0.352, 0.23]
    )
    # fig.savefig('curvas_e_i_p.eps', bbox_inches="tight")
    fig.savefig('curvas_e_i_p.svg', bbox_inches="tight")
