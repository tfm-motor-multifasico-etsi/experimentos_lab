/*=======================================================================================================================
ADC Module Configuration
=======================================================================================================================*/

interrupt void PTC5Fadc_isr(){
    
   Ia_medido = (float) AdcMirror.ADCRESULT0;
	Ib_medido = (float) AdcMirror.ADCRESULT1;
	Id_medido = (float) AdcMirror.ADCRESULT2;
	Ie_medido = (float) AdcMirror.ADCRESULT3;
	
	
	Ia_medido = (Ia_medido-CONST_OFFSET_IA)*CONST_GAINFACT_IA;
	Ib_medido = (Ib_medido-CONST_OFFSET_IB)*CONST_GAINFACT_IB;
	Id_medido = (Id_medido-CONST_OFFSET_ID)*CONST_GAINFACT_ID;
	Ie_medido = (Ie_medido-CONST_OFFSET_IE)*CONST_GAINFACT_IE;

	// Reinitialize for next ADC sequence
	
	AdcRegs.ADCTRL2.bit.RST_SEQ1   = 1;            // Reset SEQ1    	
	AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;            // Clear INT SEQ1 bit
	PieCtrlRegs.PIEACK.bit.ACK1    = 0;            // Clear the PIEACK of Group 1 for enables Interrupt Resquest at CPU Level

} // interrupt void PTC5Fadc_isr()


void PTC5Fadc_start(){                                    // ADC Module Configuration
     
    volatile Uint32 adcnt;
     
    EALLOW;                                                // Enable CPU writing to EALLOW protected registers
    SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;                  // Enable ADC clock
    EDIS;                                                  // Disable CPU writing to EALLOW protected registers
     
    AdcRegs.ADCTRL3.bit.ADCBGRFDN = 3;                     // Power up bandgap/reference
	 AdcRegs.ADCTRL3.bit.ADCPWDN   = 1; 							//ADC power up     
     
    AdcRegs.ADCTRL1.bit.ACQ_PS = 0x1;                      // S/H width in ADC module periods = 16 ADC clocks
    AdcRegs.ADCTRL1.bit.CONT_RUN = 0;                      // Start-Stop Mode
	 AdcRegs.ADCTRL1.bit.SEQ_CASC = 1;                      // Cascaded Mode
     
    AdcRegs.ADCTRL3.bit.SMODE_SEL = 0;							  // Sampling mode --> SEQUENTIAL
    AdcRegs.ADCTRL1.bit.CPS       = 0;                     // CPS=1; ADCLK=HSPCLK/(ADCLKPS*CPS)
	 AdcRegs.ADCTRL3.bit.ADCCLKPS  = 3;                     // ADC module clock = HSPCLK/(1*8)   = 150MHz/(1*8) = 18.75MHz
	 														// aprox 0.853 us por conversion
    AdcRegs.ADCMAXCONV.all = 4;                            // Setup the number of conv's on SEQ1
     
//  Initialize ADC Input Channel Select Sequencing Control Register:
	 //  Placa gris
	AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0;                   // Setup the 1st SEQ1 conv.
	AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 2;                   // Setup the 2st SEQ1 conv.
	AdcRegs.ADCCHSELSEQ1.bit.CONV02 = 4;                   // Setup the 3st SEQ1 conv.
	AdcRegs.ADCCHSELSEQ1.bit.CONV03 = 6;                   // Setup the 4st SEQ1 conv.
	AdcRegs.ADCCHSELSEQ2.bit.CONV04 = 1;                   // Setup the 5st SEQ2 conv. (DC-LINK)
	 
	AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 = 1;                // Enable SOCA from ePWM to start SEQ1	
	AdcRegs.ADCTRL2.bit.INT_MOD_SEQ1   = 0;                // SEQ1 Interrupt mode at every end of SEQ1
	AdcRegs.ADCTRL2.bit.RST_SEQ1		  = 1;                // Reset SEQ1
	
	
//  Start ADC with ePWM1 timer 1 event:
	EPwm1Regs.ETSEL.bit.SOCAEN  = ET_ETSEL_ENABLE;         // Enable SOC on A group
	EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_PRD;              // Generate SOC when time-base counter equal to period.
	EPwm1Regs.ETPS.bit.SOCAPRD  = ET_1ST;                  // Generate pulse on 1st event 
	
//  start PWM generation
	EALLOW;                                                // Enable writing to EALLOW protected registers
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;                 // Start all the timers synced
	EDIS;                                                  // Disable writing to EALLOW protected areas
	
	
//  Delay routine 5ms
    for(adcnt=0;adcnt<76000;adcnt++);

	AdcRegs.ADCTRL2.bit.RST_SEQ1   = 1;            // Reset SEQ1
	
    AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1	= 0;                // Disable SEQ1 interrupt (every EOS)
    
}//void PTC5Fadc_start()
