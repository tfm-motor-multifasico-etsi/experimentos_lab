DATE=$$(date +'%Y-%m-%d')
FILE="$(DATE)/Session.md"

dir:
	mkdir "$(DATE)"
	cp template.md $(FILE)
	okular $(FILE) &
	kate $(FILE) &
