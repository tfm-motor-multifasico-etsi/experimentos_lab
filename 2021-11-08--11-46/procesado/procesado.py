from pathlib import Path


man_res = [
    -2.08,
    1.99,
    -2.06,
    2.08,
    -2.09,
    2.05,
    -2.09,
    2.04,
    -2.06,
    2.04,
]


def get_file_names():
    return sorted(Path('.').glob('cal*/*/i*'))


def main():
    counter = 0
    hold = 1.5
    I_res = []
    I_c_calc = []
    files = []
    for file_name in get_file_names():
        run_tot = 0
        with open(file_name, 'r') as f:
            for x in f:
                run_tot += int(x)
        I_internal = (run_tot/10000.0)/1638.0-20
        I_c_calc.append(I_internal)
        counter += 1
        if abs(I_internal) > hold:
            I_res.append(I_internal)
            files.append(file_name)
        if counter == 4:
            I_c = -sum(I_c_calc)
            if abs(I_c) > hold:
                I_res.append(I_c)
                files.append('Fase C')
            counter = 0
            I_c_calc = []
    error = [(abs(i1 - i2) / 2) * 100 for i1, i2 in zip(I_res, man_res)]
    for index in range(len(I_res)):
        print("{} & ${:.3g}$ & ${:.3g}$ & ${:.3g}$ \\\\".format(files[index],
              I_res[index],
              man_res[index],
              error[index]))
    return 0


if __name__ == '__main__':
    main()
